"""myproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from User import views


urlpatterns = [
     url(r'^admin/', admin.site.urls),
     url(r'^''', include('home.urls')),
     url(r'^accounts/', include('allauth.urls')),
     url(r'^user/', include('User.urls')),
     url(r'^dashboard/', include('dashboard.urls')),
     url(r'^paypal/', include('paypal.standard.ipn.urls')),
     # url(r'^search_events/$',views.search_events,name='search_events'),
     # url(r'^account_settings/$',views.account_settings,name='account_settings'),
     # url(r'^create_event/$',views.create_event,name='create_event'),
     # url(r'^tickets/$',views.tickets,name='tickets'),
     # url(r'^manage_events/$',views.manage_events,name='manage_events'),
     # url(r'^event/(?P<id>\d+)/$',views.view_event,name='view_event'),
     # url(r'^manage_events/event/(?P<id>\d+)/$',views.view_event,name='view_event'),
     # url(r'^edit_event/$',views.edit_event,name='edit_event'),
     # url(r'^manage_events/edit_event/$',views.manage_edit_event,name='manage_edit_event'),
     # url(r'^delete_event/$',views.delete_event,name='delete_event'),
     # url(r'^browse_events/$',views.browse_events,name='browse_events'),
     # url(r'^category_filters/$',views.category_filters,name='category_filters'),
     # url(r'^change_email/$',views.change_email,name='change_email'),
     # url(r'^upload_image/$',views.upload_image,name='upload_image'),
     # url(r'^free_events/$',views.get_free_events,name='get_free_events'),
     # url(r'^paid_events/$',views.get_free_events,name='get_free_events'),
     # url(r'^filter_events/$',views.get_filtered_events,name='get_filtered_events'),
     # url(r'^create_event_data/$',views.create_event_data,name='create_event_data'),
     # url(r'^upload_pic/$',views.upload_image,name='upload_image'),
     # url(r'^tickets_info/$',views.tickets_info,name='tickets_info'),
     # url(r'^contacts/$',views.contacts,name='contacts'),
     # url(r'^gmail/$',views.gmail,name='gmail'),   
     # #url(r'^get-state-token/(?P<action_type_id>\d+)/(?P<action_id>\d+)/$', google_get_state_token, name='google_contacts_get_state_token'),
     # #url(r'^login/$', google_login, name='google_contacts_login'), 
     # #url(r'^logout/$', google_logout, name='google_contacts_logout'),
     # url(r'^referrals/$',views.referrals,name='referrals'),
     # url(r'^referrals-attendees/$',views.referrals_attendees,name='referrals_attendees'),
     # url(r'^password/$',views.password,name='password'),
     # url(r'^social-settings/$',views.social_settings,name='social_settings'),
     # url(r'^creditcards/$',views.creditcards,name='creditcards'),
     # url(r'^email-preferences/$',views.email_preferences,name='email_preferences'),
     # url(r'^venues-organizers/$',views.venues_organizers,name='venues_organizers'),
     # url(r'^permissions/$',views.permissions,name='permissions'),
     # url(r'^extensions/$',views.extensions,name='extensions'),
     # url(r'^account-close/$',views.account_close,name='account_close'),
     # url(r'^charges/$',views.charges,name='charges'),
     # url(r'^payment-information/$',views.payment_information,name='payment_information'),
     # url(r'^invoices/$',views.invoices,name='invoices'),
     # url(r'^tax_information/$',views.tax_information,name='tax_information'),
     # url(r'^tax_information/forms/$',views.tax_forms,name='tax_forms'),
     # url(r'^close_account/$',views.close_account,name='close_account'),
     # url(r'^upload_event_image/$',views.upload_event_image,name='upload_event_image'),
     # url(r'^upload_subevent_image/$',views.upload_subevent_image,name='upload_subevent_image'),
     # url(r'^create_sub_event/$',views.create_sub_event,name='create_sub_event'),
     # url(r'^quantity_selected/$',views.quantity_selected,name='quantity_selected'),
     # url(r'^get_subevent_form/$',views.get_subevent_form,name='get_subevent_form'),
     # url(r'^register_order/$',views.register_order,name='register_order'),
     # url(r'^register_ticket/$',views.register_ticket,name='register_ticket'),
     # url(r'^autocomplete_events/$',views.autocomplete_events,name='autocomplete_events'),
     # url(r'^contact_support/$',views.contact_support,name='contact_support'),
     # url(r'^contact_sales/$',views.contact_sales,name='contact_sales'),
     # url(r'^edit_subevents/$',views.edit_subevents,name='edit_subevents'),
     # url(r'^edit_subevent/$',views.edit_subevent,name='edit_subevent'),
     # url(r'^update_subevent/$',views.update_subevent,name='update_subevent'),
     # url(r'^update_subevent_image/$',views.update_subevent_image,name='update_subevent_image'),
     # url(r'^manage_particular_event/$',views.manage_particular_event,name='manage_particular_event'),
     # url(r'^view_subevents/$',views.view_subevents,name='view_subevents'),
     # url(r'^view_this_subevent/$',views.view_this_subevent,name='view_this_subevent'),
     # url(r'^save_order/$',views.save_order,name='save_order'),
     # url(r'^send_msg/$',views.send_msg,name='send_msg'),
     # url(r'^view_ticket/$',views.view_ticket,name='view_ticket'),
     # url(r'^user_tickets_detail/$',views.user_tickets_detail,name='user_tickets_detail'),
     # url(r'^manage_progressbar/$',views.manage_progressbar,name='manage_progressbar'),
     # url(r'^manage_particular_events/$',views.manage_particular_events,name='manage_particular_events'),
     # url(r'^booked_particular_ticket/$',views.booked_particular_ticket,name='booked_particular_ticket'),
     # url(r'^edit_particular_ticket/$',views.edit_particular_ticket,name='edit_particular_ticket'),
     # url(r'^delete_particular_ticket/$',views.delete_particular_ticket,name='delete_particular_ticket'),
     # url(r'^update_event_data/$',views.update_event_data,name='update_event_data'),
     # url(r'^update_event_image/$',views.update_event_image,name='update_event_image'),
     # url(r'^manage_ticketgraph/$',views.manage_ticketgraph,name='manage_ticketgraph'),
     # url(r'^event_publish/$',views.event_publish,name='event_publish'),
     # url(r'^event_unpublish/$',views.event_unpublish,name='event_unpublish'),
     # url(r'^complete_registration/$',views.complete_registration,name='complete_registration'),
     # url(r'^subevent_registration/$',views.subevent_registration,name='subevent_registration'),
     # url(r'^sub_quantity_selected/$',views.sub_quantity_selected,name='sub_quantity_selected'),
     # url(r'^sub_register_ticket/$',views.sub_register_ticket,name='sub_register_ticket'),
     # url(r'^view_qr_image/$',views.view_qr_image,name='view_qr_image'),
     # url(r'^saved_events/$',views.saved_events,name='saved_events'),
     # url(r'^update_user_info/$',views.update_user_info,name='update_user_info'),
     # url(r'^upload$', views.upload, name='upload'),
     # url(r'^sub_event_upload$', views.sub_event_upload, name='sub_event_upload'),
     # url(r'^create_subevent_data/$',views.create_subevent_data,name='create_subevent_data'),
    
]