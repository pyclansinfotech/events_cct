from __future__ import unicode_literals

from django.db import models
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
import uuid

#####################UserAccount#########################
class UserAccount(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    bio = models.TextField()
    prefix = models.CharField(max_length=56, blank=True)
    sufix = models.CharField(max_length=56)
    home_phone = models.IntegerField(blank=True)
    cell_phone = models.IntegerField(blank=True)
    job_title = models.CharField(max_length=255, blank=True)
    company = models.CharField(max_length=255, blank=True)
    website = models.CharField(max_length=255, blank=True)
    home_address = models.CharField(max_length=255, blank=True)
    home_city = models.CharField(max_length=255, blank=True)
    home_country = models.CharField(max_length=255, blank=True)
    home_state = models.CharField(max_length=255, blank=True)
    home_zip_code = models.CharField(max_length=255, blank=True)

    billing_address = models.CharField(max_length=255, blank=True)
    billing_city = models.CharField(max_length=255, blank=True)
    billing_country = models.CharField(max_length=255, blank=True)
    billing_state = models.CharField(max_length=255, blank=True)
    billing_zip_code = models.CharField(max_length=255, blank=True)

    shipping_address = models.CharField(max_length=255, blank=True)
    shipping_city = models.CharField(max_length=255, blank=True)
    shipping_country = models.CharField(max_length=255, blank=True)
    shipping_state = models.CharField(max_length=255, blank=True)
    shipping_zip_code = models.CharField(max_length=255, blank=True)

    work_address = models.CharField(max_length=255, blank=True)
    work_city = models.CharField(max_length=255, blank=True)
    work_country = models.CharField(max_length=255, blank=True)
    work_state = models.CharField(max_length=255, blank=True)
    work_zip_code = models.CharField(max_length=255, blank=True)

    gender = models.CharField(max_length=50)
    date_of_birth = models.DateField()
    age = models.IntegerField(blank=True)
    profile_pic = models.ImageField(upload_to='home/static/images/profiles', blank=True)
    saved_event = models.ManyToManyField('Event')

    def __str__(self):
    	return self.user

###################Category##################
class Category(models.Model):
    category_name = models.CharField(max_length=255,unique=True)

    def __str__(self):
        return self.category_name

##################SubCategory###############
# class SubCategory(models.Model):
#     category = models.ForeignKey(Category)
#     sub_category_name = models.CharField(max_length=255)

#     def __str__(self):
#         return self.sub_category_name

#########################Location###############
class Location(models.Model):
	city=models.CharField(max_length=255,unique=True)
	state=models.CharField(max_length=255) 
	country=models.CharField(max_length=255) 
	
	def __str__(self):
        	return self.city 

########################Event Banner######################################
class EventBanner(models.Model):
	event_id = models.CharField(null=True,max_length=255)
	user = models.ForeignKey(User)
	event_banner = models.ImageField(
		upload_to='home/static/images/eventbanner', null=True, blank=True, default='static/images/events.jpg')

####################SubEventBanner#################
class SubEventBanner(models.Model): #added
	sub_event_id = models.CharField(null=True,max_length=255)
	user = models.ForeignKey(User)
	sub_event_banner = models.ImageField(
		upload_to='home/static/images/subeventbanner', null=True, blank=True, default='static/images/sub_events.jpg')
	sub_event_index = models.PositiveSmallIntegerField()
	def __str__(self):
		return self.id	

####################Event###################################
class Event(models.Model):
	event_name=models.CharField(null=True, max_length=255)
	description = models.TextField()
	college_name=models.CharField(null=True, max_length=255)	
	location = models.ForeignKey(Location)
	registered_datetime=models.DateTimeField(null=True)
	start_datetime=models.DateTimeField(null=True)
	end_datetime=models.DateTimeField(null=True)	
	organizer_name = models.CharField(max_length=255)
	organizer_description = models.TextField()
	image = models.CharField(max_length=255)
	category = models.ForeignKey(Category)
	publish_event=models.CharField(max_length=255, default='0')
	user = models.ForeignKey(User)
	
	def __str__(self):
		return "{}".format(self.event_name)

	def get_absolute_url(self):
		return reverse('event:event_view', args=(self.slug, ))

	def save(self, *args, **kwargs):
		self.slug = slugify("{}".format(
			self.event_name))
		super(Event, self).save(*args, **kwargs)

###################SubEvent####################
class SubEvent(models.Model):
	event = models.ForeignKey(Event)
	venue = models.CharField(null=True, max_length=255)
	registered_datetime=models.DateTimeField(null=True)
	start_datetime=models.DateTimeField(null=True)
	end_datetime=models.DateTimeField(null=True)
	sub_event_name=models.CharField(max_length=255)
	organizer_name =  models.CharField(max_length=255)
	organizer_description= models.TextField()
	category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE)
	image = models.CharField(max_length=255)
	description=models.TextField()
	booking_check = models.BooleanField(default=False)
	visible_check=models.CharField(max_length=255, default='0')
	user = models.ForeignKey(User)
	
	def str(self): 
		return self.sub_event_name


#########################FreeTickets###############
class EventTickets(models.Model):
	ticket_name=models.CharField(max_length=255)
	ticket_type = models.CharField(max_length=255)
	sale_channel=models.CharField(max_length=255)
	order_min=models.CharField(null=True, max_length=255)
	order_max=models.CharField(null=True, max_length=255)
	ticket_description=models.CharField(max_length=255)
	ticket_count=models.CharField(max_length=255, default=0)
	price=models.CharField(max_length=255,default=0)
	registered_datetime=models.DateTimeField(null=True)
	start_datetime=models.DateTimeField(null=True)
	end_datetime=models.DateTimeField(null=True)
	event=models.ForeignKey(Event,null=True)
	sub_event=models.ForeignKey(SubEvent,null=True)
	ticket_booked=models.CharField(max_length=255, null=True)
	
	def str(self): 
		return self.ticket_name

###################################Ticket Booked Model#########################################
class TicketBooked(models.Model):
	ticket=models.ForeignKey(EventTickets)
	transaction_id=models.UUIDField(primary_key=True,default=uuid.uuid4, editable=False)
	quantity_booked=models.CharField(max_length=255, verbose_name='number of tickets')
	timestamp = models.DateTimeField(null=True, verbose_name='booked time')
	user=models.ForeignKey(User)
	total_payment=models.CharField(null=True, max_length=255)
	portal_fee=models.CharField(max_length=255,null=True)
	attendee_status=models.CharField(max_length=255,null=True)
	
	def str(self): 
		return self.user

###################################QR Image Model#########################################
class ImageQR(models.Model):
	event = models.ForeignKey(Event)
	sub_event = models.ForeignKey(SubEvent,null=True)
	transaction_id=models.ForeignKey('TicketBooked',on_delete=models.CASCADE)
	image_path=models.CharField(max_length=255)
	guest_email=models.CharField(max_length=255,null=True)
	def str(self): 
		return self.guest_email


###################################Saved Event Model#########################################
class SavedEvent(models.Model):
	user=models.ForeignKey(User)
	event=models.ForeignKey(Event)
	saved_flag=models.CharField(max_length=255, default='0')
	def str(self):
		return self.user
    
