# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category_name', models.CharField(unique=True, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('event_name', models.CharField(max_length=255, null=True)),
                ('description', models.TextField()),
                ('college_name', models.CharField(max_length=255, null=True)),
                ('registered_datetime', models.DateTimeField(null=True)),
                ('start_datetime', models.DateTimeField(null=True)),
                ('end_datetime', models.DateTimeField(null=True)),
                ('organizer_name', models.CharField(max_length=255)),
                ('organizer_description', models.TextField()),
                ('image', models.CharField(max_length=255)),
                ('publish_event', models.CharField(default='0', max_length=255)),
                ('category', models.ForeignKey(to='dashboard.Category')),
            ],
        ),
        migrations.CreateModel(
            name='EventBanner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('event_id', models.CharField(max_length=255, null=True)),
                ('event_banner', models.ImageField(default='static/images/events.jpg', null=True, upload_to='home/static/images/eventbanner', blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='EventTickets',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ticket_name', models.CharField(max_length=255)),
                ('ticket_type', models.CharField(max_length=255)),
                ('sale_channel', models.CharField(max_length=255)),
                ('order_min', models.CharField(max_length=255, null=True)),
                ('order_max', models.CharField(max_length=255, null=True)),
                ('ticket_description', models.CharField(max_length=255)),
                ('ticket_count', models.CharField(default=0, max_length=255)),
                ('price', models.CharField(default=0, max_length=255)),
                ('registered_datetime', models.DateTimeField(null=True)),
                ('start_datetime', models.DateTimeField(null=True)),
                ('end_datetime', models.DateTimeField(null=True)),
                ('ticket_booked', models.CharField(max_length=255, null=True)),
                ('event', models.ForeignKey(to='dashboard.Event', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ImageQR',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_path', models.CharField(max_length=255)),
                ('guest_email', models.CharField(max_length=255, null=True)),
                ('event', models.ForeignKey(to='dashboard.Event')),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.CharField(unique=True, max_length=255)),
                ('state', models.CharField(max_length=255)),
                ('country', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='SavedEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('saved_flag', models.CharField(default='0', max_length=255)),
                ('event', models.ForeignKey(to='dashboard.Event')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SubEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('venue', models.CharField(max_length=255, null=True)),
                ('registered_datetime', models.DateTimeField(null=True)),
                ('start_datetime', models.DateTimeField(null=True)),
                ('end_datetime', models.DateTimeField(null=True)),
                ('sub_event_name', models.CharField(max_length=255)),
                ('organizer_name', models.CharField(max_length=255)),
                ('organizer_description', models.TextField()),
                ('image', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('booking_check', models.BooleanField(default=False)),
                ('visible_check', models.CharField(default='0', max_length=255)),
                ('category', models.ForeignKey(to='dashboard.Category', null=True)),
                ('event', models.ForeignKey(to='dashboard.Event')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SubEventBanner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sub_event_id', models.CharField(max_length=255, null=True)),
                ('sub_event_banner', models.ImageField(default='static/images/sub_events.jpg', null=True, upload_to='home/static/images/subeventbanner', blank=True)),
                ('sub_event_index', models.PositiveSmallIntegerField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='TicketBooked',
            fields=[
                ('transaction_id', models.UUIDField(default=uuid.uuid4, serialize=False, editable=False, primary_key=True)),
                ('quantity_booked', models.CharField(max_length=255, verbose_name='number of tickets')),
                ('timestamp', models.DateTimeField(null=True, verbose_name='booked time')),
                ('total_payment', models.CharField(max_length=255, null=True)),
                ('portal_fee', models.CharField(max_length=255, null=True)),
                ('attendee_status', models.CharField(max_length=255, null=True)),
                ('ticket', models.ForeignKey(to='dashboard.EventTickets')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bio', models.TextField()),
                ('prefix', models.CharField(max_length=56, blank=True)),
                ('sufix', models.CharField(max_length=56)),
                ('home_phone', models.IntegerField(blank=True)),
                ('cell_phone', models.IntegerField(blank=True)),
                ('job_title', models.CharField(max_length=255, blank=True)),
                ('company', models.CharField(max_length=255, blank=True)),
                ('website', models.CharField(max_length=255, blank=True)),
                ('home_address', models.CharField(max_length=255, blank=True)),
                ('home_city', models.CharField(max_length=255, blank=True)),
                ('home_country', models.CharField(max_length=255, blank=True)),
                ('home_state', models.CharField(max_length=255, blank=True)),
                ('home_zip_code', models.CharField(max_length=255, blank=True)),
                ('billing_address', models.CharField(max_length=255, blank=True)),
                ('billing_city', models.CharField(max_length=255, blank=True)),
                ('billing_country', models.CharField(max_length=255, blank=True)),
                ('billing_state', models.CharField(max_length=255, blank=True)),
                ('billing_zip_code', models.CharField(max_length=255, blank=True)),
                ('shipping_address', models.CharField(max_length=255, blank=True)),
                ('shipping_city', models.CharField(max_length=255, blank=True)),
                ('shipping_country', models.CharField(max_length=255, blank=True)),
                ('shipping_state', models.CharField(max_length=255, blank=True)),
                ('shipping_zip_code', models.CharField(max_length=255, blank=True)),
                ('work_address', models.CharField(max_length=255, blank=True)),
                ('work_city', models.CharField(max_length=255, blank=True)),
                ('work_country', models.CharField(max_length=255, blank=True)),
                ('work_state', models.CharField(max_length=255, blank=True)),
                ('work_zip_code', models.CharField(max_length=255, blank=True)),
                ('gender', models.CharField(max_length=50)),
                ('date_of_birth', models.DateField()),
                ('age', models.IntegerField(blank=True)),
                ('profile_pic', models.ImageField(upload_to='home/static/images/profiles', blank=True)),
                ('saved_event', models.ManyToManyField(to='dashboard.Event')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='imageqr',
            name='sub_event',
            field=models.ForeignKey(to='dashboard.SubEvent', null=True),
        ),
        migrations.AddField(
            model_name='imageqr',
            name='transaction_id',
            field=models.ForeignKey(to='dashboard.TicketBooked'),
        ),
        migrations.AddField(
            model_name='eventtickets',
            name='sub_event',
            field=models.ForeignKey(to='dashboard.SubEvent', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='location',
            field=models.ForeignKey(to='dashboard.Location'),
        ),
        migrations.AddField(
            model_name='event',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
