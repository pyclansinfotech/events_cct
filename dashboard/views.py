from django.shortcuts import redirect
#from json_response import JsonResponse
#from models import ActionState

###########################################################
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib
from django.http import JsonResponse

#import gdata.contacts.service
from django.http import JsonResponse
from django.utils import timezone
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext,Context, Template,RequestContext
import hashlib
from random import randint
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.core.context_processors import csrf
import datetime
#from .google_contacts.utils import google_get_state, google_import

from django.shortcuts import render
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect,Http404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from dashboard.models import Category,Event,EventTickets,SubEvent,TicketBooked,ImageQR,SavedEvent,UserAccount,Location,EventBanner,SubEventBanner
from  datetime import timedelta
import time
from django.template.loader import render_to_string,get_template
from datetime import datetime
from dateutil.relativedelta import relativedelta
import base64 
from django.db.models import Q
import operator
import os
import datetime
import time
import json
import qrcode
#import pdfcrowd

from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User
from django.contrib.auth import logout as auth_logout
from home.utils import image_path_editor
from django.core.urlresolvers import reverse
from paypal.standard.forms import PayPalPaymentsForm
import requests
import json
import hashlib
import certifi
import urllib3
import httplib,urllib
from django.template import loader, Context
from django.core.urlresolvers import reverse
from paypal.standard.ipn.models import PayPalIPN
from paypal.standard.ipn.forms import PayPalIPNForm
from paypal.standard.ipn.models import PayPalIPN
from django.views.decorators.http import require_POST
import uuid
import strgen

#import ho.pisa as pisa
#import cStringIO as StringIO
# Create your views here.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#############Account referral###################
@csrf_exempt
def referrals(request):
	logged_in_user=request.user
	return render(request,'dashboard/referral.html',{'logged_in_user':logged_in_user})


#############Account referrals_attendees###################
@csrf_exempt
def referrals_attendees(request):
	logged_in_user=request.user
	return render(request,'dashboard/referrals_attendees.html',{'logged_in_user':logged_in_user})


############Upload#########################################
@csrf_exempt
def upload(request):
    print 'request',request
    """
    Find the eid in the request.session if you can't find it then propably it's
    not saved this event so your job to find that one because you are the first
    one saving into database
    """
    logged_in_user=request.user
    image_id = request.session.get('image_id')
    if request.method == 'POST':
    	event_obj = EventBanner.objects.create(id=image_id,user=request.user)
    	event_obj.event_banner=request.FILES.get('file')       
        event_obj.save()
    return HttpResponse('saved')
    #return HttpResponseRedirect(reverse('events:edit'))


##################upload image for edit event ################
@csrf_exempt
def upload_edit_event_image(request):
    logged_in_user=request.user
    image_id = request.session.get('image_id')
    if request.method == 'POST':
    	event_id=request.POST.get('event_id')
    	event_obj = EventBanner.objects.create(id=image_id,user=request.user,event_id=event_id)       
        event_obj.event_banner=request.FILES.get('file')
        event_obj.save()
    return HttpResponse('saved')




 ################upload image for edit subevent######################
@csrf_exempt
def upload_edit_subevent_image(request):
    logged_in_user=request.user
    image_id = request.session.get('image_id')
    if request.method == 'POST':
    	sub_event_id=request.POST.get('subevent_id')
    	subevent_obj = SubEventBanner.objects.create(id=image_id,user=request.user,sub_event_id=sub_event_id,sub_event_index=sub_event_id)       
        subevent_obj.sub_event_banner=request.FILES.get('file')
        subevent_obj.save()
    return HttpResponse('saved')

 ############Upload#########################################
image_list=[]
@csrf_exempt
def sub_event_upload(request):
    """
    Find the eid in the request.session if you can't find it then propably it's
    not saved this event so your job to find that one because you are the first
    one saving into database
    """
    logged_in_user=request.user
    image_id = request.session.get('image_id')
    if request.method == 'POST':
        sub_event_index = request.POST['data']
        sub_event_obj = SubEventBanner.objects.create(user=logged_in_user,sub_event_index=sub_event_index)
        sub_event_obj.sub_event_banner = request.FILES.get('file')
        image_list1=request.FILES.get('file')
        image_list.append(image_list1)
        sub_event_obj.user=logged_in_user
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        timestamp=st
        sub_event_obj.timestamp=timestamp
        sub_event_obj.save()
    return HttpResponse('saved')
#############Account Change Password###################
@csrf_exempt
def password(request):
	logged_in_user=request.user
	if request.method == 'POST':
		oldpassword=request.POST['oldpassword']
		newpassword1=request.POST['newpassword1']
		newpassword2=request.POST['newpassword2']
		converted_new=make_password(newpassword1)	
		loggedin_user=User.objects.filter(email=logged_in_user)
		for user in loggedin_user:
			user_password=user.password
			check=check_password(oldpassword,user_password)
		if check==True:
			for u in loggedin_user:
				if u.is_active:
					u = User.objects.get(email=logged_in_user)
					u.set_password(newpassword1)
					u.save()
					auth_login(request, u)
					return HttpResponse('authentic')
				else:
					return HttpResponse('non-authentic')
		else:
			return HttpResponse('non-authentic')
	else:
		return render(request,'dashboard/change_password.html',{'logged_in_user':logged_in_user})


#############Account Social Settings###################
@csrf_exempt
def social_settings(request):
	logged_in_user=request.user
	return render(request,'dashboard/social_settings.html',{'logged_in_user':logged_in_user})

#############Account Credit Cards###################
@csrf_exempt
def creditcards(request):
	logged_in_user=request.user
	return render(request,'dashboard/creditcards.html',{'logged_in_user':logged_in_user})

#############Account Email Preferences###################
@csrf_exempt
def email_preferences(request):
	logged_in_user=request.user
	return render(request,'dashboard/email_preferences.html',{'logged_in_user':logged_in_user})

#############Account Venues Organizers###################
@csrf_exempt
def venues_organizers(request):
	logged_in_user=request.user
	return render(request,'dashboard/venues_organizers.html',{'logged_in_user':logged_in_user})

#############Account Permissions###################
@csrf_exempt
def permissions(request):
	logged_in_user=request.user
	return render(request,'dashboard/permissions.html',{'logged_in_user':logged_in_user})

#############Account Extensions###################
@csrf_exempt
def extensions(request):
	logged_in_user=request.user
	return render(request,'dashboard/extensions.html',{'logged_in_user':logged_in_user})

#############Account Close Account###################
@csrf_exempt
def account_close(request):
	logged_in_user=request.user
	return render(request,'dashboard/account_close.html',{'logged_in_user':logged_in_user})

#############Account Charges and Credits###################
@csrf_exempt
def charges(request):
	logged_in_user=request.user
	return render(request,'dashboard/charges.html',{'logged_in_user':logged_in_user})

#############Account Payout Methods###################
@csrf_exempt
def payment_information(request):
	logged_in_user=request.user
	return render(request,'dashboard/payment_information.html',{'logged_in_user':logged_in_user})

#############Account Invoices###################
@csrf_exempt
def invoices(request):
	logged_in_user=request.user
	return render(request,'dashboard/invoices.html',{'logged_in_user':logged_in_user})

#############Account Taxpayer Information###################
@csrf_exempt
def tax_information(request):
	logged_in_user=request.user
	return render(request,'dashboard/tax_information.html',{'logged_in_user':logged_in_user})

#############Account Taxpayer Forms###################
@csrf_exempt
def tax_forms(request):
	logged_in_user=request.user
	return render(request,'dashboard/tax_forms.html',{'logged_in_user':logged_in_user})

#############Account close account###################
@csrf_exempt
def close_account(request):
	logged_in_user=request.user
	if request.method == 'POST':
		reason=request.POST['reason']
		close=request.POST['close']
		password=request.POST['password_user']
		logged_user=User.objects.filter(email=logged_in_user)
		for user in logged_user:
			user_password=user.password
			check=check_password(password,user_password)
		if check==True:
			for u in logged_user:
				if u.is_active:
					u = User.objects.get(email=logged_in_user)
					u.delete()
					return HttpResponse('removed')
				else:
					return HttpResponse('not-removed')
		else:
			return HttpResponse('not-removed')
	else:
		return render(request,'dashboard/account_close.html',{'logged_in_user':logged_in_user})


############Search for events from home###################
@csrf_exempt
def search_events(request):
	try:
		if(request.POST['category']):
			category=request.POST['category']
		else:
			category=''
		if(request.POST['location']):
			location=request.POST['location']
		else:
			location=''
		if(request.POST['date']):
			date=request.POST['date']
		else:
			date=''
		today=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') 
		events_list=[]
		events_dict={'publish_event':'','event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		category_data=Event.objects.filter(event_name=category)
		for data in category_data:
			category_id=data.category_id
		##########################Category and date Filter##################################
		if((category != '') and (date == 'All Dates')):
			events_data=Event.objects.filter(category_id=category_id)
		if((category != '') and (date == 'Today')):
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__startswith=today)
		if((category != '') and (date == 'Tomorrow')):
			date_tomorrow = datetime.now()+ relativedelta(days=1)
			date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d %H:%M:%S') 
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__startswith=date_tomorrow_new)
		if((category != '') and (date == 'This Week')):
			start_date=today
			end_date=datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__range=(start_date, end_date_new))
		if((category != '') and (date == 'This Weekend')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=5)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__range=(start_date_new1, end_date_new))
		if((category != '') and (date == 'Next Week')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=7)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=14)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__range=(start_date_new1, end_date_new))
		if((category != '') and (date == 'Next Month')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=30)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=60)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__range=(start_date_new1, end_date_new))
		#########################################Ends here###################################################################

		##########################################Location and dates filter##################################################
		if((location != '') and (date == 'All Dates')):
			events_data=Event.objects.filter(city=location)
		if((location != '') and (date == 'Today')):
			events_data=Event.objects.filter(city=location).filter(timestamp__contains=today)
		if((location != '') and (date == 'Tomorrow')):
			date_tomorrow = datetime.now()+ relativedelta(days=1)
			date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d %H:%M:%S') 
			events_data=Event.objects.filter(city=location).filter(timestamp__startswith=date_tomorrow_new)
		if((location != '') and (date == 'This Week')):
			start_date=today
			end_date=datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(city=location).filter(timestamp__range=(start_date, end_date_new))
		if((location != '') and (date == 'This Weekend')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=5)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(city=location).filter(timestamp__range=(start_date_new1, end_date_new))
		if((location != '') and (date == 'Next Week')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=7)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=14)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(city=location).filter(timestamp__range=(start_date_new1, end_date_new))
		if((location != '') and (date == 'Next Month')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=30)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=60)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(city=location).filter(timestamp__range=(start_date_new1, end_date_new))


		#########################################Ends here##################################################################
		for event in events_data:
			event_id=str(event.event_id)
			city=event.city
			country=event.country
			time_new=event.timestamp
			time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
			name=event.event_name
			price=event.price
			image=event.image
			description=event.description
			category_id=event.category_id
			publish_event=event.publish_event
			events_dict['event_id']=event_id
			events_dict['city']=city
			events_dict['country']=country
			events_dict['time']=time_register
			events_dict['name']=name
			events_dict['price']=price
			events_dict['image']=image
			events_dict['description']=description
			events_dict['category']=category
			events_dict['publish_event']=publish_event
			events_list.append(events_dict.copy())
		html = render_to_string('myapp/search_results.html', {'events_data':events_list })
		return HttpResponse(html)
	except Exception as e:
		print('error',e)


def tickets(request):
	logged_in_user=request.user
	return render(request,'dashboard/tickets.html',{'logged_in_user':logged_in_user})


def manage_events(request):
	try:
		logged_in_user=request.user.id
		total_tickets=request.session['total_tickets']
		if total_tickets == "":
			total_tickets=0
		else:
			total_tickets=total_tickets	
		events_data_list=[]
		events_data_dict={'publish_event':'','flag':'','event_id':'','city':'','country':'','start_time':'','end_time':'','name':'','price':'','image':'','description':'','category':''}
		subevents_data_list=[]
		subevents_data_dict={'event_id_id':'','sub_event_id':'','time':'','sub_event_name':'','price':'','image':'','description':'',
		'sub_event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':''}
		events_info=Event.objects.filter(user=logged_in_user)
		images_info_list=[]
		images_info_dict={'name':''}
		if events_info:
			for info in events_info:
				event_id=str(info.id)
				city=info.location
				location_object= Location.objects.get(city =city )			
				country=location_object.country
				start_datetime=info.start_datetime
				start_datetime=start_datetime.strftime("%B %d, %Y, %I:%M %p")
				end_datetime=info.end_datetime
				end_datetime=end_datetime.strftime("%B %d, %Y, %I:%M %p")
				name=info.event_name
				eventticket_object=EventTickets.objects.get(event_id=event_id,sub_event_id=None)
				price = eventticket_object.price
				description=info.description
				category_name=info.category
				publish_event=info.publish_event
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['start_time']=start_datetime
				events_data_dict['end_time']=end_datetime
				events_data_dict['name']=name
				events_data_dict['price']=price
				events_data_dict['description']=description
				events_data_dict['category']=category_name
				events_data_dict['publish_event']=publish_event
				events_data_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
				subevents_info=SubEvent.objects.filter(event_id=event_id)
				if subevents_info:
					events_data_dict['flag']=1
				else:
					events_data_dict['flag']=0
				events_data_list.append(events_data_dict.copy())
		else:
			events_data_list=[]
			events_data_list.append(events_data_dict.copy())
		return render(request,'dashboard/manage_events.html',{'total_tickets':total_tickets,'events_data':events_data_list,'logged_in_user':logged_in_user})	
	except Exception as e:
		print 'error',e				
				
def contacts(request):
	logged_in_user=request.user
	return render(request,'dashboard/contacts.html',{'logged_in_user':logged_in_user})

def account_settings(request):
	logged_in_user=request.user
	user_id = request.user.id
	first_name = request.user.first_name
	last_name = request.user.last_name
	user_data_list=[]
	user_data_dict={'prefix':'','first_name':'','last_name':'','suffix':'','home_phone':'','cell_phone':'',
					'job_title':'','company':'','website':'','blog':'','address':'','country':'',
					'state':'','city':'','home_zip':'','billing_address':'','billing_country':'','billing_state':'',
					'billing_city':'','billing_zip_code':'','shipping_address':'','shipping_country':'','shipping_state':'',
					'shipping_city':'','shipping_zip_code':'','work_address':'','work_country':'','work_state':'',
					'work_city':'','work_zip_code':'','gender':'','age':'','year':'','month':'','day':'',
					'profile_pic':'','timestamp':''}
	user_info=UserAccount.objects.filter(user_id=user_id)
	if user_info:
		for info in user_info:
			prefix=info.prefix
			suffix=info.sufix
			home_phone=info.home_phone
			cell_phone=info.cell_phone
			job_title=info.job_title
			company=info.company
			website=info.website
			blog=info.blog
			address=info.home_address
			country=info.home_country
			state=info.home_state
			city=info.home_city
			home_zip=info.home_zip_code
			billing_address=info.billing_address
			billing_country=info.billing_country
			billing_state=info.billing_state
			billing_city=info.billing_city
			billing_zip=info.billing_zip_code
			shipping_address=info.shipping_address
			shipping_country=info.shipping_country
			shipping_state=info.shipping_state
			shipping_city=info.shipping_city
			shipping_zip=info.shipping_zip_code
			work_address=info.work_address
			work_country=info.work_country
			work_state=info.work_state
			work_city=info.work_city
			work_zip=info.work_zip_code
			gender=info.gender
			dob=info.dob
			year=dob.split('-')[0]
			month=dob.split('-')[1]
			day=dob.split('-')[2]
			age=info.age
			profile_pic=info.profile_pic
			timestamp=info.timestamp
			user_data_dict['prefix']=prefix
			user_data_dict['first_name']=first_name
			user_data_dict['last_name']=last_name
			user_data_dict['suffix']=suffix
			user_data_dict['home_phone']=home_phone
			user_data_dict['cell_phone']=cell_phone
			user_data_dict['job_title']=job_title
			user_data_dict['company']=company
			user_data_dict['website']=website
			user_data_dict['blog']=blog
			user_data_dict['address']=address
			user_data_dict['country']=country
			user_data_dict['state']=state
			user_data_dict['city']=city
			user_data_dict['home_zip']=home_zip
			user_data_dict['billing_address']=billing_address
			user_data_dict['billing_country']=billing_country
			user_data_dict['billing_state']=billing_state
			user_data_dict['billing_city']=billing_city
			user_data_dict['billing_zip']=billing_zip
			user_data_dict['shipping_address']=shipping_address
			user_data_dict['shipping_country']=shipping_country
			user_data_dict['shipping_state']=shipping_state
			user_data_dict['shipping_city']=shipping_city
			user_data_dict['shipping_zip']=shipping_zip
			user_data_dict['work_address']=work_address
			user_data_dict['work_country']=work_country
			user_data_dict['work_state']=work_state
			user_data_dict['work_city']=work_city
			user_data_dict['work_zip']=work_zip
			user_data_dict['gender']=gender
			user_data_dict['age']=age
			user_data_dict['year']=year
			user_data_dict['month']=month
			user_data_dict['day']=day
			user_data_dict['timestamp']=timestamp
			user_data_dict['profile_pic']=profile_pic
			user_data_list.append(user_data_dict.copy())
	else:
		prefix=''
		first_name=''
		last_name=''
		suffix=''
		home_phone=''
		cell_phone=''
		job_title=''
		company=''
		website=''
		blog=''
		address=''
		country=''
		state=''
		city=''
		home_zip=''
		billing_address=''
		billing_country=''
		billing_state=''
		billing_city=''
		billing_zip=''
		shipping_address=''
		shipping_country=''
		shipping_state=''
		shipping_city=''
		shipping_zip=''
		work_address=''
		work_country=''
		work_state=''
		work_city=''
		work_zip=''
		gender=''
		year=''
		month=''
		day=''
		age=''
		timestamp=''
		profile_pic=''
		user_data_dict['prefix']=prefix
		user_data_dict['first_name']=first_name
		user_data_dict['last_name']=last_name
		user_data_dict['suffix']=suffix
		user_data_dict['home_phone']=home_phone
		user_data_dict['cell_phone']=cell_phone
		user_data_dict['job_title']=job_title
		user_data_dict['company']=company
		user_data_dict['website']=website
		user_data_dict['blog']=blog
		user_data_dict['address']=address
		user_data_dict['country']=country
		user_data_dict['state']=state
		user_data_dict['city']=city
		user_data_dict['home_zip']=home_zip
		user_data_dict['billing_address']=billing_address
		user_data_dict['billing_country']=billing_country
		user_data_dict['billing_state']=billing_state
		user_data_dict['billing_city']=billing_city
		user_data_dict['billing_zip']=billing_zip
		user_data_dict['shipping_address']=shipping_address
		user_data_dict['shipping_country']=shipping_country
		user_data_dict['shipping_state']=shipping_state
		user_data_dict['shipping_city']=shipping_city
		user_data_dict['shipping_zip']=shipping_zip
		user_data_dict['work_address']=work_address
		user_data_dict['work_country']=work_country
		user_data_dict['work_state']=work_state
		user_data_dict['work_city']=work_city
		user_data_dict['work_zip']=work_zip
		user_data_dict['gender']=gender
		user_data_dict['age']=age
		user_data_dict['year']=year
		user_data_dict['month']=month
		user_data_dict['day']=day
		user_data_dict['timestamp']=timestamp
		user_data_dict['profile_pic']=profile_pic
		user_data_list.append(user_data_dict.copy())
	try:
		total_ticket=request.session['total_ticket']
	except Exception as e:
		total_ticket=0
	return render(request,'dashboard/account_settings.html',{'user_data':user_data_list,'total_ticket':total_ticket,'logged_in_user':logged_in_user})

def create_event(request):
	try:
		logged_in_user=request.user.id
		try:
			total_ticket=request.session['total_ticket']
		except Exception as e:
			total_ticket=0
		names_list=[]
		names_dict={'event_name':'','event_id':''}
		events_names=Event.objects.filter(user=logged_in_user)
		if events_names: 
			for name in events_names:
				event_name=name.event_name
				event_id=name.id
				names_dict['event_name']=event_name
				names_dict['event_id']=event_id
				names_list.append(names_dict.copy())
		else:
			names_list=[]
		if(events_names):
			flag = '1'
		else:
			flag = '0'
		request.session['flag']=flag
		return render(request,'dashboard/create_event.html',{'total_ticket':total_ticket, 'logged_in_user':logged_in_user,'flag':flag,'names_list':names_list})
	except Exception as e:
		print e
		#logged_in_user=''
	#if not logged_in_user:
	#	return HttpResponse('empty')
	#else:

########################### Create sub event function ###########################
@csrf_exempt
def create_sub_event(request):
	try:
		x=0
		if (request.method=="POST"):
			####################Save Event Data#############
			sub_event_category=request.POST['sub_event_catagory']
			sub_category_info=SubCategory.objects.filter(sub_category_name=sub_event_category)
			if sub_category_info:
				for info in sub_category_info:
					sub_category_id=info.sub_category_id
			else:
				sub_category=SubCategory.objects.create(sub_category_name=sub_event_category)
				sub_category.save()
				sub_category_info=SubCategory.objects.filter(sub_category_name=sub_event_category)
				for info in sub_category_info:
					sub_category_id=info.sub_category_id
			sub_event_info=SubEvent.objects.all()
			sub_events_length=len(sub_event_info)
			sub_event_id=sub_events_length+1
			sub_event_name=request.POST['sub_event_name']
			sub_event_description=request.POST['sub_event_description']
			sub_event_organizer=request.POST['sub_event_organizer']
			sub_organizer_description=request.POST['sub_organizer_description']
			sub_event_type=request.POST['sub_event_type']
			ticket_name=request.POST['ticket_name']
			quantity_available=request.POST['quantity_available']
			ticket_description=request.POST['ticket_description']
			sale_channel=request.POST['sale_channel']
			minimum_tickets=request.POST['minimum_tickets']
			maximum_tickets=request.POST['maximum_tickets']
			try:
				ticket_price=request.POST['ticket_price']
			except Exception as e:
				ticket_price='0'
			subevent_image=request.POST['subevent_image']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			sub_category_id=sub_category_id
			start_date=request.POST['sub_start_date']
			start_date_str=start_date.split('/',3)
			start_date_year=start_date_str[2]
			start_date_month=start_date_str[0]
			start_date_day=start_date_str[1]
			final_start_date=start_date_year+'-'+start_date_month+'-'+start_date_day
			end_date=request.POST['sub_end_date']
			end_date_str=end_date.split('/',3)
			end_date_year=end_date_str[2]
			end_date_month=end_date_str[0]
			end_date_day=end_date_str[1]
			final_end_date=end_date_year+'-'+end_date_month+'-'+end_date_day
			start_time=request.POST['sub_start_time']
			end_time=request.POST['sub_end_time']
			timestamp_start=str(final_start_date+' '+start_time)
			start_date_object=datetime.datetime.strptime(timestamp_start,'%Y-%m-%d %I:%M %p')
			timestamp_end=final_end_date+' '+end_time
			end_date_object=datetime.datetime.strptime(timestamp_end,'%Y-%m-%d %I:%M %p')
			logged_in_user=request.user
			try:
				latest_event_id=request.session['latest_event_id']
			except Exception as e:
				latest_event_id=request.POST['event_id']
			event=SubEvent.objects.create(sub_event_name=sub_event_name,description=sub_event_description,
				timestamp=timestamp,organizer_name=sub_event_organizer,organizer_description=sub_organizer_description,
				sub_event_type=sub_event_type,price=ticket_price,image=subevent_image,timestamp_start=start_date_object,
				timestamp_end=end_date_object,sub_category_id=sub_category_id,event_id_id=latest_event_id)
			event.save()
			try:
				event_id=latest_event_id
				latest_event_record_id=event_id
			except Exception as e:
				event_id=''
			if(event_id==''):
				latest_event_record=Event.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
				latest_event_record_id=latest_event_record.event_id
				latest_record=SubEvent.objects.filter(event_id_id=latest_event_record_id).order_by('-timestamp')[0]
				latest_record_id=latest_record.sub_event_id
			else:
				latest_record=SubEvent.objects.filter(event_id_id=latest_event_record_id).order_by('-timestamp')[0]
				latest_record_id=latest_record.sub_event_id
			ticket_name=request.POST['ticket_name']
			sale_channel=request.POST['sale_channel']
			ticket_count=request.POST['quantity_available']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			ticket_start_date=request.POST['ticket_start_date']
			ticket_start_date_str=ticket_start_date.split('/',3)
			ticket_start_date_year=ticket_start_date_str[2]
			ticket_start_date_month=ticket_start_date_str[0]
			ticket_start_date_day=ticket_start_date_str[1]
			ticket_final_start_date=ticket_start_date_year+'-'+ticket_start_date_month+'-'+ticket_start_date_day
			ticket_end_date=request.POST['ticket_end_date']
			ticket_end_date_str=ticket_end_date.split('/',3)
			ticket_end_date_year=ticket_end_date_str[2]
			ticket_end_date_month=ticket_end_date_str[0]
			ticket_end_date_day=ticket_end_date_str[1]
			ticket_final_end_date=ticket_end_date_year+'-'+ticket_end_date_month+'-'+ticket_end_date_day
			ticket_start_time=request.POST['ticket_start_time']
			ticket_end_time=request.POST['ticket_end_time']
			ticket_timestamp_start=str(ticket_final_start_date+' '+ticket_start_time)
			ticket_start_date_object=datetime.datetime.strptime(ticket_timestamp_start,'%Y-%m-%d %I:%M %p')
			ticket_timestamp_end=ticket_final_end_date+' '+ticket_end_time
			ticket_end_date_object=datetime.datetime.strptime(ticket_timestamp_end,'%Y-%m-%d %I:%M %p')
			if(sub_event_type=='Free'):
				ticket_data=FreeTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
					timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
					event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id)
			if(sub_event_type=='Paid'):
				ticket_data=PaidTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
					timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
					event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id)
			if(sub_event_type=='Donation'):
				ticket_data=DonationTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
					timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
					event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id)
			ticket_data.save()
		return HttpResponse('success')
	except Exception as e:
		print e

############################Accounts page change email############################
@csrf_exempt
def change_email(request):
	try:
		current_password=request.POST['current_password']
		new_email=request.POST['new_email']
		old_email=request.POST['old_email']
		user=User.objects.filter(email=old_email)
		for us in user:
			user_password=us.password
			check=check_password(current_password,user_password)
			if check==True:
				User.objects.filter(email=old_email).update(email=new_email)
				return HttpResponse('success')
			else:
				return HttpResponse('wrong')
	except Exception as e:
		print(e)

############################Accounts page upload image############################
@csrf_exempt
def upload_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		file_name_list=file_name.split('.')
		file_name_str=file_name_list[0]
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/profiles/'+file_name_list[0]+'.png', "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)

############################Create event page upload Event image############################
@csrf_exempt
def upload_event_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/events/'+file_name, "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)


############################Create event page upload SubEvent image############################
@csrf_exempt
def upload_subevent_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/subevents/'+file_name, "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)


############################Browse Events page get free events############################
@csrf_exempt
def get_free_events(request):
	try:
		events_data_list=[]
		events_data_dict={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		filter_name=request.POST['filter']
		if(filter_name == 'free'):
			events_data=Event.objects.filter(price='0' )
		else:
			events_data=Event.objects.filter(~Q(price = '0'))
		for event in events_data:
			event_id=str(event.event_id)
			city=event.city
			country=event.country
			time_new=event.timestamp
			time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
			name=event.event_name
			price=event.price
			image=event.image
			description=event.description
			category_id=event.category_id
			category_info=Category.objects.filter(category_id=category_id)
			for cat_info in category_info:
				category_name=cat_info.category_name
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['time']=time_register
				events_data_dict['name']=name
				events_data_dict['price']=price
				events_data_dict['image']=image
				events_data_dict['description']=description
				events_data_dict['category']=category_name
				events_data_list.append(events_data_dict.copy())
		html = render_to_string('dashboard/browse_events_filter.html', {'events_data':events_data_list})
		return HttpResponse(html)
	except Exception as e:
		print(e)


############################Browse Events page get filtered events############################
@csrf_exempt
def get_filtered_events(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		try:
			start_date=request.POST['start_date']
			end_date=request.POST['end_date']
		except Exception as e:
			end_date=''
			start_date=''
		try:
			period=request.POST['period']
		except Exception as e:
			period=''
		try:
			category_name=request.POST['category']
		except Exception as e:
			category_name=''
		try:
			location_name=request.POST['location']
		except Exception as e:
			location_name=''
		today=datetime.datetime.now().strftime('%Y-%m-%d') 
		events_data_list=[]
		events_data_dict={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		if(start_date != '' and end_date != ''):
			start_date=str(start_date)
			end_date=str(end_date)
			start_date_list=start_date.split('/')
			year=start_date_list[2]
			day=start_date_list[1]
			month=start_date_list[0]
			start=year+'-'+month+'-'+day
			start_filter=datetime.strptime(start, '%Y-%m-%d').date()
			end_date_list=end_date.split('/')
			year1=end_date_list[2]
			day1=end_date_list[1]
			month1=end_date_list[0]
			end=year1+'-'+month1+'-'+day1
			end_filter=datetime.strptime(end, '%Y-%m-%d').date()
			events_data=Event.objects.filter(timestamp__range=(start_filter, end_filter))
		if(period != ''):
			if(period == 'Today'):
				today=datetime.datetime.now().strftime('%Y-%m-%d') 
				events_data=Event.objects.filter(timestamp__startswith=today)
			if(period == 'Tomorrow'):
				date_tomorrow = datetime.datetime.now()+ relativedelta(days=1)
				date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d') 
				events_data=Event.objects.filter(timestamp__startswith=date_tomorrow_new)
			if(period == 'This Week'):
				start_date=today
				end_date=datetime.datetime.now()+ relativedelta(days=7)
				end_date_new=end_date.strftime('%Y-%m-%d')
				events_data=Event.objects.filter(timestamp__range=(start_date, end_date_new))
			if(period =='This Weekend'):
				start_date=today
				start_date_new=datetime.datetime.now()+ relativedelta(days=5)
				start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
				end_date=datetime.datetime.now()+ relativedelta(days=7)
				end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
				events_data=Event.objects.filter(timestamp__range=(start_date_new1, end_date_new))
			if(period == 'Next Week'):
				start_date=today
				start_date_new=datetime.datetime.now()+ relativedelta(days=7)
				start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
				end_date=datetime.datetime.now()+ relativedelta(days=14)
				end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
				events_data=Event.objects.filter(timestamp__range=(start_date_new1, end_date_new))
			if(period == 'Next Month'):
				start_date=today
				start_date_new=datetime.datetime.now()+ relativedelta(days=30)
				start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
				end_date=datetime.datetime.now()+ relativedelta(days=60)
				end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
				events_data=Event.objects.filter(timestamp__range=(start_date_new1, end_date_new))

		if (category_name != ''):
			category_data=Category.objects.filter(category_name=category_name)
			for data in category_data:
				category_id=data.category_id
			events_data=Event.objects.filter(category_id = category_id)
		if(location_name  != ''):
			events_data=Event.objects.filter(city = location_name)
		for event in events_data:
			event_id=str(event.event_id)
			city=event.city
			country=event.country
			time_new=event.timestamp
			time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
			name=event.event_name
			price=event.price
			image=event.image
			description=event.description
			category_id=event.category_id
			category_info=Category.objects.filter(category_id=category_id)
			for cat_info in category_info:
				category_name=cat_info.category_name
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['time']=time_register
				events_data_dict['name']=name
				events_data_dict['price']=price
				events_data_dict['image']=image
				events_data_dict['description']=description
				events_data_dict['category']=category_name
				events_data_list.append(events_data_dict.copy())
		user_saved_events_list=[]
		user_saved_events_dict={'user_name':'','event_id':'','saved_event_flag':''}
		print '>>>>>1'
		saved_events_data=SavedEvent.objects.filter(user_name=logged_in_user)
		print '>>>>>2'
		for saved in saved_events_data:
			user_name=saved.user_name
			event_id=saved.event_id
			saved_event_flag=saved.saved_flag
			user_saved_events_dict['user_name']=user_name
			user_saved_events_dict['event_id']=event_id
			user_saved_events_dict['saved_event_flag']=saved_event_flag
			user_saved_events_list.append(user_saved_events_dict.copy())
		html = render_to_string('dashboard/browse_events_filter.html', {'saved_events_data':user_saved_events_list,'events_data':events_data_list})
		return HttpResponse(html)
	except Exception as e:
		print(e)



def gmail(request):
    try:
    	request.session[settings.GOOGLE_REDIRECT_SESSION_VAR] = request.path
        google_state = google_get_state(request)
        gcs = gdata.contacts.service.ContactsService()
        google_contacts = google_import(request, gcs, cache=True)
        
    

        return render_to_response('dashboard/gmail.html', { 
        	'google_state': google_state,
        	'google_contacts': google_contacts
        }, context_instance=RequestContext(request))
    except Exception as e:
    	print e


############################### Create Event image upload ############################

@csrf_exempt
def upload_pic(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		file_name_list=file_name.split('.')
		file_name_str=file_name_list[0]
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open("/home/kashyap/mydjango/myproject/myapp/static/images/events/"+file_name_list[0]+'.png', "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print e


################################### Create Event ###################################

@csrf_exempt
def create_event_data(request):
	try:
		logged_in_user=request.user.id
		request.POST
		if request.method=="POST":
		 	####################Save Event Data#############
			event_category=request.POST['event_category']
		 	category =Category.objects.get(category_name=Category.objects.get_or_create(category_name=event_category)[0])
		 	event_city=request.POST['city_name']
		 	event_country=request.POST['country_name']
		 	event_state=request.POST['state']		 	
			location =Location.objects.get(city=Location.objects.get_or_create(city=event_city,state=event_state,country=event_country)[0])
		 	ts = time.time()
		 	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		 	timestamp=st
		 	event_name=request.POST['event_name']
		 	event_description=request.POST['event_description']
		 	category_name=request.POST['event_category']
		 	college_name=request.POST['college_name']
		 	organizer_name=request.POST['organizer_name']
		 	organizer_description=request.POST['organizer_description']
		 	start_datetime=request.POST['start_date']
		 	end_datetime=request.POST['end_date']
		 	event_banner_object=EventBanner.objects.values_list('id', flat=True).filter(user=logged_in_user).filter(event_id=None)
		 	state = request.POST['state']
		 	user = User.objects.get(id=logged_in_user)
		 	event=Event.objects.create(category=category,registered_datetime=timestamp,image=event_banner_object,location=location,
		 		event_name=event_name,description=event_description,
		 		college_name=college_name,start_datetime=start_datetime,end_datetime=end_datetime,
		 		organizer_description=organizer_description,organizer_name=organizer_name,user=user)
		 	event.save()
		 	print 'event is saved'
		 	#########LatestEvent###########
		 	latest_record=Event.objects.filter(user=logged_in_user).order_by('-registered_datetime')[0]
		 	latest_record_id=latest_record.id
		 	event_image_list=latest_record.image[1:-1].split(' ') 
		 	images_id_list = []
		 	for val in event_image_list:
		 		if ',' in val:
		 			images_id_list.append(val[0:-1])
		 		else:
		 			images_id_list.append(val)
		 	event_image_object=EventBanner.objects.filter(user=logged_in_user).filter(id__in=images_id_list).update(event_id=latest_record_id)
		 	request.session['latest_event_id']=latest_record_id
		 	sale_channel=request.POST['sale_channel']
		 	ticket_count=request.POST['quantity_available']
		 	ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		 	registered_datetime=st
		 	start_datetime=request.POST['ticket_start']
		 	end_datetime=request.POST['ticket_end']
		 	minimum_tickets=request.POST['minimum_tickets']
		 	maximum_tickets=request.POST['maximum_tickets']
		 	ticket_description=request.POST['ticket_desc']
		 	price=request.POST['ticket_price']
		 	if price == "":
		 		price = 0
		 	else:
		 		price = price	
		 	ticket_type=(str(request.POST['ticket_type'])).lower()
		 	ticket_name = request.POST['ticket_name']
			event_ticket_object=EventTickets.objects.create(ticket_description=ticket_description,ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
					registered_datetime=registered_datetime,start_datetime=start_datetime,end_datetime=end_datetime,
		 			event_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets,ticket_type=ticket_type,price=price)
		 	event_ticket_object.save()

		return HttpResponse(latest_record_id)
	except Exception as e:
		print e



# #########################create_subevent_data############################
	
@csrf_exempt
def create_subevent_data(request):
	try:
		logged_in_user=request.user.id
		if request.method == "POST":								
			try:
				################### fetch subevent details ##################
				latest_event_id=request.POST['latest_event_id']			
				sub_event_name=request.POST["sub_event_title"]
				description=request.POST["sub_event_desc"]
				venue=request.POST["sub_venue"]
				organizer_name=request.POST["sub_organizer_name"]
				visible_check=request.POST['visible_check']
				organizer_description=request.POST["sub_organizer_desc"]			
				sub_event_category=request.POST["sub_event_category"]
				category =Category.objects.get(category_name=Category.objects.get_or_create(category_name=sub_event_category)[0])
				ts = time.time()
				st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			 	registered_datetime=st
				start_datetime=request.POST["sub_start_time"]
				end_datetime=request.POST["sub_end_time"]
				sub_event_count = int(request.POST["sub_event_count"])
				#######fetch ticket details ############						
				# latest_record=Event.objects.filter(user=logged_in_user).order_by('-registered_datetime')[0]
				# print 'latest_record',latest_record
				subevent_banner_object=SubEventBanner.objects.filter(user=logged_in_user).filter(sub_event_id=None).filter(sub_event_index=sub_event_count)	
				data_list=[]
				for data in subevent_banner_object:
					data_list.append(data.id)
				subevent=SubEvent.objects.create(event_id=latest_event_id,venue=venue,registered_datetime=registered_datetime,start_datetime=start_datetime,end_datetime=end_datetime,
					sub_event_name=sub_event_name,organizer_name=organizer_name,organizer_description=organizer_description,
					category=category,image=data_list,description=description,visible_check=visible_check,user=request.user)
				subevent.save()
			except Exception as e:
				print 'error during saving sub event ',e
			try:		
				subevent_id=subevent.id
				# sub_event_object=SubEvent.objects.filter(id=id_subevent)
				# for info in sub_event_object:
				# subevent_id = info.id						
				# sub_event_image_list = (subevent.image.encode('utf-8')[1:-1]).split(' ')
				# image_id_list = [ int(val[0:-1]) if ',' in val else int(val) for val in sub_event_image_list]
				event_image_object=SubEventBanner.objects.filter(user=logged_in_user).filter(id__in=data_list).update(sub_event_id=subevent_id)					 						 	
				data_list = []
				# sub_event_image_list=[]
				# image_id_list = []
			except Exception as e:
				print 'error during updation of subevent banner ',e
			try:				
				#######save tickets############						
				ticket_name=request.POST['sub_ticket_name']
				sub_event_type=(str(request.POST["sub_type_ticket"])).lower()
			 	sale_channel=request.POST['sub_sale_channel']
			 	minimum_tickets=request.POST['sub_minimum_tickets']
			 	maximum_tickets=request.POST['sub_maximum_tickets']
			 	ticket_description=request.POST['sub_ticket_description']
			 	ticket_count=request.POST['sub_quantity_available']
			 	price=request.POST['sub_ticket_price']
			 	if price == "":
			 		price = 0
			 	else:
			 		price=price	
			 	start_datetime=request.POST['sub_ticket_start']
			 	end_datetime=request.POST['sub_ticket_end']
			 	###############save subevent##################				
				subevent_banner_object=SubEventBanner.objects.filter(user=logged_in_user).filter(sub_event_id=None).filter(sub_event_index=sub_event_count)	
				data_list=[]
				for data in subevent_banner_object:
					data_list.append(data.id)
				subevent=SubEvent.objects.create(event_id=latest_event_id,venue=venue,registered_datetime=registered_datetime,start_datetime=start_datetime,end_datetime=end_datetime,
					sub_event_name=sub_event_name,organizer_name=organizer_name,organizer_description=organizer_description,
					category=category,image=data_list,description=description,visible_check=visible_check,user=request.user)
				subevent.save()								
				subevent_id=subevent.id
				##############update subeventbanner #################
				event_image_object=SubEventBanner.objects.filter(user=logged_in_user).filter(id__in=data_list).update(sub_event_id=subevent_id)					 						 											
			 	###############subevent-ticket-check#################			 	
			 	if  EventTickets.objects.filter(event_id=latest_event_id,sub_event_id=subevent_id).exists() is not True:
				 	subevent_ticket_object=EventTickets(ticket_name=ticket_name,ticket_type=sub_event_type,sale_channel=sale_channel,
				 		order_min=minimum_tickets,order_max=maximum_tickets,ticket_description=ticket_description,
				 		ticket_count=ticket_count,price=price,registered_datetime=registered_datetime,start_datetime=start_datetime,
				 		end_datetime=end_datetime,event_id=latest_event_id,sub_event_id=subevent_id)
				 	subevent_ticket_object.save()
			except Exception as e:
				print 'error while saving subevent details in database',e																						 	
			return HttpResponse('success')
	except Exception as e:
		print e

# #######################Ends Here##############################


		

################################# Event Location #################################

@csrf_exempt
def tickets_info(request):
		return HttpResponse('Success')

################################### New #########################################

def google_get_state_token(request, action_type_id, action_id):
    action_state = ActionState.objects.create(**{
        'action_type_id': action_type_id,
        'action_id': action_id,
        'data': request.GET
    })
    return JsonResponse({'stat': 'ok', 'token': action_state.uuid})

def google_login(request):
    token_login = request.GET.get('token')
    
    if token_login:
        gcs = gdata.contacts.service.ContactsService()
        gcs.SetAuthSubToken(token_login)
        gcs.UpgradeToSessionToken()
        request.session[settings.GOOGLE_COOKIE_CONSENT] = gcs.GetAuthSubToken()
    
    return redirect(request.session.get(settings.GOOGLE_REDIRECT_SESSION_VAR))


def google_logout(request):
    if request.session.get(settings.GOOGLE_COOKIE_CONSENT):
        del request.session[settings.GOOGLE_COOKIE_CONSENT]
    if request.session.get('google_contacts_cached'):
        del request.session['google_contacts_cached']
    return redirect(request.session.get(settings.GOOGLE_REDIRECT_SESSION_VAR))




############Delete an event###################
@csrf_exempt
def delete_event(request):
	try:
		event_id=request.POST['event_id']
		event_ticket_id= EventTickets.objects.get(event_id=event_id,sub_event_id=None)
		sub_event_id_list = SubEvent.objects.values_list('id', flat=True).filter(event_id=event_id)
		if 	sub_event_id_list:
			sub_event_tickets_id_list = EventTickets.objects.values_list('id', flat=True).filter(event_id=event_id).filter(sub_event_id__in=sub_event_id_list)
			######delete subevent data#####
			ImageQR.objects.filter(event_id=event_id).filter(sub_event_id__in=sub_event_id_list).delete()
			TicketBooked.objects.filter(ticket_id__in=sub_event_tickets_id_list).delete()			
			EventTickets.objects.filter(sub_event_id__in=sub_event_id_list).filter(event_id=event_id).delete()
			SubEvent.objects.filter(event_id=event_id,id__in=sub_event_id_list).delete()
			sub_event_id_list=[str(x) for x in sub_event_id_list]
			SubEventBanner.objects.filter(sub_event_id__in=sub_event_id_list).delete()
		######delete event data ####### 
		ImageQR.objects.filter(event_id=event_id).delete()
		TicketBooked.objects.filter(ticket_id=event_ticket_id).delete()
		EventTickets.objects.get(event_id=event_id,sub_event_id=None).delete()
		EventBanner.objects.filter(event_id=event_id).delete()
		Event.objects.get(id=event_id).delete()
		return HttpResponse('success')
	except Exception as e:
		print 'error',e

###########Delete an subevent##########
@csrf_exempt
def delete_subevent(request):
	try:
		event_id=request.POST['event_id']
		sub_event_id=request.POST['sub_event_id']
		ImageQR.objects.filter(event_id=event_id).filter(sub_event_id=sub_event_id).delete()
		TicketBooked.objects.filter(ticket_id=EventTickets.objects.get(event_id=event_id,sub_event_id=sub_event_id).id).delete()
		EventTickets.objects.get(event_id=event_id,sub_event_id=sub_event_id).delete()
		SubEventBanner.objects.filter(sub_event_id=sub_event_id).delete()
		SubEvent.objects.get(event_id=event_id,id=sub_event_id).delete()	
		event = Event.objects.get(id=event_id)
		try:
			subevent_info=SubEvent.objects.get(event_id=event_id)
		except Exception as e:
			subevent_info=''
		event.delete()
		#subevent.delete()
		return HttpResponse('success')
	except Exception as e:
		print e



############## Quantity selected ####################
@csrf_exempt
def quantity_selected(request):
	try:
		qua_list=[]
		qua_dict={'selected':''}
		select_val=request.POST['select_val']
		qua_dict['selected']=select_val
		qua_list.append(qua_dict.copy()) 
		html = render_to_string('dashboard/quantity_selected.html', {'qua_list':qua_list })
		return HttpResponse(html)
	except Exception as e:
		print e

############## Get Subevents form ####################
@csrf_exempt
def get_subevent_form(request):
	try:
		index=request.POST['index']
		html = render_to_string('dashboard/sub_event_ajax.html', {'index':index})
		return HttpResponse(html)
	except Exception as e:
		print e

############Edit an event###################
@csrf_exempt
def edit_event(request):
	try:
		logged_in_user=request.user
		event_id=request.POST['event_id']
		return HttpResponse(event_id)
	except Exception as e:
		print e




def manage_edit_event(request,id):
	try:
		logged_in_user=request.user
		total_tickets=request.session['total_tickets']
		event_id=id
		events_data_list=[]
		image_data_list = []
		events_data_dict={'event_id':'','name':'','description':'','college_name':'','country':'','city':'',
		'organizer_name':'','organizer_description':'','event_type':'','category':'','start_datetime':'',
		'end_datetime':'','image_data':'','ticket_name':'','ticket_count':'','price':'','state':'',
		'ticket_description':'','sale_channel':'','ticket_start_datetime':'','ticket_end_datetime':'',
		'ticket_order_min':'','ticket_order_max':'','user_email':'','publish_event':''}
		info=Event.objects.get(id=event_id)		
		event_id=str(info.id)
		event_name=info.event_name
		description=info.description
		college_name=info.college_name
		city=str(info.location)
		location_object=Location.objects.get(city=city)
		country = location_object.country
		state = location_object.state
		organizer_name=info.organizer_name
		organizer_description=info.organizer_description
		category_name=str(info.category)		
		start_datetime=info.start_datetime.replace(tzinfo=None,microsecond=0)
		start_datetime=str(datetime.datetime.strptime(str(start_datetime),"%Y-%m-%d %H:%M:%S"))
		end_datetime=info.end_datetime.replace(tzinfo=None,microsecond=0)
		end_datetime=str(datetime.datetime.strptime(str(end_datetime),"%Y-%m-%d %H:%M:%S"))
		eventbanner_object = EventBanner.objects.filter(event_id=event_id)
		for image_info in eventbanner_object:
			image_data_list.append({'image':str(image_info.event_banner).split('/',1)[1],'image_id':image_info.id})						 		
		eventsticket_object=EventTickets.objects.get(event_id=event_id,sub_event_id=None)
		ticket_name = eventsticket_object.ticket_name
		ticket_count = eventsticket_object.ticket_count
		price = eventsticket_object.price
		ticket_type=eventsticket_object.ticket_type
		ticket_description = eventsticket_object.ticket_description
		sale_channel=eventsticket_object.sale_channel
		ticket_start_datetime=eventsticket_object.start_datetime.replace(tzinfo=None,microsecond=0)
		ticket_start_datetime=str(datetime.datetime.strptime(str(ticket_start_datetime),"%Y-%m-%d %H:%M:%S"))
		ticket_endt_datetime=eventsticket_object.end_datetime.replace(tzinfo=None,microsecond=0)
		ticket_endt_datetime=str(datetime.datetime.strptime(str(ticket_endt_datetime),"%Y-%m-%d %H:%M:%S"))
		ticket_order_min=eventsticket_object.order_min
		ticket_order_max=eventsticket_object.order_max		
		user_email=str(info.user)
		publish_event=info.publish_event
		events_data_dict['event_id']=event_id
		events_data_dict['name']=event_name
		events_data_dict['description']=description
		events_data_dict['college_name']=college_name
		events_data_dict['country']=country
		events_data_dict['city']=city
		events_data_dict['state']=state
		events_data_dict['organizer_name']=organizer_name
		events_data_dict['organizer_description']=organizer_description
		events_data_dict['event_type']=ticket_type
		events_data_dict['category']=category_name
		events_data_dict['start_datetime']=start_datetime
		events_data_dict['end_datetime']=end_datetime
		events_data_dict['image_data']=image_data_list
		events_data_dict['ticket_name']=ticket_name
		events_data_dict['ticket_count']=ticket_count
		events_data_dict['price']=price	
		events_data_dict['ticket_description']=ticket_description
		events_data_dict['sale_channel']=sale_channel		
		events_data_dict['ticket_start_datetime']=ticket_start_datetime
		events_data_dict['ticket_end_datetime']=ticket_endt_datetime
		events_data_dict['ticket_order_min']=ticket_order_min
		events_data_dict['ticket_order_max']=ticket_order_max		
		events_data_dict['user_email']=user_email
		events_data_dict['publish_event']=publish_event		
		events_data_list.append(events_data_dict.copy())
		return render(request,'dashboard/edit_event.html',{'logged_in_user':logged_in_user,'event_data':events_data_list,'total_tickets':total_tickets})

	except Exception as e:
		print 'error',e	

	
	
#####################################Autocomplete for Events#######################
@csrf_exempt
def autocomplete_events(request):
	try:
		term=str(request.POST['term'])
		results=[]
		events_data=Event.objects.filter(event_name__contains=term)
		for data in events_data:
			event_name=data.event_name
			user_json = {}
			user_json['label'] = event_name
			user_json['value'] = event_name
			results.append(user_json)
			data = json.dumps(results)
			mimetype = 'application/json'
		return HttpResponse(data, mimetype)
	except Exception as e:
		print e


######################## View Sub Events #########################

@csrf_exempt
def view_subevents(request):
	try:
		event_id=request.POST['event_id']	
		return HttpResponse(event_id)	
	except Exception as e:
		print 'error',e			


######################### subevent_data_handler ################
@csrf_exempt
def subevent_data_handler(request,id):
	try:
		total_tickets=request.session['total_tickets']
		event_id=id
		events_data_list=[]
		events_data_dict={'event_id':'','event_name':''}
		subevents_data_list=[]
		subevents_data_dict={'sub_event_id':'','sub_event_img':'','sub_event_name':'','visible':''}
		events_info=Event.objects.filter(id=event_id)
		for info in events_info:
			event_id=str(info.id)
			name=info.event_name
			category=info.category
			events_data_dict['event_id']=event_id
			events_data_dict['event_name']=name
			events_data_list.append(events_data_dict.copy())
			subevents_info=SubEvent.objects.filter(event_id=event_id)
			if subevents_info:
				for info in subevents_info:
					sub_event_id=str(info.id)									
					sub_event_name=info.sub_event_name
					visible=info.visible_check
					subevents_data_dict['sub_event_id']=sub_event_id
					subevents_data_dict['visible']=visible
					subevents_data_dict['sub_event_img']=image_path_editor(SubEventBanner.objects.values_list('sub_event_banner', flat=True).filter(sub_event_id=sub_event_id))
					subevents_data_dict['sub_event_name']=sub_event_name
					subevents_data_list.append(subevents_data_dict.copy())
		return render(request,'dashboard/view_this_subevent.html',{'subevent_data':subevents_data_list,'event_data':events_data_list,'total_tickets':total_tickets})
	except Exception as e:
		print 'error',e



######################View Sub Event#########################

@csrf_exempt
def view_this_subevent(request):
	logged_in_user=request.user
	event_data=request.session['events_data_view']
	subevent_data=request.session['subevents_data_view']
	try:
		flag=request.session['flag']
	except Exception as e:
		flag=''
	return render(request,'dashboard/view_this_subevent.html',{'logged_in_user':logged_in_user,'subevent_data':subevent_data,'event_data':event_data,'flag':flag})


			
################### make subevent invisible from registration page #####################
@csrf_exempt
def visible_check_disable(request):
	try:
		event_id = request.POST['event_id']
		sub_event_id = request.POST['sub_event_id']
		sub_event_object = SubEvent.objects.filter(id=sub_event_id).filter(event_id=event_id) 
		for info in sub_event_object:
			visible_check = info.visible_check
		if 	visible_check == '0':
			return HttpResponse('wrong')
		else:
			SubEvent.objects.filter(id=sub_event_id).filter(event_id=event_id).update(visible_check=0)
			return HttpResponse('success')
	except Exception as e:
		print 'error',e


################### make subevent invisible from registration page #####################
@csrf_exempt
def visible_check_enable(request):
	try:
		event_id = request.POST['event_id']
		sub_event_id = request.POST['sub_event_id']
		sub_event_object = SubEvent.objects.filter(id=sub_event_id).filter(event_id=event_id) 
		for info in sub_event_object:
			visible_check = info.visible_check
		if 	visible_check == '1':
			return HttpResponse('wrong')
		else:
			SubEvent.objects.filter(id=sub_event_id).filter(event_id=event_id).update(visible_check=1)
			return HttpResponse('success')
	except Exception as e:
		print 'error',e


#################### Edit sub event ########################

@csrf_exempt
def edit_subevent_dataprovider(request):
	try:
		logged_in_user=request.user
		subevent_id=request.POST['sub_event_id']
		return HttpResponse(subevent_id)
	except Exception as e:
		print e

#################### Edit Subevents ###########################

@csrf_exempt
def edit_subevents(request,id):
	try:
		sub_event_id=id
		total_tickets=request.session['total_tickets']
		image_data_list = []
		subevents_data_list=[]
		subevents_data_dict={'sub_event_id':'','sub_event_name':'','description':'','venue':'',
		'organizer_name':'','organizer_description':'','sub_event_type':'','category':'','start_datetime':'',
		'end_datetime':'','image_data':'','ticket_name':'','ticket_count':'','price':'',
		'ticket_description':'','sale_channel':'','ticket_start_datetime':'','ticket_end_datetime':'',
		'ticket_order_min':'','ticket_order_max':'',}		
		subevents_info=SubEvent.objects.filter(id=sub_event_id)
		if subevents_info:
			for info in subevents_info:
				sub_event_id=info.id
				sub_event_name=info.sub_event_name
				description=info.description
				venue = info.venue
				organizer_name=info.organizer_name
				organizer_description=info.organizer_description
				category_name=info.category
				start_datetime=info.start_datetime.replace(tzinfo=None,microsecond=0)
				start_datetime=str(datetime.datetime.strptime(str(start_datetime),"%Y-%m-%d %H:%M:%S"))
				end_datetime=info.end_datetime.replace(tzinfo=None,microsecond=0)
				end_datetime=str(datetime.datetime.strptime(str(end_datetime),"%Y-%m-%d %H:%M:%S"))	
				subeventbanner_object = SubEventBanner.objects.filter(sub_event_id=sub_event_id)
				for image_info in subeventbanner_object:
					image_data_list.append({'image':str(image_info.sub_event_banner).split('/',1)[1],'image_id':image_info.id})
				subeventsticket_object=EventTickets.objects.get(sub_event_id=sub_event_id)
				ticket_name = subeventsticket_object.ticket_name
				ticket_count = subeventsticket_object.ticket_count
				price = subeventsticket_object.price
				ticket_type=subeventsticket_object.ticket_type
				ticket_description = subeventsticket_object.ticket_description
				sale_channel=subeventsticket_object.sale_channel
				ticket_start_datetime=subeventsticket_object.start_datetime.replace(tzinfo=None,microsecond=0)
				ticket_start_datetime=str(datetime.datetime.strptime(str(ticket_start_datetime),"%Y-%m-%d %H:%M:%S"))
				ticket_endt_datetime=subeventsticket_object.end_datetime.replace(tzinfo=None,microsecond=0)
				ticket_endt_datetime=str(datetime.datetime.strptime(str(ticket_endt_datetime),"%Y-%m-%d %H:%M:%S"))
				ticket_order_min=subeventsticket_object.order_min
				ticket_order_max=subeventsticket_object.order_max						
				subevents_data_dict['sub_event_id']=sub_event_id
				subevents_data_dict['sub_event_name']=sub_event_name
				subevents_data_dict['description']=description
				subevents_data_dict['venue']=venue
				subevents_data_dict['organizer_name']=organizer_name
				subevents_data_dict['organizer_description']=organizer_description
				subevents_data_dict['sub_event_type']=ticket_type
				subevents_data_dict['category']=category_name
				subevents_data_dict['start_datetime']=start_datetime
				subevents_data_dict['end_datetime']=end_datetime
				subevents_data_dict['image_data']=image_data_list
				subevents_data_dict['ticket_name']=ticket_name
				subevents_data_dict['ticket_count']=ticket_count
				subevents_data_dict['price']=price	
				subevents_data_dict['ticket_description']=ticket_description
				subevents_data_dict['sale_channel']=sale_channel		
				subevents_data_dict['ticket_start_datetime']=ticket_start_datetime
				subevents_data_dict['ticket_end_datetime']=ticket_endt_datetime
				subevents_data_dict['ticket_order_min']=ticket_order_min
				subevents_data_dict['ticket_order_max']=ticket_order_max			
				subevents_data_list.append(subevents_data_dict.copy())		
		else:
			subevents_data_list =[]
		return render(request,'dashboard/edit_subevents.html',{'subevent_data':subevents_data_list,'total_tickets':total_tickets})				
	except Exception as e:
		print 'error',e
	


############################Create event page upload SubEvent image############################
@csrf_exempt
def update_subevent_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		#file_name_list=file_name.split('.')
		#file_name_str=file_name_list[0]
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/subevents/'+file_name, "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)



##################### Update Subevent############################

@csrf_exempt
def update_subevent_data(request):
	try:
		if (request.method=="POST"):
			sub_event_id=request.POST['sub_event_id']
			sub_event_name=request.POST['sub_event_name']
			description=request.POST['sub_event_description']
			venue = request.POST['venue']
			organizer_name=request.POST['sub_event_organizer']
			organizer_description=request.POST['sub_organizer_description']
			category = request.POST['category']
			start_datetime=request.POST['start_datetime']
			end_datetime=request.POST['end_datetime']
			ticket_name=request.POST['ticket_name']
			sale_channel=request.POST['sale_channel']
			ticket_count=request.POST['ticket_count']
			price = request.POST['price']
			sub_event_type=(str(request.POST['sub_event_type'])).lower()
			ticket_description = request.POST['ticket_description']
			sale_channel = request.POST['sale_channel']
			ticket_start_datetime=request.POST['ticket_start_datetime']
			ticket_end_datetime=request.POST['ticket_end_datetime']
			ticket_order_min=request.POST['ticket_order_min']
			ticket_order_max=request.POST['ticket_order_max']
			####update category####
			category =Category.objects.get(category_name=Category.objects.get_or_create(category_name=category)[0])
			######update subevent#####
			SubEvent.objects.filter(id=sub_event_id).update(venue=venue,start_datetime=start_datetime,
				end_datetime=end_datetime,sub_event_name=sub_event_name,organizer_name=organizer_name,
				organizer_description=organizer_description,category=category,description=description)

			EventTickets.objects.filter(sub_event_id=sub_event_id).update(
				ticket_name=ticket_name,ticket_type=sub_event_type,sale_channel=sale_channel,	
				order_min=ticket_order_min,order_max=ticket_order_max,ticket_description=ticket_description,
				ticket_count=ticket_count,price=price,start_datetime=ticket_start_datetime,
				end_datetime=ticket_end_datetime)
			
			return HttpResponse('success')
	except Exception as e:
		print e


###################### manage particular event ##############

@csrf_exempt
def manage_particular_event(request):
	logged_in_user=request.user
	event_data=request.session['events_data_view']
	total_tickets=request.session['total_tickets']
	if total_tickets == "":
		total_tickets = 0
	else:
		total_tickets=total_tickets
	flag=request.session['flag']
	event_ticket_data=request.session['events_tickets_view']
	#booked_tickets=request.session['events_booked_view']
	return render(request,'dashboard/manage_particular_event.html',{'logged_in_user':logged_in_user,'event_data':event_data,'event_ticket_data':event_ticket_data,'flag':flag,'total_tickets':total_tickets})
###################### manage particular event ##############

@csrf_exempt
def manage_particular_events(request):
	event_id=request.POST['event_id']
	event_data_list=[]
	event_ticket_list=[]
	event_booked_list=[]
	event_data_dict={'publish_event':'','event_id':'','city':'','country':'','event_type':'','time_start':'','time_end':'','name':'','price':'','image':'','description':'','category':''}
	event_ticket_dict={'timestamp_end':'','order_max':'','order_min':'','count':''}

	events_info=Event.objects.filter(id=event_id)
	if events_info:
		for info in events_info:
			event_id=str(info.id)
			city=str(info.location)
			location_object=Location.objects.get(city=city)
			country = location_object.country
			start_datetime=info.start_datetime
			start_datetime=start_datetime.strftime("%B %d, %Y, %I:%M %p")
			end_datetime=info.end_datetime
			end_datetime=end_datetime.strftime("%B %d, %Y, %I:%M %p")
			event_name=info.event_name
			eventsticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)
			price=eventsticket_object.price
			event_type=eventsticket_object.ticket_type
			order_min = eventsticket_object.order_min
			order_max = eventsticket_object.order_max
			ticket_count = eventsticket_object.ticket_count
			image=info.image
			description=info.description
			category_name=str(info.category)			
			publish_event=info.publish_event							
			event_data_dict['event_id']=event_id
			event_data_dict['city']=city
			event_data_dict['country']=country
			event_data_dict['time_start']=start_datetime
			event_data_dict['time_end']=end_datetime
			event_data_dict['name']=event_name
			event_data_dict['price']=price
			event_data_dict['image']=image
			event_data_dict['description']=description
			event_data_dict['category']=category_name
			event_data_dict['event_type']=event_type
			event_data_dict['publish_event']=publish_event
			event_data_list.append(event_data_dict.copy())
			event_ticket_dict['order_min']=order_min
			event_ticket_dict['order_max']=order_max
			event_ticket_dict['count']=ticket_count
			event_ticket_list.append(event_ticket_dict.copy())
		flag='1'	
	else:
		event_data_list=[]
		flag='0'
	request.session['flag']=flag
	request.session['events_data_view']=event_data_list
	request.session['events_tickets_view']=event_ticket_list
	#request.session['events_booked_view']=event_booked_list
	return HttpResponse('success')


####################### Manage Progressbar ##############################

@csrf_exempt
def manage_progressbar(request):
	try:
		event_id=request.POST['event_id']
		quantity_booked=0
		eventsticket_object=EventTickets.objects.get(event_id=event_id,sub_event_id=None)
		eventsticket_id= eventsticket_object.id
		book_info=TicketBooked.objects.filter(ticket_id=eventsticket_id)
		if book_info:
			for info in book_info:
				transaction_id=info.transaction_id
				#event_id_id=str(info.event_id_id)
				ticket_booked1=int(info.quantity_booked)
				quantity_booked=quantity_booked+ticket_booked1
		return HttpResponse(quantity_booked)
	except Exception as e:
		print e


############ register_ticket ###################
@csrf_exempt
def register_ticket(request):
	try:
		try:
			logged_in_user=request.user.id
		except Exception as e:
			logged_in_user=''
		event_id=request.POST['eventid']
		ticket_id=request.POST['ticket_id']
		ticket_name=request.POST['ticket_name']
		selected_quantity=request.POST['selected_quantity']
		selected_quantity1=int(selected_quantity)
		event_data_list=[]
		event_data_dict={'ticket_id':'', 'ticket_name':'', 'selected_quantity':'', 'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'',
		'category':'','event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':'','college_name':'','publish_id':''}

		quantity_booked=0
		events_info=Event.objects.filter(id=event_id)
		for info in events_info:
			event_id=str(info.id)
			publish_id = info.publish_event
			city=str(info.location)
			location_object = Location.objects.get(city=city)
			country=location_object.country
			time=info.registered_datetime
			time_start=info.start_datetime
			time_end=info.end_datetime
			time_register=time.strftime("%B %d, %Y, %I:%M %p")
			start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
			end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
			college_name=info.college_name
			name=info.event_name
			#####TicketDetails#####
			eventsticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)
			price=eventsticket_object.price
			ticket_name = eventsticket_object.ticket_name
			ticket_id = eventsticket_object.id
			event_type=eventsticket_object.ticket_type
			tic_end_time = eventsticket_object.end_datetime
			min_ticket=eventsticket_object.order_min
			max_ticket=eventsticket_object.order_max
			tic_count=eventsticket_object.ticket_count
			#####TicketDetailsEnd#####
			description=info.description
			organizer_name=info.organizer_name
			organizer_description=info.organizer_description
			college_name=info.college_name
			user_email=str(info.user)
			college_name=info.college_name
			category_name=str(info.category)			
			event_data_dict['event_id']=event_id
			event_data_dict['publish_id']=publish_id
			event_data_dict['city']=city
			event_data_dict['country']=country
			event_data_dict['time']=time_register
			event_data_dict['name']=name
			event_data_dict['price']=price
			event_data_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
			event_data_dict['description']=description
			event_data_dict['start_time']=start_time
			event_data_dict['end_time']=end_time
			event_data_dict['college_name']=college_name
			event_data_dict['user_email']=user_email
			event_data_dict['category']=category_name
			event_data_dict['event_type']=event_type
			event_data_dict['selected_quantity']=selected_quantity
			event_data_dict['organizer_name']=organizer_name
			event_data_dict['organizer_description']=organizer_description
			event_data_dict['ticket_name']=ticket_name
			event_data_dict['ticket_id']=ticket_id
			event_data_list.append(event_data_dict.copy())
			tic_count1=int(tic_count)
			book_info=TicketBooked.objects.filter(ticket_id=ticket_id)
			if book_info:
				for info in book_info:
					transaction_id=info.transaction_id
					event_id=event_id
					ticket_booked1=int(info.quantity_booked)
					quantity_booked=quantity_booked+ticket_booked1
		tickets_pending=tic_count1-quantity_booked
		if (tickets_pending >= selected_quantity1):
			request.session['event_data_view']=event_data_list
			return HttpResponse('success')
		else:
			return HttpResponse('wrong')
	except Exception as e:
		print e

############## Register Order ####################
@csrf_exempt
def register_order(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		event_data_view=request.session['event_data_view']
		return render(request,'dashboard/register_order.html',{'event_data_view':event_data_view,'logged_in_user':logged_in_user})
	except Exception as e:
		print e



############## Send Message to contact organizer ####################
@csrf_exempt
def send_msg(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		sender_name=request.POST['sender_name']
		sender_email=request.POST['sender_email']
		sender_comment=request.POST['sender_comment']
		if sender_name and sender_comment and sender_email:
			try:
				send_mail(sender_name, sender_comment, sender_email, [sender_email])
			except BadHeaderError:
				return HttpResponse('Invalid header found.')
			return HttpResponseRedirect('/dashboard/register_order/')
		else:
			# In reality we'd use a form class
			# to get proper validation errors.
			return HttpResponse('Make sure all fields are entered and valid.')
		return HttpResponse('success')
	except Exception as e:
		print e

############## View your tickets ####################
@csrf_exempt
def view_ticket(request):
	try:
		try:
			logged_in_user=request.user.id
		except Exception as e:
			logged_in_user=''
		#event_id=request.POST['event_id']
		#ticket_id=request.POST['ticket_id']
		#email_address=request.POST['email_address']
		book_tic_list=[]
		book_tic_dict={'transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'','quantity_book':'',
						'booked_ticket_id':'','booked_event_id':'','booked_ticket_name':'', 'booked_sub_event_id':''}
		ticked_booked_info=TicketBooked.objects.filter(user=logged_in_user).order_by('-timestamp')
		for ticket in ticked_booked_info:
			transaction_id=str(ticket.transaction_id)
			booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
			qunatity_book=ticket.quantity_booked
			ticket_id=ticket.ticket_id
			eventsticket_object = EventTickets.objects.get(id=ticket_id)
			event_id=str(eventsticket_object.event_id)
			sub_event_id=str(eventsticket_object.sub_event_id)
			eventsticket_object = EventTickets.objects.get(id=ticket_id)
			ticket_name=eventsticket_object.ticket_name
			book_tic_dict['transaction_id']=transaction_id
			book_tic_dict['booked_time']=booked_time
			# book_tic_dict['first_name']=first_name
			# book_tic_dict['last_name']=last_name
			book_tic_dict['quantity_book']=qunatity_book
			book_tic_dict['booked_ticket_id']=ticket_id
			book_tic_dict['booked_event_id']=event_id
			book_tic_dict['booked_ticket_name']=ticket_name
			book_tic_dict['booked_sub_event_id']=sub_event_id

			book_tic_list.append(book_tic_dict.copy())
		request.session['booked_data']=book_tic_list
		return HttpResponse('success')
	except Exception as e:
		print e




############ Particular booked ticket view ###################
@csrf_exempt
def booked_particular_ticket(request):
	try:
		booked_event_data_list=request.session['booked_event_data_list']
		booked_subevent_data_list=request.session['booked_subevent_data_list']
		total_tickets=request.session['total_tickets']
		if total_tickets == "":
			total_tickets = 0
		else:
			total_tickets=total_tickets
		user=request.user
		request.session['logged_in_user']=str(user)
		return render(request,'dashboard/booked_particular_ticket.html',{'booked_event_data_list':booked_event_data_list,
			'booked_subevent_data_list':booked_subevent_data_list,'logged_in_user':user,'total_tickets':total_tickets})		
	except Exception as e:
		print('e',e)


############ Edit particular ticket ###################
@csrf_exempt
def edit_particular_ticket(request):
	try:
		owner_first_name=request.POST['owner_first_name']
		owner_last_name=request.POST['owner_last_name']
		owner_email_address=request.POST['owner_email_address']
		transaction_id=request.POST['transaction_id']
		ticketbooked_object = TicketBooked.objects.get(transaction_id=transaction_id)
		user_id = ticketbooked_object.user_id
		User.objects.filter(pk=user_id).update(first_name=owner_first_name,last_name=owner_last_name)
		return HttpResponse('success')
	except Exception as e:
		print('e',e)

############ Delete particular ticket ###################
@csrf_exempt
def delete_particular_ticket(request):
	try:
		transaction_id=request.POST['transaction_id']
		ticket = TicketBooked.objects.get(transaction_id=transaction_id)
		ticket.delete()
		return HttpResponse('success')
	except Exception as e:
		print e

################### Update Event ######################

@csrf_exempt
def update_event_data(request):
	try:
		if (request.method=="POST"):
			event_id=request.POST['event_id']
			event_name=request.POST['event_name']
			description=request.POST['event_description']
			college_name=request.POST['college_name']
			country=request.POST['country_name']
			city=request.POST['city_name']
			state = request.POST['state']
			organizer_name=request.POST['organizer_name']
			organizer_description=request.POST['organizer_description']
			category=request.POST['category']			
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			event_start_datetime=request.POST['event_start_datetime']			
			event_end_datetime=request.POST['event_end_datetime']			
			ticket_name=request.POST['ticket_name']
			sale_channel=request.POST['sale_channel']
			ticket_count=request.POST['ticket_count']
			price = request.POST['price']
			event_type=(str(request.POST['event_type'])).lower()
			ticket_description = request.POST['ticket_description']
			sale_channel = request.POST['sale_channel']
			# event_image=request.POST['events_image']
			ticket_start_datetime=request.POST['ticket_start_datetime']
			ticket_end_datetime=request.POST['ticket_end_datetime']
			ticket_order_min=request.POST['ticket_order_min']
			ticket_order_max=request.POST['ticket_order_max']
			category =Category.objects.get(category_name=Category.objects.get_or_create(category_name=category)[0])
			location =Location.objects.get(city=Location.objects.get_or_create(city=city,state=state,country=country)[0])			
 			################# Update Event Object #################
			Event.objects.filter(id=event_id).update(event_name=event_name,description=description,
				college_name=college_name,location=location,start_datetime=event_start_datetime,
				end_datetime=event_end_datetime,organizer_name=organizer_name,
				organizer_description=organizer_description,category=category)				
			################# Update EventTickets Object #################
			EventTickets.objects.filter(event_id=event_id).filter(sub_event_id=None).update(
				ticket_name=ticket_name,ticket_type=event_type,sale_channel=sale_channel,	
				order_min=ticket_order_min,order_max=ticket_order_max,ticket_description=ticket_description,
				ticket_count=ticket_count,price=price,start_datetime=ticket_start_datetime,
				end_datetime=ticket_end_datetime)				
	    	return HttpResponse('success')
    	except Exception as e:
			print e

############################delete event image############################
@csrf_exempt
def delete_event_image(request):
	try:
		event_id=request.POST['event_id']
		image_id=request.POST['image_id']
		eventbanner_object=EventBanner.objects.get(id=image_id,event_id=event_id)
		if eventbanner_object:
			eventbanner_object.delete()
		return HttpResponse('success')
	except Exception as e:
		print(e)

############################delete event image############################
@csrf_exempt
def delete_subevent_image(request):
	try:
		sub_event_id=request.POST['subevent_id']
		image_id=request.POST['image_id']
		subeventbanner_object=SubEventBanner.objects.get(id=image_id,sub_event_id=sub_event_id)
		if subeventbanner_object:
			subeventbanner_object.delete()
		return HttpResponse('success')
	except Exception as e:
		print(e)		

#####################Manage ticketgraph#########################


@csrf_exempt
def manage_ticketgraph(request):
	event_id=request.POST['event_id']
	time_threshold = timezone.now() - timedelta(days=30)
	today=timezone.now()
	timestamp_ticket=[]
	quantity_booked1=[]
	eventsticket_object=EventTickets.objects.get(event_id=event_id,sub_event_id=None)
	eventsticket_id= eventsticket_object.id
	ticket_info=TicketBooked.objects.filter(ticket_id=eventsticket_id).filter(timestamp__range=[time_threshold, today])
	tickets_data_list=[]
	tickets_data_dict={'timestamp':'','quantity':''}
	if ticket_info:
		for info in ticket_info:
			ticket_id=info.transaction_id
			timestamp_end=info.timestamp
			timestamp_ticket.append(timestamp_end)
			time_value=timestamp_end.strftime('%Y-%m-%d')
			quantity_booked=int(info.quantity_booked)
			quantity_booked1.append(quantity_booked)
			tickets_data_dict['timestamp']=time_value
			tickets_data_dict['quantity']=quantity_booked
			tickets_data_list.append(tickets_data_dict.copy())
	else:
		tickets_data_list = []	
	return JsonResponse({'tickets_data':tickets_data_list})

###################### Event Publish ######################

@csrf_exempt
def event_publish(request):
	try:
		event_id=request.POST['event_id']
		Event.objects.filter(id=event_id).update(publish_event='1')
		return HttpResponse('success')
	except Exception as e:
		print e


#####################Event Unpublish#######################

@csrf_exempt
def event_unpublish(request):
	try:
		event_id=request.POST['event_id']
		Event.objects.filter(id=event_id).update(publish_event='0')
		return HttpResponse('success')
	except Exception as e:
		print e

#####################complete registration########################

@csrf_exempt
def complete_registration(request):
	try:
		event_id=request.POST['event_id']
		SubEvent.objects.filter(event_id=event_id).update(booking_check=False)
		return HttpResponse('success')
	except Exception as e:
		print e


############## save Order function ####################
@csrf_exempt
def save_order(request):
	try:
		try:
			logged_in_user1=request.user
			logged_in_user=str(logged_in_user)
			logged_in_user_id = request.user.id
		except Exception as e:
			logged_in_user=''
		ticket_id=request.POST['ticket_id']
		event_id=request.POST['event_id']
		sub_event_id1=request.POST['sub_event_id']
		if sub_event_id1 is '0':
			sub_event_id='NULL'
		else:
			sub_event_id=sub_event_id1
		last_name=request.POST['last_name']
		first_name=request.POST['first_name']
		quantity=request.POST['quantity']
		ticket_name=request.POST['ticket_name']
		email_address=request.POST['input_emails']
		ticketbooked_data_list=[]
		transaction_id_list=[]
		ticketbooked_data_dict ={'transaction_id':'','booked_time':'','quantity_book_string':'','ticket_id':''}
		timestamp=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		if int(quantity) > 1:
			for count in range(int(quantity)):
				order_data=TicketBooked.objects.create(ticket_id=ticket_id,quantity_booked=1,
				timestamp=timestamp,user=request.user)
				transaction_id_list.append(order_data.transaction_id)
		else:
			order_data=TicketBooked.objects.create(ticket_id=ticket_id,quantity_booked=quantity,
				timestamp=timestamp,user=request.user)
			transaction_id_list.append(order_data.transaction_id)							
		User.objects.filter(pk=request.user.id).update(first_name=first_name,last_name=last_name)
		if (sub_event_id1 == '0') :
			events_info=Event.objects.filter(id=event_id)
			for info in events_info:
				event_id=str(info.id)
				city=str(info.location)
				location_object=Location.objects.get(city=city)
				country=location_object.country
				time=info.registered_datetime
				time_start=info.start_datetime
				time_end=info.end_datetime
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
				college_name=info.college_name
				event_name=info.event_name
				eventsticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)
				price=str(eventsticket_object.price)
				ticket_id = eventsticket_object.id
				event_type=eventsticket_object.ticket_type
				description=info.description
				organizer_name=info.organizer_name
				organizer_description=info.organizer_description
				college_name=info.college_name
				user_email=str(info.user)
				category_name=str(info.category)
				ticked_booked_info=TicketBooked.objects.filter(transaction_id__in=transaction_id_list)
				for data in ticked_booked_info:
					ticketbooked_data_dict['transaction_id']=data.transaction_id		
					ticketbooked_data_dict['booked_time']=data.timestamp.strftime('%Y-%m-%d %H:%M:%S')					
					ticketbooked_data_dict['quantity_book_string']=str(data.quantity_booked)
					ticketbooked_data_dict['ticket_id']=data.ticket_id
					ticketbooked_data_list.append(ticketbooked_data_dict.copy())
				transaction_id_list=[]		
				d = {}				
				guest_email=[i for i in email_address.strip('[]').split(',')]
				for x in range(0,len(guest_email)):										
					img_id=strgen.StringGenerator("[\d\w]{10}").render()+'-'+str(x)
					img = qrcode.make(img_id)
					#img_id=str(transaction_id)+'.'+str(x)
					filename=img_id+'.png'
					img.save("home/static/qr/"+filename)
					transaction_id=ticketbooked_data_list[x]['transaction_id']
					quantity_book_string=ticketbooked_data_list[x]['quantity_book_string']
					ticket_id=ticketbooked_data_list[x]['ticket_id']
					booked_time=ticketbooked_data_list[x]['booked_time']
					try:
						image_info=ImageQR.objects.create(event_id=event_id,image_path=filename,guest_email=guest_email[x],transaction_id_id=transaction_id)
						image_info.save()
					except Exception as e:
						print 'error during saving imageqr object in if part',e					
					strFrom = 'cct.jagsirsingh@gmail.com'
					strTo = guest_email[x]
					# Create the root message and fill in the from, to, and subject headers
					msgRoot = MIMEMultipart('related')
					msgRoot['Subject'] = 'test message'
					msgRoot['From'] = strFrom
					msgRoot['To'] = strTo
					msgRoot.preamble = 'This is a multi-part message in MIME format.'
					# Encapsulate the plain and HTML versions of the message body in an
					# 'alternative' part, so message agents can decide which they want to display.
					msgAlternative = MIMEMultipart('alternative')
					msgRoot.attach(msgAlternative)					
					d['msgText_%02d' % x] = MIMEText('<div><b><h2>Welcome : <i> Sir/Madam </i></h2></b></div> '+
										'<div style="width:50%; float:left;"> Recently your ticket is booked for <strong>'+ event_name +'</strong>'+
											'<div style="width:100%">'+
												'<div><h1>Ticket details :</h1></div>'+
												'<ul style="list-style:none;">'+
													'<li>Ticket Booked by : ' + first_name + ' ' + last_name + '</li>'
													'<li>Your order Id is : ' + str(transaction_id) +'</li>'+
													'<li>Ticket name : ' + ticket_name +'</li>'+
													'<li>Booked at : ' + booked_time +'</li>'+
													'<li>Price per ticket is : ' + price +'</li>'+
												'</ul>'+
											'</div>'+
											'<div style="width:100%">'+
												'<div><h1>Organizer :</h1></div>'+
												'<h4><b>' + event_name + '</b> event is organized by <b>' + organizer_name + '<b></h4>'+
												'<h4> Here is the organizer Description</h4>'+
												organizer_description +
											'</div>'+
											'<div style="width:100%">'+
												'<div><h1>Time and venue :</h1></div>'+
												'<ul style="list-style:none;">'+
													'<li>College : ' + college_name +'</li>'+
													'<li>City : ' + city +'</li>'+
													'<li>Country : ' + country +'</li>'+
													'<li>Event start at : ' + start_time +'</li>'+
													'<li>Event ends at : ' + end_time +'</li>'+
												'</ul>'+
											'</div>'+
										'</div>'+
										'<div style="width:50%; float:left;"><b> QRcode Image is: </b><img src="cid:image1"></div>'+
										'<div style="width:100%; float:left;"> If you have and query you can email the organizer at ' + user_email +'</div>', 'html')
					msgAlternative.attach(d['msgText_%02d' % x])
					# This example assumes the image is in the current directory
					fp = open(BASE_DIR+'/home/static/qr/'+filename, 'rb')
					msgImage = MIMEImage(fp.read())
					fp.close()
					# Define the image's ID as referenced above
					msgImage.add_header('Content-ID', '<image1>')
					msgRoot.attach(msgImage)
					# Send the email (this example assumes SMTP authentication is required)
					username = 'cct.jagsirsingh@gmail.com'
					password = '8Nu09#MR'
					server = smtplib.SMTP('smtp.gmail.com:587')
					server.ehlo()
					server.starttls()
					server.login(username,password)
					server.sendmail(strFrom, strTo, msgRoot.as_string())
					server.quit()
				################### Mail to event Organizer ##################################
				# Define these once; use them twice!
				strFrom = 'cct.jagsirsingh@gmail.com'
				strTo = user_email
				# Create the root message and fill in the from, to, and subject headers
				msgRoot = MIMEMultipart('related')
				msgRoot['Subject'] = 'Ticket booked for '+ event_name
				msgRoot['From'] = strFrom
				msgRoot['To'] = strTo
				msgRoot.preamble = 'This is a multi-part message in MIME format.'
				# Encapsulate the plain and HTML versions of the message body in an
				# 'alternative' part, so message agents can decide which they want to display.
				msgAlternative = MIMEMultipart('alternative')
				msgRoot.attach(msgAlternative)

				
				d['msgText_%02d' % x] = MIMEText('<div><b><h2>Hello : <i>' + user_email + '</i></h2></b></div> '+
									'<div style="width:50%; float:left;"> Recently' + quantity_book_string +' tickets are booked from <strong>'+ event_name +'</strong>'+
										'<div style="width:100%">'+
											'<div><h1>Booking details :</h1></div>'+
											'<ul style="list-style:none;">'+
												'<li>Order Id is : ' + str(transaction_id) +'</li>'+
												'<li>Ticket name : ' + ticket_name +'</li>'+
												'<li>Booked at : ' + booked_time +'</li>'+
												'<li>Price per ticket is : ' + price +'</li>'+
												'<li>Booked by : ' + first_name + ' ' + last_name +'</li>'+
												'<li>Attendee email : ' + logged_in_user +'</li>'+
											'</ul>'+
										'</div>', 'html')
				msgAlternative.attach(d['msgText_%02d' % x])
				# Send the email (this example assumes SMTP authentication is required)
				username = 'cct.jagsirsingh@gmail.com'
				password = '8Nu09#MR'
				server = smtplib.SMTP('smtp.gmail.com:587')
				server.ehlo()
				server.starttls()
				server.login(username,password)
				server.sendmail(strFrom, strTo, msgRoot.as_string())
				server.quit()
				print 'printing response'
				return HttpResponse('success')
		else :
			sub_events_info=SubEvent.objects.filter(id=sub_event_id)
			for info in sub_events_info:
				sub_event_id=str(info.id)
				user_email=str(info.user)
				event_id=str(info.event_id)
				time=info.registered_datetime
				time_start=info.start_datetime
				time_end=info.end_datetime
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
				sub_event_name=info.sub_event_name
				subeventticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=sub_event_id)
				price=str(subeventticket_object.price)
				subevent_ticket_id = subeventticket_object.id
				sub_event_type=subeventticket_object.ticket_type
				description=info.description
				organizer_name=info.organizer_name
				organizer_description=info.organizer_description
				ticked_booked_info=TicketBooked.objects.filter(transaction_id__in=transaction_id_list)
				for ticket in ticked_booked_info:
					ticketbooked_data_dict['transaction_id']=ticket.transaction_id		
					ticketbooked_data_dict['booked_time']=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')					
					ticketbooked_data_dict['quantity_book_string']=str(ticket.quantity_booked)
					ticketbooked_data_dict['ticket_id']=ticket.ticket_id
					ticketbooked_data_list.append(ticketbooked_data_dict.copy())
				transaction_id_list=[]	
				d = {}
				email_address=request.POST['input_emails']
				guest_email=[i for i in email_address.strip('[]').split(',')]
				for x in range(0,len(guest_email)):				
					img_id=strgen.StringGenerator("[\d\w]{10}").render()+'-'+str(x)
					img = qrcode.make(img_id)
					filename=img_id+'.png'
					transaction_id=ticketbooked_data_list[x]['transaction_id']
					quantity_book_string=ticketbooked_data_list[x]['quantity_book_string']
					ticket_id=ticketbooked_data_list[x]['ticket_id']
					booked_time=ticketbooked_data_list[x]['booked_time']
					try:
						image_info=ImageQR.objects.create(event_id=event_id,sub_event_id=sub_event_id,image_path=filename,guest_email=guest_email[x],transaction_id_id=transaction_id)
						image_info.save()
					except Exception as e:
						print 'error during saving imageqr object in else part',e	
					img.save("home/static/qr/"+filename)
					
					strFrom = 'cct.jagsirsingh@gmail.com'
					strTo = guest_email[x]
					# Create the root message and fill in the from, to, and subject headers
					msgRoot = MIMEMultipart('related')
					msgRoot['Subject'] = 'test message'
					msgRoot['From'] = strFrom
					msgRoot['To'] = strTo
					msgRoot.preamble = 'This is a multi-part message in MIME format.'
					# Encapsulate the plain and HTML versions of the message body in an
					# 'alternative' part, so message agents can decide which they want to display.
					msgAlternative = MIMEMultipart('alternative')
					msgRoot.attach(msgAlternative)

					d['msgText_%02d' % x] = MIMEText('<div><b><h2>Welcome : <i> Sir/Madam </i></h2></b></div> '+
										'<div style="width:50%; float:left;"> Recently your ticket is booked for <strong>'+ sub_event_name +'</strong>'+
											'<div style="width:100%">'+
												'<div><h1>Ticket details :</h1></div>'+
												'<ul style="list-style:none;">'+
													'<li>Ticket Booked by : ' + first_name + ' ' + last_name + '</li>'
													'<li>Your order Id is : ' + str(transaction_id) +'</li>'+
													'<li>Ticket name : ' + ticket_name +'</li>'+
													'<li>Booked at : ' + booked_time +'</li>'+
													'<li>Price per ticket is : ' + price +'</li>'+
												'</ul>'+
											'</div>'+
											'<div style="width:100%">'+
												'<div><h1>Organizer :</h1></div>'+
												'<h4><b>' + sub_event_name + '</b> event is organized by <b>' + organizer_name + '<b></h4>'+
												'<h4> Here is the organizer Description</h4>'+
												organizer_description +
											'</div>'+
											'<div style="width:100%">'+
												'<div><h1>Time and venue :</h1></div>'+
												'<ul style="list-style:none;">'+
													'<li>Event start at : ' + start_time +'</li>'+
													'<li>Event ends at : ' + end_time +'</li>'+
												'</ul>'+
											'</div>'+
										'</div>'+
										'<div style="width:50%; float:left;"><b> QRcode Image is: </b><img src="cid:image1"></div>'+
										'<div style="width:100%; float:left;"> If you have and query you can email the organizer at ' + user_email +'</div>', 'html')
					msgAlternative.attach(d['msgText_%02d' % x])
					# This example assumes the image is in the current directory
					fp = open(BASE_DIR+'/home/static/qr/'+filename, 'rb')
					msgImage = MIMEImage(fp.read())
					fp.close()
					# Define the image's ID as referenced above
					msgImage.add_header('Content-ID', '<image1>')
					msgRoot.attach(msgImage)
					# Send the email (this example assumes SMTP authentication is required)
					username = 'cct.jagsirsingh@gmail.com'
					password = '8Nu09#MR'
					server = smtplib.SMTP('smtp.gmail.com:587')
					server.ehlo()
					server.starttls()
					server.login(username,password)
					server.sendmail(strFrom, strTo, msgRoot.as_string())
					server.quit()
				################### Mail to event Organizer ##################################
				# Define these once; use them twice!
				strFrom = 'cct.jagsirsingh@gmail.com'
				strTo = user_email
				# Create the root message and fill in the from, to, and subject headers
				msgRoot = MIMEMultipart('related')
				msgRoot['Subject'] = 'Ticket booked for '+ sub_event_name
				msgRoot['From'] = strFrom
				msgRoot['To'] = strTo
				msgRoot.preamble = 'This is a multi-part message in MIME format.'
				# Encapsulate the plain and HTML versions of the message body in an
				# 'alternative' part, so message agents can decide which they want to display.
				msgAlternative = MIMEMultipart('alternative')
				msgRoot.attach(msgAlternative)

				
				d['msgText_%02d' % x] = MIMEText('<div><b><h2>Hello : <i>' + user_email + '</i></h2></b></div> '+
									'<div style="width:50%; float:left;"> Recently' + quantity_book_string +' tickets are booked from <strong>'+ sub_event_name +'</strong>'+
										'<div style="width:100%">'+
											'<div><h1>Booking details :</h1></div>'+
											'<ul style="list-style:none;">'+
												'<li>Order no is : ' + str(transaction_id) +'</li>'+
												'<li>Ticket name : ' + ticket_name +'</li>'+
												'<li>Booked at : ' + booked_time +'</li>'+
												'<li>Price per ticket is : ' + price +'</li>'+
												'<li>Booked by : ' + first_name + ' ' + last_name +'</li>'+
												'<li>Attendee email : ' + logged_in_user +'</li>'+
											'</ul>'+
										'</div>', 'html')
				msgAlternative.attach(d['msgText_%02d' % x])
				# Send the email (this example assumes SMTP authentication is required)
				'''username = 'cct.jagsirsingh@gmail.com'
				password = 'Admin@123#'
				server = smtplib.SMTP('smtp.gmail.com:587')
				server.ehlo()
				server.starttls()
				server.login(username,password)
				server.sendmail(strFrom, strTo, msgRoot.as_string())
				server.quit()'''
		print 'end of if else'		
		return HttpResponse('success')
	except Exception as e:
		print e

####################Sub event registration########################

@csrf_exempt
def subevent_registration(request):
	try:
		sub_event_id=request.POST['sub_event_id']
		event_id=request.POST['event_id']
		quantity_list=[]
		quantity_dict={'selected':'','sub_event_type':'','subevent_id_id':'','timestamp_end':'','order_min':'','order_max':'',
			'ticket_name':'','ticket_id':'','event_id':'','pending_tickets':''}
		sub_events_info=SubEvent.objects.filter(id=sub_event_id)
		for info in sub_events_info:
			sub_event_id=str(info.id)
			subeventticket_object = EventTickets.objects.get(sub_event_id=sub_event_id)
			sub_event_type=subeventticket_object.ticket_type
			end_datetime=subeventticket_object.end_datetime
			order_min=subeventticket_object.order_min
			order_max=subeventticket_object.order_max
			tic_time_close=end_datetime.strftime("%B %d, %Y, %I:%M %p")
			ticket_id=subeventticket_object.id
			ticket_name=subeventticket_object.ticket_name
			tic_count=subeventticket_object.ticket_count
			tic_count1=int(tic_count)
			quantity_booked=0
			sub_book_info=TicketBooked.objects.filter(ticket_id=ticket_id)
			for info in sub_book_info:
				transaction_id=info.transaction_id
				ticket_booked1=int(info.quantity_booked)
				quantity_booked=quantity_booked+ticket_booked1
			pending_tickets=tic_count1-quantity_booked
			quantity_dict['sub_event_type']=sub_event_type
			quantity_dict['subevent_id_id']=sub_event_id
			quantity_dict['event_id']=event_id
			quantity_dict['timestamp_end']=tic_time_close
			quantity_dict['order_min']=order_min
			quantity_dict['order_max']=order_max
			quantity_dict['ticket_name']=ticket_name
			quantity_dict['ticket_id']=ticket_id
			quantity_dict['pending_tickets']=pending_tickets
		quantity_list.append(quantity_dict.copy()) 
		html = render_to_string('dashboard/subevent_registration.html', {'quantity_list':quantity_list })
		return HttpResponse(html)
	except Exception as e:
		print e

############## sub Quantity selected ####################
@csrf_exempt
def sub_quantity_selected(request):
	try:
		qua_list=[]
		qua_dict={'selected':''}
		select_val=request.POST['select_val']
		qua_dict['selected']=select_val
		qua_list.append(qua_dict.copy()) 
		html = render_to_string('dashboard/sub_quantity_selected.html', {'qua_list':qua_list })
		return HttpResponse(html)
	except Exception as e:
		print e


############ sub register_ticket ###################
@csrf_exempt
def sub_register_ticket(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		sub_event_id=request.POST['sub_eventid']
		event_id='0'
		sub_ticket_id=request.POST['sub_ticket_id']
		sub_ticket_name=request.POST['sub_ticket_name']
		selected_quantity=request.POST['selected_quantity']
		selected_quantity1=int(selected_quantity)
		subevent_data_list=[]
		subevent_data_dict={'ticket_id':'', 'ticket_name':'', 'selected_quantity':'', 'sub_event_id':'',
		'time':'','sub_event_name':'','price':'','image':'','description':'',
		'sub_event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':'','event_id':''}

		#quantity_booked=0
		#events_info=Event.objects.filter(event_id=event_id)
		#for info in events_info:
		#	user_email=info.user_email
		sub_events_info=SubEvent.objects.filter(id=sub_event_id)
		if sub_events_info:
			for info in sub_events_info:
				sub_event_id=str(info.id)
				time=info.registered_datetime
				time_start=info.start_datetime
				time_end=info.end_datetime
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
				sub_event_name=info.sub_event_name
				subeventticket_object = EventTickets.objects.get(sub_event_id=sub_event_id)
				price=subeventticket_object.price
				sub_event_type=subeventticket_object.ticket_type
				description=info.description
				organizer_name=info.organizer_name
				organizer_description=info.organizer_description
				subevent_data_dict['sub_event_id']=sub_event_id
				subevent_data_dict['time']=time_register
				subevent_data_dict['sub_event_name']=sub_event_name
				subevent_data_dict['price']=price
				subevent_data_dict['image']=image_path_editor(SubEventBanner.objects.values_list('sub_event_banner', flat=True).filter(sub_event_id=sub_event_id))
				subevent_data_dict['description']=description
				subevent_data_dict['start_time']=start_time
				subevent_data_dict['end_time']=end_time
				subevent_data_dict['event_type']=sub_event_type
				subevent_data_dict['selected_quantity']=selected_quantity1
				subevent_data_dict['organizer_name']=organizer_name
				subevent_data_dict['organizer_description']=organizer_description
				subevent_data_dict['ticket_name']=sub_ticket_name
				subevent_data_dict['ticket_id']=sub_ticket_id
				subevent_data_dict['event_id']=event_id
				subevent_data_list.append(subevent_data_dict.copy())

			request.session['event_data_view']=subevent_data_list
			return HttpResponse('success')
		else:
			return HttpResponse('wrong')
	except Exception as e:
		print e


@csrf_exempt
def user_tickets_detail(request):
	try:
		try:
			logged_in_user1=request.user.id
			logged_in_user=str(logged_in_user1)
		except Exception as e:
			logged_in_user=''	
		event_data=[]
		event_data_dict={'event_id':'','city':'','country':'','event_type':'',
		'time':'','name':'','price':'','image':'','description':'','category':'',
		'transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'','quantity_book':'',
		 'booked_ticket_id':'','booked_ticket_name':''}
		subevent_data =[]
		subevent_data_dict={'sub_event_id':'','sub_event_type':'','time':'','sub_event_name':'','price':'',
		'image':'','description':'','transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'',
		'quantity_book':'','booked_ticket_id':'','booked_event_id':'','booked_ticket_name':''}				
		ticked_booked_info=TicketBooked.objects.filter(user=logged_in_user).order_by('-timestamp')	
		for ticket_info in ticked_booked_info:
			ticket_id = ticket_info.ticket_id
			####cindition for event #####
			eventticket_object = EventTickets.objects.get(id=ticket_id)
			if eventticket_object.sub_event_id is None:
				event_id = eventticket_object.event_id
				sub_event_id = eventticket_object.sub_event_id				
				transaction_id=ticket_info.transaction_id
				booked_time=ticket_info.timestamp.strftime("%B %d, %Y, %I:%M %p")
				user_object = User.objects.get(pk=request.user.id)
				first_name=user_object.first_name
				last_name=user_object.last_name
				qunatity_book=ticket_info.quantity_booked
				ticket_name=eventticket_object.ticket_name
				ticket_id = ticket_id
				event_data_dict['transaction_id']=transaction_id
				event_data_dict['booked_time']=booked_time
				event_data_dict['first_name']=first_name
				event_data_dict['last_name']=last_name
				event_data_dict['quantity_book']=qunatity_book
				event_data_dict['booked_ticket_id']=ticket_id
				event_data_dict['booked_event_id']=event_id
				event_data_dict['booked_ticket_name']=ticket_name
				event_info=Event.objects.filter(id=event_id)
				for info in event_info:
					event_id=info.id
					city=info.location
					location_object = Location.objects.get(city=city)
					country=location_object.country
					time=info.registered_datetime
					time_register=time.strftime("%B %d, %Y, %I:%M %p")
					start_time=info.start_datetime.strftime("%B %d, %Y, %I:%M %p")
					end_time=info.end_datetime.strftime("%B %d, %Y, %I:%M %p")
					event_name=info.event_name
					price=eventticket_object.price
					event_type=eventticket_object.ticket_type
					description=info.description
					category_name=info.category	
					event_data_dict['event_id']=event_id
					event_data_dict['city']=city
					event_data_dict['country']=country
					event_data_dict['time']=time_register
					event_data_dict['name']=event_name
					event_data_dict['price']=price
					event_data_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
					event_data_dict['description']=description
					event_data_dict['category']=category_name
					event_data_dict['event_type']=event_type
					event_data_dict['start_time']=start_time
					event_data_dict['end_time']=end_time
					event_data_dict['event_name']=event_name
					event_data.append(event_data_dict.copy())
					event_data=[i for n, i in enumerate(event_data) if i not in event_data[n + 1:]]				
			else:
				event_id = eventticket_object.event_id
				sub_event_id = eventticket_object.sub_event_id
				transaction_id=ticket_info.transaction_id
				booked_time=ticket_info.timestamp.strftime('%Y-%m-%d %H:%M:%S')
				user_object = User.objects.get(pk=request.user.id)
				first_name=user_object.first_name
				last_name=user_object.last_name
				qunatity_book=ticket_info.quantity_booked
				ticket_name=eventticket_object.ticket_name
				ticket_id = ticket_id
				subevent_data_dict['transaction_id']=transaction_id
				subevent_data_dict['booked_time']=booked_time
				subevent_data_dict['first_name']=first_name
				subevent_data_dict['last_name']=last_name
				subevent_data_dict['quantity_book']=qunatity_book
				subevent_data_dict['booked_ticket_id']=ticket_id
				subevent_data_dict['booked_event_id']=event_id
				subevent_data_dict['booked_ticket_name']=ticket_name
				# booked_ticket_view_list.append(booket_ticket_view_dict.copy())
				event_info=SubEvent.objects.filter(id=sub_event_id).order_by('-start_datetime')
				for info in event_info:
					sub_event_id=str(info.id)
					time=info.registered_datetime
					time_register=time.strftime("%B %d, %Y, %I:%M %p")
					start_time=info.start_datetime.strftime("%B %d, %Y, %I:%M %p")
					end_time=info.end_datetime.strftime("%B %d, %Y, %I:%M %p")
					sub_event_name=info.sub_event_name
					price=eventticket_object.price
					description=info.description
					sub_event_type=eventticket_object.ticket_type					
					subevent_data_dict['sub_event_id']=sub_event_id
					subevent_data_dict['time']=time_register
					subevent_data_dict['sub_event_name']=sub_event_name
					subevent_data_dict['price']=price
					subevent_data_dict['image']=image_path_editor(SubEventBanner.objects.values_list('sub_event_banner', flat=True).filter(sub_event_id=sub_event_id))
					subevent_data_dict['description']=description
					subevent_data_dict['description']=description
					subevent_data_dict['sub_event_type']=sub_event_type
					subevent_data_dict['start_time']=start_time
					subevent_data_dict['end_time']=end_time
					subevent_data.append(subevent_data_dict.copy())
					subevent_data=[i for n, i in enumerate(subevent_data) if i not in subevent_data[n + 1:]] 
		ticket_info=TicketBooked.objects.filter(user=logged_in_user)
		total_tickets=len(ticket_info)
		request.session['total_tickets']=total_tickets
		return render(request,'dashboard/user_tickets_detail.html',{'total_tickets':total_tickets, 'subevent_data':subevent_data,'event_data':event_data,
			'logged_in_user':logged_in_user})
	except Exception as e:
		print e



############ Edit particular ticket ###################
@csrf_exempt
def sub_edit_particular_ticket(request):
	try:
		owner_first_name=request.POST['owner_first_name']
		owner_last_name=request.POST['owner_last_name']
		owner_email_address=request.POST['owner_email_address']
		transaction_id=request.POST['transaction_id']
		SubTicketBooked.objects.filter(transaction_id=transaction_id).update(first_name=owner_first_name,last_name=owner_last_name,
				email_address=owner_email_address)
		return HttpResponse('success')
	except Exception as e:
		print('e',e)


############ Delete particular ticket ###################
@csrf_exempt
def sub_delete_particular_ticket(request):
	try:
		transaction_id=request.POST['transaction_id']
		ticket = SubTicketBooked.objects.get(transaction_id=transaction_id)
		ticket.delete()
		return HttpResponse('success')
	except Exception as e:
		print e


#################To get Qr image####################
@csrf_exempt
def view_qr_image(request):
	try:
		booked_event_data_list = []
		booked_subevent_data_list =[]
		booked_events_data_dict={'transaction_id':'','quantity_booked':'','first_name':'','last_name':'',
		'ticket_name':'','ticket_type':'','ticket_price':'','ticket_description':'','ticket_start_datetime':'',
		'ticket_end_datetime':'','event_name':'','event_description':'','college_name':'','city':'',
		'state':'','country':'','event_start_datetime':'','event_end_datetime':'','event_organizer_name':'',
		'event_organizer_description':'','event_image':'','imageqr_id':'','imageqr_path':'','imageqr_guest_email':'',
		'user_email':''
		}
		booked_subevent_data_dict={'transaction_id':'','quantity_booked':'','first_name':'','last_name':'',
		'ticket_name':'','ticket_type':'','ticket_price':'','ticket_description':'','ticket_start_datetime':'',
		'ticket_end_datetime':'','event_name':'','event_description':'','college_name':'','city':'',
		'state':'','country':'','event_start_datetime':'','event_end_datetime':'','event_organizer_name':'',
		'event_organizer_description':'','event_image':'','subevent_event_id':'','subevent_venue':'',
		'subevent_start_datetime':'','subevent_end_datetime':'','sub_event_name':'','subevent_organizer_name':'',
		'subevent_organizer_description':'','subevent_image':'','subevent_description':'',
		'imageqr_id':'','imageqr_path':'','imageqr_guest_email':'','user_email':''}
		transaction_id=request.POST['transaction_id']
		event_id=request.POST['event_id']
		subevent_id=request.POST['sub_event_id']						
		if(int(event_id) !=0):  #####if event ticket then need only event data #######
			####bookedticket_data########
			booked_ticket_info=TicketBooked.objects.get(transaction_id=transaction_id)
			booked_events_data_dict['quantity_booked']=booked_ticket_info.quantity_booked
			booked_events_data_dict['booked_time']=booked_ticket_info.timestamp.strftime('%B %d, %Y, %I:%M %p')
			ticketbooked_user_id=booked_ticket_info.user_id
			user_object = User.objects.get(id=ticketbooked_user_id)
			booked_events_data_dict['first_name'] = user_object.first_name
			booked_events_data_dict['last_name'] = user_object.last_name
			booked_events_data_dict['user_email'] = user_object.username
			#########ticket_details########
			eventsticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)
			booked_events_data_dict['ticket_name']=eventsticket_object.ticket_name
			booked_events_data_dict['ticket_type']=eventsticket_object.ticket_type
			booked_events_data_dict['ticket_price']=eventsticket_object.price
			booked_events_data_dict['ticket_description']=eventsticket_object.ticket_description
			booked_events_data_dict['ticket_start_datetime']=eventsticket_object.start_datetime.strftime("%B %d, %Y, %I:%M %p")
			booked_events_data_dict['ticket_end_datetime']=eventsticket_object.end_datetime.strftime("%B %d, %Y, %I:%M %p")
			#########event_details#########
			event_info=Event.objects.get(id=event_id)
			booked_events_data_dict['event_name']=event_info.event_name
			booked_events_data_dict['event_description']=event_info.description
			booked_events_data_dict['college_name']=event_info.college_name
			city=str(event_info.location)
			booked_events_data_dict['city']=city					
			location_object = Location.objects.get(city=city)
			booked_events_data_dict['state']=location_object.state
			booked_events_data_dict['country']=location_object.country
			booked_events_data_dict['event_start_datetime']=event_info.start_datetime.strftime("%B %d, %Y, %I:%M %p")
			booked_events_data_dict['event_end_datetime']=event_info.end_datetime.strftime("%B %d, %Y, %I:%M %p")
			booked_events_data_dict['event_organizer_name']=event_info.organizer_name
			booked_events_data_dict['event_organizer_description']=event_info.organizer_description
			booked_events_data_dict['event_image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
			############qr_data###########
			qr_object=ImageQR.objects.get(event_id=event_id,transaction_id=transaction_id)
			booked_events_data_dict['imageqr_id']=qr_object.id
			booked_events_data_dict['imageqr_path']=qr_object.image_path
			booked_events_data_dict['imageqr_guest_email']=qr_object.guest_email
			booked_events_data_dict['transaction_id']=transaction_id
			booked_event_data_list.append(booked_events_data_dict.copy())
		else: #####if subevent ticket then need  event and subevent data both #######
			####bookedticket_data########
			booked_ticket_info=TicketBooked.objects.get(transaction_id=transaction_id)
			booked_subevent_data_dict['quantity_booked']=booked_ticket_info.quantity_booked
			booked_subevent_data_dict['booked_time']=booked_ticket_info.timestamp.strftime('%B %d, %Y, %I:%M %p')
			ticketbooked_user_id=booked_ticket_info.user_id
			user_object = User.objects.get(id=ticketbooked_user_id)
			booked_subevent_data_dict['first_name'] = user_object.first_name
			booked_subevent_data_dict['last_name'] = user_object.last_name
			booked_subevent_data_dict['user_email'] = user_object.username

			##########ticket_data########
			subeventticket_object = EventTickets.objects.get(sub_event_id=subevent_id)
			booked_subevent_data_dict['ticket_name']=subeventticket_object.ticket_name
			booked_subevent_data_dict['ticket_type']=subeventticket_object.ticket_type
			booked_subevent_data_dict['ticket_price']=subeventticket_object.price
			booked_subevent_data_dict['ticket_description']=subeventticket_object.ticket_description
			booked_subevent_data_dict['ticket_start_datetime']=subeventticket_object.start_datetime.strftime("%B %d, %Y, %I:%M %p")
			booked_subevent_data_dict['ticket_end_datetime']=subeventticket_object.end_datetime.strftime("%B %d, %Y, %I:%M %p")
			subevent_event_id=subeventticket_object.event_id
			booked_subevent_data_dict['subevent_event_id']=subevent_event_id					
			######event_data#######
			event_info=Event.objects.get(id=subevent_event_id)
			event_id=event_info.id
			booked_subevent_data_dict['event_name']=event_info.event_name
			booked_subevent_data_dict['event_description']=event_info.description
			booked_subevent_data_dict['college_name']=event_info.college_name
			city=str(event_info.location)
			booked_subevent_data_dict['city']=city
			location_object = Location.objects.get(city=city)
			booked_subevent_data_dict['state']=location_object.state
			booked_subevent_data_dict['country']=location_object.country
			booked_subevent_data_dict['event_start_datetime']=event_info.start_datetime.strftime("%B %d, %Y, %I:%M %p")
			booked_subevent_data_dict['event_end_datetime']=event_info.end_datetime.strftime("%B %d, %Y, %I:%M %p")
			booked_subevent_data_dict['event_organizer_name']=event_info.organizer_name
			booked_subevent_data_dict['event_organizer_description']=event_info.organizer_description
			booked_subevent_data_dict['event_image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
			######subevent_data#####
			subevent_info=SubEvent.objects.get(id=subevent_id)
			booked_subevent_data_dict['subevent_venue']=subevent_info.venue
			booked_subevent_data_dict['subevent_start_datetime']=subevent_info.start_datetime.strftime("%B %d, %Y, %I:%M %p")
			booked_subevent_data_dict['subevent_end_datetime']=subevent_info.end_datetime.strftime("%B %d, %Y, %I:%M %p")
			booked_subevent_data_dict['subevent_name']=subevent_info.sub_event_name
			booked_subevent_data_dict['subevent_organizer_name']=subevent_info.organizer_name
			booked_subevent_data_dict['subevent_organizer_description']=subevent_info.organizer_description
			booked_subevent_data_dict['subevent_description']=subevent_info.description
			booked_subevent_data_dict['subevent_image']=image_path_editor(SubEventBanner.objects.values_list('sub_event_banner', flat=True).filter(sub_event_id=subevent_id))
			##########qr_data#####
			qr_object=ImageQR.objects.get(event_id=event_id,sub_event_id=subevent_id,transaction_id=transaction_id)
			booked_subevent_data_dict['imageqr_id']=qr_object.id	
			booked_subevent_data_dict['imageqr_path']=qr_object.image_path
			booked_subevent_data_dict['imageqr_guest_email']=qr_object.guest_email
			booked_subevent_data_dict['transaction_id']=transaction_id
			booked_subevent_data_list.append(booked_subevent_data_dict.copy())
		request.session['booked_event_data_list']=booked_event_data_list
		request.session['booked_subevent_data_list']=booked_subevent_data_list										
		return HttpResponse('success')
	except Exception as e:
		print e				
##########################Saved Events############################


@csrf_exempt
def saved_events(request):
	try:
		event_id=request.POST['event_id']
		user_name=request.user.email
		saved_flag=int(request.POST['saved_flag'])
		users_info=SavedEvent.objects.all()
		user_info=len(users_info)
		if user_info >= 1:
			save_info1=SavedEvent.objects.filter(event_id_id=event_id).filter(user_name=user_name)
			save_info=len(save_info1)
			if save_info >= 1:
				SavedEvent.objects.filter(event_id_id=event_id).filter(user_name=user_name).update(saved_flag=saved_flag)
			else:
				user=SavedEvent.objects.create(event_id_id=event_id,user_name=user_name,saved_flag=saved_flag)
				user.save()
			return HttpResponse('success')
		else:
			user=SavedEvent.objects.create(event_id_id=event_id,user_name=user_name,saved_flag=saved_flag)
			user.save()
			return HttpResponse('success')
	except Exception as e:
		print('error',e)

########################## Update User Information ############################


@csrf_exempt
def update_user_info(request):
	try:
		user=request.user
		user_id=request.user.id
		prefix=request.POST['prefix']
		first_name=request.POST['first_name']
		last_name=request.POST['last_name']
		suffix=request.POST['suffix']
		home_phone=request.POST['home_phone']
		cell_phone=request.POST['cell_phone']
		job_title=request.POST['job_title']
		company=request.POST['company']
		website=request.POST['website']
		blog=request.POST['blog']
		address=request.POST['address']
		country=request.POST['countryId']
		state=request.POST['stateId']
		city=request.POST['cityId']
		a_zip=request.POST['zip']
		b_address=request.POST['b_address']
		country1=request.POST['countryId1']
		state1=request.POST['stateId1']
		city1=request.POST['cityId1']
		b_zip=request.POST['b_zip']
		s_address=request.POST['s_address']
		country2=request.POST['countryId2']
		state2=request.POST['stateId2']
		city2=request.POST['cityId2']
		s_zip=request.POST['s_zip']
		w_address=request.POST['w_address']
		country3=request.POST['countryId3']
		state3=request.POST['stateId3']
		city3=request.POST['cityId3']
		w_postal=request.POST['w_postal']
		birth_month=request.POST['birth_month']
		birth_day=request.POST['birth_day']
		birth_year=request.POST['birth_year']
		dob=birth_year + '-' + birth_month + '-' + birth_day
		gender=request.POST['gender']
		age=request.POST['age']
		profile_pic=request.POST['profile_pic']
		timestamp_now=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		User.objects.filter(email=user).update(first_name=first_name,last_name=last_name)
		account_info=userAccount.objects.filter(user_id=user_id)
		print 'account_info>>>>>>',account_info
		if account_info:
			userAccount.objects.update(prefix=prefix,sufix=suffix,home_phone=home_phone,
										cell_phone=cell_phone,job_title=job_title,company=company,
										website=website,blog=blog,home_address=address,
										home_country=country,home_state=state,home_city=city,
										home_zip_code=a_zip,billing_address=b_address,billing_country=country1,
										billing_state=state1,billing_city=city1,billing_zip_code=b_zip,
										shipping_address=s_address,shipping_country=country2,shipping_state=state2,
										shipping_city=city2,shipping_zip_code=s_zip,work_address=w_address,
										work_country=country3,work_state=state3,work_city=city3,
										work_zip_code=w_postal,gender=gender,age=age,dob=dob,timestamp=timestamp_now,
										profile_pic=profile_pic)
		else:
			user = userAccount.objects.create(user_id_id=user_id,prefix=prefix,sufix=suffix,home_phone=home_phone,
										cell_phone=cell_phone,job_title=job_title,company=company,
										website=website,blog=blog,home_address=address,
										home_country=country,home_state=state,home_city=city,
										home_zip_code=a_zip,billing_address=b_address,billing_country=country1,
										billing_state=state1,billing_city=city1,billing_zip_code=b_zip,
										shipping_address=s_address,shipping_country=country2,shipping_state=state2,
										shipping_city=city2,shipping_zip_code=s_zip,work_address=w_address,
										work_country=country3,work_state=state3,work_city=city3,
										work_zip_code=w_postal,gender=gender,age=age,dob=dob,timestamp=timestamp,
										profile_pic=profile_pic)
			user.save()
		return HttpResponse('success')
	except Exception as e:
		print('error',e)





############################my method#############
@csrf_exempt
def payment_response(request):
	print 'request data',request.POST.body

#########PayPalPaymentsForm###########
@csrf_exempt
def view_that_asks_for_money(request):

    # What you want the button to do.
    paypal_dict = {
        "business": "shank.nsg@gmail.com",
        "amount": "1000.00",
        "item_name": "ticket for blood donation camp",
        "invoice": "BDB1",
        "notify_url": "http://127.0.0.1:8000/dashboard/payment-response/" + reverse('paypal-ipn'),
        "return_url": "http://127.0.0.1:8000/dashboard/payment-success/",
        "cancel_return": "http://127.0.0.1:8000/",
        "custom": "Upgrade all users!",  # Custom command to correlate to some function later (optional)
    } 
    form = PayPalPaymentsForm(initial=paypal_dict)  
    return render('dashboard/payment.html',context={'form':form})

    

@csrf_exempt
def paypal_payment_response(request):
	print 'request',request.GET.get('amount')

	return render(request,'dashboard/paypal_payment_response.html')



##################checkout for event ticket registration ########################
def checkout(request):
	try:
		logged_in_user	= request.user
		event_data_view=request.session['event_data_view']
		event_data_view
		for dictionary in event_data_view:
			price = int(dictionary['price'])
			quantity = int(dictionary['selected_quantity'])
			ticket_name = dictionary['ticket_name']
		total = price*quantity
		hash_object = hashlib.sha256(b'randint(0,20)')
		invoice=hash_object.hexdigest()[0:20]	
		paypal_dict = {
        "business": "shank.nsg@gmail.com",
        "amount": price,
        "quantity":quantity,
        "item_name": ticket_name,
        "invoice": invoice,
        "notify_url": "http://127.0.0.1:8000/dashboard/ipn-data/" + reverse('paypal-ipn'),
        "return_url": "http://127.0.0.1:8000/dashboard/payment-success/",
        "cancel_return": "http://127.0.0.1:8000/",
        "custom": "Upgrade all users!",  # Custom command to correlate to some function later (optional)
        }
		form = PayPalPaymentsForm(initial=paypal_dict)
		return render(request,'dashboard/checkout.html',{'event_data_view':event_data_view,'logged_in_user':logged_in_user,'form':form,'total':total})
	except Exception as e:
		print e
def make_payment(request):

	posted={}
	#######compulsary fields to send#########
	MERCHANT_KEY = "Tb2xvOEE"
	key="Tb2xvOEE"
	hash_object = hashlib.sha256(b'randint(0,20)')
	txnid=hash_object.hexdigest()[0:20]
	amount = str(request.POST.get('amount'))
	firstname = str(request.POST.get('firstname'))
	email = str(request.POST.get('email'))
	productinfo = str(request.POST.get('productinfo'))
	phone = str(request.POST.get('phone'))	
	SALT = "h9K3zm1iPj"
	url = "https://test.payu.in/_payment"
	surl="http://127.0.0.1:8000/dashboard/success/"
	furl="http://127.0.0.1:8000/dashboard/failure/"
	checksum=key+"|"+txnid+"|"+amount+"|"+productinfo+"|"+firstname+"|"+email+"|"+SALT
	checksum=hashlib.sha512(checksum).hexdigest().lower()
	posted['key']=key
	posted['txnid']=txnid
	posted['amount']=amount
	posted['firstname']=firstname
	posted['email']=email
	posted['productinfo']=productinfo
	posted['phone']=phone
	posted['surl']=surl
	posted['furl']=furl
	posted['Checksum ']=checksum
	posted['service_provider']='payu_paisa'
	headers = {'content-type': 'application/json'}
	resp = requests.get(url, params=posted, headers=headers)

	print resp.content




def myhome(request):
	MERCHANT_KEY ="Tb2xvOEE"
	key="Tb2xvOEE"
	SALT = "h9K3zm1iPj"
	PAYU_BASE_URL = "https://test.payu.in/_payment"
	surl="http://127.0.0.1:8000/dashboard/success/"
	furl="http://127.0.0.1:8000/dashboard/failure/"
	curl="http://127.0.0.1:8000/dashboard/myhome/"
	amount="325"
	product_info="ticket for blood donation camp"
	action = ''
	posted={}
	for i in request.POST:
		posted[i]=request.POST[i]
	hash_object = hashlib.sha256(b'randint(0,20)')
	txnid=hash_object.hexdigest()[0:20]
	hashh = ''
	posted['txnid']=txnid
	hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
	posted['key']=key
	posted['furl']=furl
	posted['surl']=surl
	posted['curl']=curl
	hash_string=''
	hashVarsSeq=hashSequence.split('|')
	for i in hashVarsSeq:
		try:
			hash_string+=str(posted[i])
		except Exception:
			hash_string+=''
		hash_string+='|'
	hash_string+=SALT
	hashh=hashlib.sha512(hash_string).hexdigest().lower()
	action =PAYU_BASE_URL
	if(posted.get("key")!=None and posted.get("txnid")!=None and posted.get("productinfo")!=None and posted.get("firstname")!=None and posted.get("email")!=None):
		return render(request,'dashboard/current_datetime.html',context={"posted":posted,"hashh":hashh,"surl":surl,"furl":furl,'curl':curl,'amount':amount,'product_info':product_info,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":"https://test.payu.in/_payment" })
	else:
		return render(request,'dashboard/current_datetime.html',context={"posted":posted,"hashh":hashh,"surl":surl,"furl":furl,'curl':curl,'amount':amount,'product_info':product_info,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":"." })

@csrf_protect
@csrf_exempt
def success(request):
	c = {}
    	c.update(csrf(request))
	status=request.POST["status"]
	firstname=request.POST["firstname"]
	amount=request.POST["amount"]
	txnid=request.POST["txnid"]
	posted_hash=request.POST["hash"]
	key=request.POST["key"]
	productinfo=request.POST["productinfo"]
	email=request.POST["email"]
	salt="h9K3zm1iPj"
	try:
		additionalCharges=request.POST["additionalCharges"]
		retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	except Exception:
		retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
	if(hashh !=posted_hash):
		print "Invalid Transaction. Please try again"
	else:
		print "Thank You. Your order status is ", status
		print "Your Transaction ID for this transaction is ",txnid
		print "We have received a payment of Rs. ", amount ,". Your order will soon be shipped."
	return render_to_response('dashboard/sucess.html',RequestContext(request,{"txnid":txnid,"status":status,"amount":amount}))


@csrf_protect
@csrf_exempt
def failure(request):
	c = {}
    	c.update(csrf(request))
	status=request.POST["status"]
	firstname=request.POST["firstname"]
	amount=request.POST["amount"]
	txnid=request.POST["txnid"]
	posted_hash=request.POST["hash"]
	key=request.POST["key"]
	productinfo=request.POST["productinfo"]
	email=request.POST["email"]
	salt="h9K3zm1iPj"
	try:
		additionalCharges=request.POST["additionalCharges"]
		retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	except Exception:
		retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
	if(hashh !=posted_hash):
		print "Invalid Transaction. Please try again"
	else:
		print "Thank You. Your order status is ", status
		print "Your Transaction ID for this transaction is ",txnid
		print "We have received a payment of Rs. ", amount ,". Your order will soon be shipped."
 	return render_to_response("dashboard/failure.html",RequestContext(request,c))


######################### paypal IPN ###########################
@require_POST
def ipn(request, item_check_callable=None):
    flag = None
    ipn_obj = None
    form = PayPalIPNForm(request.POST)
    if form.is_valid():
        try:
            ipn_obj = form.save(commit=False)
        except Exception, e:
            flag = "Exception while processing. (%s)" % e
    else:
        flag = "Invalid form. (%s)" % form.errors

    if ipn_obj is None:
        ipn_obj = PayPalIPN()    

    ipn_obj.initialize(request)

    if flag is not None:
        ipn_obj.set_flag(flag)
    else:
        # Secrets should only be used over SSL.
        if request.is_secure() and 'secret' in request.GET:
            ipn_obj.verify_secret(form, request.GET['secret'])
        else:
            ipn_obj.verify(item_check_callable)

    ipn_obj.save()
    return HttpResponse("OKAY")


	