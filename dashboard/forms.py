from django import forms
from paypal.standard.forms import PayPalStandardBaseForm 
from paypal.standard.ipn.models import PayPalIPN


class FileFieldForm(forms.Form):
    subevent_iamges = forms.FileField(label='sub event images',widget=forms.ClearableFileInput(attrs={'multiple': True}))


class PayPalIPNForm(PayPalStandardBaseForm):
    """
    Form used to receive and record PayPal IPN notifications.
    
    PayPal IPN test tool:
    https://developer.paypal.com/us/cgi-bin/devscr?cmd=_tools-session
    """
    class Meta:
        model = PayPalIPN    

  