from django.conf.urls import url
from django.contrib import admin
from . import views

#from views import google_get_state_token, google_login, google_logout

urlpatterns = [
     url(r'^register/$',views.register,name='register'),
     url(r'^dashboard/$',views.dashboard,name='dashboard'),
     url(r'^login/$',views.login,name='login'),
     url(r'^logout/$',views.logout,name='logout'),
     
]
