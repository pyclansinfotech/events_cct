from django.shortcuts import redirect
#from json_response import JsonResponse
#from models import ActionState

###########################################################
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib

#import gdata.contacts.service
from django.http import JsonResponse
from django.utils import timezone
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
#from .google_contacts.utils import google_get_state, google_import

from django.shortcuts import render
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from dashboard.models import Category,Event,EventTickets,SubEvent,TicketBooked,ImageQR,UserAccount,EventBanner,SubEventBanner,Location
from  datetime import timedelta
import time
import datetime
from django.template.loader import render_to_string
from datetime import datetime
from dateutil.relativedelta import relativedelta
import base64 
from django.db.models import Q
import operator
import os
import datetime
import time
import json
import qrcode
#import pdfcrowd

from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User
from django.contrib.auth import logout as auth_logout
from home.utils import image_path_editor
#import ho.pisa as pisa
#import cStringIO as StringIO
# Create your views here.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#############Account referral###################
@csrf_exempt
def referrals(request):
	logged_in_user=request.user
	return render(request,'myapp/referral.html',{'logged_in_user':logged_in_user})


#############Account referrals_attendees###################
@csrf_exempt
def referrals_attendees(request):
	logged_in_user=request.user
	return render(request,'myapp/referrals_attendees.html',{'logged_in_user':logged_in_user})


############Upload#########################################
@csrf_exempt
def upload(request):
    """
    Find the eid in the request.session if you can't find it then propably it's
    not saved this event so your job to find that one because you are the first
    one saving into database
    """
    logged_in_user=request.user.email
    image_id = request.session.get('image_id')
    if request.method == 'POST':
        event_obj, created = EventBanner.objects.get_or_create(image_id=image_id)
        event_obj.event_banner = request.FILES.get('file')
        image1=request.FILES.get('file')
        event_obj.user_email=logged_in_user
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        timestamp=st
        event_obj.timestamp=timestamp
        event_obj.save()
    return HttpResponse('saved')
    #return HttpResponseRedirect(reverse('events:edit'))

 ################Sub event upload######################

 ############Upload#########################################
image_list=[]
@csrf_exempt
def sub_event_upload(request):
    """
    Find the eid in the request.session if you can't find it then propably it's
    not saved this event so your job to find that one because you are the first
    one saving into database
    """
    logged_in_user=request.user.email
    image_id = request.session.get('image_id')
    if request.method == 'POST':
        sub_event_obj, created = SubEventBanner.objects.get_or_create(image_id=image_id)
        sub_event_obj.sub_event_banner = request.FILES.get('file')
        image_list1=request.FILES.get('file')
        image_list.append(image_list1)
        sub_event_obj.user_email=logged_in_user
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        timestamp=st
        sub_event_obj.timestamp=timestamp
        sub_event_obj.save()
    return HttpResponse('saved')
    #return HttpResponseRedirect(reverse('events:edit'))

#############Account Change Password###################
@csrf_exempt
def password(request):
	logged_in_user=request.user
	if request.method == 'POST':
		oldpassword=request.POST['oldpassword']
		newpassword1=request.POST['newpassword1']
		newpassword2=request.POST['newpassword2']
		converted_new=make_password(newpassword1)	
		loggedin_user=User.objects.filter(email=logged_in_user)
		for user in loggedin_user:
			user_password=user.password
			check=check_password(oldpassword,user_password)
		if check==True:
			for u in loggedin_user:
				if u.is_active:
					u = User.objects.get(email=logged_in_user)
					u.set_password(newpassword1)
					u.save()
					auth_login(request, u)
					return HttpResponse('authentic')
				else:
					return HttpResponse('non-authentic')
		else:
			return HttpResponse('non-authentic')
	else:
		return render(request,'myapp/change_password.html',{'logged_in_user':logged_in_user})


#############Account Social Settings###################
@csrf_exempt
def social_settings(request):
	logged_in_user=request.user
	return render(request,'myapp/social_settings.html',{'logged_in_user':logged_in_user})

#############Account Credit Cards###################
@csrf_exempt
def creditcards(request):
	logged_in_user=request.user
	return render(request,'myapp/creditcards.html',{'logged_in_user':logged_in_user})

#############Account Email Preferences###################
@csrf_exempt
def email_preferences(request):
	logged_in_user=request.user
	return render(request,'myapp/email_preferences.html',{'logged_in_user':logged_in_user})

#############Account Venues Organizers###################
@csrf_exempt
def venues_organizers(request):
	logged_in_user=request.user
	return render(request,'myapp/venues_organizers.html',{'logged_in_user':logged_in_user})

#############Account Permissions###################
@csrf_exempt
def permissions(request):
	logged_in_user=request.user
	return render(request,'myapp/permissions.html',{'logged_in_user':logged_in_user})

#############Account Extensions###################
@csrf_exempt
def extensions(request):
	logged_in_user=request.user
	return render(request,'myapp/extensions.html',{'logged_in_user':logged_in_user})

#############Account Close Account###################
@csrf_exempt
def account_close(request):
	logged_in_user=request.user
	return render(request,'myapp/account_close.html',{'logged_in_user':logged_in_user})

#############Account Charges and Credits###################
@csrf_exempt
def charges(request):
	logged_in_user=request.user
	return render(request,'myapp/charges.html',{'logged_in_user':logged_in_user})

#############Account Payout Methods###################
@csrf_exempt
def payment_information(request):
	logged_in_user=request.user
	return render(request,'myapp/payment_information.html',{'logged_in_user':logged_in_user})

#############Account Invoices###################
@csrf_exempt
def invoices(request):
	logged_in_user=request.user
	return render(request,'myapp/invoices.html',{'logged_in_user':logged_in_user})

#############Account Taxpayer Information###################
@csrf_exempt
def tax_information(request):
	logged_in_user=request.user
	return render(request,'myapp/tax_information.html',{'logged_in_user':logged_in_user})

#############Account Taxpayer Forms###################
@csrf_exempt
def tax_forms(request):
	logged_in_user=request.user
	return render(request,'myapp/tax_forms.html',{'logged_in_user':logged_in_user})

#############Account close account###################
@csrf_exempt
def close_account(request):
	logged_in_user=request.user
	if request.method == 'POST':
		reason=request.POST['reason']
		close=request.POST['close']
		password=request.POST['password_user']
		logged_user=User.objects.filter(email=logged_in_user)
		for user in logged_user:
			user_password=user.password
			check=check_password(password,user_password)
		if check==True:
			for u in logged_user:
				if u.is_active:
					u = User.objects.get(email=logged_in_user)
					u.delete()
					return HttpResponse('removed')
				else:
					return HttpResponse('not-removed')
		else:
			return HttpResponse('not-removed')
	else:
		return render(request,'myapp/account_close.html',{'logged_in_user':logged_in_user})
#############Sign Up###################
@csrf_exempt
def register(request):
	try:
		email=request.POST['email']
		password=request.POST['password']
		password_new=make_password(password)
		em=User.objects.filter(email=email)
		if (len(em)>0):
			return HttpResponse('already exists')
		else:
			user_data=User.objects.create(email=email,password=password_new,username=email)
			new_user = authenticate(username=email, password=password)
			if new_user:
				auth_login(request, new_user)
				request.session['loggedin_user']=email
				return HttpResponse('saved')
			else:
				return HttpResponse('non-authentic')
	except Exception as e:
		print('error',e)


#############Login###################
@csrf_exempt
def login(request):
	try:
		if request.method == 'POST':
			email=request.POST['email']
			password=request.POST['password']
			user=User.objects.filter(email=email)
			for us in user:
				user_password=us.password
				check=check_password(password,user_password)
			if check==True:
				new_user = authenticate(username=email, password=password)
				if new_user:
					auth_login(request, new_user)
					request.session['loggedin_user']=email
					total_ticket_list = []
					user_tickets_dict = {'transaction_id':''}
					user_tickets=TicketBooked.objects.filter(user=request.user.id)
					for ticket in user_tickets:
						user_tickets_dict['transaction_id'] = ticket.transaction_id
						total_ticket_list.append(user_tickets_dict.copy())
					total_ticket = len(total_ticket_list)
					request.session['total_ticket']=total_ticket

					return HttpResponse('authentic')
				else:
					return HttpResponse('non-authentic')
			else:
				return HttpResponse('non-authentic')
	except Exception as e:
		print('error',e)
	return HttpResponse('non-authentic')

#############Render Dashboard###################
@csrf_exempt
def dashboard(request):
	try:
		logged_in_user=request.user.id
		try:
			total_tickets=request.session['total_tickets']
		except Exception as e:
			total_tickets = 0
		events_data_list=[]
		images_data_list=[]
		images_data_dict={'event_id':'','image_name':''}
		events_data_dict={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'',
		'category':'','event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':'','college_name':'','start_date':'','end_date':''}
		events_info=Event.objects.filter(user_id=logged_in_user)
		for info in events_info:
			event_id=str(info.id)
			city = info.location
			location_object = Location.objects.get(city=city)
			country = location_object.country
			time=info.registered_datetime
			start_datetime=info.start_datetime
			end_datetime=info.end_datetime
			registered_datetime=time.strftime("%B %d, %Y, %I:%M %p")

			start_time=start_datetime.strftime("%I:%M %p")
			start_date=start_datetime.strftime("%m/%d/%Y")

			end_time=end_datetime.strftime("%I:%M %p")
			end_date=end_datetime.strftime("%m/%d/%Y")

			event_start_time =info.start_datetime.strftime("%B %d, %Y, %I:%M %p")
			college_name=info.college_name
			name=info.event_name
			eventbanner_object = EventBanner.objects.filter(event_id=event_id)
			for val in eventbanner_object:
				image=val.event_banner				
			description=info.description
			organizer_name=info.organizer_name
			organizer_description=info.organizer_description
			user_email=info.user
			category_name=info.category		
			eventticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)			
			price = eventticket_object.price 
			ticket_type = eventticket_object.ticket_type
			events_data_dict['event_id']=event_id
			events_data_dict['city']=city
			events_data_dict['country']=country
			events_data_dict['time']=event_start_time
			events_data_dict['name']=name
			events_data_dict['price']=price
			events_data_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
			events_data_dict['description']=description
			events_data_dict['start_time']=start_time
			events_data_dict['end_time']=end_time
			events_data_dict['start_date']=start_date
			events_data_dict['end_date']=end_date
			events_data_dict['college_name']=college_name
			events_data_dict['user_email']=user_email
			events_data_dict['category']=category_name
			events_data_dict['event_type']=ticket_type
			events_data_dict['organizer_name']=organizer_name
			events_data_dict['organizer_description']=organizer_description
			events_data_list.append(events_data_dict.copy())	
		return render(request,'User/dashboard.html',{'total_tickets':total_tickets, 'events_data':events_data_list,'logged_in_user':logged_in_user})	
	except Exception as e:
		print('error',e)

############Logout###################
@csrf_exempt
def logout(request):
	try:
		request.user=''
		auth_logout(request)
		return render(request,'home/index.html',{})		
	except Exception as e:
		print('error',e)


############Search for events from home###################
@csrf_exempt
def search_events(request):
	try:
		if(request.POST['category']):
			category=request.POST['category']
		else:
			category=''
		if(request.POST['location']):
			location=request.POST['location']
		else:
			location=''
		if(request.POST['date']):
			date=request.POST['date']
		else:
			date=''
		today=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') 
		events_list=[]
		events_dict={'publish_event':'','event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		category_data=Event.objects.filter(event_name=category)
		for data in category_data:
			category_id=data.category_id
		##########################Category and date Filter##################################
		if((category != '') and (date == 'All Dates')):
			events_data=Event.objects.filter(category_id=category_id)
		if((category != '') and (date == 'Today')):
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__startswith=today)
		if((category != '') and (date == 'Tomorrow')):
			date_tomorrow = datetime.now()+ relativedelta(days=1)
			date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d %H:%M:%S') 
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__startswith=date_tomorrow_new)
		if((category != '') and (date == 'This Week')):
			start_date=today
			end_date=datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__range=(start_date, end_date_new))
		if((category != '') and (date == 'This Weekend')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=5)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__range=(start_date_new1, end_date_new))
		if((category != '') and (date == 'Next Week')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=7)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=14)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__range=(start_date_new1, end_date_new))
		if((category != '') and (date == 'Next Month')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=30)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=60)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(category_id=category_id).filter(timestamp__range=(start_date_new1, end_date_new))
		#########################################Ends here###################################################################

		##########################################Location and dates filter##################################################
		if((location != '') and (date == 'All Dates')):
			events_data=Event.objects.filter(city=location)
		if((location != '') and (date == 'Today')):
			events_data=Event.objects.filter(city=location).filter(timestamp__contains=today)
		if((location != '') and (date == 'Tomorrow')):
			date_tomorrow = datetime.now()+ relativedelta(days=1)
			date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d %H:%M:%S') 
			events_data=Event.objects.filter(city=location).filter(timestamp__startswith=date_tomorrow_new)
		if((location != '') and (date == 'This Week')):
			start_date=today
			end_date=datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(city=location).filter(timestamp__range=(start_date, end_date_new))
		if((location != '') and (date == 'This Weekend')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=5)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(city=location).filter(timestamp__range=(start_date_new1, end_date_new))
		if((location != '') and (date == 'Next Week')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=7)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=14)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(city=location).filter(timestamp__range=(start_date_new1, end_date_new))
		if((location != '') and (date == 'Next Month')):
			start_date=today
			start_date_new=datetime.now()+ relativedelta(days=30)
			start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
			end_date=datetime.now()+ relativedelta(days=60)
			end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
			events_data=Event.objects.filter(city=location).filter(timestamp__range=(start_date_new1, end_date_new))


		#########################################Ends here##################################################################
		for event in events_data:
			event_id=str(event.event_id)
			city=event.city
			country=event.country
			time_new=event.timestamp
			time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
			name=event.event_name
			price=event.price
			image=event.image
			description=event.description
			category_id=event.category_id
			publish_event=event.publish_event
			events_dict['event_id']=event_id
			events_dict['city']=city
			events_dict['country']=country
			events_dict['time']=time_register
			events_dict['name']=name
			events_dict['price']=price
			events_dict['image']=image
			events_dict['description']=description
			events_dict['category']=category
			events_dict['publish_event']=publish_event
			events_list.append(events_dict.copy())
		html = render_to_string('myapp/search_results.html', {'events_data':events_list })
		return HttpResponse(html)
	except Exception as e:
		print('error',e)

############Search for events from home###################
@csrf_exempt
def view_event(request,id):
	try:
		event_id=id
		events_data_list=[]
		subevents_data_list=[]
		event_ticket_list=[]
		subevent_ticket_list=[]
		events_data_dict={'booking_check':'','event_id':'','city':'','country':'','event_type':'','time':'','name':'','price':'','image':'','description':'','category':''}
		subevents_data_dict={'booking_check':'','sub_event_id':'','start_time':'','end_time':'','sub_event_type':'','name':'','price':'','image':'','description':'','category':''}
		event_ticket_dict={'timestamp_end':'','order_max':'','order_min':'','ticket_name':'','ticket_id':'','pending_tickets':''}
		subevent_ticket_dict={'subevent_id_id':'','timestamp_end':'','order_max':'','order_min':'','ticket_name':'','ticket_id':'','pending_tickets':''}
		user=request.user
		
		events_info=Event.objects.filter(event_id=event_id)
		for info in events_info:
			event_id=str(info.event_id)
			city=info.city
			country=info.country
			time=info.timestamp
			time_register=time.strftime("%B %d, %Y, %I:%M %p")
			name=info.event_name
			price=info.price
			image=info.image
			description=info.description
			category_id=info.category_id
			event_type=info.event_type
			category_info=Category.objects.filter(category_id=category_id)
			for cat_info in category_info:
				category_name=cat_info.category_name
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['time']=time_register
				events_data_dict['name']=name
				events_data_dict['price']=price
				events_data_dict['image']=image
				events_data_dict['description']=description
				events_data_dict['category']=category_name
				events_data_dict['event_type']=event_type
				events_data_list.append(events_data_dict.copy())
				################# ticket date data ###################################
				if(event_type=='Free'):
					ticket_info=FreeTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
						ticket_name=tic_info.ticket_name
						ticket_id=tic_info.ticket_id
						tic_count=tic_info.ticket_count
				if(event_type=='Paid'):
					ticket_info=PaidTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
						ticket_name=tic_info.ticket_name
						ticket_id=tic_info.ticket_id
						tic_count=tic_info.ticket_count
				if(event_type=='Donation'):
					ticket_info=DonationTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
						ticket_name=tic_info.ticket_name
						ticket_id=tic_info.ticket_id
						tic_count=tic_info.ticket_count
				tic_count1=int(tic_count)
				quantity_booked=0
				book_info=TicketBooked.objects.filter(event_id_id=event_id)
				for info in book_info:
					transaction_id=info.transaction_id
					event_id_id=str(info.event_id_id)
					ticket_booked1=int(info.quantity_booked)
					quantity_booked=quantity_booked+ticket_booked1
				pending_tickets=tic_count1-quantity_booked
				event_ticket_dict['timestamp_end']=tic_time_close
				event_ticket_dict['order_min']=min_ticket
				event_ticket_dict['order_max']=max_ticket
				event_ticket_dict['ticket_name']=ticket_name
				event_ticket_dict['ticket_id']=ticket_id
				event_ticket_dict['pending_tickets']=pending_tickets
				event_ticket_list.append(event_ticket_dict.copy())
			sub_events_info=SubEvent.objects.filter(event_id_id=event_id)
			for info in sub_events_info:
				sub_event_id=str(info.sub_event_id)
				time=info.timestamp
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=info.timestamp_start
				sub_event_start_time=start_time.strftime("%B %d, %Y, %I:%M %p")
				end_time=info.timestamp_end
				sub_event_end_time=end_time.strftime("%B %d, %Y, %I:%M %p")
				name=info.sub_event_name
				price=info.price
				image=info.image
				description=info.description
				sub_category_id=info.sub_category_id
				sub_event_type=info.sub_event_type
				booking_check=info.booking_check
				subcategory_info=SubCategory.objects.filter(sub_category_id=sub_category_id)
				for cat_info in subcategory_info:
					subcategory_name=cat_info.sub_category_name
					subevents_data_dict['sub_event_id']=sub_event_id
					subevents_data_dict['start_time']=sub_event_start_time
					subevents_data_dict['end_time']=sub_event_end_time
					subevents_data_dict['name']=name
					subevents_data_dict['price']=price
					subevents_data_dict['image']=image
					subevents_data_dict['description']=description
					subevents_data_dict['category']=subcategory_name
					subevents_data_dict['sub_event_type']=sub_event_type
					subevents_data_dict['booking_check']=booking_check
					subevents_data_list.append(subevents_data_dict.copy())
					################# ticket date data ###################################
					if(sub_event_type=='Free'):
						ticket_info=FreeTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=sub_event_id)
						for tic_info in ticket_info:
							subevent_id_id=tic_info.sub_event_id_id
							tic_end_time=tic_info.timestamp_end
							min_ticket=tic_info.order_min
							max_ticket=tic_info.order_max
							tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
							ticket_name=tic_info.ticket_name
							ticket_id=tic_info.ticket_id
							tic_count=tic_info.ticket_count
					if(sub_event_type=='Paid'):
						ticket_info=PaidTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=sub_event_id)
						for tic_info in ticket_info:
							subevent_id_id=tic_info.sub_event_id_id
							tic_end_time=tic_info.timestamp_end
							min_ticket=tic_info.order_min
							max_ticket=tic_info.order_max
							tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
							ticket_name=tic_info.ticket_name
							ticket_id=tic_info.ticket_id
							tic_count=tic_info.ticket_count
					if(sub_event_type=='Donation'):
						ticket_info=DonationTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=sub_event_id)
						for tic_info in ticket_info:
							subevent_id_id=info.sub_event_id_id
							tic_end_time=tic_info.timestamp_end
							min_ticket=tic_info.order_min
							max_ticket=tic_info.order_max
							tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
							ticket_name=tic_info.ticket_name
							ticket_id=tic_info.ticket_id
							tic_count=tic_info.ticket_count
					tic_count1=int(tic_count)
					#quantity_booked=0
					#sub_book_info=TicketBooked.objects.filter(subevent_id_id=sub_event_id)
					#for info in sub_book_info:
					#	transaction_id=info.transaction_id
					#	event_id_id=str(info.event_id_id)
					#	ticket_booked1=int(info.quantity_booked)
					#	quantity_booked=quantity_booked+ticket_booked1
					#pending_tickets=tic_count1-quantity_booked
					subevent_ticket_dict['subevent_id_id']=subevent_id_id
					subevent_ticket_dict['timestamp_end']=tic_time_close
					subevent_ticket_dict['order_min']=min_ticket
					subevent_ticket_dict['order_max']=max_ticket
					subevent_ticket_dict['ticket_name']=ticket_name
					subevent_ticket_dict['ticket_id']=ticket_id
					
					#subevent_ticket_dict['pending_tickets']=pending_tickets
					subevent_ticket_list.append(subevent_ticket_dict.copy())

		try:
			user=request.user
		except Exception as e:
			user=''
		return render(request,'myapp/events.html',{'events_data':events_data_list,'logged_in_user':user,
			'subevents_data':subevents_data_list,'event_ticket_data':event_ticket_list,'subevent_ticket_data':subevent_ticket_list})
	except Exception as e:
		print 'error',e
###########################Render to browsepage ###############################
@csrf_exempt
def category_filters(request):
	try:
		logged_in_user=request.user
	except Exception as e:
		logged_in_user=''
	try:
		category_get=request.POST['category']
	except Exception as e:
		category_get=''
	if category_get:
		events_data_list1=[]
		events_data_dict1={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		category_info=Category.objects.filter(category_name=category_get)
		for category in category_info:
			category_id=category.category_id
		events_info_data=Event.objects.filter(category_id=category_id)
		for event in events_info_data:
			event_id=str(event.event_id)
			city=event.city
			country=event.country
			time=event.timestamp
			time_register=time.strftime("%B %d, %Y, %I:%M %p")
			name=event.event_name
			price=event.price
			image=event.image
			description=event.description
			category=category_get
			events_data_dict1['event_id']=event_id
			events_data_dict1['city']=city
			events_data_dict1['country']=country
			events_data_dict1['time']=time_register
			events_data_dict1['name']=name
			events_data_dict1['price']=price
			events_data_dict1['image']=image
			events_data_dict1['description']=description
			events_data_dict1['category']=category_get
			events_data_list1.append(events_data_dict1.copy())
			return HttpResponseRedirect("/customer_dashboard/123456")
		return render(request,'myapp/browse_events.html',{'category_events_data':events_data_list1,'logged_in_user':logged_in_user})
		#return HttpResponse('success')
	else:
		category_get=''
		###################################Ends here#############################################

def contact_us(request):
    return render(request,'myapp/contactus.html',{})

def about_us(request):
    return render(request,'myapp/aboutus.html',{})

def careers(request):
    return render(request,'myapp/careers.html',{})

def security(request):
    return render(request,'myapp/security.html',{})

def help(request):
    return render(request,'myapp/help.html',{})

def press(request):
    return render(request,'myapp/press.html',{})

def terms(request):
    return render(request,'myapp/terms.html',{})

def cookies(request):
    return render(request,'myapp/cookies.html',{})

def privacy(request):
    return render(request,'myapp/privacy.html',{})

def developers(request):
    return render(request,'myapp/developers.html',{})

def blog(request):
    return render(request,'myapp/blog.html',{})

def tickets(request):
	logged_in_user=request.user
	return render(request,'myapp/tickets.html',{'logged_in_user':logged_in_user})


def manage_events(request):
	try:
		logged_in_user=request.user
		try:
			total_ticket=request.session['total_ticket']
		except Exception as e:
			total_ticket=0
		events_data_list=[]
		events_data_dict={'publish_event':'','flag':'','event_id':'','city':'','country':'','start_time':'','end_time':'','name':'','price':'','image':'','description':'','category':''}
		subevents_data_list=[]
		subevents_data_dict={'event_id_id':'','sub_event_id':'','time':'','sub_event_name':'','price':'','image':'','description':'',
		'sub_event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':''}
		events_info=Event.objects.filter(user_email=logged_in_user)
		images_info_list=[]
		images_info_dict={'name':''}
		for info in events_info:
			event_id=str(info.event_id)
			city=info.city
			country=info.country
			start_time=info.timestamp_start
			time_start=start_time.strftime("%B %d, %Y, %I:%M %p")
			end_time=info.timestamp_end
			time_end=end_time.strftime("%B %d, %Y, %I:%M %p")
			name=info.event_name
			price=info.price
			image=info.image
			description=info.description
			category_id=info.category_id
			publish_event=info.publish_event
			category_info=Category.objects.filter(category_id=category_id)
			for cat_info in category_info:
				category_name=cat_info.category_name
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['start_time']=time_start
				events_data_dict['end_time']=time_end
				events_data_dict['name']=name
				events_data_dict['price']=price
				#events_data_dict['image']=image
				events_data_dict['description']=description
				events_data_dict['category']=category_name
				events_data_dict['publish_event']=publish_event
				images_info=EventBanner.objects.filter(event_id_id=event_id)
				for images in images_info:
					image_name=images.event_banner
					images_info_dict['name']=str(image_name)
					images_info_list.append(images_info_dict.copy())
				events_data_dict['image']=images_info_list
				subevents_info=SubEvent.objects.filter(event_id_id=event_id)
				if subevents_info:
					for info in subevents_info :
						event_id_id=str(info.event_id_id)
						sub_event_id=str(info.sub_event_id)
						flag='1'
						time=info.timestamp
						time_start=info.timestamp_start
						time_end=info.timestamp_end
						time_register=time.strftime("%B %d, %Y, %I:%M %p")
						start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
						end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
						sub_event_name=info.sub_event_name
						price=info.price
						sub_event_type=info.sub_event_type
						image=info.image
						description=info.description
						organizer_name=info.organizer_name
						organizer_description=info.organizer_description
						sub_category_id=info.sub_category_id
						sub_category_info=Category.objects.filter(category_id=sub_category_id)
						for cat_info in sub_category_info:
							category_name=cat_info.category_name
							subevents_data_dict['event_id_id']=event_id_id
							subevents_data_dict['sub_event_id']=sub_event_id
							events_data_dict['flag']=flag
							subevents_data_dict['time']=time_register
							subevents_data_dict['sub_event_name']=sub_event_name
							subevents_data_dict['price']=price
							subevents_data_dict['image']=image
							subevents_data_dict['description']=description
							subevents_data_dict['start_time']=start_time
							subevents_data_dict['end_time']=end_time
							subevents_data_dict['sub_event_type']=sub_event_type
							subevents_data_dict['organizer_name']=organizer_name
							subevents_data_dict['organizer_description']=organizer_description
							subevents_data_list.append(subevents_data_dict.copy())
				else:
					subevents_data_list=[]
				if subevents_info:
					flag = '1'
				else:
					flag = '0'
				subevents_data_dict['flag']=flag
				events_data_dict['flag']=flag
				events_data_list.append(events_data_dict.copy())
				print 'events_data>>',events_data_list
		return render(request,'myapp/manage_events.html',{'total_ticket':total_ticket,'events_data':events_data_list,'subevents_data':subevents_data_list,'logged_in_user':logged_in_user})
	except Exception as e:
		print'e',e



def account_settings(request):
	logged_in_user=request.user
	user_id = request.user.id
	first_name = request.user.first_name
	last_name = request.user.last_name
	user_data_list=[]
	user_data_dict={'prefix':'','first_name':'','last_name':'','suffix':'','home_phone':'','cell_phone':'',
					'job_title':'','company':'','website':'','blog':'','address':'','country':'',
					'state':'','city':'','home_zip':'','billing_address':'','billing_country':'','billing_state':'',
					'billing_city':'','billing_zip_code':'','shipping_address':'','shipping_country':'','shipping_state':'',
					'shipping_city':'','shipping_zip_code':'','work_address':'','work_country':'','work_state':'',
					'work_city':'','work_zip_code':'','gender':'','age':'','year':'','month':'','day':'',
					'profile_pic':'','timestamp':''}
	user_info=userAccount.objects.filter(user_id_id=user_id)
	if user_info:
		for info in user_info:
			prefix=info.prefix
			suffix=info.sufix
			home_phone=info.home_phone
			cell_phone=info.cell_phone
			job_title=info.job_title
			company=info.company
			website=info.website
			blog=info.blog
			address=info.home_address
			country=info.home_country
			state=info.home_state
			city=info.home_city
			home_zip=info.home_zip_code
			billing_address=info.billing_address
			billing_country=info.billing_country
			billing_state=info.billing_state
			billing_city=info.billing_city
			billing_zip=info.billing_zip_code
			shipping_address=info.shipping_address
			shipping_country=info.shipping_country
			shipping_state=info.shipping_state
			shipping_city=info.shipping_city
			shipping_zip=info.shipping_zip_code
			work_address=info.work_address
			work_country=info.work_country
			work_state=info.work_state
			work_city=info.work_city
			work_zip=info.work_zip_code
			gender=info.gender
			dob=info.dob
			year=dob.split('-')[0]
			month=dob.split('-')[1]
			day=dob.split('-')[2]
			age=info.age
			profile_pic=info.profile_pic
			timestamp=info.timestamp
			user_data_dict['prefix']=prefix
			user_data_dict['first_name']=first_name
			user_data_dict['last_name']=last_name
			user_data_dict['suffix']=suffix
			user_data_dict['home_phone']=home_phone
			user_data_dict['cell_phone']=cell_phone
			user_data_dict['job_title']=job_title
			user_data_dict['company']=company
			user_data_dict['website']=website
			user_data_dict['blog']=blog
			user_data_dict['address']=address
			user_data_dict['country']=country
			user_data_dict['state']=state
			user_data_dict['city']=city
			user_data_dict['home_zip']=home_zip
			user_data_dict['billing_address']=billing_address
			user_data_dict['billing_country']=billing_country
			user_data_dict['billing_state']=billing_state
			user_data_dict['billing_city']=billing_city
			user_data_dict['billing_zip']=billing_zip
			user_data_dict['shipping_address']=shipping_address
			user_data_dict['shipping_country']=shipping_country
			user_data_dict['shipping_state']=shipping_state
			user_data_dict['shipping_city']=shipping_city
			user_data_dict['shipping_zip']=shipping_zip
			user_data_dict['work_address']=work_address
			user_data_dict['work_country']=work_country
			user_data_dict['work_state']=work_state
			user_data_dict['work_city']=work_city
			user_data_dict['work_zip']=work_zip
			user_data_dict['gender']=gender
			user_data_dict['age']=age
			user_data_dict['year']=year
			user_data_dict['month']=month
			user_data_dict['day']=day
			user_data_dict['timestamp']=timestamp
			user_data_dict['profile_pic']=profile_pic
			user_data_list.append(user_data_dict.copy())
	else:
		prefix=''
		first_name=''
		last_name=''
		suffix=''
		home_phone=''
		cell_phone=''
		job_title=''
		company=''
		website=''
		blog=''
		address=''
		country=''
		state=''
		city=''
		home_zip=''
		billing_address=''
		billing_country=''
		billing_state=''
		billing_city=''
		billing_zip=''
		shipping_address=''
		shipping_country=''
		shipping_state=''
		shipping_city=''
		shipping_zip=''
		work_address=''
		work_country=''
		work_state=''
		work_city=''
		work_zip=''
		gender=''
		year=''
		month=''
		day=''
		age=''
		timestamp=''
		profile_pic=''
		user_data_dict['prefix']=prefix
		user_data_dict['first_name']=first_name
		user_data_dict['last_name']=last_name
		user_data_dict['suffix']=suffix
		user_data_dict['home_phone']=home_phone
		user_data_dict['cell_phone']=cell_phone
		user_data_dict['job_title']=job_title
		user_data_dict['company']=company
		user_data_dict['website']=website
		user_data_dict['blog']=blog
		user_data_dict['address']=address
		user_data_dict['country']=country
		user_data_dict['state']=state
		user_data_dict['city']=city
		user_data_dict['home_zip']=home_zip
		user_data_dict['billing_address']=billing_address
		user_data_dict['billing_country']=billing_country
		user_data_dict['billing_state']=billing_state
		user_data_dict['billing_city']=billing_city
		user_data_dict['billing_zip']=billing_zip
		user_data_dict['shipping_address']=shipping_address
		user_data_dict['shipping_country']=shipping_country
		user_data_dict['shipping_state']=shipping_state
		user_data_dict['shipping_city']=shipping_city
		user_data_dict['shipping_zip']=shipping_zip
		user_data_dict['work_address']=work_address
		user_data_dict['work_country']=work_country
		user_data_dict['work_state']=work_state
		user_data_dict['work_city']=work_city
		user_data_dict['work_zip']=work_zip
		user_data_dict['gender']=gender
		user_data_dict['age']=age
		user_data_dict['year']=year
		user_data_dict['month']=month
		user_data_dict['day']=day
		user_data_dict['timestamp']=timestamp
		user_data_dict['profile_pic']=profile_pic
		user_data_list.append(user_data_dict.copy())
	try:
		total_ticket=request.session['total_ticket']
	except Exception as e:
		total_ticket=0
	return render(request,'myapp/account_settings.html',{'user_data':user_data_list,'total_ticket':total_ticket,'logged_in_user':logged_in_user})

def create_event(request):
	try:
		logged_in_user=request.user
		try:
			total_ticket=request.session['total_ticket']
		except Exception as e:
			total_ticket=0
		names_list=[]
		names_dict={'event_name':'','event_id':''}
		events_names=Event.objects.filter(user_email=logged_in_user)
		if events_names: 
			for name in events_names:
				event_name=name.event_name
				event_id=name.event_id
				names_dict['event_name']=event_name
				names_dict['event_id']=event_id
				names_list.append(names_dict.copy())
		else:
			names_list=[]
		if(events_names):
			flag = '1'
		else:
			flag = '0'
		request.session['flag']=flag
		return render(request,'dashboard/create_event.html',{'total_ticket':total_ticket, 'logged_in_user':logged_in_user,'flag':flag,'names_list':names_list})
	except Exception as e:
		print e
		#logged_in_user=''
	#if not logged_in_user:
	#	return HttpResponse('empty')
	#else:

########################### Create sub event function ###########################
@csrf_exempt
def create_sub_event(request):
	try:
		x=0
		if (request.method=="POST"):
			####################Save Event Data#############
			sub_event_category=request.POST['sub_event_catagory']
			sub_category_info=SubCategory.objects.filter(sub_category_name=sub_event_category)
			if sub_category_info:
				for info in sub_category_info:
					sub_category_id=info.sub_category_id
			else:
				sub_category=SubCategory.objects.create(sub_category_name=sub_event_category)
				sub_category.save()
				sub_category_info=SubCategory.objects.filter(sub_category_name=sub_event_category)
				for info in sub_category_info:
					sub_category_id=info.sub_category_id
			sub_event_info=SubEvent.objects.all()
			sub_events_length=len(sub_event_info)
			sub_event_id=sub_events_length+1
			sub_event_name=request.POST['sub_event_name']
			sub_event_description=request.POST['sub_event_description']
			sub_event_organizer=request.POST['sub_event_organizer']
			sub_organizer_description=request.POST['sub_organizer_description']
			sub_event_type=request.POST['sub_event_type']
			ticket_name=request.POST['ticket_name']
			quantity_available=request.POST['quantity_available']
			ticket_description=request.POST['ticket_description']
			sale_channel=request.POST['sale_channel']
			minimum_tickets=request.POST['minimum_tickets']
			maximum_tickets=request.POST['maximum_tickets']
			try:
				ticket_price=request.POST['ticket_price']
			except Exception as e:
				ticket_price='0'
			subevent_image=request.POST['subevent_image']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			sub_category_id=sub_category_id
			start_date=request.POST['sub_start_date']
			start_date_str=start_date.split('/',3)
			start_date_year=start_date_str[2]
			start_date_month=start_date_str[0]
			start_date_day=start_date_str[1]
			final_start_date=start_date_year+'-'+start_date_month+'-'+start_date_day
			end_date=request.POST['sub_end_date']
			end_date_str=end_date.split('/',3)
			end_date_year=end_date_str[2]
			end_date_month=end_date_str[0]
			end_date_day=end_date_str[1]
			final_end_date=end_date_year+'-'+end_date_month+'-'+end_date_day
			start_time=request.POST['sub_start_time']
			end_time=request.POST['sub_end_time']
			timestamp_start=str(final_start_date+' '+start_time)
			start_date_object=datetime.datetime.strptime(timestamp_start,'%Y-%m-%d %I:%M %p')
			timestamp_end=final_end_date+' '+end_time
			end_date_object=datetime.datetime.strptime(timestamp_end,'%Y-%m-%d %I:%M %p')
			logged_in_user=request.user
			try:
				latest_event_id=request.session['latest_event_id']
			except Exception as e:
				latest_event_id=request.POST['event_id']
			event=SubEvent.objects.create(sub_event_name=sub_event_name,description=sub_event_description,
				timestamp=timestamp,organizer_name=sub_event_organizer,organizer_description=sub_organizer_description,
				sub_event_type=sub_event_type,price=ticket_price,image=subevent_image,timestamp_start=start_date_object,
				timestamp_end=end_date_object,sub_category_id=sub_category_id,event_id_id=latest_event_id)
			event.save()
			try:
				event_id=latest_event_id
				latest_event_record_id=event_id
			except Exception as e:
				event_id=''
			if(event_id==''):
				latest_event_record=Event.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
				latest_event_record_id=latest_event_record.event_id
				latest_record=SubEvent.objects.filter(event_id_id=latest_event_record_id).order_by('-timestamp')[0]
				latest_record_id=latest_record.sub_event_id
			else:
				latest_record=SubEvent.objects.filter(event_id_id=latest_event_record_id).order_by('-timestamp')[0]
				latest_record_id=latest_record.sub_event_id
			ticket_name=request.POST['ticket_name']
			sale_channel=request.POST['sale_channel']
			ticket_count=request.POST['quantity_available']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			ticket_start_date=request.POST['ticket_start_date']
			ticket_start_date_str=ticket_start_date.split('/',3)
			ticket_start_date_year=ticket_start_date_str[2]
			ticket_start_date_month=ticket_start_date_str[0]
			ticket_start_date_day=ticket_start_date_str[1]
			ticket_final_start_date=ticket_start_date_year+'-'+ticket_start_date_month+'-'+ticket_start_date_day
			ticket_end_date=request.POST['ticket_end_date']
			ticket_end_date_str=ticket_end_date.split('/',3)
			ticket_end_date_year=ticket_end_date_str[2]
			ticket_end_date_month=ticket_end_date_str[0]
			ticket_end_date_day=ticket_end_date_str[1]
			ticket_final_end_date=ticket_end_date_year+'-'+ticket_end_date_month+'-'+ticket_end_date_day
			ticket_start_time=request.POST['ticket_start_time']
			ticket_end_time=request.POST['ticket_end_time']
			ticket_timestamp_start=str(ticket_final_start_date+' '+ticket_start_time)
			ticket_start_date_object=datetime.datetime.strptime(ticket_timestamp_start,'%Y-%m-%d %I:%M %p')
			ticket_timestamp_end=ticket_final_end_date+' '+ticket_end_time
			ticket_end_date_object=datetime.datetime.strptime(ticket_timestamp_end,'%Y-%m-%d %I:%M %p')
			if(sub_event_type=='Free'):
				ticket_data=FreeTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
					timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
					event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id)
			if(sub_event_type=='Paid'):
				ticket_data=PaidTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
					timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
					event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id)
			if(sub_event_type=='Donation'):
				ticket_data=DonationTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
					timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
					event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id)
			ticket_data.save()
		return HttpResponse('success')
	except Exception as e:
		print e

############################Accounts page change email############################
@csrf_exempt
def change_email(request):
	try:
		current_password=request.POST['current_password']
		new_email=request.POST['new_email']
		old_email=request.POST['old_email']
		user=User.objects.filter(email=old_email)
		for us in user:
			user_password=us.password
			check=check_password(current_password,user_password)
			if check==True:
				User.objects.filter(email=old_email).update(email=new_email)
				return HttpResponse('success')
			else:
				return HttpResponse('wrong')
	except Exception as e:
		print(e)

############################Accounts page upload image############################
@csrf_exempt
def upload_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		file_name_list=file_name.split('.')
		file_name_str=file_name_list[0]
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/profiles/'+file_name_list[0]+'.png', "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)

############################Create event page upload Event image############################
@csrf_exempt
def upload_event_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/events/'+file_name, "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)


############################Create event page upload SubEvent image############################
@csrf_exempt
def upload_subevent_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/subevents/'+file_name, "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)


############################Browse Events page get free events############################
@csrf_exempt
def get_free_events(request):
	try:
		events_data_list=[]
		events_data_dict={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		filter_name=request.POST['filter']
		if(filter_name == 'free'):
			events_data=Event.objects.filter(price='0' )
		else:
			events_data=Event.objects.filter(~Q(price = '0'))
		for event in events_data:
			event_id=str(event.event_id)
			city=event.city
			country=event.country
			time_new=event.timestamp
			time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
			name=event.event_name
			price=event.price
			image=event.image
			description=event.description
			category_id=event.category_id
			category_info=Category.objects.filter(category_id=category_id)
			for cat_info in category_info:
				category_name=cat_info.category_name
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['time']=time_register
				events_data_dict['name']=name
				events_data_dict['price']=price
				events_data_dict['image']=image
				events_data_dict['description']=description
				events_data_dict['category']=category_name
				events_data_list.append(events_data_dict.copy())
		html = render_to_string('myapp/browse_events_filter.html', {'events_data':events_data_list})
		return HttpResponse(html)
	except Exception as e:
		print(e)


############################Browse Events page get filtered events############################
# @csrf_exempt
# def get_filtered_events(request):
# 	try:
# 		try:
# 			logged_in_user=request.user
# 		except Exception as e:
# 			logged_in_user=''
# 		try:
# 			start_date=request.POST['start_date']
# 			end_date=request.POST['end_date']
# 		except Exception as e:
# 			end_date=''
# 			start_date=''
# 		try:
# 			period=request.POST['period']
# 		except Exception as e:
# 			period=''
# 		try:
# 			category_name=request.POST['category']
# 		except Exception as e:
# 			category_name=''
# 		try:
# 			location_name=request.POST['location']
# 		except Exception as e:
# 			location_name=''
# 		today=datetime.datetime.now().strftime('%Y-%m-%d') 
# 		events_data_list=[]
# 		events_data_dict={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
# 		if(start_date != '' and end_date != ''):
# 			start_date=str(start_date)
# 			end_date=str(end_date)
# 			start_date_list=start_date.split('/')
# 			year=start_date_list[2]
# 			day=start_date_list[1]
# 			month=start_date_list[0]
# 			start=year+'-'+month+'-'+day
# 			start_filter=datetime.strptime(start, '%Y-%m-%d').date()
# 			end_date_list=end_date.split('/')
# 			year1=end_date_list[2]
# 			day1=end_date_list[1]
# 			month1=end_date_list[0]
# 			end=year1+'-'+month1+'-'+day1
# 			end_filter=datetime.strptime(end, '%Y-%m-%d').date()
# 			events_data=Event.objects.filter(timestamp__range=(start_filter, end_filter))
# 		if(period != ''):
# 			if(period == 'Today'):
# 				today=datetime.datetime.now().strftime('%Y-%m-%d') 
# 				events_data=Event.objects.filter(timestamp__startswith=today)
# 			if(period == 'Tomorrow'):
# 				date_tomorrow = datetime.datetime.now()+ relativedelta(days=1)
# 				date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d') 
# 				events_data=Event.objects.filter(timestamp__startswith=date_tomorrow_new)
# 			if(period == 'This Week'):
# 				start_date=today
# 				end_date=datetime.datetime.now()+ relativedelta(days=7)
# 				end_date_new=end_date.strftime('%Y-%m-%d')
# 				events_data=Event.objects.filter(timestamp__range=(start_date, end_date_new))
# 			if(period =='This Weekend'):
# 				start_date=today
# 				start_date_new=datetime.datetime.now()+ relativedelta(days=5)
# 				start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
# 				end_date=datetime.datetime.now()+ relativedelta(days=7)
# 				end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
# 				events_data=Event.objects.filter(timestamp__range=(start_date_new1, end_date_new))
# 			if(period == 'Next Week'):
# 				start_date=today
# 				start_date_new=datetime.datetime.now()+ relativedelta(days=7)
# 				start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
# 				end_date=datetime.datetime.now()+ relativedelta(days=14)
# 				end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
# 				events_data=Event.objects.filter(timestamp__range=(start_date_new1, end_date_new))
# 			if(period == 'Next Month'):
# 				start_date=today
# 				start_date_new=datetime.datetime.now()+ relativedelta(days=30)
# 				start_date_new1=start_date_new.strftime('%Y-%m-%d %H:%M:%S')
# 				end_date=datetime.datetime.now()+ relativedelta(days=60)
# 				end_date_new=end_date.strftime('%Y-%m-%d %H:%M:%S')
# 				events_data=Event.objects.filter(timestamp__range=(start_date_new1, end_date_new))

# 		if (category_name != ''):
# 			category_data=Category.objects.filter(category_name=category_name)
# 			for data in category_data:
# 				category_id=data.category_id
# 			events_data=Event.objects.filter(category_id = category_id)
# 		if(location_name  != ''):
# 			events_data=Event.objects.filter(city = location_name)
# 		for event in events_data:
# 			event_id=str(event.event_id)
# 			city=event.city
# 			country=event.country
# 			time_new=event.timestamp
# 			time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
# 			name=event.event_name
# 			price=event.price
# 			image=event.image
# 			description=event.description
# 			category_id=event.category_id
# 			category_info=Category.objects.filter(category_id=category_id)
# 			for cat_info in category_info:
# 				category_name=cat_info.category_name
# 				events_data_dict['event_id']=event_id
# 				events_data_dict['city']=city
# 				events_data_dict['country']=country
# 				events_data_dict['time']=time_register
# 				events_data_dict['name']=name
# 				events_data_dict['price']=price
# 				events_data_dict['image']=image
# 				events_data_dict['description']=description
# 				events_data_dict['category']=category_name
# 				events_data_list.append(events_data_dict.copy())
# 		user_saved_events_list=[]
# 		user_saved_events_dict={'user_name':'','event_id':'','saved_event_flag':''}
# 		print '>>>>>1'
# 		saved_events_data=SavedEvent.objects.filter(user_name=logged_in_user)
# 		print '>>>>>2'
# 		for saved in saved_events_data:
# 			user_name=saved.user_name
# 			event_id=saved.event_id
# 			saved_event_flag=saved.saved_flag
# 			user_saved_events_dict['user_name']=user_name
# 			user_saved_events_dict['event_id']=event_id
# 			user_saved_events_dict['saved_event_flag']=saved_event_flag
# 			user_saved_events_list.append(user_saved_events_dict.copy())
# 		html = render_to_string('myapp/browse_events_filter.html', {'saved_events_data':user_saved_events_list,'events_data':events_data_list})
# 		return HttpResponse(html)
# 	except Exception as e:
# 		print(e)



def gmail(request):
    try:
    	request.session[settings.GOOGLE_REDIRECT_SESSION_VAR] = request.path
        google_state = google_get_state(request)
        gcs = gdata.contacts.service.ContactsService()
        google_contacts = google_import(request, gcs, cache=True)
        
    

        return render_to_response('myapp/gmail.html', { 
        	'google_state': google_state,
        	'google_contacts': google_contacts
        }, context_instance=RequestContext(request))
    except Exception as e:
    	print e


############################### Create Event image upload ############################

@csrf_exempt
def upload_pic(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		file_name_list=file_name.split('.')
		file_name_str=file_name_list[0]
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open("/home/kashyap/mydjango/myproject/myapp/static/images/events/"+file_name_list[0]+'.png', "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print e


################################### Create Event ###################################

@csrf_exempt
def create_event_data(request):
	try:
		logged_in_user=request.user.email
		print 'request>>>>',request.POST
		if request.method=="POST":
		 	####################Save Event Data#############
			event_category=request.POST['event_category']
		 	#button_click=request.POST['button_click']

		 	category_info=Category.objects.filter(category_name=event_category)
		 	if category_info:
		 		for info in category_info:
		 			category_id=info.category_id
		 	else:
		 		category_info=Category.objects.all()
		 		categories_length=len(category_info)
		 		category_id=categories_length+1
		 		category=Category.objects.create(category_name=event_category)
		 		category.save()
		 		category_info=Category.objects.filter(category_name=event_category)
		 		for info in category_info:
		 			category_id=info.category_id
	 		event_city=request.POST['city_name']
		 	event_country=request.POST['country_name']
		 	event_state=request.POST['state']
		 	event_name=request.POST['event_name']
		 	try:
		 		event_price=request.POST['ticket_price']
		 	except Exception as e:
		 		event_price='0'
		 	#event_image=request.POST['event_image']
		 	ts = time.time()
		 	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		 	timestamp=st
		 	event_description=request.POST['event_description']
		 	category=request.POST['event_category']
		 	category_id=category_id
		 	college_name=request.POST['college_name']
		 	#event_type=request.POST['event_type']
		 	organizer_name=request.POST['organizer_name']
		 	organizer_description=request.POST['organizer_description']
		 	timestamp_start=request.POST['start_date']
		 	#start_date_str=start_date.split('/',3)
		 	#start_date_year=start_date_str[2]
		 	#start_date_month=start_date_str[0]
		 	#start_date_day=start_date_str[1]
		 	#final_start_date=start_date_year+'-'+start_date_month+'-'+start_date_day
		 	timestamp_end=request.POST['end_date']
		 	image=request.POST['image_name']
			#end_date_str=end_date.split('/',3)
		 	#end_date_year=end_date_str[2]
		 	#end_date_month=end_date_str[0]
		 	#end_date_day=end_date_str[1]
		 	#final_end_date=end_date_year+'-'+end_date_month+'-'+end_date_day
		 	#start_time=request.POST['start_time']
		 	#end_time=request.POST['end_time']
		 	#timestamp_start=str(final_start_date+' '+start_time)
		 	#start_date_object=datetime.datetime.strptime(timestamp_start,'%Y-%m-%d %I:%M %p')
		 	#timestamp_end=final_end_date+' '+end_time
		 	#end_date_object=datetime.datetime.strptime(timestamp_end,'%Y-%m-%d %I:%M %p')
		 	#logged_in_user=request.user.email
		 	#print 'user',logged_in_user
		 	event_type=request.POST['ticket_type']
		 	event=Event.objects.create(category_id=category_id, event_type=event_type,timestamp=timestamp,image=image,city=event_city,country=event_country,
		 		event_name=event_name,price=event_price,description=event_description,category_name=category,
		 		college_name=college_name,timestamp_start=timestamp_start,timestamp_end=timestamp_end,
		 		organizer_description=organizer_description,organizer_name=organizer_name,user_email=logged_in_user)
		 	event.save()
		 	latest_record=Event.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
		 	latest_record_id=latest_record.event_id
		 	request.session['latest_event_id']=latest_record_id
		 	image_info=EventBanner.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
		 	image_id=image_info.image_id
		 	image_count=int(request.POST['image_count'])
		 	image_count1=image_count-1
		 	print ">>>>>>>>>>>>image count should be 1",image_count1
		 	for i in range(0,image_count):
		 		image_id1=image_id-i
		 		image_info=EventBanner.objects.filter(image_id=image_id1).update(event_id_id=latest_record_id)
		 	ticket_name=request.POST['ticket_name']
		 	sale_channel=request.POST['sale_channel']
		 	ticket_count=request.POST['quantity_available']
		 	ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		 	timestamp=st
		 	timestamp_start=request.POST['ticket_start']
		 	#ticket_start_date_str=ticket_start_date.split('/',3)
		 	#ticket_start_date_year=ticket_start_date_str[2]
		 	#ticket_start_date_month=ticket_start_date_str[0]
		 	#ticket_start_date_day=ticket_start_date_str[1]
		 	#ticket_final_start_date=ticket_start_date_year+'-'+ticket_start_date_month+'-'+ticket_start_date_day
		 	timestamp_end=request.POST['ticket_end']
		 	#ticket_end_date_str=ticket_end_date.split('/',3)
		 	#ticket_end_date_year=ticket_end_date_str[2]
		 	#ticket_end_date_month=ticket_end_date_str[0]
			#ticket_end_date_day=ticket_end_date_str[1]
		 	#ticket_final_end_date=ticket_end_date_year+'-'+ticket_end_date_month+'-'+ticket_end_date_day
		 	#ticket_start_time=request.POST['ticket_start_time']
			#ticket_end_time=request.POST['ticket_end_time']
		 	#ticket_timestamp_start=str(ticket_final_start_date+' '+ticket_start_time)
		 	#ticket_start_date_object=datetime.datetime.strptime(ticket_timestamp_start,'%Y-%m-%d %I:%M %p')
		 	#ticket_timestamp_end=ticket_final_end_date+' '+ticket_end_time
		 	#ticket_end_date_object=datetime.datetime.strptime(ticket_timestamp_end,'%Y-%m-%d %I:%M %p')
		 	minimum_tickets=request.POST['minimum_tickets']
		 	maximum_tickets=request.POST['maximum_tickets']
		 	ticket_description=request.POST['ticket_desc']
		 	price=request.POST['ticket_price']
		 	event_type=request.POST['ticket_type']
		 	if(event_type=='free'):
				ticket_data=FreeTickets.objects.create(ticket_description=ticket_description,ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
					timestamp=timestamp,timestamp_start=timestamp_start,timestamp_end=timestamp_end,
		 			event_id_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets)
		 	if(event_type=='paid'):
		 		ticket_data=PaidTickets.objects.create(ticket_description=ticket_description,ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
		 			timestamp=timestamp,timestamp_start=timestamp_start,timestamp_end=timestamp_end,
		 			event_id_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets,
		 			price=price)
		 	#if(event_type=='Donation'):
		 	#	ticket_data=DonationTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
		 	#		timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
		 	#		event_id_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets)
		 	ticket_data.save()
		 	#if (button_click=='1'):
		 		####################Save SubEvent Data#############
		 	#	sub_event_category=request.POST['sub_event_catagory']
		 	#	sub_category_info=SubCategory.objects.filter(sub_category_name=sub_event_category)
		 	#	if sub_category_info:
		 	#		for info in sub_category_info:
		 	#			sub_category_id=info.sub_category_id
		 	#	else:
		 	#		sub_category=SubCategory.objects.create(sub_category_name=sub_event_category)
		 	#		sub_category.save()
		 	#		sub_category_info=SubCategory.objects.filter(sub_category_name=sub_event_category)
		 	#		for info in sub_category_info:
		 	#			sub_category_id=info.sub_category_id
		 	#	sub_event_info=SubEvent.objects.all()
		 	#	sub_events_length=len(sub_event_info)
		 	#	sub_event_id=sub_events_length+1
		 	#	sub_event_name=request.POST['sub_event_name']
		 	#	sub_event_description=request.POST['sub_event_description']
		 	#	sub_event_organizer=request.POST['sub_event_organizer']
		 	#	sub_organizer_description=request.POST['sub_organizer_description']
		 	#	sub_event_type=request.POST['sub_event_type']
		 	#	ticket_name=request.POST['sub_ticket_name']
		 	#	quantity_available=request.POST['sub_quantity_available']
		 	#	ticket_description=request.POST['sub_ticket_description']
			#	sale_channel=request.POST['sub_sale_channel']
			#	minimum_tickets=request.POST['sub_minimum_tickets']
		 	#	maximum_tickets=request.POST['sub_maximum_tickets']
		 	#	booking_check=request.POST['booking_check']
			#	try:
		 	#		ticket_price=request.POST['sub_ticket_price']
		 	#	except Exception as e:
		 	#		ticket_price='0'
		 	#	subevent_image=request.POST['subevent_image']
		 	#	ts = time.time()
		 	#	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			#	timestamp=st
		 	#	sub_category_id=sub_category_id
		 	#	start_date=request.POST['sub_start_date']
		 	#	start_date_str=start_date.split('/',3)
		 	#	start_date_year=start_date_str[2]
		 	#	start_date_month=start_date_str[0]
			#	start_date_day=start_date_str[1]
		 	#	final_start_date=start_date_year+'-'+start_date_month+'-'+start_date_day
		 	#	end_date=request.POST['sub_end_date']
		 	#	end_date_str=end_date.split('/',3)
			#	end_date_year=end_date_str[2]
		 	#	end_date_month=end_date_str[0]
			#	end_date_day=end_date_str[1]
			#	final_end_date=end_date_year+'-'+end_date_month+'-'+end_date_day
		 	#	start_time=request.POST['sub_start_time']
		 	#	end_time=request.POST['sub_end_time']
		 	#	timestamp_start=str(final_start_date+' '+start_time)
		 	#	sub_start_date_object=datetime.datetime.strptime(timestamp_start,'%Y-%m-%d %I:%M %p')
		 	#	timestamp_end=final_end_date+' '+end_time
		 	#	sub_end_date_object=datetime.datetime.strptime(timestamp_end,'%Y-%m-%d %I:%M %p')
		 	#	logged_in_user=request.user
		 	#	try:
		 	#		latest_event_id=request.session['latest_event_id']
		 	#	except Exception as e:
		 	#		latest_event_id=request.POST['event_id']
		 	#	event=SubEvent.objects.create(sub_event_name=sub_event_name,description=sub_event_description,
		 	#		timestamp=timestamp,organizer_name=sub_event_organizer,organizer_description=sub_organizer_description,
		 	#		sub_event_type=sub_event_type,price=ticket_price,image=subevent_image,timestamp_start=sub_start_date_object,
		 	#		timestamp_end=sub_end_date_object,sub_category_id=sub_category_id,event_id_id=latest_event_id,booking_check=booking_check)
		 	#	event.save()
		 	#	try:
		 	#		event_id=latest_event_id
		 	#		latest_event_record_id=event_id
		 	#	except Exception as e:
		 	#		event_id=''
		 	#	if(event_id==''):
		 	#		latest_event_record=Event.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
		 	#		latest_event_record_id=latest_event_record.event_id
		 	#		latest_record=SubEvent.objects.filter(event_id_id=latest_event_record_id).order_by('-timestamp')[0]
		 	#		latest_record_id=latest_record.sub_event_id
		 	#	else:
		 	#		latest_record=SubEvent.objects.filter(event_id_id=latest_event_record_id).order_by('-timestamp')[0]
		 	#		latest_record_id=latest_record.sub_event_id
		 	#	ticket_name=request.POST['sub_ticket_name']
		 	#	sale_channel=request.POST['sub_sale_channel']
		 	#	ticket_count=request.POST['sub_quantity_available']
		 	#	ts = time.time()
		 	#	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		 	#	timestamp=st
		 	#	ticket_start_date=request.POST['sub_ticket_start_date']
		 	#	ticket_start_date_str=ticket_start_date.split('/',3)
		 	#	ticket_start_date_year=ticket_start_date_str[2]
		 	#	ticket_start_date_month=ticket_start_date_str[0]
		 	#	ticket_start_date_day=ticket_start_date_str[1]
		 	#	ticket_final_start_date=ticket_start_date_year+'-'+ticket_start_date_month+'-'+ticket_start_date_day
		 	#	ticket_end_date=request.POST['sub_ticket_end_date']
		 	#	ticket_end_date_str=ticket_end_date.split('/',3)
		 	#	ticket_end_date_year=ticket_end_date_str[2]
		 	#	ticket_end_date_month=ticket_end_date_str[0]
		 	#	ticket_end_date_day=ticket_end_date_str[1]
		 	#	ticket_final_end_date=ticket_end_date_year+'-'+ticket_end_date_month+'-'+ticket_end_date_day
		 	#	ticket_start_time=request.POST['sub_ticket_start_time']
		 	#	ticket_end_time=request.POST['sub_ticket_end_time']
		 	#	ticket_timestamp_start=str(ticket_final_start_date+' '+ticket_start_time)
		 	#	ticket_start_date_object=datetime.datetime.strptime(ticket_timestamp_start,'%Y-%m-%d %I:%M %p')
		 	#	ticket_timestamp_end=ticket_final_end_date+' '+ticket_end_time
		 	#	ticket_end_date_object=datetime.datetime.strptime(ticket_timestamp_end,'%Y-%m-%d %I:%M %p')
		 	#	if(sub_event_type=='Free'):
		 	#		ticket_data=FreeTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
		 	#			timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
		 	#			event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets)
		 	#	if(sub_event_type=='Paid'):
		 	#		ticket_data=PaidTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
		 	#			timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
		 	#			event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets)
		 	#	if(sub_event_type=='Donation'):
		 	#		ticket_data=DonationTickets.objects.create(ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
		 	#			timestamp=timestamp,timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,
		 	#			event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets)
		 	#	ticket_data.save()
		 	#else:
		 	#	pass
		return HttpResponse('success')
	except Exception as e:
		print e



#########################create_subevent_data############################

	
@csrf_exempt
def create_subevent_data(request):
	try:
		logged_in_user=request.user.email
		
		if request.method == "POST":
			sub_event_name=request.POST["sub_event_title"]
			sub_event_type=request.POST["sub_type_ticket"]
			description=request.POST["sub_event_desc"]
			sub_event_category=request.POST["sub_event_category"]
			#college_name=request.POST["sub_college_name"]
			venue=request.POST["sub_venue"]
			organizer_name=request.POST["sub_organizer_name"]
			organizer_description=request.POST["sub_organizer_desc"]
			image=request.POST["image"]
			visible_check=request.POST['visible_check']
			sub_event_type=request.POST['sub_type_ticket']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		 	timestamp=st
			timestamp_start=request.POST["sub_start_time"]
			timestamp_end=request.POST["sub_end_time"]
			latest_record=Event.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
		 	latest_record_id=latest_record.event_id
		 	request.session['latest_event_id']=latest_record_id
			logged_in_user=request.user
	 		try:
	 			latest_event_id=request.session['latest_event_id']
	 		except Exception as e:
	 			latest_event_id=request.POST['event_id']
			subevent=SubEvent.objects.create(sub_category_name=sub_event_category,sub_event_type=sub_event_type,visible_check=visible_check,image=image,sub_event_name=sub_event_name,description=description,
			venue=venue,organizer_name=organizer_name,timestamp=timestamp,
			organizer_description=organizer_description,timestamp_start=timestamp_start,timestamp_end=timestamp_end,
			event_id_id=latest_event_id)
			subevent.save()
			try:
		 		event_id=latest_event_id
		 		latest_event_record_id=event_id
		 	except Exception as e:
		 		event_id=''
		 	
		 	if(event_id==''):
		 		latest_event_record=Event.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
		 		latest_event_record_id=latest_event_record.event_id
		 		latest_record=SubEvent.objects.filter(event_id_id=latest_event_record_id).order_by('-timestamp')[0]
		 		latest_record_id=latest_record.sub_event_id
		 		
		 	else:
		 		latest_record=SubEvent.objects.filter(event_id_id=latest_event_record_id).order_by('-timestamp')[0]
		 		latest_record_id=latest_record.sub_event_id
		 	latest_event_record=Event.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
		 	latest_event_record_id=latest_event_record.event_id
		 	sub_latest_record=SubEvent.objects.filter(event_id_id=latest_event_id).order_by('-timestamp')[0]
		 	sub_latest_record_id=latest_record.sub_event_id
		 	image_info=SubEventBanner.objects.filter(user_email=logged_in_user).order_by('-timestamp')[0]
		 	image_id=image_info.image_id
		 	image_count=int(request.POST['image_count'])
		 	image_less=int(request.POST['images_value'])
		 	image_id2=image_id-image_less
		 	print "image_id",image_id2
		 	print "latest sub event id",sub_latest_record_id
		 	for i in range(0,image_count):
		 		image_id1=image_id2-i
		 		image_info=SubEventBanner.objects.filter(image_id=image_id1).update(sub_event_id_id=sub_latest_record_id)


			ticket_name=request.POST['sub_ticket_name']
		 	sale_channel=request.POST['sub_sale_channel']
		 	ticket_count=request.POST['sub_quantity_available']
		 	ts = time.time()
		 	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		 	timestamp=st
		 	ticket_start_date=request.POST['sub_ticket_start']
		 	ticket_end_date=request.POST['sub_ticket_end']
		 	minimum_tickets=request.POST['sub_minimum_tickets']
		 	maximum_tickets=request.POST['sub_maximum_tickets']
		 	price=request.POST['sub_ticket_price']
		 	ticket_description=request.POST['sub_ticket_description']
		 	if(sub_event_type=='free'):
		 		ticket_data=FreeTickets.objects.create(ticket_description=ticket_description,price=price,ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
		 			timestamp=timestamp,timestamp_start=ticket_start_date,timestamp_end=ticket_end_date,
		 			event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets)
		 	if(sub_event_type=='paid'):
		 		ticket_data=PaidTickets.objects.create(ticket_description=ticket_description,price=price,ticket_name=ticket_name,sale_channel=sale_channel,ticket_count=ticket_count,
		 			timestamp=timestamp,timestamp_start=ticket_start_date,timestamp_end=timestamp_end,
		 			event_id_id=latest_event_record_id,sub_event_id_id=latest_record_id,order_min=minimum_tickets,order_max=maximum_tickets)
		 	ticket_data.save()
		return HttpResponse('success')
	except Exception as e:
		print e

#######################Ends Here##############################
		

################################# Event Location #################################

@csrf_exempt
def tickets_info(request):
		return HttpResponse('Success')


###############################How it works################################
def how_it_works(request):
	return render(request,'myapp/how_it_works.html',{})

###############################Large complex events################################
def large_complex_events(request):
	return render(request,'myapp/large_complex_events.html',{})

############################# mobile app ##############################
def mobile_app(request):
	return render(request,'myapp/mobile_app.html',{})

############################# Reserved Seating ##############################
def reserved_seating(request):
	return render(request,'myapp/reserved_seating.html',{})

############################ Rally ##############################
def rally(request):
	return render(request,'myapp/rally.html',{})
################################### New #########################################

def google_get_state_token(request, action_type_id, action_id):
    action_state = ActionState.objects.create(**{
        'action_type_id': action_type_id,
        'action_id': action_id,
        'data': request.GET
    })
    return JsonResponse({'stat': 'ok', 'token': action_state.uuid})

def google_login(request):
    token_login = request.GET.get('token')
    
    if token_login:
        gcs = gdata.contacts.service.ContactsService()
        gcs.SetAuthSubToken(token_login)
        gcs.UpgradeToSessionToken()
        request.session[settings.GOOGLE_COOKIE_CONSENT] = gcs.GetAuthSubToken()
    
    return redirect(request.session.get(settings.GOOGLE_REDIRECT_SESSION_VAR))


def google_logout(request):
    if request.session.get(settings.GOOGLE_COOKIE_CONSENT):
        del request.session[settings.GOOGLE_COOKIE_CONSENT]
    if request.session.get('google_contacts_cached'):
        del request.session['google_contacts_cached']
    return redirect(request.session.get(settings.GOOGLE_REDIRECT_SESSION_VAR))




############Delete an event###################
@csrf_exempt
def delete_event(request):
	try:
		event_id=request.POST['event_id']
		event = Event.objects.get(event_id=event_id)
		try:
			subevent_info=SubEvent.objects.get(event_id_id=event_id)
		except Exception as e:
			subevent_info=''
		event.delete()
		#subevent.delete()
		return HttpResponse('success')
	except Exception as e:
		print e

############## Quantity selected ####################
@csrf_exempt
def quantity_selected(request):
	try:
		qua_list=[]
		qua_dict={'selected':''}
		select_val=request.POST['select_val']
		qua_dict['selected']=select_val
		qua_list.append(qua_dict.copy()) 
		html = render_to_string('myapp/quantity_selected.html', {'qua_list':qua_list })
		return HttpResponse(html)
	except Exception as e:
		print e

############## Get Subevents form ####################
@csrf_exempt
def get_subevent_form(request):
	try:
		index=request.POST['index']
		html = render_to_string('myapp/sub_event_ajax.html', {'index':index})
		return HttpResponse(html)
	except Exception as e:
		print e

############Edit an event###################
@csrf_exempt
def edit_event(request):
	try:
		logged_in_user=request.user
		event_id=request.POST['event_id']
		events_data_list=[]
		events_data_dict={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'',
		'category':'','event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':'','college_name':'','start_date':'','end_date':''}
		events_info=Event.objects.filter(event_id=event_id)
		for info in events_info:
			event_id=str(info.event_id)
			city=info.city
			country=info.country
			time=info.timestamp
			time_start=info.timestamp_start
			time_end=info.timestamp_end
			time_register=time.strftime("%B %d, %Y, %I:%M %p")
			start_time=time_start.strftime("%I:%M %p")
			start_date=time_start.strftime("%m/%d/%Y")
			end_time=time_end.strftime("%I:%M %p")
			end_date=time_end.strftime("%m/%d/%Y")
			college_name=info.college_name
			name=info.event_name
			price=info.price
			event_type=info.event_type
			image=info.image
			description=info.description
			organizer_name=info.organizer_name
			organizer_description=info.organizer_description
			college_name=info.college_name
			user_email=info.user_email
			college_name=info.college_name
			category_id=info.category_id
			category_info=Category.objects.filter(category_id=category_id)
			for cat_info in category_info:
				category_name=cat_info.category_name
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['time']=time_register
				events_data_dict['name']=name
				events_data_dict['price']=price
				events_data_dict['image']=image
				events_data_dict['description']=description
				events_data_dict['start_time']=start_time
				events_data_dict['end_time']=end_time
				events_data_dict['start_date']=start_date
				events_data_dict['end_date']=end_date
				events_data_dict['college_name']=college_name
				events_data_dict['user_email']=user_email
				events_data_dict['category']=category_name
				events_data_dict['event_type']=event_type
				events_data_dict['organizer_name']=organizer_name
				events_data_dict['organizer_description']=organizer_description
				events_data_list.append(events_data_dict.copy())
		request.session['events_data_view']=events_data_list
		return HttpResponse('success')
	except Exception as e:
		print e




def manage_edit_event(request):
	logged_in_user=request.user
	event_data=request.session['events_data_view']
	return render(request,'myapp/edit_event.html',{'logged_in_user':logged_in_user,'event_data':event_data})

#####################################Autocomplete for Events#######################
@csrf_exempt
def autocomplete_events(request):
	try:
		term=str(request.POST['term'])
		results=[]
		events_data=Event.objects.filter(event_name__contains=term)
		for data in events_data:
			event_name=data.event_name
			user_json = {}
			user_json['label'] = event_name
			user_json['value'] = event_name
			results.append(user_json)
			data = json.dumps(results)
			mimetype = 'application/json'
		return HttpResponse(data, mimetype)
	except Exception as e:
		print e

######################## View Sub Events #########################

@csrf_exempt
def view_subevents(request):
	event_id=request.POST['event_id']
	events_data_list=[]
	events_data_dict={'event_id':'','event_name':''}
	subevents_data_list=[]
	subevents_data_dict={'sub_event_id_id':'','sub_event_id':'','sub_event_img':'','sub_event_name':'','visible':''}
	events_info=Event.objects.filter(event_id=event_id)
	for info in events_info:
		event_id=str(info.event_id)
		name=info.event_name
		category_id=info.category_id
		#category_info=Category.objects.filter(category_id=category_id)
		#for cat_info in category_info:
		events_data_dict['event_id']=event_id
		events_data_dict['event_name']=name
		events_data_list.append(events_data_dict.copy())
		subevents_info=SubEvent.objects.filter(event_id_id=event_id)
		for info in subevents_info:
			sub_event_id=str(info.sub_event_id)
			print ">>>>>>>>>.sub_event_id",sub_event_id
			if sub_event_id != '' :
				#sub_event_id_id=info.event_id_id
				sub_event_img=info.image
				sub_event_name=info.sub_event_name
				print ">>>>>>>>>.sub event name",sub_event_name
				visible=info.visible_check
				#sub_category_id=info.sub_category_id
				#sub_category_info=SubCategory.objects.filter(sub_category_id=sub_category_id)
				#for cat_info in sub_category_info:
				subevents_data_dict['sub_event_id']=sub_event_id
				subevents_data_dict['visible']=visible
				subevents_data_dict['sub_event_img']=sub_event_img
				#subevents_data_dict['sub_event_id_id']=sub_event_id_id
				subevents_data_dict['sub_event_name']=sub_event_name
				subevents_data_list.append(subevents_data_dict.copy())
			request.session['events_data_view']=events_data_list
			request.session['subevents_data_view']=subevents_data_list
			return HttpResponse('success')
		return HttpResponse('wrong')
			

#################### Edit sub event ########################

@csrf_exempt
def edit_subevent(request):
	try:
		logged_in_user=request.user
		subevent_id=request.POST['sub_event_id']
		subevents_data_list=[]
		subevents_data_dict={'sub_event_id':'','time':'','sub_event_name':'','price':'','image':'','description':'',
		'sub_event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':'','start_date':'','end_date':''}
		subevents_info=SubEvent.objects.filter(sub_event_id=subevent_id)
		for info in subevents_info:
			sub_event_id=str(info.sub_event_id)
			time=info.timestamp
			time_start=info.timestamp_start
			time_end=info.timestamp_end
			time_register=time.strftime("%B %d, %Y, %I:%M %p")
			start_time=time_start.strftime("%I:%M %p")
			start_date=time_start.strftime("%m/%d/%Y")
			end_time=time_end.strftime("%I:%M %p")
			end_date=time_end.strftime("%m/%d/%Y")
			sub_event_name=info.sub_event_name
			price=info.price
			sub_event_type=info.sub_event_type
			image=info.image
			description=info.description
			organizer_name=info.organizer_name
			organizer_description=info.organizer_description
			sub_category_id=info.sub_category_id
			sub_category_info=SubCategory.objects.filter(sub_category_id=sub_category_id)
			for cat_info in sub_category_info:
				subevents_data_dict['sub_event_id']=sub_event_id
				sub_category_name=cat_info.sub_category_name
				subevents_data_dict['time']=time_register
				subevents_data_dict['sub_event_name']=sub_event_name
				subevents_data_dict['price']=price
				subevents_data_dict['image']=image
				subevents_data_dict['description']=description
				subevents_data_dict['start_time']=start_time
				subevents_data_dict['end_time']=end_time
				subevents_data_dict['start_date']=start_date
				subevents_data_dict['end_date']=end_date
				subevents_data_dict['sub_event_type']=sub_event_type
				subevents_data_dict['organizer_name']=organizer_name
				subevents_data_dict['organizer_description']=organizer_description
				subevents_data_list.append(subevents_data_dict.copy())
		request.session['subevents_data_view']=subevents_data_list
		return HttpResponse('success')
	except Exception as e:
		print e


############################Create event page upload SubEvent image############################
@csrf_exempt
def update_subevent_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		#file_name_list=file_name.split('.')
		#file_name_str=file_name_list[0]
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/subevents/'+file_name, "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)

#################### Edit Subevents ###########################

@csrf_exempt
def edit_subevents(request):
	logged_in_user=request.user
	subevent_data=request.session['subevents_data_view']
	return render(request,'myapp/edit_subevents.html',{'logged_in_user':logged_in_user,'subevent_data':subevent_data})

##################### Update Subevent############################

@csrf_exempt
def update_subevent(request):
	try:
		if (request.method=="POST"):
			sub_event_id=request.POST['sub_event_id']
			sub_event_name=request.POST['sub_event_name']
			description=request.POST['sub_event_description']
			organizer_name=request.POST['sub_event_organizer']
			organizer_description=request.POST['sub_organizer_description']
			sub_event_type=request.POST['sub_event_type']
			sub_event_image=request.POST['sub_event_image']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			start_date=request.POST['sub_start_date']
			start_date_str=start_date.split('/',3)
			start_date_year=start_date_str[2]
			start_date_month=start_date_str[0]
			start_date_day=start_date_str[1]
			final_start_date=start_date_year+'-'+start_date_month+'-'+start_date_day
			end_date=request.POST['sub_end_date']
			end_date_str=end_date.split('/',3)
			end_date_year=end_date_str[2]
			end_date_month=end_date_str[0]
			end_date_day=end_date_str[1]
			final_end_date=end_date_year+'-'+end_date_month+'-'+end_date_day
			start_time=request.POST['sub_start_time']
			end_time=request.POST['sub_end_time']
			timestamp_start=str(final_start_date+' '+start_time)
			start_date_object=datetime.datetime.strptime(timestamp_start,'%Y-%m-%d %I:%M %p')
			timestamp_end=final_end_date+' '+end_time
			end_date_object=datetime.datetime.strptime(timestamp_end,'%Y-%m-%d %I:%M %p')
			ticket_name=request.POST['sub_ticket_name']
			sale_channel=request.POST['sale_channel']
			ticket_count=request.POST['quantity_available']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			ticket_start_date=request.POST['ticket_start_date']
			ticket_start_date_str=ticket_start_date.split('/',3)
			ticket_start_date_year=ticket_start_date_str[2]
			ticket_start_date_month=ticket_start_date_str[0]
			ticket_start_date_day=ticket_start_date_str[1]
			ticket_final_start_date=ticket_start_date_year+'-'+ticket_start_date_month+'-'+ticket_start_date_day
			ticket_end_date=request.POST['ticket_end_date']
			ticket_end_date_str=ticket_end_date.split('/',3)
			ticket_end_date_year=ticket_end_date_str[2]
			ticket_end_date_month=ticket_end_date_str[0]
			ticket_end_date_day=ticket_end_date_str[1]
			ticket_final_end_date=ticket_end_date_year+'-'+ticket_end_date_month+'-'+ticket_end_date_day
			ticket_start_time=request.POST['ticket_start_time']
			ticket_end_time=request.POST['ticket_end_time']
			ticket_timestamp_start=str(ticket_final_start_date+' '+ticket_start_time)
			ticket_start_date_object=datetime.datetime.strptime(ticket_timestamp_start,'%Y-%m-%d %I:%M %p')
			ticket_timestamp_end=ticket_final_end_date+' '+ticket_end_time
			ticket_end_date_object=datetime.datetime.strptime(ticket_timestamp_end,'%Y-%m-%d %I:%M %p')
			minimum_tickets=request.POST['minimum_tickets']
			maximum_tickets=request.POST['maximum_tickets']

			if(sub_event_type=='Free'):
				FreeTickets.objects.filter(sub_event_id=sub_event_id).update(ticket_name=ticket_name,
					sale_channel=sale_channel,ticket_count=ticket_count,timestamp=timestamp,
					timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object)
			if(sub_event_type=='Paid'):
				FreeTickets.objects.filter(sub_event_id=sub_event_id).update(ticket_name=ticket_name,
					sale_channel=sale_channel,ticket_count=ticket_count,timestamp=timestamp,
					timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object)
			if(sub_event_type=='Donation'):
				FreeTickets.objects.filter(sub_event_id=sub_event_id).update(ticket_name=ticket_name,
					sale_channel=sale_channel,ticket_count=ticket_count,timestamp=timestamp,
					timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object)



			SubEvent.objects.filter(sub_event_id=sub_event_id).update(sub_event_name=sub_event_name,description=description,
				organizer_name=organizer_name,organizer_description=organizer_description,sub_event_type=sub_event_type,
				image=sub_event_image,timestamp=timestamp,timestamp_start=start_date_object,timestamp_end=end_date_object)
			
			return HttpResponse('success')
	except Exception as e:
		print e

######################### Check-in App ###############################

def checkin_app(request):
    return render(request,'myapp/checkin_app.html',{})


######################## Event Spectrum##########################################

def event_spectrum(request):
	return render(request,'myapp/event_spectrum.html',{})

######################## Site Map####################################

def sitemap(request):
    return render(request,'myapp/sitemap.html',{})

######################## Conference MAnagement ##################################

def conference_management(request):
	return render(request,'myapp/conference_management.html',{})

######################### Classes & Workshops #############################

def classes_workshop(request):
	return render(request,'myapp/classes_workshop.html',{})

##################### Nonprofits & Fundraises ############################

def nonprofit_fundraises(request):
	return render(request,'myapp/nonprofit_fundraises.html',{})

################## Reunions #########################

def reunions(request):
	return render(request,'myapp/reunions.html',{})

######################### Sell Tickets ################################

def sell_tickets(request):
	return render(request,'myapp/sell_tickets.html',{})

################### Event management & planning ############################

def event_management(request):
	return render(request,'myapp/event_management.html',{})

######################## Event Registration ######################

def event_registration(request):
	return render(request,'myapp/event_registration.html',{})

########################### RSVP Online #############################

def rsvp_online(request):
	return render(request,'myapp/rsvp_online.html',{})

######################### Venue Booking ############################

def venue_booking(request):
	return render(request,'myapp/venue_booking.html',{})

#################### Contact Support ################################

def contact_support(request):
	return render(request,'myapp/contact_support.html',{})

######################### Contact Sales##################################

def contact_sales(request):
	return render(request,'myapp/contact_sales.html',{})

###################### manage particular event ##############

@csrf_exempt
def manage_particular_event(request):
	logged_in_user=request.user
	event_data=request.session['events_data_view']
	flag=request.session['flag']
	event_ticket_data=request.session['events_tickets_view']
	#booked_tickets=request.session['events_booked_view']
	return render(request,'myapp/manage_particular_event.html',{'logged_in_user':logged_in_user,'event_data':event_data,'event_ticket_data':event_ticket_data,'flag':flag})
###################### manage particular event ##############

@csrf_exempt
def manage_particular_events(request):
	event_id=request.POST['event_id']
	event_data_list=[]
	event_ticket_list=[]
	event_booked_list=[]
	event_data_dict={'publish_event':'','event_id':'','city':'','country':'','event_type':'','time_start':'','time_end':'','name':'','price':'','image':'','description':'','category':''}
	event_ticket_dict={'timestamp_end':'','order_max':'','order_min':'','count':''}
	#event_booked_dict={'transaction_id':'','ticket_id':'','event_id_id':'','tic_name':'',
		#'timestamp':'','quantity_booked':'','first_name':'','last_name':'','email_address':'',
		#'total_payment':'','portal_fee':'','attendee_status':'','tickets_pending':''}
	events_info=Event.objects.filter(event_id=event_id)
	if events_info:
		for info in events_info:
			event_id=str(info.event_id)
			city=info.city
			country=info.country
			time_start=info.timestamp_start
			time_register=time_start.strftime("%B %d, %Y, %I:%M %p")
			time_end=info.timestamp_end
			time_register_end=time_end.strftime("%B %d, %Y, %I:%M %p")
			name=info.event_name
			price=info.price
			image=info.image
			description=info.description
			category_id=info.category_id
			event_type=info.event_type
			publish_event=info.publish_event
			category_info=Category.objects.filter(category_id=category_id)
			for cat_info in category_info:
				category_name=cat_info.category_name
				event_data_dict['event_id']=event_id
				event_data_dict['city']=city
				event_data_dict['country']=country
				event_data_dict['time_start']=time_register
				event_data_dict['time_end']=time_register_end
				event_data_dict['name']=name
				event_data_dict['price']=price
				event_data_dict['image']=image
				event_data_dict['description']=description
				event_data_dict['category']=category_name
				event_data_dict['event_type']=event_type
				event_data_dict['publish_event']=publish_event
				event_data_list.append(event_data_dict.copy())
				################# ticket date data ###################################
				if(event_type=='Free'):
					ticket_info=FreeTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_count=tic_info.ticket_count
						#tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
				if(event_type=='Paid'):
					ticket_info=PaidTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_count=tic_info.ticket_count
						#tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
				if(event_type=='Donation'):
					ticket_info=DonationTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_count=tic_info.ticket_count
						#tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
				#try:
				#	tic_count1=int(tic_count)
				#except Exception as e:
				#	tic_count1=0
				#event_ticket_dict['timestamp_end']=tic_time_close
					event_ticket_dict['order_min']=min_ticket
					event_ticket_dict['order_max']=max_ticket
					event_ticket_dict['count']=tic_count
					event_ticket_list.append(event_ticket_dict.copy())
				#book_info=TicketBooked.objects.filter(event_id_id=event_id)
				#for info in book_info:
				#	event_id_id=str(info.event_id_id)
				#	transaction_id=info.transaction_id
				#	ticket_id=info.ticket_id
				#	tic_name=info.tic_name
				#	timestamp=info.timestamp
				#	booking_time=timestamp.strftime("%B %d, %Y, %I:%M %p")
				#	quantity_booked=info.quantity_booked
				#	first_name=info.first_name
				#	last_name=info.last_name
				#	email_address=info.email_address
				#	total_payment=info.total_payment
				#	portal_fee=info.portal_fee
				#	attendee_status=info.attendee_status
				#	quantity_booked1=int(quantity_booked)
				#	tickets_pending=tic_count1-quantity_booked1
				#	event_booked_dict['event_id_id']=event_id_id
				#	event_booked_dict['transaction_id']=transaction_id
				#	event_booked_dict['ticket_id']=ticket_id
				#	event_booked_dict['tic_name']=tic_name
				#	event_booked_dict['timestamp']=booking_time
				#	event_booked_dict['quantity_booked']=quantity_booked
				#	event_booked_dict['first_name']=first_name
				#	event_booked_dict['last_name']=last_name
				#	event_booked_dict['email_address']=email_address
				#	event_booked_dict['total_payment']=total_payment
				#	event_booked_dict['portal_fee']=portal_fee
				#	event_booked_dict['attendee_status']=attendee_status
				#	event_booked_dict['tickets_pending']=tickets_pending
				#	event_booked_list.append(event_booked_dict.copy())
	else:
		event_data_list=[]
	if events_info:
		flag='1'
	else:
		flag='0'
	request.session['flag']=flag
	request.session['events_data_view']=event_data_list
	request.session['events_tickets_view']=event_ticket_list
	#request.session['events_booked_view']=event_booked_list
	return HttpResponse('success')


####################### Manage Progressbar ##############################

@csrf_exempt
def manage_progressbar(request):
	try:
		event_id=request.POST['event_id']
		quantity_booked=0
		book_info=TicketBooked.objects.filter(event_id_id=event_id)
		for info in book_info:
			transaction_id=info.transaction_id
			event_id_id=str(info.event_id_id)
			ticket_booked1=int(info.quantity_booked)
			quantity_booked=quantity_booked+ticket_booked1
		return HttpResponse(quantity_booked)
	except Exception as e:
		print e

######################View Sub Event#########################

@csrf_exempt
def view_this_subevent(request):
	logged_in_user=request.user
	event_data=request.session['events_data_view']
	subevent_data=request.session['subevents_data_view']
	try:
		flag=request.session['flag']
	except Exception as e:
		flag=''
	return render(request,'myapp/view_this_subevent.html',{'logged_in_user':logged_in_user,'subevent_data':subevent_data,'event_data':event_data,'flag':flag})


############ register_ticket ###################
@csrf_exempt
def register_ticket(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		event_id=request.POST['eventid']
		ticket_id=request.POST['ticket_id']
		ticket_name=request.POST['ticket_name']
		selected_quantity=request.POST['selected_quantity']
		selected_quantity1=int(selected_quantity)
		event_data_list=[]
		event_data_dict={'ticket_id':'', 'ticket_name':'', 'selected_quantity':'', 'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'',
		'category':'','event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':'','college_name':''}

		quantity_booked=0
		events_info=Event.objects.filter(event_id=event_id)
		for info in events_info:
			event_id=str(info.event_id)
			city=info.city
			country=info.country
			time=info.timestamp
			time_start=info.timestamp_start
			time_end=info.timestamp_end
			time_register=time.strftime("%B %d, %Y, %I:%M %p")
			start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
			end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
			college_name=info.college_name
			name=info.event_name
			price=info.price
			event_type=info.event_type
			image=info.image
			description=info.description
			organizer_name=info.organizer_name
			organizer_description=info.organizer_description
			college_name=info.college_name
			user_email=info.user_email
			college_name=info.college_name
			category_id=info.category_id
			category_info=Category.objects.filter(category_id=category_id)
			for cat_info in category_info:
				category_name=cat_info.category_name
				event_data_dict['event_id']=event_id
				event_data_dict['city']=city
				event_data_dict['country']=country
				event_data_dict['time']=time_register
				event_data_dict['name']=name
				event_data_dict['price']=price
				event_data_dict['image']=image
				event_data_dict['description']=description
				event_data_dict['start_time']=start_time
				event_data_dict['end_time']=end_time
				event_data_dict['college_name']=college_name
				event_data_dict['user_email']=user_email
				event_data_dict['category']=category_name
				event_data_dict['event_type']=event_type
				event_data_dict['selected_quantity']=selected_quantity
				event_data_dict['organizer_name']=organizer_name
				event_data_dict['organizer_description']=organizer_description
				event_data_dict['ticket_name']=ticket_name
				event_data_dict['ticket_id']=ticket_id
				event_data_list.append(event_data_dict.copy())

				if(event_type=='Free'):
					ticket_info=FreeTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_count=tic_info.ticket_count
						tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
				if(event_type=='Paid'):
					ticket_info=PaidTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_count=tic_info.ticket_count
						tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
				if(event_type=='Donation'):
					ticket_info=DonationTickets.objects.filter(event_id_id=event_id).filter(sub_event_id_id=None)
					for tic_info in ticket_info:
						tic_end_time=tic_info.timestamp_end
						min_ticket=tic_info.order_min
						max_ticket=tic_info.order_max
						tic_count=tic_info.ticket_count
						tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
				tic_count1=int(tic_count)
				book_info=TicketBooked.objects.filter(event_id_id=event_id)
				for info in book_info:
					transaction_id=info.transaction_id
					event_id_id=str(info.event_id_id)
					ticket_booked1=int(info.quantity_booked)
					quantity_booked=quantity_booked+ticket_booked1
		tickets_pending=tic_count1-quantity_booked
		if (tickets_pending >= selected_quantity1):
			request.session['event_data_view']=event_data_list
			return HttpResponse('success')
		else:
			return HttpResponse('wrong')
	except Exception as e:
		print e

############## Register Order ####################
@csrf_exempt
def register_order(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		event_data_view=request.session['event_data_view']
		return render(request,'myapp/register_order.html',{'event_data_view':event_data_view,'logged_in_user':logged_in_user})
	except Exception as e:
		print e



############## Send Message to contact organizer ####################
@csrf_exempt
def send_msg(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		sender_name=request.POST['sender_name']
		sender_email=request.POST['sender_email']
		sender_comment=request.POST['sender_comment']
		if sender_name and sender_comment and sender_email:
			try:
				send_mail(sender_name, sender_comment, sender_email, [sender_email])
			except BadHeaderError:
				return HttpResponse('Invalid header found.')
			return HttpResponseRedirect('/register_order/')
		else:
			# In reality we'd use a form class
			# to get proper validation errors.
			return HttpResponse('Make sure all fields are entered and valid.')
		return HttpResponse('success')
	except Exception as e:
		print e

############## View your tickets ####################
@csrf_exempt
def view_ticket(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		#event_id=request.POST['event_id']
		#ticket_id=request.POST['ticket_id']
		#email_address=request.POST['email_address']
		book_tic_list=[]
		book_tic_dict={'transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'','quantity_book':'',
						'booked_ticket_id':'','booked_event_id':'','booked_ticket_name':'', 'booked_sub_event_id':''}
		ticked_booked_info=TicketBooked.objects.filter(email_address=logged_in_user).order_by('-timestamp')
		for ticket in ticked_booked_info:
			transaction_id=str(ticket.transaction_id)
			booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
			first_name=ticket.first_name
			last_name=ticket.last_name
			qunatity_book=ticket.quantity_booked
			ticket_id=ticket.ticket_id
			event_id=str(ticket.event_id_id)
			sub_event_id=str(ticket.sub_event_id_id)
			ticket_name=ticket.tic_name
			book_tic_dict['transaction_id']=transaction_id
			book_tic_dict['booked_time']=booked_time
			book_tic_dict['first_name']=first_name
			book_tic_dict['last_name']=last_name
			book_tic_dict['quantity_book']=qunatity_book
			book_tic_dict['booked_ticket_id']=ticket_id
			book_tic_dict['booked_event_id']=event_id
			book_tic_dict['booked_ticket_name']=ticket_name
			book_tic_dict['booked_sub_event_id']=sub_event_id

			book_tic_list.append(book_tic_dict.copy())
		request.session['booked_data']=book_tic_list
		return HttpResponse('success')
	except Exception as e:
		print e

############## User tickets detail ####################
'''@csrf_exempt
def user_tickets_detail(request):
	print 'into user_tickets_detail'
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		event_data_view=request.session['event_data_view']
		booked_ticket_data=request.session['booked_data']
		return render(request,'myapp/user_tickets_detail.html',{'event_data_view':event_data_view,'booked_ticket_data':booked_ticket_data,'logged_in_user':logged_in_user})
	except Exception as e:
		print e'''


############ Particular booked ticket view ###################
##################This is latest###################
'''@csrf_exempt
def booked_particular_ticket(request,id):
	try:
		particular_ticket_id=id
		booked_event_data_list=[]
		booked_ticket_list=[]
		
		booked_event_data_dict={'college_name':'','organizer_desc':'','event_id':'','city':'','country':'','event_type':'','time':'','name':'','price':'','image':'','description':'','category':''}
		booked_ticket_dict={'booked_sub_event_id':'','transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'','quantity_book':'',
						'booked_ticket_id':'','booked_event_id':'','booked_ticket_name':'','start_time':'','end_time':'','event_name':''}
		booked_subevent_data_dict={'organizer_desc':'','sub_event_id':'','event_type':'','time':'',
		'event_name':'','price':'','image':'','description':''}
		booked_ticket_info=TicketBooked.objects.filter(transaction_id=particular_ticket_id)
		for ticket in booked_ticket_info:
			booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
			first_name=ticket.first_name
			last_name=ticket.last_name
			qunatity_book=ticket.quantity_booked
			ticket_id=ticket.ticket_id
			event_id=str(ticket.event_id_id)
			sub_event_id=str(ticket.sub_event_id_id)
			ticket_name=ticket.tic_name
			booked_ticket_dict['transaction_id']=particular_ticket_id
			booked_ticket_dict['booked_time']=booked_time
			booked_ticket_dict['first_name']=first_name
			booked_ticket_dict['last_name']=last_name
			booked_ticket_dict['quantity_book']=qunatity_book
			booked_ticket_dict['booked_ticket_id']=ticket_id
			booked_ticket_dict['booked_event_id']=event_id
			booked_ticket_dict['booked_sub_event_id']=sub_event_id
			booked_ticket_dict['booked_ticket_name']=ticket_name

			booked_ticket_list.append(booked_ticket_dict.copy())
			event_info=Event.objects.filter(event_id=event_id)
			for info in event_info:
				event_id=str(info.event_id)
				city=info.city
				country=info.country
				time=info.timestamp
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=info.timestamp_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=info.timestamp_end.strftime("%B %d, %Y, %I:%M %p")
				event_name=info.event_name
				name=info.organizer_name
				organizer_desc=info.organizer_description
				price=info.price
				image=info.image
				college_name=info.college_name
				description=info.description
				category_id=info.category_id
				event_type=info.event_type
				category_info=Category.objects.filter(category_id=category_id)
				for cat_info in category_info:
					category_name=cat_info.category_name
					booked_event_data_dict['event_id']=event_id
					booked_event_data_dict['city']=city
					booked_event_data_dict['country']=country
					booked_event_data_dict['time']=time_register
					booked_event_data_dict['name']=name
					booked_event_data_dict['organizer_desc']=organizer_desc
					booked_event_data_dict['college_name']=college_name
					booked_event_data_dict['price']=price
					booked_event_data_dict['image']=image
					booked_event_data_dict['description']=description
					booked_event_data_dict['category']=category_name
					booked_event_data_dict['event_type']=event_type
					booked_event_data_dict['start_time']=start_time
					booked_event_data_dict['end_time']=end_time
					booked_event_data_dict['event_name']=event_name
					booked_event_data_list.append(booked_event_data_dict.copy())
			sub_event_info=SubEvent.objects.filter(sub_event_id=sub_event_id)
			for info in sub_event_info:
				sub_event_id=str(info.sub_event_id)
				time=info.timestamp
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=info.timestamp_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=info.timestamp_end.strftime("%B %d, %Y, %I:%M %p")
				sub_event_name=info.sub_event_name
				organizer_name=info.organizer_name
				organizer_desc=info.organizer_description
				price=info.price
				image=info.image
				description=info.description
				sub_event_type=info.sub_event_type
				
				booked_subevent_data_dict['sub_event_id']=sub_event_id
				booked_subevent_data_dict['time']=time_register
				booked_subevent_data_dict['event_name']=sub_event_name
				booked_subevent_data_dict['organizer_desc']=organizer_desc
				
				booked_subevent_data_dict['price']=price
				booked_subevent_data_dict['image']=image
				booked_subevent_data_dict['description']=description
				booked_subevent_data_dict['organizer_name']=organizer_name
				booked_subevent_data_dict['event_type']=sub_event_type
				booked_subevent_data_dict['start_time']=start_time
				booked_subevent_data_dict['end_time']=end_time
				booked_event_data_list.append(booked_subevent_data_dict.copy())
	
		try:
			user=request.user
		except Exception as e:
			user=''
		return render(request,'myapp/booked_particular_ticket.html',{'booked_event_data_list':booked_event_data_list,'logged_in_user':user,
			'booked_ticket_list':booked_ticket_list})
	except Exception as e:
		print('e',e)'''





############ Particular booked ticket view ###################
@csrf_exempt
def booked_particular_ticket(request):
	try:
		event_image_qr_list=request.session['event_qrimage_view']
		booked_event_data_list=request.session['booked_event_data_list']
		booked_ticket_list=request.session['booked_ticket_list']
		
		try:
			user=request.user
		except Exception as e:
			user=''
			
			request.session['logged_in_user']=user
		
		return render(request,'myapp/booked_particular_ticket.html',{'booked_event_data_list':booked_event_data_list,
			'booked_ticket_list':booked_ticket_list,'event_image_qr_list':event_image_qr_list,'logged_in_user':user})
	except Exception as e:
		print('e',e)


############ Edit particular ticket ###################
@csrf_exempt
def edit_particular_ticket(request):
	try:
		owner_first_name=request.POST['owner_first_name']
		owner_last_name=request.POST['owner_last_name']
		owner_email_address=request.POST['owner_email_address']
		transaction_id=request.POST['transaction_id']
		TicketBooked.objects.filter(transaction_id=transaction_id).update(first_name=owner_first_name,last_name=owner_last_name,
				email_address=owner_email_address)
		return HttpResponse('success')
	except Exception as e:
		print('e',e)

############ Delete particular ticket ###################
@csrf_exempt
def delete_particular_ticket(request):
	try:
		transaction_id=request.POST['transaction_id']
		ticket = TicketBooked.objects.get(transaction_id=transaction_id)
		ticket.delete()
		return HttpResponse('success')
	except Exception as e:
		print e

################### Update Event ######################

@csrf_exempt
def update_event_data(request):
	try:
		if (request.method=="POST"):
			event_id=request.POST['event_id']
			event_name=request.POST['event_name']
			description=request.POST['event_description']
			organizer_name=request.POST['organizer_name']
			organizer_description=request.POST['organizer_description']
			event_type=request.POST['event_type']
			event_image=request.POST['events_image']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			start_date=request.POST['start_date']
			start_date_str=start_date.split('/',3)
			start_date_year=start_date_str[2]
			start_date_month=start_date_str[0]
			start_date_day=start_date_str[1]
			final_start_date=start_date_year+'-'+start_date_month+'-'+start_date_day
			end_date=request.POST['end_date']
			end_date_str=end_date.split('/',3)
			end_date_year=end_date_str[2]
			end_date_month=end_date_str[0]
			end_date_day=end_date_str[1]
			final_end_date=end_date_year+'-'+end_date_month+'-'+end_date_day
			start_time=request.POST['start_time']
			end_time=request.POST['end_time']
			timestamp_start=str(final_start_date+' '+start_time)
			start_date_object=datetime.datetime.strptime(timestamp_start,'%Y-%m-%d %I:%M %p')
			timestamp_end=final_end_date+' '+end_time
			end_date_object=datetime.datetime.strptime(timestamp_end,'%Y-%m-%d %I:%M %p')
			ticket_name=request.POST['ticket_name']
			sale_channel=request.POST['sale_channel']
			ticket_count=request.POST['quantity_available']
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			timestamp=st
			ticket_start_date=request.POST['ticket_start_date']
			ticket_start_date_str=ticket_start_date.split('/',3)
			ticket_start_date_year=ticket_start_date_str[2]
			ticket_start_date_month=ticket_start_date_str[0]
			ticket_start_date_day=ticket_start_date_str[1]
			ticket_final_start_date=ticket_start_date_year+'-'+ticket_start_date_month+'-'+ticket_start_date_day
			ticket_end_date=request.POST['ticket_end_date']
			ticket_end_date_str=ticket_end_date.split('/',3)
			ticket_end_date_year=ticket_end_date_str[2]
			ticket_end_date_month=ticket_end_date_str[0]
			ticket_end_date_day=ticket_end_date_str[1]
			ticket_final_end_date=ticket_end_date_year+'-'+ticket_end_date_month+'-'+ticket_end_date_day
			ticket_start_time=request.POST['ticket_start_time']
			ticket_end_time=request.POST['ticket_end_time']
			ticket_timestamp_start=str(ticket_final_start_date+' '+ticket_start_time)
			ticket_start_date_object=datetime.datetime.strptime(ticket_timestamp_start,'%Y-%m-%d %I:%M %p')
			ticket_timestamp_end=ticket_final_end_date+' '+ticket_end_time
			ticket_end_date_object=datetime.datetime.strptime(ticket_timestamp_end,'%Y-%m-%d %I:%M %p')
			minimum_tickets=request.POST['minimum_tickets']
			maximum_tickets=request.POST['maximum_tickets']
			college_name=request.POST['college_name']
			country_name=request.POST['country_name']
			city_name=request.POST['city_name']

			if(event_type=='Free'):
				FreeTickets.objects.filter(event_id=event_id).filter(sub_event_id=None).update(ticket_name=ticket_name,
					sale_channel=sale_channel,ticket_count=ticket_count,timestamp=timestamp,
					timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,order_min=minimum_tickets,order_max=maximum_tickets)
			if(event_type=='Paid'):
				FreeTickets.objects.filter(event_id=event_id).filter(sub_event_id=None).update(ticket_name=ticket_name,
					sale_channel=sale_channel,ticket_count=ticket_count,timestamp=timestamp,
					timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,order_min=minimum_tickets,order_max=maximum_tickets)
			if(event_type=='Donation'):
				FreeTickets.objects.filter(event_id=event_id).filter(sub_event_id=None).update(ticket_name=ticket_name,
					sale_channel=sale_channel,ticket_count=ticket_count,timestamp=timestamp,
					timestamp_start=ticket_start_date_object,timestamp_end=ticket_end_date_object,order_min=minimum_tickets,order_max=maximum_tickets)



			Event.objects.filter(event_id=event_id).update(event_name=event_name,description=description,
				organizer_name=organizer_name,organizer_description=organizer_description,event_type=event_type,
				timestamp=timestamp,timestamp_start=start_date_object,timestamp_end=end_date_object,
				city=city_name,college_name=college_name,country=country_name,image=event_image)
			


	    	return HttpResponse('success')
    	except Exception as e:
			print e

############################upload Event image############################
@csrf_exempt
def update_event_image(request):
	try:
		base64_text=request.POST['base_string']
		file_name=request.POST['file_name']
		#file_name_list=file_name.split('.')
		#file_name_str=file_name_list[0]
		base64_list=base64_text.split(',')
		base64_str=base64_list[1]
		png_recovered = base64.decodestring(base64_str)
		f = open(BASE_DIR+'/myapp/static/images/events/'+file_name, "w")
		f.write(png_recovered)
		f.close()
		return HttpResponse('success')
	except Exception as e:
		print(e)

#####################Manage ticketgraph#########################


@csrf_exempt
def manage_ticketgraph(request):
	event_id=request.POST['event_id']
	time_threshold = timezone.now() - timedelta(days=30)
	today=timezone.now()
	timestamp_ticket=[]
	quantity_booked1=[]
	ticket_info=TicketBooked.objects.filter(event_id_id=event_id).filter(timestamp__range=[time_threshold, today])
	tickets_data_list=[]
	tickets_data_dict={'timestamp':'','quantity':''}
	for info in ticket_info:
		ticket_id=info.transaction_id
		timestamp_end=info.timestamp
		timestamp_ticket.append(timestamp_end)
		time_value=timestamp_end.strftime('%Y-%m-%d')
		quantity_booked=int(info.quantity_booked)
		quantity_booked1.append(quantity_booked)
		tickets_data_dict['timestamp']=time_value
		tickets_data_dict['quantity']=quantity_booked
		tickets_data_list.append(tickets_data_dict.copy())
	return JsonResponse({'tickets_data':tickets_data_list})

###################### Event Publish ######################

@csrf_exempt
def event_publish(request):
	try:
		event_id=request.POST['event_id']
		Event.objects.filter(event_id=event_id).update(publish_event='1')
		return HttpResponse('success')
	except Exception as e:
		print e


#####################Event Unpublish#######################

@csrf_exempt
def event_unpublish(request):
	try:
		event_id=request.POST['event_id']
		Event.objects.filter(event_id=event_id).update(publish_event='0')
		return HttpResponse('success')
	except Exception as e:
		print e

#####################complete registration########################

@csrf_exempt
def complete_registration(request):
	try:
		event_id=request.POST['event_id']
		SubEvent.objects.filter(event_id_id=event_id).update(booking_check=False)
		return HttpResponse('success')
	except Exception as e:
		print e


############## save Order function ####################
@csrf_exempt
def save_order(request):
	try:
		try:
			logged_in_user1=request.user
			logged_in_user=str(logged_in_user)
		except Exception as e:
			logged_in_user=''
		ticket_id=request.POST['ticket_id']
		event_id=request.POST['event_id']
		
		sub_event_id1=request.POST['sub_event_id']
		if sub_event_id1 == 0:
			sub_event_id='NULL'
		else:
			sub_event_id=sub_event_id1
		last_name=request.POST['last_name']
		first_name=request.POST['first_name']
		quantity=request.POST['quantity']
		ticket_name=request.POST['ticket_name']
		timestamp=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		order_data=TicketBooked.objects.create(tic_name=ticket_name,ticket_id=ticket_id,quantity_booked=quantity,
			timestamp=timestamp,first_name=first_name,last_name=last_name,
			email_address=logged_in_user1,event_id_id=event_id,sub_event_id_id=sub_event_id)
		if sub_event_id == '0':
			print 'into if'
			events_info=Event.objects.filter(event_id=event_id)
			for info in events_info:
				event_id=str(info.event_id)
				city=info.city
				country=info.country
				time=info.timestamp
				time_start=info.timestamp_start
				time_end=info.timestamp_end
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
				college_name=info.college_name
				event_name=info.event_name
				price=str(info.price)
				event_type=info.event_type
				image=info.image
				description=info.description
				organizer_name=info.organizer_name
				organizer_description=info.organizer_description
				college_name=info.college_name
				user_email=info.user_email
				category_id=info.category_id
				ticked_booked_info=TicketBooked.objects.filter(event_id_id=event_id).filter(sub_event_id_id='0').order_by('-timestamp')[:1]
				for ticket in ticked_booked_info:
					transaction_id=int(ticket.transaction_id)
					transaction_id_string=str(transaction_id)
					booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
					first_name=ticket.first_name
					last_name=ticket.last_name
					quantity_book=int(ticket.quantity_booked)
					quantity_book_string=str(quantity_book)
					ticket_id=ticket.ticket_id
					event_id=str(ticket.event_id_id)
					ticket_name=ticket.tic_name
					d = {}
				email_address=request.POST['input_emails']
				guest_email=[i for i in email_address.strip('[]').split(',')]
				for x in range(0,len(guest_email)):
				#for x in range(0, quantity_book):

					img_id=str(transaction_id)+'-'+str(x)
					img = qrcode.make(img_id)
					#img_id=str(transaction_id)+'.'+str(x)
					filename=str(img_id)+'.png'
					img.save("myapp/static/qr/"+filename)
					image_info=ImageQR.objects.create(event_id_id=event_id,image_path=filename,transaction_id_id=transaction_id,guest_email=guest_email[x])
					image_info.save()
					
					strFrom = 'cct.jagsirsingh@gmail.com'
					strTo = guest_email[x]
					# Create the root message and fill in the from, to, and subject headers
					msgRoot = MIMEMultipart('related')
					msgRoot['Subject'] = 'test message'
					msgRoot['From'] = strFrom
					msgRoot['To'] = strTo
					msgRoot.preamble = 'This is a multi-part message in MIME format.'
					# Encapsulate the plain and HTML versions of the message body in an
					# 'alternative' part, so message agents can decide which they want to display.
					msgAlternative = MIMEMultipart('alternative')
					msgRoot.attach(msgAlternative)

					
					d['msgText_%02d' % x] = MIMEText('<div><b><h2>Welcome : <i> Sir/Madam </i></h2></b></div> '+
										'<div style="width:50%; float:left;"> Recently your ticket is booked for <strong>'+ event_name +'</strong>'+
											'<div style="width:100%">'+
												'<div><h1>Ticket details :</h1></div>'+
												'<ul style="list-style:none;">'+
													'<li>Ticket Booked by : ' + first_name + ' ' + last_name + '</li>'
													'<li>Your order no is : ' + transaction_id_string +'</li>'+
													'<li>Ticket name : ' + ticket_name +'</li>'+
													'<li>Booked at : ' + booked_time +'</li>'+
													'<li>Price per ticket is : ' + price +'</li>'+
												'</ul>'+
											'</div>'+
											'<div style="width:100%">'+
												'<div><h1>Organizer :</h1></div>'+
												'<h4><b>' + event_name + '</b> event is organized by <b>' + organizer_name + '<b></h4>'+
												'<h4> Here is the organizer Description</h4>'+
												organizer_description +
											'</div>'+
											'<div style="width:100%">'+
												'<div><h1>Time and venue :</h1></div>'+
												'<ul style="list-style:none;">'+
													'<li>College : ' + college_name +'</li>'+
													'<li>City : ' + city +'</li>'+
													'<li>Country : ' + country +'</li>'+
													'<li>Event start at : ' + start_time +'</li>'+
													'<li>Event ends at : ' + end_time +'</li>'+
												'</ul>'+
											'</div>'+
										'</div>'+
										'<div style="width:50%; float:left;"><b> QRcode Image is: </b><img src="cid:image1"></div>'+
										'<div style="width:100%; float:left;"> If you have and query you can email the organizer at ' + user_email +'</div>', 'html')
					msgAlternative.attach(d['msgText_%02d' % x])
					# This example assumes the image is in the current directory
					fp = open(BASE_DIR+'/myapp/static/qr/'+filename, 'rb')
					msgImage = MIMEImage(fp.read())
					fp.close()
					# Define the image's ID as referenced above
					msgImage.add_header('Content-ID', '<image1>')
					msgRoot.attach(msgImage)
					# Send the email (this example assumes SMTP authentication is required)
					username = 'cct.jagsirsingh@gmail.com'
					password = '8Nu09#MR'
					server = smtplib.SMTP('smtp.gmail.com:587')
					server.ehlo()
					server.starttls()
					server.login(username,password)
					server.sendmail(strFrom, strTo, msgRoot.as_string())
					server.quit()
				################### Mail to event Organizer ##################################
				# Define these once; use them twice!
				strFrom = 'cct.jagsirsingh@gmail.com'
				strTo = user_email
				# Create the root message and fill in the from, to, and subject headers
				msgRoot = MIMEMultipart('related')
				msgRoot['Subject'] = 'Ticket booked for '+ event_name
				msgRoot['From'] = strFrom
				msgRoot['To'] = strTo
				msgRoot.preamble = 'This is a multi-part message in MIME format.'
				# Encapsulate the plain and HTML versions of the message body in an
				# 'alternative' part, so message agents can decide which they want to display.
				msgAlternative = MIMEMultipart('alternative')
				msgRoot.attach(msgAlternative)

				
				d['msgText_%02d' % x] = MIMEText('<div><b><h2>Hello : <i>' + user_email + '</i></h2></b></div> '+
									'<div style="width:50%; float:left;"> Recently' + quantity_book_string +' tickets are booked from <strong>'+ event_name +'</strong>'+
										'<div style="width:100%">'+
											'<div><h1>Booking details :</h1></div>'+
											'<ul style="list-style:none;">'+
												'<li>Order no is : ' + transaction_id_string +'</li>'+
												'<li>Ticket name : ' + ticket_name +'</li>'+
												'<li>Booked at : ' + booked_time +'</li>'+
												'<li>Price per ticket is : ' + price +'</li>'+
												'<li>Booked by : ' + first_name + ' ' + last_name +'</li>'+
												'<li>Attendee email : ' + logged_in_user +'</li>'+
											'</ul>'+
										'</div>', 'html')
				msgAlternative.attach(d['msgText_%02d' % x])
				# Send the email (this example assumes SMTP authentication is required)
				username = 'cct.jagsirsingh@gmail.com'
				password = '8Nu09#MR'
				server = smtplib.SMTP('smtp.gmail.com:587')
				server.ehlo()
				server.starttls()
				server.login(username,password)
				server.sendmail(strFrom, strTo, msgRoot.as_string())
				server.quit()
		else :
			user_email=''
			events_info=Event.objects.filter(event_id=event_id)
			for info in events_info:
				user_email=info.user_email
			sub_events_info=SubEvent.objects.filter(sub_event_id=sub_event_id)
			for info in sub_events_info:
				sub_event_id=str(info.sub_event_id)
				event_id=str(info.event_id_id)
				time=info.timestamp
				time_start=info.timestamp_start
				time_end=info.timestamp_end
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
				sub_event_name=info.sub_event_name
				price=str(info.price)
				sub_event_type=info.sub_event_type
				image=info.image
				description=info.description
				organizer_name=info.organizer_name
				organizer_description=info.organizer_description
				ticked_booked_info=TicketBooked.objects.filter(sub_event_id_id=sub_event_id).order_by('-timestamp')[:1]
				for ticket in ticked_booked_info:
					transaction_id=int(ticket.transaction_id)
					transaction_id_string=str(transaction_id)
					booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
					first_name=ticket.first_name
					last_name=ticket.last_name
					quantity_book=int(ticket.quantity_booked)
					quantity_book_string=str(quantity_book)
					ticket_id=ticket.ticket_id
					sub_event_id=str(ticket.sub_event_id_id)
					ticket_name=ticket.tic_name
					d = {}
				email_address=request.POST['input_emails']
				guest_email=[i for i in email_address.strip('[]').split(',')]
				for x in range(0,len(guest_email)):
				#for x in range(0, quantity_book):
					img_id=str(transaction_id)+'-'+str(x)
					img = qrcode.make(img_id)
					#img_id=str(transaction_id)+'.'+str(x)
					filename=str(img_id)+'.png'
					image_info=ImageQR.objects.create(event_id_id=event_id,sub_event_id_id=sub_event_id,image_path=filename,transaction_id_id=transaction_id,guest_email=guest_email[x])
					image_info.save()
					img.save("myapp/static/qr/"+filename)
					
					strFrom = 'cct.jagsirsingh@gmail.com'
					strTo = guest_email[x]
					# Create the root message and fill in the from, to, and subject headers
					msgRoot = MIMEMultipart('related')
					msgRoot['Subject'] = 'test message'
					msgRoot['From'] = strFrom
					msgRoot['To'] = strTo
					msgRoot.preamble = 'This is a multi-part message in MIME format.'
					# Encapsulate the plain and HTML versions of the message body in an
					# 'alternative' part, so message agents can decide which they want to display.
					msgAlternative = MIMEMultipart('alternative')
					msgRoot.attach(msgAlternative)

					d['msgText_%02d' % x] = MIMEText('<div><b><h2>Welcome : <i> Sir/Madam </i></h2></b></div> '+
										'<div style="width:50%; float:left;"> Recently your ticket is booked for <strong>'+ sub_event_name +'</strong>'+
											'<div style="width:100%">'+
												'<div><h1>Ticket details :</h1></div>'+
												'<ul style="list-style:none;">'+
													'<li>Ticket Booked by : ' + first_name + ' ' + last_name + '</li>'
													'<li>Your order no is : ' + transaction_id_string +'</li>'+
													'<li>Ticket name : ' + ticket_name +'</li>'+
													'<li>Booked at : ' + booked_time +'</li>'+
													'<li>Price per ticket is : ' + price +'</li>'+
												'</ul>'+
											'</div>'+
											'<div style="width:100%">'+
												'<div><h1>Organizer :</h1></div>'+
												'<h4><b>' + sub_event_name + '</b> event is organized by <b>' + organizer_name + '<b></h4>'+
												'<h4> Here is the organizer Description</h4>'+
												organizer_description +
											'</div>'+
											'<div style="width:100%">'+
												'<div><h1>Time and venue :</h1></div>'+
												'<ul style="list-style:none;">'+
													'<li>Event start at : ' + start_time +'</li>'+
													'<li>Event ends at : ' + end_time +'</li>'+
												'</ul>'+
											'</div>'+
										'</div>'+
										'<div style="width:50%; float:left;"><b> QRcode Image is: </b><img src="cid:image1"></div>'+
										'<div style="width:100%; float:left;"> If you have and query you can email the organizer at ' + user_email +'</div>', 'html')
					msgAlternative.attach(d['msgText_%02d' % x])
					# This example assumes the image is in the current directory
					fp = open(BASE_DIR+'/myapp/static/qr/'+filename, 'rb')
					msgImage = MIMEImage(fp.read())
					fp.close()
					# Define the image's ID as referenced above
					msgImage.add_header('Content-ID', '<image1>')
					msgRoot.attach(msgImage)
					# Send the email (this example assumes SMTP authentication is required)
					username = 'cct.jagsirsingh@gmail.com'
					password = '8Nu09#MR'
					server = smtplib.SMTP('smtp.gmail.com:587')
					server.ehlo()
					server.starttls()
					server.login(username,password)
					server.sendmail(strFrom, strTo, msgRoot.as_string())
					server.quit()
				################### Mail to event Organizer ##################################
				# Define these once; use them twice!
				strFrom = 'cct.jagsirsingh@gmail.com'
				strTo = user_email
				# Create the root message and fill in the from, to, and subject headers
				msgRoot = MIMEMultipart('related')
				msgRoot['Subject'] = 'Ticket booked for '+ sub_event_name
				msgRoot['From'] = strFrom
				msgRoot['To'] = strTo
				msgRoot.preamble = 'This is a multi-part message in MIME format.'
				# Encapsulate the plain and HTML versions of the message body in an
				# 'alternative' part, so message agents can decide which they want to display.
				msgAlternative = MIMEMultipart('alternative')
				msgRoot.attach(msgAlternative)

				
				d['msgText_%02d' % x] = MIMEText('<div><b><h2>Hello : <i>' + user_email + '</i></h2></b></div> '+
									'<div style="width:50%; float:left;"> Recently' + quantity_book_string +' tickets are booked from <strong>'+ sub_event_name +'</strong>'+
										'<div style="width:100%">'+
											'<div><h1>Booking details :</h1></div>'+
											'<ul style="list-style:none;">'+
												'<li>Order no is : ' + transaction_id_string +'</li>'+
												'<li>Ticket name : ' + ticket_name +'</li>'+
												'<li>Booked at : ' + booked_time +'</li>'+
												'<li>Price per ticket is : ' + price +'</li>'+
												'<li>Booked by : ' + first_name + ' ' + last_name +'</li>'+
												'<li>Attendee email : ' + logged_in_user +'</li>'+
											'</ul>'+
										'</div>', 'html')
				msgAlternative.attach(d['msgText_%02d' % x])
				# Send the email (this example assumes SMTP authentication is required)
				'''username = 'cct.jagsirsingh@gmail.com'
				password = 'Admin@123#'
				server = smtplib.SMTP('smtp.gmail.com:587')
				server.ehlo()
				server.starttls()
				server.login(username,password)
				server.sendmail(strFrom, strTo, msgRoot.as_string())
				server.quit()'''
		return HttpResponse('success')
	except Exception as e:
		print e

####################Sub event registration########################

@csrf_exempt
def subevent_registration(request):
	try:
		sub_event_id=request.POST['sub_event_id']
		event_id=request.POST['event_id']
		quantity_list=[]
		quantity_dict={'selected':'','sub_event_type':'','subevent_id_id':'','timestamp_end':'','order_min':'','order_max':'',
			'ticket_name':'','ticket_id':'','event_id':'','pending_tickets':''}
		sub_events_info=SubEvent.objects.filter(sub_event_id=sub_event_id)
		for info in sub_events_info:
			sub_event_id=str(info.sub_event_id)
			sub_event_type=info.sub_event_type
			################# ticket date data ###################################
			if(sub_event_type=='Free'):
				ticket_info=FreeTickets.objects.filter(sub_event_id_id=sub_event_id)
				for tic_info in ticket_info:
					subevent_id_id=tic_info.sub_event_id_id
					tic_end_time=tic_info.timestamp_end
					min_ticket=tic_info.order_min
					max_ticket=tic_info.order_max
					tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
					ticket_name=tic_info.ticket_name
					ticket_id=tic_info.ticket_id
					tic_count=tic_info.ticket_count
			if(sub_event_type=='Paid'):
				ticket_info=PaidTickets.objects.filter(sub_event_id_id=sub_event_id)
				for tic_info in ticket_info:
					subevent_id_id=tic_info.sub_event_id_id
					tic_end_time=tic_info.timestamp_end
					min_ticket=tic_info.order_min
					max_ticket=tic_info.order_max
					tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
					ticket_name=tic_info.ticket_name
					ticket_id=tic_info.ticket_id
					tic_count=tic_info.ticket_count
			if(sub_event_type=='Donation'):
				ticket_info=DonationTickets.objects.filter(sub_event_id_id=sub_event_id)
				for tic_info in ticket_info:
					subevent_id_id=info.sub_event_id_id
					tic_end_time=tic_info.timestamp_end
					min_ticket=tic_info.order_min
					max_ticket=tic_info.order_max
					tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
					ticket_name=tic_info.ticket_name
					ticket_id=tic_info.ticket_id
					tic_count=tic_info.ticket_count
			tic_count1=int(tic_count)
			quantity_booked=0
			sub_book_info=TicketBooked.objects.filter(sub_event_id_id=sub_event_id)
			for info in sub_book_info:
				transaction_id=info.transaction_id
				ticket_booked1=int(info.quantity_booked)
				quantity_booked=quantity_booked+ticket_booked1
			pending_tickets=tic_count1-quantity_booked
			quantity_dict['sub_event_type']=sub_event_type
			quantity_dict['subevent_id_id']=subevent_id_id
			quantity_dict['event_id']=event_id
			quantity_dict['timestamp_end']=tic_time_close
			quantity_dict['order_min']=min_ticket
			quantity_dict['order_max']=max_ticket
			quantity_dict['ticket_name']=ticket_name
			quantity_dict['ticket_id']=ticket_id
			quantity_dict['pending_tickets']=pending_tickets
		quantity_list.append(quantity_dict.copy()) 
		html = render_to_string('myapp/subevent_registration.html', {'quantity_list':quantity_list })
		return HttpResponse(html)
	except Exception as e:
		print e

############## sub Quantity selected ####################
@csrf_exempt
def sub_quantity_selected(request):
	try:
		qua_list=[]
		qua_dict={'selected':''}
		select_val=request.POST['select_val']
		qua_dict['selected']=select_val
		qua_list.append(qua_dict.copy()) 
		html = render_to_string('myapp/sub_quantity_selected.html', {'qua_list':qua_list })
		return HttpResponse(html)
	except Exception as e:
		print e


############ sub register_ticket ###################
@csrf_exempt
def sub_register_ticket(request):
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		sub_event_id=request.POST['sub_eventid']
		event_id='0'
		sub_ticket_id=request.POST['sub_ticket_id']
		sub_ticket_name=request.POST['sub_ticket_name']
		selected_quantity=request.POST['selected_quantity']
		selected_quantity1=int(selected_quantity)
		subevent_data_list=[]
		subevent_data_dict={'ticket_id':'', 'ticket_name':'', 'selected_quantity':'', 'sub_event_id':'',
		'time':'','sub_event_name':'','price':'','image':'','description':'',
		'sub_event_type':'','start_time':'','end_time':'','organizer_name':'','organizer_description':'',
		'user_email':'','event_id':''}

		#quantity_booked=0
		#events_info=Event.objects.filter(event_id=event_id)
		#for info in events_info:
		#	user_email=info.user_email
		sub_events_info=SubEvent.objects.filter(sub_event_id=sub_event_id)
		for info in sub_events_info:
			sub_event_id=str(info.sub_event_id)
			time=info.timestamp
			time_start=info.timestamp_start
			time_end=info.timestamp_end
			time_register=time.strftime("%B %d, %Y, %I:%M %p")
			start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
			end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
			sub_event_name=info.sub_event_name
			price=info.price
			sub_event_type=info.sub_event_type
			image=info.image
			description=info.description
			organizer_name=info.organizer_name
			organizer_description=info.organizer_description
			subevent_data_dict['sub_event_id']=sub_event_id
			subevent_data_dict['time']=time_register
			subevent_data_dict['sub_event_name']=sub_event_name
			subevent_data_dict['price']=price
			subevent_data_dict['image']=image
			subevent_data_dict['description']=description
			subevent_data_dict['start_time']=start_time
			subevent_data_dict['end_time']=end_time
			subevent_data_dict['event_type']=sub_event_type
			subevent_data_dict['selected_quantity']=selected_quantity1
			subevent_data_dict['organizer_name']=organizer_name
			subevent_data_dict['organizer_description']=organizer_description
			subevent_data_dict['ticket_name']=sub_ticket_name
			subevent_data_dict['ticket_id']=sub_ticket_id
			subevent_data_dict['event_id']=event_id
			subevent_data_list.append(subevent_data_dict.copy())

			'''if(event_type=='Free'):
				ticket_info=FreeTickets.objects.filter(sub_event_id_id=sub_event_id)
				for tic_info in ticket_info:
					tic_end_time=tic_info.timestamp_end
					min_ticket=tic_info.order_min
					max_ticket=tic_info.order_max
					tic_count=tic_info.ticket_count
					tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
			if(event_type=='Paid'):
				ticket_info=PaidTickets.objects.filter(sub_event_id_id=sub_event_id)
				for tic_info in ticket_info:
					tic_end_time=tic_info.timestamp_end
					min_ticket=tic_info.order_min
					max_ticket=tic_info.order_max
					tic_count=tic_info.ticket_count
					tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")
			if(event_type=='Donation'):
				ticket_info=DonationTickets.objects.filter(sub_event_id_id=sub_event_id)
				for tic_info in ticket_info:
					tic_end_time=tic_info.timestamp_end
					min_ticket=tic_info.order_min
					max_ticket=tic_info.order_max
					tic_count=tic_info.ticket_count
					tic_time_close=tic_end_time.strftime("%B %d, %Y, %I:%M %p")'''
			'''tic_count1=int(tic_count)
			book_info=TicketBooked.objects.filter(event_id_id=event_id)
			for info in book_info:
				transaction_id=info.transaction_id
				event_id_id=str(info.event_id_id)
				ticket_booked1=int(info.quantity_booked)
				quantity_booked=quantity_booked+ticket_booked1
		tickets_pending=tic_count1-quantity_booked
		if (tickets_pending >= selected_quantity1):'''
			request.session['event_data_view']=subevent_data_list
			return HttpResponse('success')
		else:
			return HttpResponse('wrong')
	except Exception as e:
		print e

############## sub Register Order ####################
'''@csrf_exempt
def sub_register_order(request):
	print 'into sub_register_order'
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		subevent_data_view=request.session['subevent_data_view']
		return render(request,'myapp/sub_register_order.html',{'subevent_data_view':subevent_data_view,'logged_in_user':logged_in_user})
	except Exception as e:
		print e'''


################ sub_save_order #########################


'''@csrf_exempt
def sub_save_order(request):
	print 'into save_order'
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		ticket_id=request.POST['ticket_id']
		sub_event_id=request.POST['sub_event_id']
		event_id=request.POST['event_id']
		last_name=request.POST['last_name']
		first_name=request.POST['first_name']
		quantity=request.POST['quantity']
		ticket_name=request.POST['ticket_name']
		timestamp=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		order_data=TicketBooked.objects.create(tic_name=ticket_name,ticket_id=ticket_id,quantity_booked=quantity,
			timestamp=timestamp,first_name=first_name,last_name=last_name,
			email_address=logged_in_user,sub_event_id_id=sub_event_id,event_id_id=event_id)
		order_data.save()
		events_info=Event.objects.filter(event_id=event_id)
		for info in events_info:
			user_email=info.user_email
		sub_events_info=SubEvent.objects.filter(sub_event_id=sub_event_id)
		for info in sub_events_info:
			sub_event_id=str(info.sub_event_id)
			time=info.timestamp
			time_start=info.timestamp_start
			time_end=info.timestamp_end
			time_register=time.strftime("%B %d, %Y, %I:%M %p")
			start_time=time_start.strftime("%B %d, %Y, %I:%M %p")
			end_time=time_end.strftime("%B %d, %Y, %I:%M %p")
			sub_event_name=info.sub_event_name
			price=str(info.price)
			sub_event_type=info.sub_event_type
			image=info.image
			description=info.description
			organizer_name=info.organizer_name
			organizer_description=info.organizer_description
			ticked_booked_info=TicketBooked.objects.filter(sub_event_id_id=sub_event_id).order_by('-timestamp')[:1]
			for ticket in ticked_booked_info:
				transaction_id=int(ticket.transaction_id)
				transaction_id_string=str(transaction_id)
				booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
				first_name=ticket.first_name
				last_name=ticket.last_name
				quantity_book=int(ticket.quantity_booked)
				quantity_book_string=str(quantity_book)
				ticket_id=ticket.ticket_id
				sub_event_id=str(ticket.sub_event_id_id)
				ticket_name=ticket.tic_name
				d = {}
			email_address=request.POST['input_emails']
			print '>>>>>>>>>>>>>>>>>>>>>>>>>>>>',email_address
			guest_email=[i for i in email_address.strip('[]').split(',')]
			for x in range(0,len(guest_email)):
			#for x in range(0, quantity_book):
				img_id=str(transaction_id)+'-'+str(x)
				img = qrcode.make(img_id)
				#img_id=str(transaction_id)+'.'+str(x)
				filename=str(img_id)+'.png'
				img.save("myapp/static/qr/"+filename)
				
				strFrom = 'cct.jagsirsingh@gmail.com'
				strTo = guest_email[x]
				# Create the root message and fill in the from, to, and subject headers
				msgRoot = MIMEMultipart('related')
				msgRoot['Subject'] = 'test message'
				msgRoot['From'] = strFrom
				msgRoot['To'] = strTo
				msgRoot.preamble = 'This is a multi-part message in MIME format.'
				# Encapsulate the plain and HTML versions of the message body in an
				# 'alternative' part, so message agents can decide which they want to display.
				msgAlternative = MIMEMultipart('alternative')
				msgRoot.attach(msgAlternative)

				
				d['msgText_%02d' % x] = MIMEText('<div><b><h2>Welcome : <i> Sir/Madam </i></h2></b></div> '+
									'<div style="width:50%; float:left;"> Recently your ticket is booked for <strong>'+ sub_event_name +'</strong>'+
										'<div style="width:100%">'+
											'<div><h1>Ticket details :</h1></div>'+
											'<ul style="list-style:none;">'+
												'<li>Ticket Booked by : ' + first_name + ' ' + last_name + '</li>'
												'<li>Your order no is : ' + transaction_id_string +'</li>'+
												'<li>Ticket name : ' + ticket_name +'</li>'+
												'<li>Booked at : ' + booked_time +'</li>'+
												'<li>Price per ticket is : ' + price +'</li>'+
											'</ul>'+
										'</div>'+
										'<div style="width:100%">'+
											'<div><h1>Organizer :</h1></div>'+
											'<h4><b>' + sub_event_name + '</b> event is organized by <b>' + organizer_name + '<b></h4>'+
											'<h4> Here is the organizer Description</h4>'+
											organizer_description +
										'</div>'+
										'<div style="width:100%">'+
											'<div><h1>Time and venue :</h1></div>'+
											'<ul style="list-style:none;">'+
												'<li>Event start at : ' + start_time +'</li>'+
												'<li>Event ends at : ' + end_time +'</li>'+
											'</ul>'+
										'</div>'+
									'</div>'+
									'<div style="width:50%; float:left;"><b> QRcode Image is: </b><img src="cid:image1"></div>'+
									'<div style="width:100%; float:left;"> If you have and query you can email the organizer at ' + user_email +'</div>', 'html')
				msgAlternative.attach(d['msgText_%02d' % x])
				# This example assumes the image is in the current directory
				fp = open('/home/kashyap/mydjango/events_app/events_app/myapp/static/qr/'+filename, 'rb')
				msgImage = MIMEImage(fp.read())
				fp.close()
				# Define the image's ID as referenced above
				msgImage.add_header('Content-ID', '<image1>')
				msgRoot.attach(msgImage)
				# Send the email (this example assumes SMTP authentication is required)
				username = 'cct.jagsirsingh@gmail.com'
				password = 'Admin@123#'
				server = smtplib.SMTP('smtp.gmail.com:587')
				server.ehlo()
				server.starttls()
				server.login(username,password)
				server.sendmail(strFrom, strTo, msgRoot.as_string())
				server.quit()
			################### Mail to event Organizer ##################################
			# Define these once; use them twice!
			strFrom = 'cct.jagsirsingh@gmail.com'
			strTo = user_email
			# Create the root message and fill in the from, to, and subject headers
			msgRoot = MIMEMultipart('related')
			msgRoot['Subject'] = 'Ticket booked for '+ sub_event_name
			msgRoot['From'] = strFrom
			msgRoot['To'] = strTo
			msgRoot.preamble = 'This is a multi-part message in MIME format.'
			# Encapsulate the plain and HTML versions of the message body in an
			# 'alternative' part, so message agents can decide which they want to display.
			msgAlternative = MIMEMultipart('alternative')
			msgRoot.attach(msgAlternative)

			
			d['msgText_%02d' % x] = MIMEText('<div><b><h2>Hello : <i>' + user_email + '</i></h2></b></div> '+
								'<div style="width:50%; float:left;"> Recently' + quantity_book_string +' tickets are booked from <strong>'+ sub_event_name +'</strong>'+
									'<div style="width:100%">'+
										'<div><h1>Booking details :</h1></div>'+
										'<ul style="list-style:none;">'+
											'<li>Order no is : ' + transaction_id_string +'</li>'+
											'<li>Ticket name : ' + ticket_name +'</li>'+
											'<li>Booked at : ' + booked_time +'</li>'+
											'<li>Price per ticket is : ' + price +'</li>'+
											'<li>Booked by : ' + first_name + ' ' + last_name +'</li>'+
											'<li>Attendee email : ' + logged_in_user +'</li>'+
										'</ul>'+
									'</div>', 'html')
			msgAlternative.attach(d['msgText_%02d' % x])
			# Send the email (this example assumes SMTP authentication is required)
			username = 'cct.jagsirsingh@gmail.com'
			password = 'Admin@123#'
			server = smtplib.SMTP('smtp.gmail.com:587')
			server.ehlo()
			server.starttls()
			server.login(username,password)
			server.sendmail(strFrom, strTo, msgRoot.as_string())
			server.quit()
			
		return HttpResponse('success')
	except Exception as e:
		print e'''

############## View your sub events tickets ####################
'''@csrf_exempt
def view_sub_ticket(request):
	print 'here is view_ticket>>>>>>>>'
	try:
		try:
			logged_in_user=request.user
		except Exception as e:
			logged_in_user=''
		#event_id=request.POST['event_id']
		#ticket_id=request.POST['ticket_id']
		#email_address=request.POST['email_address']
		book_tic_list=[]
		book_tic_dict={'transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'','quantity_book':'',
						'booked_ticket_id':'','booked_sub_event_id':'','booked_ticket_name':''}
		ticked_booked_info=TicketBooked.objects.filter(email_address=logged_in_user).order_by('-timestamp')
		for ticket in ticked_booked_info:
			transaction_id=str(ticket.transaction_id)
			booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
			first_name=ticket.first_name
			last_name=ticket.last_name
			qunatity_book=ticket.quantity_booked
			ticket_id=ticket.ticket_id
			sub_event_id=str(ticket.sub_event_id_id)
			ticket_name=ticket.tic_name
			book_tic_dict['transaction_id']=transaction_id
			book_tic_dict['booked_time']=booked_time
			book_tic_dict['first_name']=first_name
			book_tic_dict['last_name']=last_name
			book_tic_dict['quantity_book']=qunatity_book
			book_tic_dict['booked_ticket_id']=ticket_id
			book_tic_dict['booked_sub_event_id']=sub_event_id
			book_tic_dict['booked_ticket_name']=ticket_name

			book_tic_list.append(book_tic_dict.copy())
		request.session['booked_data']=book_tic_list
		return HttpResponse('success')
	except Exception as e:
		print e'''

############## User tickets detail ####################
@csrf_exempt
def user_tickets_detail(request):
	try:
		try:
			logged_in_user1=request.user
			logged_in_user=str(logged_in_user1)
		except Exception as e:
			logged_in_user=''

		event_data_view_list=[]
		subevent_data_view_list=[]
		booked_ticket_view_list=[]
		booked_event_data_dict={'sub_event_id':'','event_id':'','city':'','country':'','event_type':'','time':'','name':'','price':'','image':'','description':'','category':''}
		booked_subevent_data_dict={'sub_event_id':'','sub_event_type':'','time':'','sub_event_name':'','price':'','image':'','description':''}
		booket_ticket_view_dict={'transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'','quantity_book':'',
						'booked_ticket_id':'','booked_event_id':'','booked_ticket_name':'', 'booked_sub_event_id':''}
		ticked_booked_info=TicketBooked.objects.filter(email_address=logged_in_user).order_by('-timestamp')
		for ticket in ticked_booked_info:
			transaction_id=ticket.transaction_id
			booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
			first_name=ticket.first_name
			last_name=ticket.last_name
			qunatity_book=ticket.quantity_booked
			ticket_id=ticket.ticket_id
			event_id=ticket.event_id_id
			sub_event_id=ticket.sub_event_id_id
			ticket_name=ticket.tic_name
			booket_ticket_view_dict['transaction_id']=transaction_id
			booket_ticket_view_dict['booked_time']=booked_time
			booket_ticket_view_dict['first_name']=first_name
			booket_ticket_view_dict['last_name']=last_name
			booket_ticket_view_dict['quantity_book']=qunatity_book
			booket_ticket_view_dict['booked_ticket_id']=ticket_id
			booket_ticket_view_dict['booked_event_id']=event_id
			booket_ticket_view_dict['booked_ticket_name']=ticket_name
			booket_ticket_view_dict['booked_sub_event_id']=sub_event_id

			booked_ticket_view_list.append(booket_ticket_view_dict.copy())
			if (sub_event_id == 0):
				event_info=Event.objects.filter(event_id=event_id).order_by('-timestamp')
				for info in event_info:
					event_id=info.event_id
					city=info.city
					#sub_event_id='0'
					country=info.country
					time=info.timestamp
					time_register=time.strftime("%B %d, %Y, %I:%M %p")
					start_time=info.timestamp_start.strftime("%B %d, %Y, %I:%M %p")
					end_time=info.timestamp_end.strftime("%B %d, %Y, %I:%M %p")
					event_name=info.event_name
					name=info.event_name
					price=info.price
					image=info.image
					description=info.description
					category_id=info.category_id
					event_type=info.event_type
					category_info=Category.objects.filter(category_id=category_id)
					for cat_info in category_info:
						category_name=cat_info.category_name
						booked_event_data_dict['event_id']=event_id
						#booked_event_data_dict['sub_event_id']=sub_event_id
						booked_event_data_dict['city']=city
						booked_event_data_dict['country']=country
						booked_event_data_dict['time']=time_register
						booked_event_data_dict['name']=name
						booked_event_data_dict['price']=price
						booked_event_data_dict['image']=image
						booked_event_data_dict['description']=description
						booked_event_data_dict['category']=category_name
						booked_event_data_dict['event_type']=event_type
						booked_event_data_dict['start_time']=start_time
						booked_event_data_dict['end_time']=end_time
						booked_event_data_dict['event_name']=event_name
						event_data_view_list.append(booked_event_data_dict.copy())
						event_data_view_list=[i for n, i in enumerate(event_data_view_list) if i not in event_data_view_list[n + 1:]]
			else:
				event_info=SubEvent.objects.filter(sub_event_id=sub_event_id).order_by('-timestamp')
				for info in event_info:
					sub_event_id=str(info.sub_event_id)
					time=info.timestamp
					time_register=time.strftime("%B %d, %Y, %I:%M %p")
					start_time=info.timestamp_start.strftime("%B %d, %Y, %I:%M %p")
					end_time=info.timestamp_end.strftime("%B %d, %Y, %I:%M %p")
					sub_event_name=info.sub_event_name
					price=info.price
					image=info.image
					description=info.description
					sub_event_type=info.sub_event_type
					
					booked_subevent_data_dict['sub_event_id']=sub_event_id
					booked_subevent_data_dict['time']=time_register
					booked_subevent_data_dict['sub_event_name']=sub_event_name
					booked_subevent_data_dict['price']=price
					booked_subevent_data_dict['image']=image
					booked_subevent_data_dict['description']=description
					booked_subevent_data_dict['sub_event_type']=sub_event_type
					booked_subevent_data_dict['start_time']=start_time
					booked_subevent_data_dict['end_time']=end_time
					subevent_data_view_list.append(booked_subevent_data_dict.copy())
					subevent_data_view_list=[i for n, i in enumerate(subevent_data_view_list) if i not in subevent_data_view_list[n + 1:]]
		#booked_ticket_data=request.session['booked_data']
		ticket_info=TicketBooked.objects.filter(email_address=logged_in_user)
		total_tickets=len(ticket_info)
		request.session['total_tickets']=total_tickets
		return render(request,'myapp/user_tickets_detail.html',{'total_tickets':total_tickets, 'subevent_data_view':subevent_data_view_list,'event_data_view':event_data_view_list,'booked_ticket_data':booked_ticket_view_list,'logged_in_user':logged_in_user})
	except Exception as e:
		print e


############ Particular booked ticket view ###################
'''@csrf_exempt
def book_particular_ticket(request,id):
	try:
		particular_ticket_id=id
		booked_subevent_data_list=[]
		booked_ticket_list=[]
		booked_subevent_data_dict={'subevent_id':'','sub_event_type':'','time':'','sub_event_name':'','price':'','image':'','description':''}
		booked_ticket_dict={'transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'','quantity_book':'',
						'booked_ticket_id':'','booked_subevent_id':'','booked_ticket_name':'','start_time':'','end_time':'','sub_event_name':''}
		booked_ticket_info=SubTicketBooked.objects.filter(transaction_id=particular_ticket_id)
		for ticket in booked_ticket_info:
			booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
			first_name=ticket.first_name
			last_name=ticket.last_name
			qunatity_book=ticket.quantity_booked
			ticket_id=ticket.ticket_id
			sub_event_id=str(ticket.sub_event_id_id)
			ticket_name=ticket.tic_name
			booked_ticket_dict['transaction_id']=particular_ticket_id
			booked_ticket_dict['booked_time']=booked_time
			booked_ticket_dict['first_name']=first_name
			booked_ticket_dict['last_name']=last_name
			booked_ticket_dict['quantity_book']=qunatity_book
			booked_ticket_dict['booked_ticket_id']=ticket_id
			booked_ticket_dict['booked_subevent_id']=sub_event_id
			booked_ticket_dict['booked_ticket_name']=ticket_name

			booked_ticket_list.append(booked_ticket_dict.copy())
			event_info=SubEvent.objects.filter(sub_event_id=sub_event_id)
			for info in event_info:
				sub_event_id=str(info.sub_event_id)
				time=info.timestamp
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=info.timestamp_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=info.timestamp_end.strftime("%B %d, %Y, %I:%M %p")
				sub_event_name=info.sub_event_name
				price=info.price
				image=info.image
				description=info.description
				sub_event_type=info.sub_event_type
				booked_subevent_data_dict['sub_event_id']=sub_event_id
				booked_subevent_data_dict['time']=time_register
				booked_subevent_data_dict['sub_event_name']=sub_event_name
				booked_subevent_data_dict['price']=price
				booked_subevent_data_dict['image']=image
				booked_subevent_data_dict['description']=description
				booked_subevent_data_dict['sub_event_type']=sub_event_type
				booked_subevent_data_dict['start_time']=start_time
				booked_subevent_data_dict['end_time']=end_time
				booked_subevent_data_list.append(booked_subevent_data_dict.copy())
	
		try:
			user=request.user
		except Exception as e:
			user=''
		return render(request,'myapp/book_particular_ticket.html',{'booked_event_data_list':booked_subevent_data_list,'logged_in_user':user,
			'booked_ticket_list':booked_ticket_list})
	except Exception as e:
		print('e',e)'''


############ Edit particular ticket ###################
@csrf_exempt
def sub_edit_particular_ticket(request):
	try:
		owner_first_name=request.POST['owner_first_name']
		owner_last_name=request.POST['owner_last_name']
		owner_email_address=request.POST['owner_email_address']
		transaction_id=request.POST['transaction_id']
		SubTicketBooked.objects.filter(transaction_id=transaction_id).update(first_name=owner_first_name,last_name=owner_last_name,
				email_address=owner_email_address)
		return HttpResponse('success')
	except Exception as e:
		print('e',e)


############ Delete particular ticket ###################
@csrf_exempt
def sub_delete_particular_ticket(request):
	try:
		transaction_id=request.POST['transaction_id']
		ticket = SubTicketBooked.objects.get(transaction_id=transaction_id)
		ticket.delete()
		return HttpResponse('success')
	except Exception as e:
		print e


#################To get Qr image####################
@csrf_exempt
def view_qr_image(request):
	try:
		
		particular_ticket_id=request.POST['transaction_id']
		event_image_qr_list=[]
		subevent_image_qr_list=[]
		booked_event_data_list=[]
		booked_ticket_list=[]
		booked_event_data_dict={'transaction_id':'','college_name':'','organizer_desc':'','event_id':'','city':'','country':'','event_type':'','time':'','name':'','price':'','image':'','description':'','category':''}
		booked_ticket_dict={'booked_sub_event_id':'','transaction_id':'', 'booked_time':'', 'first_name':'', 'last_name':'','quantity_book':'',
						'booked_ticket_id':'','booked_event_id':'','booked_ticket_name':'','start_time':'','end_time':'','event_name':''}
		booked_subevent_data_dict={'transaction_id':'','organizer_desc':'','sub_event_id':'','event_type':'','time':'',
		'event_name':'','price':'','image':'','description':''}
		event_image_qr_dict={'sub_event_id':'','event_id':'','image_id':'','image_path':''}
		subevent_image_qr_dict={'sub_event_id':'','event_id':'','image_id':'','image_path':'','guest_email':''}
		transaction_id1=particular_ticket_id
		booked_ticket_info=TicketBooked.objects.filter(transaction_id=particular_ticket_id)
		for ticket in booked_ticket_info:
			booked_time=ticket.timestamp.strftime('%Y-%m-%d %H:%M:%S')
			first_name=ticket.first_name
			last_name=ticket.last_name
			qunatity_book=ticket.quantity_booked
			ticket_id=ticket.ticket_id
			event_id=str(ticket.event_id_id)
			sub_event_id=str(ticket.sub_event_id_id)
			ticket_name=ticket.tic_name
			booked_ticket_dict['transaction_id']=particular_ticket_id
			booked_ticket_dict['booked_time']=booked_time
			booked_ticket_dict['first_name']=first_name
			booked_ticket_dict['last_name']=last_name
			booked_ticket_dict['quantity_book']=qunatity_book
			booked_ticket_dict['booked_ticket_id']=ticket_id
			booked_ticket_dict['booked_event_id']=event_id
			booked_ticket_dict['booked_sub_event_id']=sub_event_id
			booked_ticket_dict['booked_ticket_name']=ticket_name

			booked_ticket_list.append(booked_ticket_dict.copy())
			event_info=Event.objects.filter(event_id=event_id)
			for info in event_info:
				event_id_int=info.event_id
				event_id=str(info.event_id)
				city=info.city
				country=info.country
				time=info.timestamp
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=info.timestamp_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=info.timestamp_end.strftime("%B %d, %Y, %I:%M %p")
				event_name=info.event_name
				name=info.organizer_name
				organizer_desc=info.organizer_description
				price=info.price
				image=info.image
				college_name=info.college_name
				description=info.description
				category_id=info.category_id
				event_type=info.event_type
				category_info=Category.objects.filter(category_id=category_id)
				for cat_info in category_info:
					category_name=cat_info.category_name
					booked_event_data_dict['event_id']=event_id
					booked_event_data_dict['city']=city
					booked_event_data_dict['country']=country
					booked_event_data_dict['time']=time_register
					booked_event_data_dict['name']=name
					booked_event_data_dict['organizer_desc']=organizer_desc
					booked_event_data_dict['college_name']=college_name
					booked_event_data_dict['price']=price
					booked_event_data_dict['image']=image
					booked_event_data_dict['description']=description
					booked_event_data_dict['category']=category_name
					booked_event_data_dict['event_type']=event_type
					booked_event_data_dict['start_time']=start_time
					booked_event_data_dict['end_time']=end_time
					booked_event_data_dict['event_name']=event_name
					booked_event_data_list.append(booked_event_data_dict.copy())
			sub_event_info=SubEvent.objects.filter(sub_event_id=sub_event_id)
			for info in sub_event_info:
				sub_event_id=str(info.sub_event_id)
				time=info.timestamp
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				start_time=info.timestamp_start.strftime("%B %d, %Y, %I:%M %p")
				end_time=info.timestamp_end.strftime("%B %d, %Y, %I:%M %p")
				sub_event_name=info.sub_event_name
				organizer_name=info.organizer_name
				organizer_desc=info.organizer_description
				price=info.price
				image=info.image
				description=info.description
				sub_event_type=info.sub_event_type
				
				booked_subevent_data_dict['sub_event_id']=sub_event_id
				booked_subevent_data_dict['time']=time_register
				booked_subevent_data_dict['event_name']=sub_event_name
				booked_subevent_data_dict['organizer_desc']=organizer_desc
				
				booked_subevent_data_dict['price']=price
				booked_subevent_data_dict['image']=image
				booked_subevent_data_dict['description']=description
				booked_subevent_data_dict['organizer_name']=organizer_name
				booked_subevent_data_dict['event_type']=sub_event_type
				booked_subevent_data_dict['start_time']=start_time
				booked_subevent_data_dict['end_time']=end_time
				booked_event_data_list.append(booked_subevent_data_dict.copy())
		#event_id=request.POST['event_id']
		#sub_event_id = request.POST['sub_event_id']
		print 'here'
		if (sub_event_id == '0'):
			print 'into'
			image_info=ImageQR.objects.filter(event_id_id=event_id).filter(transaction_id=transaction_id1)
			for info in image_info:
				event_id=info.event_id_id
				sub_event_id=info.sub_event_id_id
				image_id=str(info.image_id)
				transaction_id=info.transaction_id_id
				image_path=info.image_path
				guest_email=info.guest_email
				event_image_qr_dict['event_id']=event_id
				event_image_qr_dict['image_id']=image_id
				event_image_qr_dict['image_path']=image_path
				event_image_qr_dict['transaction_id']=transaction_id
				event_image_qr_dict['guest_email']=guest_email
				event_image_qr_list.append(event_image_qr_dict.copy())
				request.session['event_qrimage_view']=event_image_qr_list

		else:
			image_info=ImageQR.objects.filter(sub_event_id_id=sub_event_id).filter(transaction_id=transaction_id1)
			for info in image_info:
				event_id=info.event_id_id
				sub_event_id=info.sub_event_id_id
				image_id=str(info.image_id)
				transaction_id=info.transaction_id_id
				image_path=info.image_path
				guest_email=info.guest_email
				event_image_qr_dict['sub_event_id']=sub_event_id
				event_image_qr_dict['image_id']=image_id
				event_image_qr_dict['image_path']=image_path
				event_image_qr_dict['transaction_id']=transaction_id
				event_image_qr_dict['guest_email']=guest_email
				event_image_qr_list.append(event_image_qr_dict.copy())
				request.session['event_qrimage_view']=event_image_qr_list
		request.session['booked_event_data_list']=booked_event_data_list
		request.session['booked_ticket_list']=booked_ticket_list

		
		return HttpResponse('success')
	except Exception as e:
		print e


######################Images in pdf###########################


'''def dm_monthly(request):
    html  = render_to_string('myapp/booked_particular_ticket.html', { 'pagesize' : 'A4', }, context_instance=RequestContext(request))
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources )
    if not pdf.err:
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
    return HttpResponse('Gremlins ate your pdf! %s' % cgi.escape(html))

def fetch_resources(uri, rel):
    path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))

    return path'''

##########################Saved Events############################


# @csrf_exempt
# def saved_events(request):
# 	try:
# 		event_id=request.POST['event_id']
# 		user_name=request.user.email
# 		saved_flag=int(request.POST['saved_flag'])
# 		users_info=SavedEvent.objects.all()
# 		user_info=len(users_info)
# 		if user_info >= 1:
# 			save_info1=SavedEvent.objects.filter(event_id_id=event_id).filter(user_name=user_name)
# 			save_info=len(save_info1)
# 			if save_info >= 1:
# 				SavedEvent.objects.filter(event_id_id=event_id).filter(user_name=user_name).update(saved_flag=saved_flag)
# 			else:
# 				user=SavedEvent.objects.create(event_id_id=event_id,user_name=user_name,saved_flag=saved_flag)
# 				user.save()
# 			return HttpResponse('success')
# 		else:
# 			user=SavedEvent.objects.create(event_id_id=event_id,user_name=user_name,saved_flag=saved_flag)
# 			user.save()
# 			return HttpResponse('success')
# 	except Exception as e:
# 		print('error',e)

########################## Update User Information ############################


@csrf_exempt
def update_user_info(request):
	try:
		user=request.user
		user_id=request.user.id
		prefix=request.POST['prefix']
		first_name=request.POST['first_name']
		last_name=request.POST['last_name']
		suffix=request.POST['suffix']
		home_phone=request.POST['home_phone']
		cell_phone=request.POST['cell_phone']
		job_title=request.POST['job_title']
		company=request.POST['company']
		website=request.POST['website']
		blog=request.POST['blog']
		address=request.POST['address']
		country=request.POST['countryId']
		state=request.POST['stateId']
		city=request.POST['cityId']
		a_zip=request.POST['zip']
		b_address=request.POST['b_address']
		country1=request.POST['countryId1']
		state1=request.POST['stateId1']
		city1=request.POST['cityId1']
		b_zip=request.POST['b_zip']
		s_address=request.POST['s_address']
		country2=request.POST['countryId2']
		state2=request.POST['stateId2']
		city2=request.POST['cityId2']
		s_zip=request.POST['s_zip']
		w_address=request.POST['w_address']
		country3=request.POST['countryId3']
		state3=request.POST['stateId3']
		city3=request.POST['cityId3']
		w_postal=request.POST['w_postal']
		birth_month=request.POST['birth_month']
		birth_day=request.POST['birth_day']
		birth_year=request.POST['birth_year']
		dob=birth_year + '-' + birth_month + '-' + birth_day
		gender=request.POST['gender']
		age=request.POST['age']
		profile_pic=request.POST['profile_pic']
		timestamp_now=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		User.objects.filter(email=user).update(first_name=first_name,last_name=last_name)
		account_info=userAccount.objects.filter(user_id=user_id)
		print 'account_info>>>>>>',account_info
		if account_info:
			userAccount.objects.update(prefix=prefix,sufix=suffix,home_phone=home_phone,
										cell_phone=cell_phone,job_title=job_title,company=company,
										website=website,blog=blog,home_address=address,
										home_country=country,home_state=state,home_city=city,
										home_zip_code=a_zip,billing_address=b_address,billing_country=country1,
										billing_state=state1,billing_city=city1,billing_zip_code=b_zip,
										shipping_address=s_address,shipping_country=country2,shipping_state=state2,
										shipping_city=city2,shipping_zip_code=s_zip,work_address=w_address,
										work_country=country3,work_state=state3,work_city=city3,
										work_zip_code=w_postal,gender=gender,age=age,dob=dob,timestamp=timestamp_now,
										profile_pic=profile_pic)
		else:
			user = userAccount.objects.create(user_id_id=user_id,prefix=prefix,sufix=suffix,home_phone=home_phone,
										cell_phone=cell_phone,job_title=job_title,company=company,
										website=website,blog=blog,home_address=address,
										home_country=country,home_state=state,home_city=city,
										home_zip_code=a_zip,billing_address=b_address,billing_country=country1,
										billing_state=state1,billing_city=city1,billing_zip_code=b_zip,
										shipping_address=s_address,shipping_country=country2,shipping_state=state2,
										shipping_city=city2,shipping_zip_code=s_zip,work_address=w_address,
										work_country=country3,work_state=state3,work_city=city3,
										work_zip_code=w_postal,gender=gender,age=age,dob=dob,timestamp=timestamp,
										profile_pic=profile_pic)
			user.save()
		return HttpResponse('success')
	except Exception as e:
		print('error',e)