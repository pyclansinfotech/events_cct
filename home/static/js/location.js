
    function ajaxCall() {
        this.send = function(data, url, method, success, type) {
          type = type||'json';
          var successRes = function(data) {
              success(data);
          };

          var errorRes = function(e) {
              console.log(e);
              // alert("Error found \nError Code: "+e.status+" \nError Message: "+e.statusText);
          };
            $.ajax({
                url: url,
                type: method,
                data: data,
                success: successRes,
                error: errorRes,
                dataType: type,
                timeout: 60000
            });

          }

        }

function locationInfo() {
    var rootUrl = "http://lab.iamrohit.in/php_ajax_country_state_city_dropdown/api.php";
    var call = new ajaxCall();
    this.getCities = function(id) {
        $(".cities option:gt(0)").remove();
        var url = rootUrl+'?type=getCities&stateId=' + id;
        var method = "post";
        var data = {};
        $('.cities').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.cities').find("option:eq(0)").html("Select City");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.cities').append(option);
                });
                $(".cities").prop("disabled",false);
            }
            else{
                 alert(data.msg);
            }
        });
    };

    this.getCities1 = function(id) {
        $(".cities1 option:gt(0)").remove();
        var url = rootUrl+'?type=getCities&stateId=' + id;
        var method = "post";
        var data = {};
        $('.cities1').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.cities1').find("option:eq(0)").html("Select City");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.cities1').append(option);
                });
                $(".cities1").prop("disabled",false);
            }
            else{
                 alert(data.msg);
            }
        });
    };

    this.getCities2 = function(id) {
        $(".cities2 option:gt(0)").remove();
        var url = rootUrl+'?type=getCities&stateId=' + id;
        var method = "post";
        var data = {};
        $('.cities2').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.cities2').find("option:eq(0)").html("Select City");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.cities2').append(option);
                });
                $(".cities2").prop("disabled",false);
            }
            else{
                 alert(data.msg);
            }
        });
    };

    this.getCities3 = function(id) {
        $(".cities3 option:gt(0)").remove();
        var url = rootUrl+'?type=getCities&stateId=' + id;
        var method = "post";
        var data = {};
        $('.cities3').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.cities3').find("option:eq(0)").html("Select City");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.cities3').append(option);
                });
                $(".cities3").prop("disabled",false);
            }
            else{
                 alert(data.msg);
            }
        });
    };

    this.getStates = function(id) {
        $(".states option:gt(0)").remove(); 
        $(".cities option:gt(0)").remove(); 
        var url = rootUrl+'?type=getStates&countryId=' + id;
        var method = "post";
        var data = {};
        $('.states').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.states').find("option:eq(0)").html("Select State");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.states').append(option);
                });
                $(".states").prop("disabled",false);
            }
            else{
                alert(data.msg);
            }
        }); 
    };

    this.getStates1 = function(id) {
        $(".states1 option:gt(0)").remove(); 
        $(".cities1 option:gt(0)").remove(); 
        var url = rootUrl+'?type=getStates&countryId=' + id;
        var method = "post";
        var data = {};
        $('.states1').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.states1').find("option:eq(0)").html("Select State");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.states1').append(option);
                });
                $(".states1").prop("disabled",false);
            }
            else{
                alert(data.msg);
            }
        }); 
    };

    this.getStates2 = function(id) {
        $(".states2 option:gt(0)").remove(); 
        $(".cities2 option:gt(0)").remove(); 
        var url = rootUrl+'?type=getStates&countryId=' + id;
        var method = "post";
        var data = {};
        $('.states2').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.states2').find("option:eq(0)").html("Select State");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.states2').append(option);
                });
                $(".states2").prop("disabled",false);
            }
            else{
                alert(data.msg);
            }
        }); 
    };

    this.getStates3 = function(id) {
        $(".states3 option:gt(0)").remove(); 
        $(".cities3 option:gt(0)").remove(); 
        var url = rootUrl+'?type=getStates&countryId=' + id;
        var method = "post";
        var data = {};
        $('.states3').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.states3').find("option:eq(0)").html("Select State");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.states3').append(option);
                });
                $(".states3").prop("disabled",false);
            }
            else{
                alert(data.msg);
            }
        }); 
    };

    this.getCountries = function() {
        var url = rootUrl+'?type=getCountries';
        var method = "post";
        var data = {};
        $('.countries').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.countries').find("option:eq(0)").html("Select Country");
            //console.log(data);
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.countries').append(option);
                });
                $(".countries").prop("disabled",false);
            }
            else{
                //alert(data.msg);
            }
        }); 
    };
    
    this.getCountries1 = function() {
        var url = rootUrl+'?type=getCountries';
        var method = "post";
        var data = {};
        $('.countries1').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.countries1').find("option:eq(0)").html("Select Country");
            //console.log(data);
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.countries1').append(option);
                });
                $(".countries1").prop("disabled",false);
            }
            else{
                //alert(data.msg);
            }
        }); 
    };

    this.getCountries2 = function() {
        var url = rootUrl+'?type=getCountries';
        var method = "post";
        var data = {};
        $('.countries2').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.countries2').find("option:eq(0)").html("Select Country");
            //console.log(data);
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.countries2').append(option);
                });
                $(".countries2").prop("disabled",false);
            }
            else{
                //alert(data.msg);
            }
        }); 
    };

    this.getCountries3 = function() {
        var url = rootUrl+'?type=getCountries';
        var method = "post";
        var data = {};
        $('.countries3').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.countries3').find("option:eq(0)").html("Select Country");
            //console.log(data);
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    var option = $('<option />');
                    option.attr('value', key).text(val);
                    $('.countries3').append(option);
                });
                $(".countries3").prop("disabled",false);
            }
            else{
                //alert(data.msg);
            }
        }); 
    };

}

$(function() {
var loc = new locationInfo();
loc.getCountries();
 $(".countries").on("change", function(ev) {
        var countryId = $(this).val();
        if(countryId != ''){
        loc.getStates(countryId);
        }
        else{
            $(".states option:gt(0)").remove();
        }
    });
 $(".states").on("change", function(ev) {
        var stateId = $(this).val();
        if(stateId != ''){
        loc.getCities(stateId);
        }
        else{
            $(".cities option:gt(0)").remove();
        }
    });
});

$(function() {
var loc = new locationInfo();
loc.getCountries1();
 $(".countries1").on("change", function(ev) {
        var countryId = $(this).val();
        if(countryId != ''){
        loc.getStates1(countryId);
        }
        else{
            $(".states1 option:gt(0)").remove();
        }
    });
 $(".states1").on("change", function(ev) {
        var stateId = $(this).val();
        if(stateId != ''){
        loc.getCities1(stateId);
        }
        else{
            $(".cities1 option:gt(0)").remove();
        }
    });
});

$(function() {
var loc = new locationInfo();
loc.getCountries2();
 $(".countries2").on("change", function(ev) {
        var countryId = $(this).val();
        if(countryId != ''){
        loc.getStates2(countryId);
        }
        else{
            $(".states2 option:gt(0)").remove();
        }
    });
 $(".states2").on("change", function(ev) {
        var stateId = $(this).val();
        if(stateId != ''){
        loc.getCities2(stateId);
        }
        else{
            $(".cities2 option:gt(0)").remove();
        }
    });
});

$(function() {
var loc = new locationInfo();
loc.getCountries3();
 $(".countries3").on("change", function(ev) {
        var countryId = $(this).val();
        if(countryId != ''){
        loc.getStates3(countryId);
        }
        else{
            $(".states3 option:gt(0)").remove();
        }
    });
 $(".states3").on("change", function(ev) {
        var stateId = $(this).val();
        if(stateId != ''){
        loc.getCities3(stateId);
        }
        else{
            $(".cities3 option:gt(0)").remove();
        }
    });
});


