/************************ accordion button account settings************************/

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}


/*********************** accordion button browse events ***************************/

var acc = document.getElementsByClassName("accordion_browse");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}


/********************** date picker *******************/

$( function() {
    $( ".datepicker" ).datepicker();
  } );


/**********************************************************/
function openCity(evt, cityName) {
    console.log('evt',evt,cityName)
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    console.log(tabcontent,tabcontent.length)
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
     //    console.log(cityName,cityName.length)
     document.getElementById(cityName).style.display = "block";
     evt.currentTarget.className += " active";
}




function init_map(){var myOptions = {zoom:10,center:new google.maps.LatLng(30.7333148,76.7794179),mapTypeId: google.maps.MapTypeId.TERRAIN};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(30.7333148,76.7794179)});infowindow = new google.maps.InfoWindow({content:'<strong>Title</strong><br>Chandigarh, India<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);


/***************** create tickets Start here *******************************/


$(document).ready(function() {

var MaxInputs       = 100; //maximum extra input boxes allowed
var Ticket_button   = $("#Ticket_button"); //Input boxes wrapper ID
var AddButton       = $("#Free_tickets_input");
var AddButton1       = $("#Paid_tickets_input");
var AddButton2       = $("#Donation_input"); //Add button ID

var x = Ticket_button.length; //initlal text box count
var FieldCount=1; //to keep track of text box added
/******************free tickets *************************/
//on add input button click
$(AddButton).click(function (e) {
        //max input box allowed
        if(x <= MaxInputs) {
            FieldCount++; //text box added increment
            //add input box
            $(Ticket_button).append('<div><input type="text" name="fticket_name[]" id="fname_field_'+ FieldCount +'"  placeholder="Early Bird, RSVP..."/><input type="text" name="fticket_num[]" id="fnumber_field_'+FieldCount+'" placeholder="100"/><label>Free</label><input type="hidden" name="f_money[]" id="fmoney_field_'+FieldCount+'" value="Free"><a href="#" id="removeclass">Remove</a></div>');
            x++; //text box increment
            
            $("#Add_More_FileId").show();
            
            $('Free_tickets_input').html("Add field");
            
            // Delete the "add"-link if there is 3 fields.
            if(x == 100) {
                $("#Add_More_FileId").hide();
                $("#lineBreak").html("<br>");
            }
        }
        return false;
});

$("body").on("click","#removeclass", function(e){ //user click on remove text
        if( x > 1 ) {
                $(this).parent('div').remove(); //remove text box
                x--; //decrement textbox
            
                $("#Add_More_FileId").show();
            
                $("#lineBreak").html("");
            
                // Adds the "add" link again when a field is removed.
                $('Free_tickets_input').html("Add field");
        }
    return false;
}) 


/*********************************paid tickets *******************************/

$(AddButton1).click(function (e) {
        //max input box allowed
        if(x <= MaxInputs) {
            FieldCount++; //text box added ncrement
            //add input box
            $(Ticket_button).append('<div><input type="text" name="pticket_name[]" id="pname_field_'+ FieldCount +'" placeholder="Early Bird, RSVP..."/><input type="text" name="pticket_num[]" id="pnumber_field_'+FieldCount+'" placeholder="100"/><input type="text" name="pticket_money[]" id="pmoney_field_'+FieldCount+'" placeholder="$ 15.00"/> <a href="#" id="removeclass1">Remove</a></div>');
            x++; //text box increment
            
            $("#Add_More_FileId").show();
            
            $('Paid_tickets_input').html("Add field");
            
            // Delete the "add"-link if there is 3 fields.
            if(x == 100) {
                $("#Add_More_FileId").hide();
                $("#lineBreak").html("<br>");
            }
        }
        return false;
});

$("body").on("click","#removeclass1", function(e){ //user click on remove text
        if( x > 1 ) {
                $(this).parent('div').remove(); //remove text box
                x--; //decrement textbox
            
                $("#Add_More_FileId").show();
            
                $("#lineBreak").html("");
            
                // Adds the "add" link again when a field is removed.
                $('Paid_tickets_input').html("Add field");
        }
    return false;
}) 

/*******************************donation**********************/

$(AddButton2).click(function (e) {
        //max input box allowed
        if(x <= MaxInputs) {
            FieldCount++; //text box added ncrement
            //add input box
            $(Ticket_button).append('<div><input type="text" name="donation_name[]" id="dname_field_'+ FieldCount +'" placeholder="Donation"/><input type="text" name="donation_num[]" id="dnumber_field_'+FieldCount+'" placeholder="0 (Unlimited)"/><label>Donation</label><input type="hidden" name="d_money[]" id="dmoney_field_'+FieldCount+'" value="Donation"><a href="#" id="removeclass2">Remove</a></div>');
            x++; //text box increment
            
            $("#Add_More_FileId").show();
            
            $('Donation_input').html("Add field");
            
            // Delete the "add"-link if there is 3 fields.
            if(x == 100) {
                $("#Add_More_FileId").hide();
                $("#lineBreak").html("<br>");
            }
        }
        return false;
});

$("body").on("click","#removeclass2", function(e){ //user click on remove text
        if( x > 1 ) {
                $(this).parent('div').remove(); //remove text box
                x--; //decrement textbox
            
                $("#Add_More_FileId").show();
            
                $("#lineBreak").html("");
            
                // Adds the "add" link again when a field is removed.
                $('Donation_input').html("Add field");
        }
    return false;
}) 

///////////////**************END*****************////////////////////

});

/***************** How it works **********************/

function openWork(evt, workName) {
    var i, work_content1, tablink_work;
    work_content1 = document.getElementsByClassName("work_content1");
    for (i = 0; i < work_content1.length; i++) {
        work_content1[i].style.display = "none";
    }
    tablink_work = document.getElementsByClassName("tablink_work");
    for (i = 0; i < tablink_work.length; i++) {
        tablink_work[i].className = tablink_work[i].className.replace(" active", "");
    }
    document.getElementById(workName).style.display = "block";
    evt.currentTarget.className += " active";
}


////////////////////******************END********************////////////////////////


/************************** Edit Event Ticket *********************/

function openTicket(evt, ticketName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(ticketName).style.display = "block";
    evt.currentTarget.className += " active";
}


////////////////////******************END********************////////////////////////