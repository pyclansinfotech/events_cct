$(document).ready(function(){

    /*********************validations for create events *****************************/
    $("#event_name").blur(function(){
    var x=$("#event_name").val();
    if (x.length == 0){
        $("#event_name").css("border-color", "red");
        }
    });
    //$("#event_name").focus(function(){
    //    $("#event_name").css("border-color", "");
    //});
    $('input').focus(function(){
    $(this).css('border-color','');
    $(this).removeClass( "empty_form_fields" );
    });
    $('select').focus(function(){
    $(this).css('border-color','');
    $(this).removeClass( "empty_form_fields" );
    });

    $("#event_desc").blur(function(){
    var x=$("#event_desc").val();
    if (x.length == 0){
        $("#event_desc").css("border-color", "red");
        }
    });

    $("#college_name").blur(function(){
    var x=$("#college_name").val();
    if (x.length == 0){
        $("#college_name").css("border-color", "red");
        }
    });
    $("#country").blur(function(){
    var x=$("#country").val();
    if (x == -1){
        $("#country").css("border-color", "red");
        }
    });
    $("#state").blur(function(){
    var x=$("#state").val();
    if (x.length == 0 || x == null || x == ""){
        $("#state").css("border-color", "red");
        }
    });
    $("#city_name").blur(function(){
    var x=$("#city_name").val();
    if (x.length == 0){
        $("#city_name").css("border-color", "red");
        }
    });
    $("#organizer_name").blur(function(){
    var x=$("#organizer_name").val();
    if (x.length == 0){
        $("#organizer_name").css("border-color", "red");
        }
    });
    $("#organizer_desc").blur(function(){
    var x=$("#organizer_desc").val();
    if (x.length == 0){
        $("#organizer_desc").css("border-color", "red");
        }
    });
    $("#event_category_tag").blur(function(){
    var x=$("#event_category_tag").val();
    if (x == 'add a tag' || x == null || x == ""){
        $("#event_category_tag").css("border-color", "red");
        }
    });

    /*****************Ends here********************/

    /***********************Custom Multiple Subevents*******************/
    var sub_array=[];
    var count = 0;
    $('#add-new-item').on('click',function(event){
        event.preventDefault();
        count++;
        params={'index':count};
        $.ajax({
                    type:"POST",
                    data:params,
                    url:"/get_subevent_form/",
                    success: function(res){
                        $('.subevents').append(res);
                        Initialize(count);                       
                    }
                });
        sub_array.push(count);
    })
    function Initialize(count)
    {
        //$('#subevent_category_'+count).tagsInput();
        $(".start_time_"+count).datetimepicker(
        'setStartDate', '{{ datetime|date:"Y-m-d" }}', {format: 'yyyy-mm-dd hh:ii'});
        $(".end_time_"+count).datetimepicker(
        'setStartDate', '{{ datetime|date:"Y-m-d" }}', {format: 'yyyy-mm-dd hh:ii'});
    

        //$("div#dropzone").dropzone(
        //{
        ///  url: "/upload",
        //  maxFiles: 3,
        //  autoProcessQueue: true,
         // acceptedFiles: "image/*",
         // uploadMultiple: true,
        //  dictInvalidFileType: "This file type is not supported.",
        // }
      //);
      // for sub event upload
      // this is hide it because of the event
        $("div#sub_dropzone_"+count).dropzone(
         {
           url: "/sub_event_upload",
           maxFiles: 3,
           autoProcessQueue: true,
           acceptedFiles: "image/*",
           dictInvalidFileType: "This file type is not supported.",
          }
       );
      //$("div#dropzone").on("complete", function(event) {
        //alert();
        //$('div.dz-success').remove();
      //})

      $("div#sub_dropzone"+count).on("complete", function(event) {
        alert();
        $('div.dz-success').remove();
      })

        /********************Free Ticket***********************/
        $('#free_ticket_form_'+count).click(function(){
            var visibility = $('.free_ticket_content_'+count).is(':visible')
            if (visibility==false) {
                $('.free_ticket_content_'+count).show();
                $('.paid_ticket_content_'+count).hide();
                $('.donation_ticket_content_'+count).hide();
                
            }
            else{
                $('.free_ticket_content_'+count).hide();
            }
        })

        $('#free_settings_'+count).click(function(){
            var visibility = $('.free_container_'+count).is(':visible')
            if (visibility==false) {
                $('.free_container_'+count).show();
            }
            else{
                $('.free_container_'+count).hide();
            }
        })
        /***************Free Ticket Ends Here****************/

        /************** paid ticket form ***************/
        $('#paid_ticket_form_'+count).click(function(){
            var visibility = $('.paid_ticket_content_'+count).is(':visible')
            if (visibility==false) {
                $('.paid_ticket_content_'+count).show();
                $('.free_ticket_content_'+count).hide();
                $('.donation_ticket_content_'+count).hide();
            }
            else{
                $('.paid_ticket_content_'+count).hide();
            }
        })

        $('#paid_settings_'+count).click(function(){
            var visibility = $('.paid_container_'+count).is(':visible')
            if (visibility==false) {
                $('.paid_container_'+count).show();
            }
            else{
                $('.paid_container_'+count).hide();
            }
        })

        /************** donation ticket form ***************/
        $('#donation_ticket_form'+count).click(function(){
            var visibility = $('.donation_ticket_content'+count).is(':visible')
            if (visibility==false) {
                $('.donation_ticket_content'+count).show();
                $('.paid_ticket_content'+count).hide();
                $('.free_ticket_content'+count).hide();
            }
            else{
                $('.donation_ticket_content'+count).hide();
            }
        })

        $('#donation_settings').click(function(){
            var visibility = $('.donation_container'+count).is(':visible')
            if (visibility==false) {
                $('.donation_container'+count).show();
            }
            else{
                $('.donation_container'+count).hide();
            }
        })

        $(".ticket_start_time_"+count).datetimepicker(
        'setStartDate', '{{ datetime|date:"Y-m-d" }}', {format: 'yyyy-mm-dd hh:ii'});
        $(".ticket_end_time_"+count).datetimepicker(
        'setStartDate', '{{ datetime|date:"Y-m-d" }}', {format: 'yyyy-mm-dd hh:ii'});

        $(".paid_ticket_start_time_"+count).datetimepicker(
        'setStartDate', '{{ datetime|date:"Y-m-d" }}', {format: 'yyyy-mm-dd hh:ii'});
        $(".paid_ticket_end_time_"+count).datetimepicker(
        'setStartDate', '{{ datetime|date:"Y-m-d" }}', {format: 'yyyy-mm-dd hh:ii'});

        $(".start_time_"+count).change(function(){   
            var ticket_start=$(".start_time_"+count).val();
            console.log('sale>>',ticket_start);
            $(".ticket_start_time_"+count).val(ticket_start);
            $(".paid_ticket_start_time_"+count).val(ticket_start);

        })  
        $(".end_time_"+count).change(function(){ 
            var ticket_ends=$(".end_time_"+count).val();
            console.log('sale>>',ticket_ends);
            $(".ticket_end_time_"+count).val(ticket_ends);
            $(".paid_ticket_end_time_"+count).val(ticket_ends);

        }) 

        $('.sub_event_div_expand_'+count).on('click',function(){
            $('.sub_event_div_expand_'+count).hide();
            $('.sub_event_div_collapse_'+count).show();
            $('.subevent_data_'+count).show();
        }) 

        $('.sub_event_div_collapse_'+count).on('click',function(){
            $('.sub_event_div_expand_'+count).show();
            $('.sub_event_div_collapse_'+count).hide();
            $('.subevent_data_'+count).hide();


        }) 

        /*****************Delete sub event form****************************/

        $('.sub_event_div_delete_'+count).on('click',function(){
            $("div").remove('.sub_event_remove_'+count);
            var index = sub_array.indexOf(count); 
            if (index > -1) {
                sub_array.splice(index, 1);
            }

        })

        /************************************/

    } 

    
    /***********************Custom Multiple Subevents Ends*******************/


    $(".start_time").change(function(){ 
        var event_start=$(".start_time").val();
        console.log('sale>>',event_start);
        $('.ticket_start_time').val(event_start);
        $('.paid_ticket_start_time').val(event_start);

    })  
    $(".end_time").change(function(){ 
        var event_ends=$(".end_time").val();
        console.log('sale>>',event_ends);
        $('.ticket_end_time').val(event_ends);
        $('.paid_ticket_end_time').val(event_ends);

    })  
/*********************Create Event Functionality*************************************/

	/************** Event validations *****************/
    //$('input:text[id="event_name"]').bind('keyup blur', function(){
        //$(this).val( $(this).val().replace(/[^a-zA-Z_\s]/g,'') );
    //});
    $('input:text[id="event_desc"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_,.\s]/g,'') );
    });
    $('input:text[id="college_name"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_\s]/g,'') );
    });
    $('input:text[id="country_name"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('input:text[id="city_name"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('input:text[id="organizer_name"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('input:text[id="organizer_desc"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_,.\s]/g,'') );
    });

    $('#event_category').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
	/************* Ends here ************************/

	/************** Ticket validaitons ********************/

    $('.js-ticket-name').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('.js-quantity-input').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
    });
    $('.js-price-input').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
    });

    $('.js-order-minimum').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
        if ($(this).val() > 10) {
            $(this).val(10)
            $('.min_max').fadeIn(2000, function () {
                $(this).fadeOut(500)
            })
        }
    });

    $('input:text[id="id_group-tickets-0-order_limit"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
    });

    $('.js-order-maximum').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
        if ($(this).val() > 10) {
            $(this).val(10)
            $('.min_max').fadeIn(2000, function () {
                $(this).fadeOut(500)
            })
        }
    });

	/************* Ends here ************************/

    /******* Sub event validations **************************/

    //$('.sub_event_name').bind('keyup blur', function(){
        //$(this).val( $(this).val().replace(/[^a-zA-Z_\s]/g,'') );
    //});
    $('.sub_event_description').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_.,\s]/g,'') );
    });
    $('.sub_organizer_name').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('.sub_organizer_description').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_,.\s]/g,'') );
    });
    $('.sub_event_type').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('.sub_event_category').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_\s]/g,'') );
    });
    /***************** ends here ***************************/


    
    /************** Ticket validaitons ********************/

    $('.js-ticket-name').bind('keyup blur', function(){

        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('.js-quantity-input').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
    });
    $('.js-price-input').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
    });

    $('.js-order-minimum').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
        if ($(this).val() > 10) {
            $(this).val(10)
            $('.min_max').fadeIn(2000, function () {
                $(this).fadeOut(500)
            })
        }
    });

    $('input:text[id="id_group-tickets-0-order_limit"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
    });

    $('.js-order-maximum').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
        if ($(this).val() > 10) {
            $(this).val(10)
            $('.min_max').fadeIn(2000, function () {
                $(this).fadeOut(500)
            })
        }
    });

    /************* Ends here ************************/


    //$('input:text[id="sub_event_name"]').bind('keyup blur', function(){
       // $(this).val( $(this).val().replace(/[^a-zA-Z_\s]/g,'') );
    //});
    $('input:text[id="sub_event_desc"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_.,\s]/g,'') );
    });

    $('input:text[id="sub_free_order_maximum"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9]/g,'') );
        if ($(this).val() > 10) {
            $(this).val(10)
            $('p').fadeIn(2000, function () {
                $(this).fadeOut(500)
            })
        }
    });
/***************** ends here**************/

    $('input:text[id="sub_event_organizer"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('input:text[id="sub_organizer_desc"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_,.\s]/g,'') );
    });
    $('input:text[id="sub_event_type"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('input:text[id="sub_event_catagory"]').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z_\s]/g,'') );
    });



    /**************Date Picker**************/
    // $( function() {
    //     $( "#start_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#end_date" ).datepicker();
    // } );

    /******** modal sub event ***************/
    // $( function() {
    //     $( "#sub_start_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#sub_end_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#free_start_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#free_end_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#paid_start_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#paid_end_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#donation_start_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#donation_end_date" ).datepicker();
    // } );

    /********* ends here *********************/

    /******** create sub event with event ***************/
    // $( function() {
    //     $( "#main_sub_start_date" ).datepicker();
    // } );
    // $( function() {
    //     $( "#main_sub_end_date" ).datepicker();
    // } );
    
    /********* ends here *********************/

    /**************Date Picker for sub event ticket**************/
    // $( function() {
    //     $( ".sub_ticket_start_date" ).datepicker();
    // } );
    // $( function() {
    //     $( ".sub_ticket_end_date" ).datepicker();
    // } );


    /*$( function(){
        $('#start_time').timepicker({ 'scrollDefault': 'now' });
    });
    $( function(){
        $('#end_time').timepicker({ 'scrollDefault': 'now' });
    });*/

    /*************** Modal sub event timepicker*****************/
    /*$( function(){
        $('#sub_start_time').timepicker({ 'scrollDefault': 'now' });
    });
    $( function(){
        $('#sub_end_time').timepicker({ 'scrollDefault': 'now' });
    });

    $( function(){
        $('#free_start_time').timepicker({ 'scrollDefault': 'now' });
    });
    $( function(){
        $('#free_end_time').timepicker({ 'scrollDefault': 'now' });
    });

    $( function(){
        $('#paid_start_time').timepicker({ 'scrollDefault': 'now' });
    });
    $( function(){
        $('#paid_end_time').timepicker({ 'scrollDefault': 'now' });
    });

    $( function(){
        $('#donation_start_time').timepicker({ 'scrollDefault': 'now' });
    });
    $( function(){
        $('#donation_end_time').timepicker({ 'scrollDefault': 'now' });
    });*/
    /******************* ends here ***************************/

    /*************** sub event timepicker with event *****************/
    /*$( function(){
        $('#main_sub_start_time').timepicker({ 'scrollDefault': 'now' });
    });
    $( function(){
        $('#main_sub_end_time').timepicker({ 'scrollDefault': 'now' });
    });*/

    /******************* ends here ***************************/

    /************** time picker for sub event ticket ********************/
    /*$( function(){
        $('.sub_ticket_start_time').timepicker({ 'scrollDefault': 'now' });
    });
    $( function(){
        $('.sub_ticket_end_time').timepicker({ 'scrollDefault': 'now' });
    });*/
    


    /************** free ticket form ***************/
    $('#free_ticket_form').click(function(){
        var visibility = $('.free_ticket_content').is(':visible')
        if (visibility==false) {
            $('.free_ticket_content').show();
            $('.paid_ticket_content').hide();
            $('.donation_ticket_content').hide();
            
        }
        else{
            $('.free_ticket_content').hide();
        }
    })

    $('#free_settings').click(function(){
        var visibility = $('.free_container').is(':visible')
        if (visibility==false) {
            $('.free_container').show();
        }
        else{
            $('.free_container').hide();
        }
    })
    

    /************** paid ticket form ***************/
    $('#paid_ticket_form').click(function(){
        var visibility = $('.paid_ticket_content').is(':visible')
        if (visibility==false) {
            $('.paid_ticket_content').show();
            $('.free_ticket_content').hide();
            $('.donation_ticket_content').hide();
        }
        else{
            $('.paid_ticket_content').hide();
        }
    })

    $('#paid_settings').click(function(){
        var visibility = $('.paid_container').is(':visible')
        if (visibility==false) {
            $('.paid_container').show();
        }
        else{
            $('.paid_container').hide();
        }
    })

    /************** donation ticket form ***************/
    $('#donation_ticket_form').click(function(){
        var visibility = $('.donation_ticket_content').is(':visible')
        if (visibility==false) {
            $('.donation_ticket_content').show();
            $('.paid_ticket_content').hide();
            $('.free_ticket_content').hide();
        }
        else{
            $('.donation_ticket_content').hide();
        }
    })

    $('#donation_settings').click(function(){
        var visibility = $('.donation_container').is(':visible')
        if (visibility==false) {
            $('.donation_container').show();
        }
        else{
            $('.donation_container').hide();
        }
    })

/******************Main Sub Ticket Container **********************/

	/**************main free ticket form ***************/
    $('#main_free_sub_form').click(function(){
        var visibility = $('#main_free_sub_container').is(':visible')
        if (visibility==false) {
            $('#main_free_sub_container').show();
            $('#main_paid_sub_container').hide();
            $('#main_donation_sub_container').hide();
        }
        else{
            $('#main_free_sub_container').hide();
        }
    })

    $('#main_sub_free_settings').click(function(){
        var visibility = $('#main_sub_free_content').is(':visible')
        if (visibility==false) {
            $('#main_sub_free_content').show();
        }
        else{
            $('#main_sub_free_content').hide();
        }
    })

    /************** main paid ticket form ***************/
    $('#main_paid_sub_form').click(function(){
        var visibility = $('#main_paid_sub_container').is(':visible')
        if (visibility==false) {
            $('#main_paid_sub_container').show();
            $('#main_free_sub_container').hide();
            $('#main_donation_sub_container').hide();
        }
        else{
            $('#main_paid_sub_container').hide();
        }
    })

    $('#main_sub_paid_settings').click(function(){
        var visibility = $('#main_sub_paid_content').is(':visible')
        if (visibility==false) {
            $('#main_sub_paid_content').show();
        }
        else{
            $('#main_sub_paid_content').hide();
        }
    })

    /************** main donation ticket form ***************/
    $('#main_donation_sub_form').click(function(){
        var visibility = $('#main_donation_sub_container').is(':visible')
        if (visibility==false) {
            $('#main_donation_sub_container').show();
            $('#main_paid_sub_container').hide();
            $('#main_free_sub_container').hide();
        }
        else{
            $('#main_donation_sub_container').hide();
        }
    })

    $('#main_sub_donation_settings').click(function(){
        var visibility = $('#main_sub_donation_content').is(':visible')
        if (visibility==false) {
            $('#main_sub_donation_content').show();
        }
        else{
            $('#main_sub_donation_content').hide();
        }
    })

/****************** Sub Ticket Container **********************/

    /************** free ticket form ***************/
    $('#free_sub_form').click(function(){
        var visibility = $('#free_sub_container').is(':visible')
        if (visibility==false) {
            $('#free_sub_container').show();
            $('#paid_sub_container').hide();
            $('#donation_sub_container').hide();
        }
        else{
            $('#free_sub_container').hide();
        }
    })

    $('#sub_free_settings').click(function(){
        var visibility = $('#sub_free_content').is(':visible')
        if (visibility==false) {
            $('#sub_free_content').show();
        }
        else{
            $('#sub_free_content').hide();
        }
    })
    
    /************** paid ticket form ***************/
    $('#paid_sub_form').click(function(){
        var visibility = $('#paid_sub_container').is(':visible')
        if (visibility==false) {
            $('#paid_sub_container').show();
            $('#free_sub_container').hide();
            $('#donation_sub_container').hide();
        }
        else{
            $('#paid_sub_container').hide();
        }
    })

    $('#sub_paid_settings').click(function(){
        var visibility = $('#sub_paid_content').is(':visible')
        if (visibility==false) {
            $('#sub_paid_content').show();
        }
        else{
            $('#sub_paid_content').hide();
        }
    })

    /************** donation ticket form ***************/
    $('#donation_sub_form').click(function(){
        var visibility = $('#donation_sub_container').is(':visible')
        if (visibility==false) {
            $('#donation_sub_container').show();
            $('#paid_sub_container').hide();
            $('#free_sub_container').hide();
        }
        else{
            $('#donation_sub_container').hide();
        }
    })

    $('#sub_donation_settings').click(function(){
        var visibility = $('#sub_donation_content').is(':visible')
        if (visibility==false) {
            $('#sub_donation_content').show();
        }
        else{
            $('#sub_donation_content').hide();
        }
    })


/************* Visibility of main sub event form ********************/
	var button_click = ''
	$('#create_main_sub_event').click(function(){
    	var visibility = $('#main_sub_content').is(':visible')
        if (visibility==false) {
            $('#main_sub_content').show();
            button_click = 1;
        }
        else{
            $('#main_sub_content').hide();
            button_click = 0;
        }
    })
    /****************************Create Event and Subevent Functionality*******************/
    $('#create_event_button').on('click',function(){
        var event_name=$('#event_name').val();
        var event_description=$('#event_desc').val();
        var college_name=$('#college_name').val();
        var country_name=$('#country').val();
        var state=$('#state').val();
        var city_name=$('#city_name').val();
        var organizer_name=$('#organizer_name').val();
        var organizer_description=$('#organizer_desc').val();
        var start_date=$('.start_time').val();
        var end_date=$('.end_time').val();
        var event_category=$('#event_category').val();
        var image_name=$(".dz-filename > span").text();
        var image_count=$('#dropzone > .dz-preview').length;
        
        /****************Ticket Detail******************/
            var ticket_name=$('#free_tic_name').val();
            var type_ticket='free'
            if (ticket_name==undefined || ticket_name == ''){
                var ticket_name=$('#paid_tic_name').val();
                var type_ticket='paid'
            }
            
            var quantity_available=$('#free_quantity').val();
            if (quantity_available==undefined || quantity_available == ''){
                var quantity_available=$('#paid_quantity').val();
            }
            
            var ticket_price=$('#free_tic_price').val();
            if (ticket_price==undefined || ticket_price == ''){
                var ticket_price=$('#paid_tic_price').val();
            }
            var ticket_desc=$('.free_ticket_description').val();
            if (ticket_price==undefined || ticket_price == ''){
                var ticket_price=$('.paid_ticket_description').val();
            }
            var sale_channel=$('.free_sale_channel').val();
            if (sale_channel==undefined || sale_channel == ''){
                var sale_channel=$('.paid_sale_channel').val();
            }
            var ticket_start=$('.ticket_start_time').val();
            if (ticket_start==undefined || ticket_start == ''){
                var ticket_start=$('#paid_ticket_start_time').val();
            }
            var ticket_end=$('.ticket_end_time').val();
            if (ticket_end==undefined || ticket_end == ''){
                var ticket_end=$('#paid_ticket_end_time').val();
            }
            var minimum_tickets=$('.free_min').val();
            if (minimum_tickets==undefined || minimum_tickets == ''){
                var minimum_tickets=$('.paid_min').val();
            }
            var maximum_tickets=$('.free_max').val();
            if (maximum_tickets==undefined || maximum_tickets == ''){
                var maximum_tickets=$('.paid_max').val();
            }
            if(image_name==''){
               $('.dropzone').css("border","1px dashed red");
            }
            if(country_name==-1){
               $('#country').css("border","1px solid red");
            }
            var is_sub_event = document.getElementsByClassName('create_content');
            var sub_event_flag = '';
            if(is_sub_event.length > 1){
                var inputCount=sub_array.length;
                var i;
                //var inputCount = document.getElementsByTagName('form').length;

                for (i = 1; i <= inputCount; i++) {
                    var j=(i-1);
                    var sub_count=sub_array[j];
                    var elements = document.querySelector('.subevent_data_'+sub_count);
                    var sub_event_title = elements["event_title"].value;
                    var sub_event_desc = elements["event_description"].value;
                    var sub_college_name = elements["college_name"].value;
                    var sub_venue = elements["venue"].value;
                    var sub_organizer_name = elements["organizer_name"].value;
                    var sub_organizer_desc = elements["organizer_description"].value;
                    var sub_event_category = elements["event_category"].value;
                    var sub_start_time = elements["start_time"].value;
                    var sub_end_time = elements["end_time"].value;
                    var image=$("#sub_dropzone_"+sub_count+" .dz-filename > span").text();
                    var visible_check1=$('#visible_'+sub_count).is(":checked");
                    if (visible_check1 == true){
                    var visible_check = 1;
                    }else{
                    var visible_check = 0;
                    }
                    var sub_event_visible = elements["visible"].value;
                    var sub_ticket_name= elements["group-tickets-0-ticket_type"].value;
                    var sub_type_ticket='free'
                    if (sub_ticket_name==undefined || sub_ticket_name == ''){
                        var sub_ticket_name= elements["group-tickets-0-ticket_type_paid"].value;
                        var sub_type_ticket='paid'
                    }

                    var sub_ticket_description= elements["group-tickets-0-description"].value;
                    if (sub_ticket_description==undefined || sub_ticket_description == ''){
                        var sub_ticket_description= elements["group-tickets-0-description_paid"].value;
                    }
                    
                    
                    var sub_quantity_available= elements["group-tickets-0-quantity_total"].value;
                    if (sub_quantity_available==undefined || sub_quantity_available == ''){
                        var sub_quantity_available= elements["group-tickets-0-quantity_total_paid"].value;
                    }
                    
                    var sub_ticket_price= elements["group-tickets-0-cost"].value;
                    if (sub_ticket_price==undefined || sub_ticket_price == ''){
                        var sub_ticket_price= elements["group-tickets-0-cost_paid"].value;
                    }
                    
                    var sub_sale_channel= elements["group-tickets-0-sales_channel_option"].value;
                    if (sub_sale_channel==undefined || sub_sale_channel == ''){
                        var sub_sale_channel= elements["group-tickets-0-sales_channel_option_paid"].value;
                    }
                    
                    var sub_ticket_start= elements["ticket_start_time"].value;
                    if (sub_ticket_start==undefined || sub_ticket_start == ''){
                        var ticket_start= elements["paid_ticket_start_time"].value;
                    }
                    var sub_ticket_end= elements["ticket_end_time"].value;
                    if (sub_ticket_end==undefined || sub_ticket_end == ''){
                        var sub_ticket_end= elements["paid_ticket_end_time"].value;
                    }
                    var sub_minimum_tickets= elements["group-tickets-0-order_minimum"].value;
                    if (sub_minimum_tickets==undefined || sub_minimum_tickets == ''){
                        var sub_minimum_tickets= elements["group-tickets-0-order_minimum_paid"].value;
                    }
                    var sub_maximum_tickets= elements["group-tickets-0-order_limit"].value;
                    if (sub_maximum_tickets==undefined || sub_maximum_tickets == ''){
                        var sub_maximum_tickets= elements["group-tickets-0-order_limit_paid"].value;
                    }
                    if(image==''){
                        $('#sub_dropzone_'+sub_count).css("border","1px dashed red");
                    }
                    

                    if(sub_event_title == '' || sub_event_desc == '' || sub_college_name == '' ||
                        sub_organizer_name =='' || sub_venue == '' || sub_organizer_desc == '' ||
                        sub_type_ticket =='' || sub_event_category == '' || image == '' ||
                        sub_start_time == '' || sub_end_time == '' || sub_ticket_name == '' ||
                        sub_quantity_available == ''){
                        $('.form :input').each(function(){ 
                        if( $.trim($(this).val()) == "" )
                            var val=$(this).attr('type');
                            if(val == "text"){
                                $(this).addClass("empty_form_fields");
                                $('.empty_form_fields').css({"border":"solid red 1px"});
                            }
                            
                        });
                        $('#empty_field_error').show();
                        sub_event_flag = '1';
                        
                     }
                     }
                     
                    } 
                    if (sub_event_flag != '1'){
                    if(event_name == '' || event_description == '' || college_name =='' || country_name == '' 
                    || state == '' || city_name == '' || organizer_name == '' ||
                    organizer_description == '' || start_date == '' || end_date == '' || event_category == '' ||
                    ticket_name == '' || quantity_available == '' || type_ticket == '' || image_name == ''){
                    $('.form :input').each(function(){ 
                    if( $.trim($(this).val()) == "" )
                        var val=$(this).attr('type');
                        if(val == "text"){
                            $(this).addClass( "empty_form_fields" );
                            $('.empty_form_fields').css({"border":"solid red 1px"});
                        }
                        
                    });
                    $('#empty_field_error').show();
                }else{

                params={event_category:event_category,event_name:event_name,
                event_description:event_description,college_name:college_name,
                country_name:country_name,state:state,city_name:city_name,
                organizer_name:organizer_name,organizer_description:organizer_description,
                start_date:start_date,end_date:end_date,ticket_name:ticket_name,
                maximum_tickets:maximum_tickets,quantity_available:quantity_available,
                ticket_price:ticket_price,minimum_tickets:minimum_tickets,
                ticket_start:ticket_start,ticket_end:ticket_end,image_name:image_name,
                ticket_type:type_ticket,sale_channel:sale_channel,ticket_desc:ticket_desc,
                image_count:image_count}
                console.log('params',params)
                $.ajax({
                        type:"POST",
                        url:"/create_event_data/",
                        data: params,
                        success: function(res){
                            if(res=='success'){
                                $("#confirm_modal").modal();
                            }
                            else{
                                console.log('not saved')
                            }
                        }
                });
                if(is_sub_event.length > 1){
                save_subevents();
            }
        }
    }
        
    });
    /****************************Ends Here***********************************************/

    /********************Subevent Functionality********************/

    function save_subevents(){
        /**************************/
    var inputCount=sub_array.length;
        var i,k,l,m;
        var sub_event_count=inputCount;
        //var inputCount = document.getElementsByTagName('form').length;
        var image_count=[];
        var sub_total_img=[];
        for (k=1; k<=inputCount; k++){
            var m=(k-1);
            var sub_count=sub_array[m];
            var image_for_this=$('#sub_dropzone_'+k+'> .dz-preview');
            var total_images=image_for_this.length;
            image_count.push(total_images);
            
        }
        var o;
        
        for(l=1; l<=(inputCount);l++){
            var sub_total_img1=0;
            for(o=1; o<=(inputCount-1);o++){
            sub_total_img1+=image_count[o];
        }
        var p;
        p=l-1;
           sub_total_img.splice(p,0,sub_total_img1); 
        }
        alert(sub_total_img);
        
        
        
        for (i = 1; i <= inputCount; i++) {
            var j=(i-1);
            var sub_count=sub_array[j];
            var elements = document.querySelector('.subevent_data_'+sub_count);
            var sub_total_image=sub_total_img[j];
            var sub_event_title = elements["event_title"].value;
            var sub_event_desc = elements["event_description"].value;
            var sub_college_name = elements["college_name"].value;
            var sub_venue = elements["venue"].value;
            var sub_organizer_name = elements["organizer_name"].value;
            var sub_organizer_desc = elements["organizer_description"].value;
            var sub_event_category = elements["event_category"].value;
            var sub_start_time = elements["start_time"].value;
            var sub_end_time = elements["end_time"].value;
            var image=$("#sub_dropzone_"+sub_count+" .dz-filename > span").text();
            var visible_check1=$('#visible_'+sub_count).is(":checked");
            if (visible_check1 == true){
            var visible_check = 1;
            }else{
            var visible_check = 0;
            }
            var sub_event_visible = elements["visible"].value;
            var sub_ticket_name= elements["group-tickets-0-ticket_type"].value;
            var sub_type_ticket='free'
            if (sub_ticket_name==undefined || sub_ticket_name == ''){
                var sub_ticket_name= elements["group-tickets-0-ticket_type_paid"].value;
                var sub_type_ticket='paid'
            }
            var sub_ticket_description= elements["group-tickets-0-description"].value;
            if (sub_event_desc==undefined || sub_event_desc == ''){
                var sub_event_desc= elements["group-tickets-0-description_paid"].value;
            }
            var sub_quantity_available= elements["group-tickets-0-quantity_total"].value;
            if (sub_quantity_available==undefined || sub_quantity_available == ''){
                var sub_quantity_available= elements["group-tickets-0-quantity_total_paid"].value;
            }
            var sub_ticket_price= elements["group-tickets-0-cost"].value;
            if (sub_ticket_price==undefined || sub_ticket_price == ''){
                var sub_ticket_price= elements["group-tickets-0-cost_paid"].value;
            }
            var sub_sale_channel= elements["group-tickets-0-sales_channel_option"].value;
            if (sub_sale_channel==undefined || sub_sale_channel == ''){
                var sub_sale_channel= elements["group-tickets-0-sales_channel_option_paid"].value;
            }
            var sub_ticket_start= elements["ticket_start_time"].value;
            if (sub_ticket_start==undefined || sub_ticket_start == ''){
                var ticket_start= elements["paid_ticket_start_time"].value;
            }
            var sub_ticket_end= elements["ticket_end_time"].value;
            if (sub_ticket_end==undefined || sub_ticket_end == ''){
                var sub_ticket_end= elements["paid_ticket_end_time"].value;
            }
            var sub_minimum_tickets= elements["group-tickets-0-order_minimum"].value;
            if (sub_minimum_tickets==undefined || sub_minimum_tickets == ''){
                var sub_minimum_tickets= elements["group-tickets-0-order_minimum_paid"].value;
            }
            var sub_maximum_tickets= elements["group-tickets-0-order_limit"].value;
            if (sub_maximum_tickets==undefined || sub_maximum_tickets == ''){
                var sub_maximum_tickets= elements["group-tickets-0-order_limit_paid"].value;
            }



            params={sub_event_category:sub_event_category,sub_event_title:sub_event_title,
            sub_event_desc:sub_event_desc,sub_college_name:sub_college_name,image:image,
            sub_venue:sub_venue,sub_organizer_name:sub_organizer_name,sub_organizer_desc:sub_organizer_desc,
            sub_start_time:sub_start_time,sub_end_time:sub_end_time,sub_ticket_name:sub_ticket_name,
            sub_maximum_tickets:sub_maximum_tickets,sub_quantity_available:sub_quantity_available,
            sub_ticket_price:sub_ticket_price,sub_minimum_tickets:sub_minimum_tickets,
            sub_ticket_start:sub_ticket_start,sub_ticket_end:sub_ticket_end,
            sub_sale_channel:sub_sale_channel,sub_type_ticket:sub_type_ticket,
            visible_check:visible_check,sub_ticket_description:sub_ticket_description,
            sub_total_image:sub_total_image}
            console.log('params',params)
            $.ajax({
                    type:"POST",
                    url:"/create_subevent_data/",
                    data: params,
                    success: function(res){
                        if(res=='success'){
                            $("#confirm_modal").modal();
                
                        }
                        else{
                            console.log('not saved')
                        }
                    }
            });
        
        }
        /*************************************/
    }

    /*******************ends here**********************/

	// $('#create_event_button').click(function(){
 //        /**********************Event details***************/
 //        if (button_click==1) {
 //        	button_click=1;
 //        	var event_name=$('#event_name').val();
	//         var event_description=$('#event_desc').val();
	//         var college_name=$('#college_name').val();
	//         var country_name=$('#country_name').val();
	//         var city_name=$('#city_name').val();
	//         var organizer_name=$('#organizer_name').val();
	//         var organizer_description=$('#organizer_desc').val();
	//         var event_type=$('#event_type').val();
	//         var start_date=$('#start_date').val();
	//         var start_time=$('#start_time').val();
	//         var end_date=$('#end_date').val();
	//         var end_time=$('#end_time').val();
	//         var event_category=$('#event_category').val();
	//         var event_image_name = event_image
	//         if (event_image_name==undefined || event_image_name == ''){
	//         	$('.note_content').css({"color":"red"});
	//         }
	//         /****************Ticket Detail******************/
	//         var ticket_name=$('#free_tic_name').val();
	//         var type_ticket='free'
	//         if (ticket_name==undefined || ticket_name == ''){
	//             var ticket_name=$('#paid_tic_name').val();
	//             var type_ticket='paid'
	//             if (ticket_name == undefined || ticket_name == ''){
	//                 var ticket_name=$('#donation_tic_name').val();
	//                 var type_ticket='donation'
	//             }
	//         }
	//         var quantity_available=$('#free_quantity').val();
	//         if (quantity_available==undefined || quantity_available == ''){
	//             var quantity_available=$('#paid_quantity').val();
	//             if (quantity_available == undefined || quantity_available == ''){
	//                 var quantity_available=$('#donation_quantity').val();
	//             }
	//         }
	//         var ticket_price=$('#free_tic_price').val();
	//         if (ticket_price==undefined || ticket_price == ''){
	//             var ticket_price=$('#paid_tic_price').val();
	//             if (ticket_price == undefined || ticket_price == ''){
	//                 var ticket_price=$('#donation_tic_price').val();
	//             }
	//         }
	//         var sale_channel=$('.free_sale_channel').val();
	//         if (sale_channel==undefined || sale_channel == ''){
	//             var sale_channel=$('.paid_sale_channel').val();
	//             if (sale_channel == undefined || sale_channel == ''){
	//                 var sale_channel=$('.donation_sale_channel').val();
	//             }
	//         }
	//         var ticket_start_date=$('#free_start_date').val();
	//         if (ticket_start_date==undefined || ticket_start_date == ''){
	//             var ticket_start_date=$('#paid_start_date').val();
	//             if (ticket_start_date == undefined || ticket_start_date == ''){
	//                 var ticket_start_date=$('#donation_start_date').val();
	//             }
	//         }
	//         var ticket_start_time=$('#free_start_time').val();
	//         if (ticket_start_time==undefined || ticket_start_time == ''){
	//             var ticket_start_time=$('#paid_start_time').val();
	//             if (ticket_start_time == undefined || ticket_start_time == ''){
	//                 var ticket_start_time=$('#donation_start_time').val();
	//             }
	//         }
	//         var ticket_end_date=$('#free_end_date').val();
	//         if (ticket_end_date==undefined || ticket_end_date == ''){
	//             var ticket_end_date=$('#paid_end_date').val();
	//             if (ticket_end_date == undefined || ticket_end_date == ''){
	//                 var ticket_end_date=$('#donation_end_date').val();
	//             }
	//         }
	//         var ticket_end_time=$('#free_end_time').val();
	//         if (ticket_end_time==undefined || ticket_end_time == ''){
	//             var ticket_end_time=$('#paid_end_time').val();
	//             if (ticket_end_time == undefined || ticket_end_time == ''){
	//                 var ticket_end_time=$('#donation_end_time').val();
	//             }
	//         }
	//         var minimum_tickets=$('.free_min').val();
	//         if (minimum_tickets==undefined || minimum_tickets == ''){
	//             var minimum_tickets=$('.paid_min').val();
	//             if (minimum_tickets == undefined || minimum_tickets == ''){
	//                 var minimum_tickets=$('.donation_min').val();
	//             }
	//         }
	//         var maximum_tickets=$('.free_max').val();
	//         if (maximum_tickets==undefined || maximum_tickets == ''){
	//             var maximum_tickets=$('.paid_max').val();
	//             if (maximum_tickets == undefined || maximum_tickets == ''){
	//                 var maximum_tickets=$('.donation_max').val();
	//             }
	//         }

	//         /************* Sub Event details*********/
	//         var sub_event_name=$('#main_sub_event_name').val();
	//         var sub_event_description=$('#main_sub_event_desc').val();
	//         var sub_event_organizer=$('#main_sub_event_organizer').val();
	//         var sub_organizer_description=$('#main_sub_organizer_desc').val();
	//         var sub_event_type=$('#main_sub_event_type').val();
	//         var sub_event_catagory=$('#main_sub_event_catagory').val();
	//         var sub_start_date=$('#main_sub_start_date').val();
	//         var sub_start_time=$('#main_sub_start_time').val();
	//         var sub_end_date=$('#main_sub_end_date').val();
	//         var sub_end_time=$('#main_sub_end_time').val();
 //            var booking_check1=$('#booking_check_box').is(":checked");
 //            if (booking_check1 == true){
 //                var booking_check = 'True'
 //            }else if (booking_check1 == false){
 //                var booking_check = 'False'
 //            }
	//         /****************Ticket Detail******************/
	//         var sub_ticket_name=$('#main_sub_free_tic_name').val();
	//         if (sub_ticket_name==undefined || sub_ticket_name == ''){
	//             var sub_ticket_name=$('#main_sub_paid_tic_name').val();
	//             var type_ticket='paid'
	//             if (sub_ticket_name == undefined || sub_ticket_name == ''){
	//                 var sub_ticket_name=$('#main_sub_donation_tic_name').val();
	//                 var type_ticket='donation'
	//             }
	//         }
	//         var sub_quantity_available=$('#main_sub_free_quantity').val();
	//         if (sub_quantity_available==undefined || sub_quantity_available == ''){
	//             var sub_quantity_available=$('#main_sub_paid_quantity').val();
	//             if (sub_quantity_available == undefined || sub_quantity_available == ''){
	//                 var sub_quantity_available=$('#main_sub_donation_quantity').val();
	//             }
	//         }

	//         var sub_ticket_price=$('#main_sub_free_tic_price').val();
	//         if (sub_ticket_price==undefined || sub_ticket_price == ''){
	//             var sub_ticket_price=$('#main_sub_paid_tic_price').val();
	//             if (sub_ticket_price == undefined || sub_ticket_price == ''){
	//                 var sub_ticket_price=$('#main_sub_donation_tic_price').val();
	//             }
	//         }
	//         var sub_ticket_description=$('.sub_free_ticket_description').val();
	//         if (sub_ticket_description==undefined || sub_ticket_description == ''){
	//             var sub_ticket_description=$('.sub_paid_ticket_description').val();
	//             if (sub_ticket_description == undefined || sub_ticket_description == ''){
	//                 var sub_ticket_description=$('.sub_donation_ticket_description').val();
	//             }
	//         }
	//         var sub_sale_channel=$('.sub_free_sale_channel').val();
	//         if (sub_sale_channel==undefined || sub_sale_channel == ''){
	//             var sub_sale_channel=$('.sub_paid_sale_channel').val();
	//             if (sub_sale_channel == undefined || sub_sale_channel == ''){
	//                 var sub_sale_channel=$('.sub_donation_sale_channel').val();
	//             }
	//         }
	//         var sub_ticket_start_date=$('#main_sub_free_start_date').val();
	//         if (sub_ticket_start_date==undefined || sub_ticket_start_date == ''){
	//             var sub_ticket_start_date=$('#main_sub_paid_start_date').val();
	//             if (sub_ticket_start_date == undefined || sub_ticket_start_date == ''){
	//                 var sub_ticket_start_date=$('#main_sub_donation_start_date').val();
	//             }
	//         }
	//         var sub_ticket_start_time=$('#main_sub_free_start_time').val();
	//         if (sub_ticket_start_time==undefined || sub_ticket_start_time == ''){
	//             var sub_ticket_start_time=$('#main_sub_paid_start_time').val();
	//             if (sub_ticket_start_time == undefined || sub_ticket_start_time == ''){
	//                 var sub_ticket_start_time=$('#main_sub_donation_start_time').val();
	//             }
	//         }
	//         var sub_ticket_end_date=$('#main_sub_free_end_date').val();
	//         if (sub_ticket_end_date==undefined || sub_ticket_end_date == ''){
	//             var sub_ticket_end_date=$('#main_sub_paid_end_date').val();
	//             if (sub_ticket_end_date == undefined || sub_ticket_end_date == ''){
	//                 var sub_ticket_end_date=$('#main_sub_donation_end_date').val();
	//             }
	//         }
	//         var sub_ticket_end_time=$('#main_sub_free_end_time').val();
	//         if (sub_ticket_end_time==undefined || sub_ticket_end_time == ''){
	//             var sub_ticket_end_time=$('#main_sub_paid_end_time').val();
	//             if (sub_ticket_end_time == undefined || sub_ticket_end_time == ''){
	//                 var sub_ticket_end_time=$('#main_sub_donation_end_time').val();
	//             }
	//         }
	//         var sub_minimum_tickets=$('#main_sub_free_order_minimum').val();
	//         if (sub_minimum_tickets==undefined || sub_minimum_tickets == ''){
	//             var sub_minimum_tickets=$('#main_sub_paid_order_minimum').val();
	//             if (sub_minimum_tickets == undefined || sub_minimum_tickets == ''){
	//                 var sub_minimum_tickets=$('#main_sub_donation_order_minimum').val();
	//             }
	//         }
	//         var sub_maximum_tickets=$('#main_sub_free_order_maximum').val();
	//         if (sub_maximum_tickets==undefined || sub_maximum_tickets == ''){
	//             var sub_maximum_tickets=$('#main_sub_paid_order_maximum').val();
	//             if (sub_maximum_tickets == undefined || sub_maximum_tickets == ''){
	//                 var sub_maximum_tickets=$('#main_sub_donation_order_maximum').val();
	//             }
	//         }
	//         var subevent_image_name= subevent_image
	//         if(event_name == '' || event_description == '' || college_name =='' || country_name == '' || city_name == '' || organizer_name == '' ||
	//             organizer_description == '' || event_type == '' || start_date == '' || end_date == '' || event_category == '' ||
	//             ticket_name == '' || quantity_available == '' || sale_channel == '' || ticket_start_date == '' || ticket_end_date == ''
	//             || ticket_start_time == '' || ticket_end_time == '' || minimum_tickets == '' || maximum_tickets == ''
	//              || event_image_name == '' ||
	//              sub_event_name == '' || sub_event_description == '' || sub_event_organizer =='' ||
	//              sub_organizer_description == '' || sub_event_type =='' || sub_event_catagory == '' || 
	//              sub_start_date == '' || sub_start_time == '' || sub_end_date =='' || sub_end_time == '' ||
	//              sub_ticket_name == '' || sub_quantity_available == '' || sub_ticket_description == '' || 
	//              sub_sale_channel == '' || sub_minimum_tickets == '' || sub_maximum_tickets == '' || subevent_image_name == ''){
	//         		$('.form :input').each(function(){ 
	// 			        if( $.trim($(this).val()) == "" )
	// 			        	$(this).addClass( "empty_form_fields" );
	// 			        	$('.empty_form_fields').css({"border":"solid red 1px"});
	// 			    	});
	//                 $('#empty_field_error').show();
	//         }else{
	//         	$('.loader').show().delay(1000).fadeOut();
	//             params={'event_category':event_category,'event_name':event_name,'event_description':event_description,'college_name':college_name,
	//                     'country_name':country_name,'city_name':city_name,'organizer_name':organizer_name,
	//                     'organizer_description':organizer_description,'event_type':event_type,
	//                     'start_date':start_date,'end_date':end_date,'ticket_name':ticket_name,'maximum_tickets':maximum_tickets,
	//                     'quantity_available':quantity_available,'ticket_price':ticket_price,'minimum_tickets':minimum_tickets,
	//                     'start_time':start_time,'end_time':end_time,'event_image':event_image_name,
	//                     'ticket_start_date':ticket_start_date,'ticket_end_date':ticket_end_date,'ticket_start_time':ticket_start_time,
	//                     'ticket_end_time':ticket_end_time,'ticket_type':type_ticket,'sale_channel':sale_channel,
	//                 	'sub_event_name':sub_event_name,'sub_event_description':sub_event_description,
	//                     'sub_event_organizer':sub_event_organizer,'sub_organizer_description':sub_organizer_description,
	//                     'sub_event_type':sub_event_type,'sub_event_catagory':sub_event_catagory,
	//                     'sub_start_date':sub_start_date,'sub_start_time':sub_start_time,'sub_end_date':sub_end_date,
	//                     'sub_end_time':sub_end_time,'sub_ticket_name':sub_ticket_name,'sub_quantity_available':sub_quantity_available,
	//                     'sub_ticket_price':sub_ticket_price,'sub_ticket_description':sub_ticket_description,'sub_sale_channel':sub_sale_channel,
	//                     'sub_ticket_start_date':sub_ticket_start_date,'sub_ticket_start_time':sub_ticket_start_time,
	//                     'sub_ticket_end_date':sub_ticket_end_date,'sub_ticket_end_time':sub_ticket_end_time,
	//                     'sub_minimum_tickets':sub_minimum_tickets,'sub_maximum_tickets':sub_maximum_tickets,'subevent_image':subevent_image_name,
	//                 	'button_click':button_click,'booking_check':booking_check}
	//             $.ajax({
	//                 type:"POST",
	//                 url:"/create_event_data/",
	//                 data: params,
	//                 success: function(res){
	//                     if(res=='success'){
	//                         $("#confirm_modal").modal();
	            
	//                     }
	//                     else{
	//                         console.log('not saved')
	//                     }
	//                 }
	//             });
	//         }	
 //        }
 //        else{   
 //        	button_click=0;     	
	//         var event_name=$('#event_name').val();
	//         var event_description=$('#event_desc').val();
	//         var college_name=$('#college_name').val();
	//         var country_name=$('#country_name').val();
	//         var city_name=$('#city_name').val();
	//         var organizer_name=$('#organizer_name').val();
	//         var organizer_description=$('#organizer_desc').val();
	//         var event_type=$('#event_type').val();
	//         var start_date=$('#start_date').val();
	//         var start_time=$('#start_time').val();
	//         var end_date=$('#end_date').val();
	//         var end_time=$('#end_time').val();
	//         var event_category=$('#event_category').val();
	//         var event_image_name = event_image
	//         if (event_image_name==undefined || event_image_name == ''){
	//         	$('.note_content').css({"color":"red"});
	//         }
	//         /****************Ticket Detail******************/
	//         var ticket_name=$('#free_tic_name').val();
	//         var type_ticket='free'
	//         if (ticket_name==undefined || ticket_name == ''){
	//             var ticket_name=$('#paid_tic_name').val();
	//             var type_ticket='paid'
	//             if (ticket_name == undefined || ticket_name == ''){
	//                 var ticket_name=$('#donation_tic_name').val();
	//                 var type_ticket='donation'
	//             }
	//         }
	//         var quantity_available=$('#free_quantity').val();
	//         if (quantity_available==undefined || quantity_available == ''){
	//             var quantity_available=$('#paid_quantity').val();
	//             if (quantity_available == undefined || quantity_available == ''){
	//                 var quantity_available=$('#donation_quantity').val();
	//             }
	//         }
	//         var ticket_price=$('#free_tic_price').val();
	//         if (ticket_price==undefined || ticket_price == ''){
	//             var ticket_price=$('#paid_tic_price').val();
	//             if (ticket_price == undefined || ticket_price == ''){
	//                 var ticket_price=$('#donation_tic_price').val();
	//             }
	//         }
	//         var sale_channel=$('.free_sale_channel').val();
	//         if (sale_channel==undefined || sale_channel == ''){
	//             var sale_channel=$('.paid_sale_channel').val();
	//             if (sale_channel == undefined || sale_channel == ''){
	//                 var sale_channel=$('.donation_sale_channel').val();
	//             }
	//         }
	//         var ticket_start_date=$('#free_start_date').val();
	//         if (ticket_start_date==undefined || ticket_start_date == ''){
	//             var ticket_start_date=$('#paid_start_date').val();
	//             if (ticket_start_date == undefined || ticket_start_date == ''){
	//                 var ticket_start_date=$('#donation_start_date').val();
	//             }
	//         }
	//         var ticket_start_time=$('#free_start_time').val();
	//         if (ticket_start_time==undefined || ticket_start_time == ''){
	//             var ticket_start_time=$('#paid_start_time').val();
	//             if (ticket_start_time == undefined || ticket_start_time == ''){
	//                 var ticket_start_time=$('#donation_start_time').val();
	//             }
	//         }
	//         var ticket_end_date=$('#free_end_date').val();
	//         if (ticket_end_date==undefined || ticket_end_date == ''){
	//             var ticket_end_date=$('#paid_end_date').val();
	//             if (ticket_end_date == undefined || ticket_end_date == ''){
	//                 var ticket_end_date=$('#donation_end_date').val();
	//             }
	//         }
	//         var ticket_end_time=$('#free_end_time').val();
	//         if (ticket_end_time==undefined || ticket_end_time == ''){
	//             var ticket_end_time=$('#paid_end_time').val();
	//             if (ticket_end_time == undefined || ticket_end_time == ''){
	//                 var ticket_end_time=$('#donation_end_time').val();
	//             }
	//         }
	//         var minimum_tickets=$('.free_min').val();
	//         if (minimum_tickets==undefined || minimum_tickets == ''){
	//             var minimum_tickets=$('.paid_min').val();
	//             if (minimum_tickets == undefined || minimum_tickets == ''){
	//                 var minimum_tickets=$('.donation_min').val();
	//             }
	//         }
	//         var maximum_tickets=$('.free_max').val();
	//         if (maximum_tickets==undefined || maximum_tickets == ''){
	//             var maximum_tickets=$('.paid_max').val();
	//             if (maximum_tickets == undefined || maximum_tickets == ''){
	//                 var maximum_tickets=$('.donation_max').val();
	//             }
	//         }

	//         if(event_name == '' || event_description == '' || college_name =='' || country_name == '' || city_name == '' || organizer_name == '' ||
	//             organizer_description == '' || event_type == '' || start_date == '' || end_date == '' || event_category == '' ||
	//             ticket_name == '' || quantity_available == '' || sale_channel == '' || ticket_start_date == '' || ticket_end_date == ''
	//             || ticket_start_time == '' || ticket_end_time == '' || minimum_tickets == '' || maximum_tickets == ''
	//              || event_image_name == ''){
	//         		$('.form :input').each(function(){ 
	// 			        if( $.trim($(this).val()) == "" )
	// 			        	$(this).addClass( "empty_form_fields" );
	// 			        	$('.empty_form_fields').css({"border":"solid red 1px"});
	// 			    	});
	//                 $('#empty_field_error').show();
	//         }else{
	//         	$('.loader').show().delay(2000).fadeOut();
	//             params={'event_category':event_category,'event_name':event_name,'event_description':event_description,'college_name':college_name,
	//                     'country_name':country_name,'city_name':city_name,'organizer_name':organizer_name,
	//                     'organizer_description':organizer_description,'event_type':event_type,'minimum_tickets':minimum_tickets,
	//                     'start_date':start_date,'end_date':end_date,'ticket_name':ticket_name,'maximum_tickets':maximum_tickets,
	//                     'quantity_available':quantity_available,'ticket_price':ticket_price,
	//                     'start_time':start_time,'end_time':end_time,'event_image':event_image_name,
	//                     'ticket_start_date':ticket_start_date,'ticket_end_date':ticket_end_date,'ticket_start_time':ticket_start_time,
	//                     'ticket_end_time':ticket_end_time,'ticket_type':type_ticket,'sale_channel':sale_channel,'button_click':button_click}
	//             $.ajax({
	//                 type:"POST",
	//                 url:"/create_event_data/",
	//                 data: params,
	//                 success: function(res){
	//                     if(res=='success'){
	//                         $("#confirm_modal").modal();
	            
	//                     }
	//                     else{
	//                         console.log('not saved')
	//                     }
	//                 }
	//             });
	//         }
	//     }
	// })
/********** Select min-max value *******************/
	var minimum_val=$('.minimum').val();
	var maximum_val=$('.maximum').val();
	var options='';
	for(i=minimum_val; i <= maximum_val; i++){
	    options+='<option value="'+i+'">'+i+'</option>';
	}
	$('.selectpicker').append(options);

	selected_tickets = ''
	$(".selectpicker").change(function(event) {
        var isDisabled = $("#checkout").is(':disabled');
        if (isDisabled) {
            $("#checkout").prop('disabled', false);
        }
	    var select_val = $(this).val();
	    selected_tickets = select_val
	    params={'select_val':select_val}
         $.ajax({
                    type:"POST",
                    url:"/quantity_selected/",
                    data: params,
                    success: function(res){
                        $('#quantity_selected').html(res);                       
                    }
                });
	});
	$("#checkout").click(function() {
        var eventid=$('.event_id').val();
        var ticket_id=$('.ticket_id').val();
        selected_quantity = selected_tickets
        var ticket_name = $('.event_name').val();
        params={'ticket_id':ticket_id, 'selected_quantity':selected_quantity,'eventid':eventid,'ticket_name':ticket_name}
         $.ajax({
                    type:"POST",
                    url:"/register_ticket/",
                    data: params,
                    success: function(res){
                        if(res=='success'){
                            window.location.href='/register_order/'
                        }
                        else{
                            $('#noavailable_tickets').css("color","red");
                        }
                                               
                    }
                });
    });
/********************* Send the email to Organizer ****************/
    $('.sender_name').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $(".send_msg").click(function() {
        var sender_name=$('.sender_name').val();
        var msg_sender_email = $('.sender_email').val();
        var sender_comment = $('.sender_comment').val();
        var email_valid=isEmail(msg_sender_email)
        if (email_valid == true){
                sender_email = msg_sender_email
            params={'sender_name':sender_name,'sender_email':sender_email,'sender_comment':sender_comment}
             $.ajax({
                        type:"POST",
                        url:"/send_msg/",
                        data: params,
                        success: function(success){
                            window.location.href='/register_order/'                      
                        }
                    });
        }else{
            $('#invalid_sender_email').show().delay(3000).fadeOut();
        }
    });

	/*********** Register Order****************/

    $(".order_button").click(function() {
        var ticket_name=$('.ordered_ticket_name').val();
        var quantity=$('.ordered_quantity').val();
        var first_name=$('.first_name').val();
        var last_name=$('.last_name').val();
        var emails =[];
        for (i=0;i<quantity;i++){
            var values = $('#guest_email_'+i).val();
            if (values != '') {
                var email_valid=isEmail(values)
                if (email_valid == true){
                    emails[i] = values
                }
                else{
                    $('#invalid_emails').show().delay(3000).fadeOut();
                }
            }
        }
        var total_emails = emails.length;
        if (total_emails == quantity) {
            var input_emails = emails
        }
        else{
            var input_emails = ''
        }
        var emails1= JSON.stringify(input_emails)
        var event_id=$('.ordered_event_id').val();
        var sub_event_id1=$('.subordered_event_id').val();
        if(sub_event_id1 == '' || sub_event_id1 == undefined){
            var sub_event_id='0';
        }else{
            var sub_event_id=sub_event_id1;
        }
        var ticket_id=$('.ordered_ticket_id').val();
        if( first_name == '' || last_name == '' || input_emails ==''){
                $('#registrationForm :text').each(function(){ 

                    if( $.trim($(this).val()) == "" )
                        $(this).addClass( "empty_form_fields" );
                        $('.empty_form_fields').css({"border":"solid red 1px"});

                });
                $('#empty_field_error').show();
        }else{
            $('.loader').show();
            params={'input_emails':emails1,'ticket_id':ticket_id, 'sub_event_id':sub_event_id, 'event_id':event_id,'last_name':last_name,
                    'first_name':first_name, 'quantity':quantity,'ticket_name':ticket_name}
             $.ajax({
                        type:"POST",
                        url:"/save_order/",
                        data: params,
                        success: function(success){
                            $('.loader').hide();
                            $("#confirmation_modal").modal();                       
                        }
                    });
         }
    });

/****************** No of email field as tickets selected *********************/
    // Number of inputs to create
    var number = $('.ordered_quantity').val();
    // Clear previous contents of the email_fields
   
    for (i=0;i<number;i++){
        // Append a node with a random text
        //email_fields.appendChild(document.createTextNode("Email " + (i+1)));
        var newNode = document.createElement('textNode');
            newNode.className = 'my_emails col-lg-6';
            newNode.innerHTML = 'Email_'+(i+1)+': *';
            email_fields.appendChild(newNode);
        // Create an <input> element, set its type and name attributes
        var input = document.createElement("input");
        input.type = "text";
        input.className = "form-control col-lg-6 reg_table emails_input email_"+i;
        input.name = "guest_email_"+i;
        input.id = "guest_email_"+i;
        
        email_fields.appendChild(input);
        // Append a line break 
        email_fields.appendChild(document.createElement("br"));
    }


    $(".view_ticket").click(function() {
         $.ajax({
                    type:"GET",
                    url:"/view_ticket/",
                    success: function(success){
                        window.location.href='/user_tickets_detail/'
                    }
                });
    });
   
/********** mendantory field error ********************/
	$(document).on('click', '.hide_error', function(){
		$('.mendantory_fields').hide();
	})

/**************** Sub event send data to backend ******************************/
      $(document).on('click', '#sub_event_button', function(){
        var event_id=event_clicked
        if (event_id == undefined){
            var event_id=''
        }else{
            var event_id=event_id

        }

        var subevent_image_name= subevent_image
        if (subevent_image_name==undefined || subevent_image_name == ''){
        	$('.note_content').css({"color":"red"});

        }
        /*******************Sub event detail*****************/
        var sub_event_name=$('#sub_event_name').val();
        var sub_event_description=$('#sub_event_desc').val();
        var sub_event_organizer=$('#sub_event_organizer').val();
        var sub_organizer_description=$('#sub_organizer_desc').val();
        var sub_event_type=$('#sub_event_type').val();
        var sub_event_catagory=$('#sub_event_catagory').val();
        var sub_start_date=$('#sub_start_date').val();
        var sub_start_time=$('#sub_start_time').val();
        var sub_end_date=$('#sub_end_date').val();
        var sub_end_time=$('#sub_end_time').val();
        /****************Ticket Detail******************/
        var ticket_name=$('#sub_free_tic_name').val();
        if (ticket_name==undefined || ticket_name == ''){
            var ticket_name=$('#sub_paid_tic_name').val();
            var type_ticket='paid'
            if (ticket_name == undefined || ticket_name == ''){
                var ticket_name=$('#sub_donation_tic_name').val();
                var type_ticket='donation'
            }
        }
        var quantity_available=$('#sub_free_quantity').val();
        if (quantity_available==undefined || quantity_available == ''){
            var quantity_available=$('#sub_paid_quantity').val();
            if (quantity_available == undefined || quantity_available == ''){
                var quantity_available=$('#sub_donation_quantity').val();
            }
        }

        var ticket_price=$('#sub_free_tic_price').val();
        if (ticket_price==undefined || ticket_price == ''){
            var ticket_price=$('#sub_paid_tic_price').val();
            if (ticket_price == undefined || ticket_price == ''){
                var ticket_price=$('#sub_donation_tic_price').val();
            }
        }
        var ticket_description=$('.sub_free_tic_description').val();
        if (ticket_description==undefined || ticket_description == ''){
            var ticket_description=$('.sub_paid_tic_description').val();
            if (ticket_description == undefined || ticket_description == ''){
                var ticket_description=$('.sub_donation_tic_description').val();
            }
        }
        var sale_channel=$('.sub_free_sale_channel').val();
        if (sale_channel==undefined || sale_channel == ''){
            var sale_channel=$('.sub_paid_sale_channel').val();
            if (sale_channel == undefined || sale_channel == ''){
                var sale_channel=$('.sub_donation_sale_channel').val();
            }
        }
        var ticket_start_date=$('#sub_free_start_date').val();
        if (ticket_start_date==undefined || ticket_start_date == ''){
            var ticket_start_date=$('#sub_paid_start_date').val();
            if (ticket_start_date == undefined || ticket_start_date == ''){
                var ticket_start_date=$('#sub_donation_start_date').val();
            }
        }
        var ticket_start_time=$('#sub_free_start_time').val();
        if (ticket_start_time==undefined || ticket_start_time == ''){
            var ticket_start_time=$('#sub_paid_start_time').val();
            if (ticket_start_time == undefined || ticket_start_time == ''){
                var ticket_start_time=$('#sub_donation_start_time').val();
            }
        }
        var ticket_end_date=$('#sub_free_end_date').val();
        if (ticket_end_date==undefined || ticket_end_date == ''){
            var ticket_end_date=$('#sub_paid_end_date').val();
            if (ticket_end_date == undefined || ticket_end_date == ''){
                var ticket_end_date=$('#sub_donation_end_date').val();
            }
        }
        var ticket_end_time=$('#sub_free_end_time').val();
        if (ticket_end_time==undefined || ticket_end_time == ''){
            var ticket_end_time=$('#sub_paid_end_time').val();
            if (ticket_end_time == undefined || ticket_end_time == ''){
                var ticket_end_time=$('#sub_donation_end_time').val();
            }
        }
        var minimum_tickets=$('#sub_free_order_minimum').val();
        if (minimum_tickets==undefined || minimum_tickets == ''){
            var minimum_tickets=$('#sub_paid_order_minimum').val();
            if (minimum_tickets == undefined || minimum_tickets == ''){
                var minimum_tickets=$('#sub_donation_order_minimum').val();
            }
        }
        var maximum_tickets=$('#sub_free_order_maximum').val();
        if (maximum_tickets==undefined || maximum_tickets == ''){
            var maximum_tickets=$('#sub_paid_order_maximum').val();
            if (maximum_tickets == undefined || maximum_tickets == ''){
                var maximum_tickets=$('#sub_donation_order_maximum').val();
            }
        }

        if( sub_event_name == '' || sub_event_description == '' || sub_event_organizer =='' ||
             sub_organizer_description == '' || sub_event_type =='' || sub_event_catagory == '' || 
             sub_start_date == '' || sub_start_time == '' || sub_end_date =='' || sub_end_time == '' ||
             ticket_name == '' || quantity_available == '' || ticket_description == '' || 
             sale_channel == '' || minimum_tickets == '' || maximum_tickets == '' || subevent_image_name == ''){
                $('.form1 :input').each(function(){ 

                    if( $.trim($(this).val()) == "" )
                        $(this).addClass( "empty_form_fields" );
                        $('.empty_form_fields').css({"border":"solid red 1px"});

                });
                $('#sub_empty_field_error').show();
        }else{
            params={'sub_event_name':sub_event_name,'sub_event_description':sub_event_description,
                    'sub_event_organizer':sub_event_organizer,'sub_organizer_description':sub_organizer_description,
                    'sub_event_type':sub_event_type,'sub_event_catagory':sub_event_catagory,
                    'sub_start_date':sub_start_date,'sub_start_time':sub_start_time,'sub_end_date':sub_end_date,
                    'sub_end_time':sub_end_time,'ticket_name':ticket_name,'quantity_available':quantity_available,
                    'ticket_price':ticket_price,'ticket_description':ticket_description,'sale_channel':sale_channel,
                    'ticket_start_date':ticket_start_date,'ticket_start_time':ticket_start_time,
                    'ticket_end_date':ticket_end_date,'ticket_end_time':ticket_end_time,
                    'minimum_tickets':minimum_tickets,'maximum_tickets':maximum_tickets,'subevent_image':subevent_image_name,
                    'event_id':event_id}
            $.ajax({
                type:"POST",
                url:"/create_sub_event/",
                data: params,
                success: function(res){
                    if(res=='success'){
                        saved_subevent_modal
                        $("#saved_subevent_modal").modal();
                        $("#sub_modal").modal('hide');

                    }
                    else{

                        window.location.href='/create_event/'
                        $('#account_not_del').show().delay(3000).fadeOut();
                    }
                }
            });
        }
    })


$('#show_sub_event_modal').click(function(){
        $("#sub_modal").modal();
        $("#confirm_modal").modal('hide');
    })
/************ Extra link to apply the validation **************/
$('#show_sub_event_modal1').click(function(){
        $("#sub_modal").modal();
        $("#confirm_modal").modal('hide');
    })

/******************** Ends here **************************/

    /**************event_affiliate and for tax from's drowdown***************/
    $('.js-xd-accordion-header').click(function(){
        var cat=$(this).attr('data-num')
        var visibility = $('#aria_tabpanel_'+cat).is(':visible')
        if (visibility==false) {
            $('#aria_tabpanel_'+cat).show()
        }
        else{
            $('#aria_tabpanel_'+cat).hide()
        }
    })
/*************Tax Forms********************************/
/*************Radio button to select different forms*****************/
    $("input[name$='form_type']").click(function() {
        var test = $(this).val();
        $(".hidden").hide();
        $("#"+test+"_form_container").show();
    });

/************* Close Account *****************/
    $("#close_account").click(function() {
        if (!$('input[name=reason]:checked').val()) {
            alert('Please tell us why you are leaving')
        }else{
            var radio_val = $('input[name=reason]:checked').val();
            if (radio_val=='other') {
                var reason=$('#reason_desc').val();
            }else{
                var reason = radio_val
            }
            var close=$('#close').val();
            var password_user=$('#passwd').val();
            if (close != "CLOSE") {
                alert('Please enter "CLOSE" in the text box below')
            }else{
                params={'reason':reason,'close':close,'password_user':password_user}
                $.ajax({
                    type:"POST",
                    url:"/close_account/",
                    data: params,
                    success: function(res){
                        if(res=='removed'){
                            window.location.href='/'
                            alert('Your account is removed successfully')
                        }
                        else{

                            window.location.href='/account-close/'
                            $('#account_not_del').show().delay(3000).fadeOut();
                        }
                    }
                });
            }
        }
    });
/*************Change Password functionality**********************/
    $('#change_password').click(function(){
        var oldpassword=$('#oldpassword').val();
        var newpassword1=$('#newpassword1').val();
        var newpassword2=$('#newpassword2').val();
        if(oldpassword == '' ||  newpassword1 == '' || newpassword2 == ''){
            $('#blank_pass').show().delay(3000).fadeOut();
        }else{
            if (newpassword1 != newpassword2) {
                $('#not_match').show().delay(3000).fadeOut();
            }else{
                params={'oldpassword':oldpassword,'newpassword1':newpassword1,'newpassword2':newpassword2}
                $.ajax({
                    type:"POST",
                    url:"/password/",
                    data: params,
                    success: function(res){
                        if(res=='authentic'){
                            window.location.href='/password/'
                            $('#pass_changed').show().delay(3000).fadeOut();
                        }
                        else{
                            window.location.href='/password/'
                            $('#pass_not_changed').show().delay(3000).fadeOut();
                        }
                    }
                });
            }
        }
    })

    /***************Search Functionality for events*************/
    $('#search_events').click(function(){
        var event=$('#event').val();
        var place=$('#place').val();
        var date=$('#date').val();
        if(event == '' &&  place == ''){
            alert('Please fill category or place')
        }else{
            params={'category':event,'location':place,'date':date}
            $.ajax({
                type:"POST",
                url:"/search_events/",
                data: params,
                success: function(res){
                    $('.search_results').html(res);
                }
            });
        }





    })
    /*************Ends here*************************/
    /***************SignUp Function*******************/
    $('#sign-up').click(function(){
        var email=$('#email').val();
        var password=$('#password').val();
        if(email == '' || password == ''){
            $('#blank_fields').show().delay(3000).fadeOut();
        }else{
            var email_valid=isEmail(email)
            if (email_valid == true){
                params={'email':email,'password':password}
                $.ajax({
                        type:"POST",
                        url:"/register/",
                        data: params,
                        success: function(res){
                            if(res=='already exists'){
                               $('#already_email').show().delay(3000).fadeOut();
                            }
                            if(res=='saved'){
                                window.location.href='/dashboard'
                            }else{
                                console.log('not saved')
                            }
                        }
                 });
            }else{
                $('#invalid_email').show().delay(3000).fadeOut();
            }
        }

    });

    /***************Login Function*******************/
    $('#login_btn').click(function(){
        var email=$('#input_email').val();
        var password=$('#input_password').val();
        if(email == '' || password == ''){
            $('#blank_fields_login').show().delay(3000).fadeOut();
        }else{
            var email_valid=isEmail(email)
            if (email_valid == true){
                params={'email':email,'password':password}
                $.ajax({
                        type:"POST",
                        url:"/login/",
                        data: params,
                        success: function(res){
                            if(res=='authentic'){
                                window.location.href='/dashboard/'
                               }
                            else{
                                $('#invalid_credentials').show().delay(3000).fadeOut();
                            }
                        }
                 });
            }else{
                $('#invalid_email').show().delay(3000).fadeOut();
            }
        }

    });

    /***********function to test email***************/
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $('.imgDescription').click(function(){
        var cat=$(this).attr('data-category')
         params={'category':cat}
                $.ajax({
                        type:"POST",
                        url:"/category_filters/",
                        data: params,
                        success: function(res){
                            if(res){
                                console.log('success')
                               }
                            else{
                               console.log('error')
                            }
                        }
                 });
    })

    /*************************Account_Settings Page********************************/
    $('#change_email').click(function(){
        $('#email_change_div').show();
    })
    $('#save_email').click(function(){
        var old_email=$('#old_email').val()
        var new_email=$('#new_email').val();
        var current_password=$('#current_password').val();
        var email_valid=isEmail(new_email)
        if(new_email != '' && current_password != ''){
            if (email_valid == true){
                params={'old_email':old_email,'current_password':current_password,'new_email':new_email}
                $.ajax({
                        type:"POST",
                        url:"/change_email/",
                        data: params,
                        success: function(res){
                            if(res=='wrong'){
                                 $('#wrong_password').show().delay(3000).fadeOut();
                               }
                            else{
                                $('#success').show().delay(3000).fadeOut();
                            }
                        }
                 });
            }else{
                $('#invalid_email_msg').show().delay(3000).fadeOut();
            }
        }else{
            $('#empty_msg').show().delay(3000).fadeOut();
        }
    })

    /*********Image Upload function***********************/
    profile_image = ''
    $('#upload').click(function(){
        var base_string=$('#profile').attr('src');
        var file_name    = document.querySelector('input[type=file]').files[0].name;
        if(base_string!=''){
            params={'base_string':base_string,'file_name':file_name}
                $.ajax({
                        type:"POST",
                        url:"/upload_image/",
                        data: params,
                        success: function(res){
                            if(res=='wrong'){
                                 $('#wrong_password').show().delay(3000).fadeOut();
                               }
                            else{
                                profile_image=file_name;
                                $('#success').show().delay(3000).fadeOut();
                            }
                        }
                 });
        }
    })

    /*********Event Image Upload function***********************/
    var event_image=''
    $('#upload_event_image ').click(function(){
        var base_string=$('#profile').attr('src');
        var file_name    = document.querySelector('input[type=file]').files[0].name;
        if(base_string!=''){
            params={'base_string':base_string,'file_name':file_name}
                $.ajax({
                        type:"POST",
                        url:"/upload_event_image/",
                        data: params,
                        success: function(res){
                            if(res=='wrong'){
                                 $('#wrong_password').show().delay(3000).fadeOut();
                               }
                            else{
                                event_image=file_name;
                            }
                        }
                 });
        }
    })

    /****************Delete an event*************/
    $('.delete_particular_event').click(function(){
            if (confirm("Are you sure?")) {
            var event_id= $(this).attr('data-id');
            params={'event_id':event_id}
            $.ajax({
                            type:"POST",
                            url:"/delete_event/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                     $("#delete_event_modal").modal();
                                   }
                            }
            });
        }else{
            return false;
        }
        


    })


     /****************Edit an event*************/
    $('.edit_particular_event').click(function(){
            var event_id= $(this).attr('data-id');
            params={'event_id':event_id}
            $.ajax({
                            type:"POST",
                            url:"/edit_event/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                     window.location.href="/manage_events/edit_event/"
                                   }
                            }
            });
        


    })

   /* $(document).on('click','.open_create_event',function(){
         $.ajax({
                        type:"GET",
                        url:"/create_event/",
                        success: function(res){
                            if(res=='empty'){
                                $('#myModal1').modal();
                               }
                            else{
                                window.location.href='/create_event/'
                            }
                        }
                 });

    })*/
    /*********SubEvent modal Image Upload function***********************/
    var subevent_image=''
    $('#upload_subevent_image ').click(function(){
        var base_string=$('#profile_sub').attr('src');
        var file_name1    = document.querySelector('#sub_event_img').files[0].name;
        if(base_string!=''){
            params={'base_string':base_string,'file_name':file_name1}
                $.ajax({
                        type:"POST",
                        url:"/upload_subevent_image/",
                        data: params,
                        success: function(res){
                            if(res=='wrong'){
                                 $('#wrong_password').show().delay(3000).fadeOut();
                               }
                            else{
                                subevent_image=file_name1;
                            }
                        }
                 });
        }
    })
/*********SubEvent main Image Upload function***********************/
    var subevent_image=''
    $('#main_upload_subevent_image ').click(function(){
        var base_string=$('#main_profile_sub').attr('src');
        var file_name1    = document.querySelector('#main_sub_event_img').files[0].name;
        if(base_string!=''){
            params={'base_string':base_string,'file_name':file_name1}
                $.ajax({
                        type:"POST",
                        url:"/upload_subevent_image/",
                        data: params,
                        success: function(res){
                            if(res=='wrong'){
                                 $('#wrong_password').show().delay(3000).fadeOut();
                               }
                            else{
                                subevent_image=file_name1;
                            }
                        }
                 });
        }
    })

    /*********************Save account information*************************************/
    $('#save_account').click(function(){
        //***Contact info*****//
        var first_name=$('#first_name').val();
        var last_name=$('#last_name').val();
        var last_name=$('#last_name').val();
        var home_phone=$('#home_phone').val();
        var cell_phone=$('#cell_phone').val();
        var job_title=$('#job_title').val();
        var company=$('#company').val();
        var website=$('#website').val();
        var blog=$('#blog').val();

        //***Home Address*****//
        var address=$('#address').val();
        var address2=$('#address2').val();
        var city=$('#city').val();
        var country=$('#country').val();
        var zip=$('#zip').val();
        var state=$('#state').val();

        //***billing Address*****//
        var b_address=$('#b_address').val();
        var b_address2=$('#b_address2').val();
        var b_city=$('#b_city').val();
        var b_country=$('#b_country').val();
        var b_zip=$('#b_zip').val();
        var b_state=$('#b_state').val();

        //***Shipping Address*****//
        var s_address=$('#s_address').val();
        var s_address2=$('#s_address2').val();
        var s_city=$('#s_city').val();
        var s_country=$('#s_country').val();
        var s_zip=$('#s_zip').val();
        var s_state=$('#s_state').val();

        //***Work Address*****//
        var w_address=$('#w_address').val();
        var w_address2=$('#w_address2').val();
        var w_city=$('#w_city').val();
        var w_country=$('#w_country').val();
        var w_zip=$('#w_zip').val();
        var w_state=$('#w_state').val();

        //***Other Info*****//
        var gender=$('#gender').val();
        var birth_month=$('#birth_month').val();
        var birth_day=$('#birth_day').val();
        var birth_year=$('#birth_year').val();
        var birth_date=birth_day+'/'+birth_month+'/'+birth_year   
        var age=$('#age').val();     

        if(first_name != '' || last_name != '' || home_phone !='' || website != '' || address != '' || city != '' || 
            country != '' || zip != '' || state != '' || age != ''){


        }
    })

    /*************************Browse events Page Filters********************************/
    /****Free events filter function****/
    $('#free_events').click(function(){
        params={'filter':'free'}
        $.ajax({
                        type:"POST",
                        url:"/free_events/",
                        data: params,
                        success: function(res){
                            if(res){
                                 $('#browse_results').html(res);
                               }
                        }
                 });


    })
    /****Free events filter function****/
    $('#paid_events').click(function(){
        params={'filter':'paid'}
        $.ajax({
                        type:"POST",
                        url:"/paid_events/",
                        data: params,
                        success: function(res){
                            if(res){
                                 $('#browse_results').html(res);
                               }
                        }
                 });


    })
    /********Filter using Category*********/
    $('.category_name').click(function(){
        var category_name=$(this).attr('data-category');
         params={'category':category_name}
        $.ajax({
                        type:"POST",
                        url:"/filter_events/",
                        data: params,
                        success: function(res){
                            if(res){
                                 $('#browse_results').html(res);
                               }
                        }
        });

    })
    /************Filter using Location***************/
     $('.location_name').click(function(){
        var location_name=$(this).attr('data-location');
         params={'location':location_name}
        $.ajax({
                        type:"POST",
                        url:"/filter_events/",
                        data: params,
                        success: function(res){
                            if(res){
                                 $('#browse_results').html(res);
                               }
                        }
        });

    })

    /************Filter using Dates***************/
     $('.period_value').click(function(){
        var period=$(this).attr('data-period');
         params={'period':period}
        $.ajax({
                        type:"POST",
                        url:"/filter_events/",
                        data: params,
                        success: function(res){
                            if(res){
                                 $('#browse_results').html(res);
                               }
                        }
        });

    })

    /************Filter using custom Dates***************/
    $('#get_dates_events').click(function(){
        var start_date=$('#datepicker').val();
        var end_date=$('#end_datepicker').val();
        params={'start_date':start_date,'end_date':end_date}
        $.ajax({
                        type:"POST",
                        url:"/filter_events/",
                        data: params,
                        success: function(res){
                            if(res){
                                 $('#browse_results').html(res);
                               }
                        }
        });


    })


/******************************** create event image upload ***************************/

    $('#upload_pic').click(function(){
            var base_string=$('#event_pic').attr('src');
            var file_name    = document.querySelector('input[type=file]').files[0].name;
            if(base_string!=''){
                params={'base_string':base_string,'file_name':file_name}
                    $.ajax({
                            type:"POST",
                            url:"/upload_pic/",
                            data: params,
                            success: function(res){
                                
                            }
                     });
            }
        })

/********************************* Create Event ****************************************/

    
    $('#event_location_change').click(function(){
        $('#location_div').show();
        $('#event_location').hide();
    })

    $('#reset_location').click(function(){
        $('#location_div').hide();
        $('#event_location').show();
    })


    $('#browse_event_pic').click(function(){
        $('#event_pic').show();
        $('#capture_image').hide();
    })


    $('#online_event').click(function(){
        $('#event_location_input').attr('disabled','disabled');
        $('#event_location_input').val("This is an online event");
        $('#online_event').hide();
        $('#event_location_change').hide();
        $('#add_location').show();
    })

    $('#add_location').click(function(){
        $('#event_location_input').attr('disabled',false);
        $('#event_location_input').val("Specify where it's held.");
        $('#online_event').show();
        $('#event_location_change').show();
        $('#add_location').hide();
    })

    $('#public_page').click(function(){
        $('#public_type').show();
        $('#private_type').hide();
    })

    $('#private_page').click(function(){
        $('#private_type').show();
        $('#public_type').hide();
    })
    var event_clicked = ''

    $('.event_subevent_modal').click(function(){
    	event_clicked=$(this).attr('data-event-id')
        $('#sub_modal').modal();
        $('#events_list').modal('hide');
    })

     /********************* Edit Sub Event ****************************/

  $('.edit_particular_subevent').click(function(){
            var sub_event_id= $(this).attr('data-id');
            params={'sub_event_id':sub_event_id}
            $.ajax({
                            type:"POST",
                            url:"/edit_subevent/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                     window.location.href="/edit_subevents/"
                                   }
                            }
            });


    })

  /*******************View sub events************************/

    $('.edit_this_event').click(function(){
            var event_id= $(this).attr('data-id');
            params={'event_id':event_id}
            $.ajax({
                            type:"POST",
                            url:"/view_subevents/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    window.location.href="/view_this_subevent/"
                                }
                            }
            });


    })

    /*************************** Update sub event ******************************/

    $('#update_subevent').click(function(){
        var sub_event_image=subevents_image
        var sub_event_id= $('#sub_event_id').val();
        var sub_event_name=$('#subevent_name').val();
        var sub_event_description=$('#subevent_desc').val();
        var sub_event_organizer=$('#organizer_name').val();
        var sub_organizer_description=$('#organizer_desc').val();
        var sub_event_type=$('#subevent_type').val();
        var sub_start_date=$('#sub_start_date').val();
        var sub_start_time=$('#sub_start_time').val();
        var sub_end_date=$('#sub_end_date').val();
        var sub_end_time=$('#sub_end_time').val();


        /****************Ticket Detail******************/
        var sub_ticket_name=$('#subevent_free_tic_name').val();
        if (sub_ticket_name==undefined || sub_ticket_name == ''){
            var sub_ticket_name=$('#subevent_paid_tic_name').val();
            var type_ticket='paid'
            if (sub_ticket_name == undefined || sub_ticket_name == ''){
                var sub_ticket_name=$('#subevent_donation_tic_name').val();
                var type_ticket='donation'
            }
        }
        var quantity_available=$('#subevent_free_quantity').val();
        if (quantity_available==undefined || quantity_available == ''){
            var quantity_available=$('#subevent_paid_quantity').val();
            if (quantity_available == undefined || quantity_available == ''){
                var quantity_available=$('#subevent_donation_quantity').val();
            }
        }

        var ticket_price=$('#subevent_free_tic_price').val();
        if (ticket_price==undefined || ticket_price == ''){
            var ticket_price=$('#subevent_paid_tic_price').val();
            if (ticket_price == undefined || ticket_price == ''){
                var ticket_price=$('#subevent_donation_tic_price').val();
            }
        }
        var ticket_description=$('.subevent_free_ticket_description').val();
        if (ticket_description==undefined || ticket_description == ''){
            var ticket_description=$('.subevent_paid_ticket_description').val();
            if (ticket_description == undefined || ticket_description == ''){
                var ticket_description=$('.subevent_donation_ticket_description').val();
            }
        }
        var sale_channel=$('.subevent_free_sale_channel').val();
        if (sale_channel==undefined || sale_channel == ''){
            var sale_channel=$('.subevent_paid_sale_channel').val();
            if (sale_channel == undefined || sale_channel == ''){
                var sale_channel=$('.subevent_donation_sale_channel').val();
            }
        }
        var ticket_start_date=$('#subevent_free_start_date').val();
        if (ticket_start_date==undefined || ticket_start_date == ''){
            var ticket_start_date=$('#subevent_paid_start_date').val();
            if (ticket_start_date == undefined || ticket_start_date == ''){
                var ticket_start_date=$('#subevent_donation_start_date').val();
            }
        }
        var ticket_start_time=$('#subevent_free_start_time').val();
        if (ticket_start_time==undefined || ticket_start_time == ''){
            var ticket_start_time=$('#subevent_paid_start_time').val();
            if (ticket_start_time == undefined || ticket_start_time == ''){
                var ticket_start_time=$('#subevent_donation_start_time').val();
            }
        }
        var ticket_end_date=$('#subevent_free_end_date').val();
        if (ticket_end_date==undefined || ticket_end_date == ''){
            var ticket_end_date=$('#subevent_paid_end_date').val();
            if (ticket_end_date == undefined || ticket_end_date == ''){
                var ticket_end_date=$('#subevent_donation_end_date').val();
            }
        }
        var ticket_end_time=$('#subevent_free_end_time').val();
        if (ticket_end_time==undefined || ticket_end_time == ''){
            var ticket_end_time=$('#subevent_paid_end_time').val();
            if (ticket_end_time == undefined || ticket_end_time == ''){
                var ticket_end_time=$('#subevent_donation_end_time').val();
            }
        }
        var minimum_tickets=$('#subevent_free_order_minimum').val();
        if (minimum_tickets==undefined || minimum_tickets == ''){
            var minimum_tickets=$('#subevent_paid_order_minimum').val();
            if (minimum_tickets == undefined || minimum_tickets == ''){
                var minimum_tickets=$('#subevent_donation_order_minimum').val();
            }
        }
        var maximum_tickets=$('#subevent_free_order_maximum').val();
        if (maximum_tickets==undefined || maximum_tickets == ''){
            var maximum_tickets=$('#subevent_paid_order_maximum').val();
            if (maximum_tickets == undefined || maximum_tickets == ''){
                var maximum_tickets=$('#subevent_donation_order_maximum').val();
            }
        }

        if(sub_event_name == '' || sub_event_description == '' || sub_event_organizer =='' ||
	         sub_organizer_description == '' || sub_event_type =='' || 
	         sub_start_date == '' || sub_start_time == '' || sub_end_date =='' || sub_end_time == '' ||
	         sub_ticket_name == '' || quantity_available == '' || ticket_description == '' || 
	         sale_channel == '' || minimum_tickets == '' || maximum_tickets == '' || sub_event_image == ''){
	    		$('.form :input').each(function(){ 
			        if( $.trim($(this).val()) == "" )
			        	$(this).addClass( "empty_form_fields" );
			        	$('.empty_form_fields').css({"border":"solid red 1px"});
			    	});
	            $('#empty_field_error').show();

        }else{params={'sub_event_id':sub_event_id,'sub_event_image':sub_event_image,'sub_event_name':sub_event_name,'sub_event_description':sub_event_description,
            'sub_event_organizer':sub_event_organizer,'sub_organizer_description':sub_organizer_description,
            'sub_event_type':sub_event_type,'sub_start_date':sub_start_date,'sub_end_date':sub_end_date,
            'sub_start_time':sub_start_time,'sub_end_time':sub_end_time,'sub_ticket_name':sub_ticket_name,
            'quantity_available':quantity_available,'ticket_price':ticket_price,
            'ticket_description':ticket_description,'sale_channel':sale_channel,'ticket_start_date':ticket_start_date,
            'ticket_start_time':ticket_start_time,'ticket_end_date':ticket_end_date,'ticket_end_time':ticket_end_time,
            'minimum_tickets':minimum_tickets,'maximum_tickets':maximum_tickets}
                $.ajax({
                            type: "POST",
                            url: "/update_subevent/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                     alert('Sub-Event Changed Successfully')
                                   }
                            }
            });
            } 
    })


/*****************Update Sub Event Image***********************/
    var subevents_image=''
    $('#update_sub_image ').click(function(){
        $('.update_subevent_image').show();
        $('.subevent_image_upload').hide();
        $('#update_sub_image').hide();
        var base_string=$('#update_profile_sub').attr('src');
        var file_name1    = document.querySelector('#subevent_update_img').files[0].name;
        if(base_string!=''){
            params={'base_string':base_string,'file_name':file_name1}
                $.ajax({
                        type:"POST",
                        url:"/update_subevent_image/",
                        data: params,
                        success: function(res){
                            if(res=='wrong'){
                                 $('#wrong_password').show().delay(3000).fadeOut();
                               }
                            else{
                                subevents_image=file_name1;
                            }
                        }
                 });
        }
    })

    $('#update_img_close ').click(function(){
        $('.update_subevent_image').hide();
        $('.subevent_image_upload').show();
        $('#update_sub_image').show();
         })

    /************************** Edit Sub Event ******************************/

$( function() {
        $( "#subevent_startdate" ).datepicker();
    } );
    $( function() {
        $( "#subevent_enddate" ).datepicker();
    } );


    /*$( function(){
        $('#subevent_starttime').timepicker({ 'scrollDefault': 'now' });
    });
    $( function(){
        $('#subevent_endtime').timepicker({ 'scrollDefault': 'now' });
    });*?

/********************Free Ticket************/

$( function() {
        $( "#subevent_free_start_date" ).datepicker();
    } );
    $( function() {
        $( "#subevent_free_end_date" ).datepicker();
    } );


    // $( function(){
    //     $('#subevent_free_start_time').timepicker({ 'scrollDefault': 'now' });
    // });
    // $( function(){
    //     $('#subevent_free_end_time').timepicker({ 'scrollDefault': 'now' });
    // });

/**************Paid ticket*****************/

$( function() {
        $( "#subevent_paid_start_date" ).datepicker();
    } );
    $( function() {
        $( "#subevent_paid_end_date" ).datepicker();
    } );


    // $( function(){
    //     $('#subevent_paid_start_time').timepicker({ 'scrollDefault': 'now' });
    // });
    // $( function(){
    //     $('#subevent_paid_end_time').timepicker({ 'scrollDefault': 'now' });
    // });

/**************donation******************/

$( function() {
        $( "#subevent_donation_start_date" ).datepicker();
    } );
    $( function() {
        $( "#subevent_donation_end_date" ).datepicker();
    } );


    // $( function(){
    //     $('#subevent_donation_start_time').timepicker({ 'scrollDefault': 'now' });
    // });
    // $( function(){
    //     $('#subevent_donation_end_time').timepicker({ 'scrollDefault': 'now' });
    // });



/***************** manage particular event *******************/

    $('#particular_invite').click(function(){
        $('#manage_tab_invite').show();
        $('#manage_tab_visit').hide();
        $('#manage_tab_tickets').hide();
    })

    $('#particular_visits').click(function(){
        $('#manage_tab_invite').hide();
        $('#manage_tab_visit').show();
        $('#manage_tab_tickets').hide();
    })

    $('#particular_tickets').click(function(){
        $('#manage_tab_invite').hide();
        $('#manage_tab_visit').hide();
        $('#manage_tab_tickets').show();
    })

    $('.manage_particular_event').click(function(){
            var event_id= $(this).attr('data-id');
            params={'event_id':event_id}
            $.ajax({
                            type:"POST",
                            url:"/manage_particular_events/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    window.location.href="/manage_particular_event/"
                                }
                            }
            });


    })

/**************************Event Publish****************************/

    $('.event_publish').click(function(){
            var event_id=$('#event_publish').attr('data-id');
            parmas={'event_id':event_id}
                $.ajax({
                            type:"POST",
                            url:"/event_publish/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    $("#live_confirm_modal").modal()
                                }
                            }
            });


    })

    $('#event_unpublish').click(function(){
        var event_id=$('#event_unpublish').attr('data-id');
            parmas={'event_id':event_id}
                $.ajax({
                            type:"POST",
                            url:"/event_unpublish/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    $("#notlive_confirm_modal").modal()
                                }
                            }
            });
    })

    /*********************Live Modal Close********************/

    $('#live_modal_close').click(function(){
    var event_id= $(this).attr('data-id');
    params={'event_id':event_id}
        $.ajax({
                            type:"POST",
                            url:"/manage_particular_events/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    window.location.href="/manage_particular_event/"
                                }
                            }
            });


    })

    $('#notlive_modal_close').click(function(){
    var event_id= $(this).attr('data-id');
    params={'event_id':event_id}
        $.ajax({
                            type:"POST",
                            url:"/manage_particular_events/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    window.location.href="/manage_particular_event/"
                                }
                            }
            });


    })

/********************** Edit Particular Ticket *****************************/

    $('.edit_ticket_info').click(function(){
        $("#first_name").prop('readonly', false);
        $("#last_name").prop('readonly', false);
        $("#email_address1").prop('readonly', false);
        $('.manage_particular_ticket').show();
        $('.edit_ticket_info').hide();
    })
    $('#cancel_editing').click(function(){
        $("#first_name").prop('readonly', true);
        $("#last_name").prop('readonly', true);
        $("#email_address1").prop('readonly', true);
        $('.manage_particular_ticket').hide();
        $('.edit_ticket_info').show();
    })
    $('#edit_particular_ticket').click(function(){
         var owner_first_name= $('#first_name').val();
         var owner_last_name= $('#last_name').val();
         var owner_email_address= $('#email_address1').val();
         var transaction_id= $('#hid_field').val();
         if(owner_first_name == '' || owner_last_name == '' || owner_email_address =='' || transaction_id ==''){
                $('.ticket_info :input').each(function(){ 
                    if( $.trim($(this).val()) == "" )
                        $(this).addClass( "empty_form_fields" );
                        $('.empty_form_fields').css({"border":"solid red 1px"});
                    });
                $('#edit_field_error').show();

        }else{params={'owner_first_name':owner_first_name,'owner_last_name':owner_last_name,
                        'owner_email_address':owner_email_address,'transaction_id':transaction_id}
                $.ajax({
                            type: "POST",
                            url: "/edit_particular_ticket/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    $('#ticket_success').show().delay(5000).fadeOut();
                                    $("#first_name").prop('readonly', true);
                                    $("#last_name").prop('readonly', true);
                                    $("#email_address1").prop('readonly', true);
                                    $('.manage_particular_ticket').hide();
                                    $('.edit_ticket_info').show();
                                   }
                            }
            });
            } 
    })

/***************** Cancel order for booked ticket confirmation **********************/
    $("#cancel_order").click(function(){
        $("#delete_message").slideDown();
    });
    $("#cancel").click(function(){
        $("#delete_message").slideUp();
    });

    $('#delete_ticket_order ').click(function(){
        var transaction_id= $('#hid_field').val();
        params={'transaction_id':transaction_id}
                $.ajax({
                            type: "POST",
                            url: "/delete_particular_ticket/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    $("#confirm_del_modal").modal();
                                    
                                   }
                            }
            });
    })



    /****************** Update Event ****************/

    $('#update_event_button').click(function(){
        var event_id=$('#update_event_id').val();
        var event_name=$('#event_name').val();
        var event_description=$('#event_desc').val();
        var college_name=$('#college_name').val();
        var country_name=$('#country_name').val();
        var city_name=$('#city_name').val();
        var organizer_name=$('#organizer_name').val();
        var organizer_description=$('#organizer_desc').val();
        var event_type=$('#event_type').val();
        var start_date=$('#start_date').val();
        var start_time=$('#start_time').val();
        var end_date=$('#end_date').val();
        var end_time=$('#end_time').val();
        var event_category=$('#event_category').val();
        var event_image_name = events_image
        
        /****************Ticket Detail******************/
        var ticket_name=$('#free_tic_name').val();
        var type_ticket='free'
        if (ticket_name==undefined || ticket_name == ''){
            var ticket_name=$('#paid_tic_name').val();
            var type_ticket='paid'
            if (ticket_name == undefined || ticket_name == ''){
                var ticket_name=$('#donation_tic_name').val();
                var type_ticket='donation'
            }
        }
        var quantity_available=$('#free_quantity').val();
        if (quantity_available==undefined || quantity_available == ''){
            var quantity_available=$('#paid_quantity').val();
            if (quantity_available == undefined || quantity_available == ''){
                var quantity_available=$('#donation_quantity').val();
            }
        }
        var ticket_price=$('#free_tic_price').val();
        if (ticket_price==undefined || ticket_price == ''){
            var ticket_price=$('#paid_tic_price').val();
            if (ticket_price == undefined || ticket_price == ''){
                var ticket_price=$('#donation_tic_price').val();
            }
        }
        var sale_channel=$('.free_sale_channel').val();
        if (sale_channel==undefined || sale_channel == ''){
            var sale_channel=$('.paid_sale_channel').val();
            if (sale_channel == undefined || sale_channel == ''){
                var sale_channel=$('.donation_sale_channel').val();
            }
        }
        var ticket_start_date=$('#free_start_date').val();
        if (ticket_start_date==undefined || ticket_start_date == ''){
            var ticket_start_date=$('#paid_start_date').val();
            if (ticket_start_date == undefined || ticket_start_date == ''){
                var ticket_start_date=$('#donation_start_date').val();
            }
        }
        var ticket_start_time=$('#free_start_time').val();
        if (ticket_start_time==undefined || ticket_start_time == ''){
            var ticket_start_time=$('#paid_start_time').val();
            if (ticket_start_time == undefined || ticket_start_time == ''){
                var ticket_start_time=$('#donation_start_time').val();
            }
        }
        var ticket_end_date=$('#free_end_date').val();
        if (ticket_end_date==undefined || ticket_end_date == ''){
            var ticket_end_date=$('#paid_end_date').val();
            if (ticket_end_date == undefined || ticket_end_date == ''){
                var ticket_end_date=$('#donation_end_date').val();
            }
        }
        var ticket_end_time=$('#free_end_time').val();
        if (ticket_end_time==undefined || ticket_end_time == ''){
            var ticket_end_time=$('#paid_end_time').val();
            if (ticket_end_time == undefined || ticket_end_time == ''){
                var ticket_end_time=$('#donation_end_time').val();
            }
        }
        var minimum_tickets=$('.free_ticket_minimum').val();
        if (minimum_tickets==undefined || minimum_tickets == ''){
            var minimum_tickets=$('.paid_ticket_minimum').val();
            if (minimum_tickets == undefined || minimum_tickets == ''){
                var minimum_tickets=$('.donation_ticket_minimum').val();
            }
        }
        var maximum_tickets=$('.free_ticket_maximum').val();
        if (maximum_tickets==undefined || maximum_tickets == ''){
            var maximum_tickets=$('.paid_ticket_maximum').val();
            if (maximum_tickets == undefined || maximum_tickets == ''){
                var maximum_tickets=$('.donation_ticket_maximum').val();
            }
        }
        if(event_name == '' || event_description == '' || college_name =='' || country_name == '' || city_name == '' || organizer_name == '' ||
            organizer_description == '' || event_type == '' || start_date == '' || end_date == '' || event_category == '' ||
                ticket_name == '' || quantity_available == '' || sale_channel == '' || ticket_start_date == '' || ticket_end_date == ''
                || ticket_start_time == '' || ticket_end_time == '' || minimum_tickets == '' || maximum_tickets == ''){
            $('.event_form :input').each(function(){ 
                        if( $.trim($(this).val()) == "" )
                            $(this).addClass( "empty_form_fields" );
                            $('.empty_form_fields').css({"border":"solid red 1px"});
                        });
                    $('#empty_field_error').show();
        }else{
            params={'event_id':event_id,'event_category':event_category,'event_name':event_name,'event_description':event_description,'college_name':college_name,
                    'country_name':country_name,'city_name':city_name,'organizer_name':organizer_name,
                    'organizer_description':organizer_description,'event_type':event_type,
                    'start_date':start_date,'end_date':end_date,'ticket_name':ticket_name,'maximum_tickets':maximum_tickets,
                    'quantity_available':quantity_available,'ticket_price':ticket_price,'minimum_tickets':minimum_tickets,
                    'start_time':start_time,'end_time':end_time,'ticket_start_date':ticket_start_date,'ticket_end_date':ticket_end_date,'ticket_start_time':ticket_start_time,
                    'ticket_end_time':ticket_end_time,'ticket_type':type_ticket,'sale_channel':sale_channel,'events_image':event_image_name}
            $.ajax({
                        type:"POST",
                        url:"/update_event_data/",
                        data: params,
                        success: function(res){
                            if(res=='success'){
                                $("#update_confirm_modal").modal();
                    
                            }
                            else{
                                console.log('not saved')
                            }
                        }
                    });

        }

    })

    
/*****************Update Event Image***********************/
    var events_image=''
    $('#update_event_image ').click(function(){
        $('.update_event_image').show();
        $('#event_image_upload').hide();
        $('#update_event_image ').hide();
        var base_string=$('#update_profile_event').attr('src');
        var file_name1    = document.querySelector('#event_update_img').files[0].name;
        if(base_string!=''){
            params={'base_string':base_string,'file_name':file_name1}
                $.ajax({
                        type:"POST",
                        url:"/update_event_image/",
                        data: params,
                        success: function(res){
                            if(res=='wrong'){
                                 $('#wrong_password').show().delay(3000).fadeOut();
                               }
                            else{
                                events_image=file_name1;
                            }
                            
                        }
                 });
        }
    })

    $('#update_image_close ').click(function(){
        $('.update_event_image').hide();
        $('#event_image_upload').show();
        $('#update_event_image ').show();
         })


    /*********************Update Modal Close********************/

    $('#update_modal_close').click(function(){
    var event_id= $(this).attr('data-id');
    params={'event_id':event_id}
        $.ajax({
                            type:"POST",
                            url:"/manage_particular_events/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    window.location.href="/manage_particular_event/"
                                }
                            }
            });


    })

    /**************************complete registration****************************/

    $('#complete_event_registration').click(function(){
            var event_id=$('#complete_event_registration').attr('data-id');
            parmas={'event_id':event_id}
                $.ajax({
                            type:"POST",
                            url:"/complete_registration/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    console.log('Done')
                                }
                            }
            });


    })


    /*******************For register modal (subevent)***********************/
    $('.subevent_registration').click(function(){
        var sub_event_id=$(this).attr('data-id');
        var event_id=$('.event_id').val();
        params={'sub_event_id':sub_event_id,'event_id':event_id}
            $.ajax({
                            type:"POST",
                            url:"/subevent_registration/",
                            data: params,
                            success: function(res){
                                $('#subevent_registration').html(res);  
                                $('#register_subevent').modal();                       
                            }
                            
            });    


    })



/*****************Number Of Email ids ***********************/

    /****************** No of email field as tickets selected *********************/
    // Number of inputs to create
    var number = $('.subordered_quantity').val();
    // Clear previous contents of the email_fields
   
    for (i=0;i<number;i++){
        // Append a node with a random text
        //email_fields.appendChild(document.createTextNode("Email " + (i+1)));
        var newNode = document.createElement('textNode');
            newNode.className = 'my_emails col-lg-6';
            newNode.innerHTML = 'Email_'+(i+1)+': *';
            email_fields.appendChild(newNode);
        // Create an <input> element, set its type and name attributes
        var input = document.createElement("input");
        input.type = "text";
        input.className = "form-control col-lg-6 reg_table emails_input email_"+i;
        input.name = "guest_email_"+i;
        input.id = "guest_email_"+i;
        
        email_fields.appendChild(input);
        // Append a line break 
        email_fields.appendChild(document.createElement("br"));
    }

    $(".view_sub_ticket").click(function() {
         $.ajax({
                    type:"GET",
                    url:"/view_sub_ticket/",
                    success: function(success){
                        window.location.href='/user_ticket_detail/'
                    }
                });
    });

/*********** Register Order****************/

   /***** $(".sub_order_button").click(function() {
        var ticket_name=$('.subordered_ticket_name').val();
        var quantity=$('.subordered_quantity').val();
        var first_name=$('.first_name').val();
        var last_name=$('.last_name').val();
        var emails =[];
        for (i=0;i<quantity;i++){
            var values = $('#guest_email_'+i).val();
            if (values != '') {
                var email_valid=isEmail(values)
                if (email_valid == true){
                    emails[i] = values
                }
                else{
                    $('#invalid_emails').show().delay(3000).fadeOut();
                }
            }
        }
        var total_emails = emails.length;
        if (total_emails == quantity) {
            var input_emails = emails
        }
        else{
            var input_emails = ''
        }
        var emails1= JSON.stringify(input_emails)
        var subevent_id=$('.subordered_event_id').val();
        var event_id=$('.ordered_event_id').val();
        
        var ticket_id=$('.subordered_ticket_id').val();
        if( first_name == '' || last_name == '' || input_emails ==''){
                $('#registrationForm :text').each(function(){ 

                    if( $.trim($(this).val()) == "" )
                        $(this).addClass( "empty_form_fields" );
                        $('.empty_form_fields').css({"border":"solid red 1px"});

                });
                $('#empty_field_error').show();
        }else{
            $('.loader').show();
            params={'input_emails':emails1,'ticket_id':ticket_id, 'sub_event_id':subevent_id,'last_name':last_name,
                    'first_name':first_name, 'quantity':quantity,'ticket_name':ticket_name,'event_id':event_id}
             $.ajax({
                        type:"POST",
                        url:"/sub_save_order/",
                        data: params,
                        success: function(success){
                            $('.loader').hide();
                            $("#sub_confirmation_modal").modal();                       
                        }
                    });
         }
    });*****/


    /********************** Edit sub event  Ticket *****************************/

    /**********$('.sub_edit_ticket_info').click(function(){
        $("#sub_first_name").prop('readonly', false);
        $("#sub_last_name").prop('readonly', false);
        $("#sub_email_address1").prop('readonly', false);
        $('.manage_particular_ticket').show();
        $('.sub_edit_ticket_info').hide();
    })
    $('#sub_cancel_editing').click(function(){
        $("#sub_first_name").prop('readonly', true);
        $("#sub_last_name").prop('readonly', true);
        $("#sub_email_address1").prop('readonly', true);
        $('.manage_particular_ticket').hide();
        $('.sub_edit_ticket_info').show();
    })
    $('#sub_edit_particular_ticket').click(function(){
         var owner_first_name= $('#sub_first_name').val();
         var owner_last_name= $('#sub_last_name').val();
         var owner_email_address= $('#sub_email_address1').val();
         var transaction_id= $('#sub_hid_field').val();
         if(owner_first_name == '' || owner_last_name == '' || owner_email_address =='' || transaction_id ==''){
                $('.ticket_info :input').each(function(){ 
                    if( $.trim($(this).val()) == "" )
                        $(this).addClass( "empty_form_fields" );
                        $('.empty_form_fields').css({"border":"solid red 1px"});
                    });
                $('#edit_field_error').show();

        }else{params={'owner_first_name':owner_first_name,'owner_last_name':owner_last_name,
                        'owner_email_address':owner_email_address,'transaction_id':transaction_id}
                $.ajax({
                            type: "POST",
                            url: "/sub_edit_particular_ticket/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    $('#ticket_success').show().delay(5000).fadeOut();
                                    $("#first_name").prop('readonly', true);
                                    $("#last_name").prop('readonly', true);
                                    $("#email_address1").prop('readonly', true);
                                    $('.manage_particular_ticket').hide();
                                    $('.edit_ticket_info').show();
                                   }
                            }
            });
            } 
    })**********/

/***************** Cancel order for booked ticket confirmation **********************/
    
    $("#sub_cancel_order").click(function(){
        $("#delete_message").slideDown();
    });
    $("#cancel").click(function(){
        $("#delete_message").slideUp();
    });

    $('#sub_delete_ticket_order ').click(function(){
        var transaction_id= $('#sub_hid_field').val();
        params={'transaction_id':transaction_id}
                $.ajax({
                            type: "POST",
                            url: "/sub_delete_particular_ticket/",
                            data: params,
                            success: function(res){
                                if(res=='success'){
                                    $("#confirm_del_modal").modal();
                                    
                                   }
                            }
            });
    })


/************************For events page******************************/


    $('.event_created').click(function(){
        var event_id= $(this).attr('data-id');
        params={'event_id':event_id}
            $.ajax({
                type:"POST",
                data:params,
                url:"/view_event/",
                success: function(res){
                    console.log("done");

                }
            });
    })


/***************** Function to print ticket ***************************/
    

    /***$("#print_ticket_preview").click(function () {
      var divToPrint=document.getElementById('#ticket_data_details');

      var newWin=window.open('','Print-Window');

      newWin.document.open();

      newWin.document.write('<html><body onload="window.print()">'+ticket_data_details.innerHTML+'</body></html>');

      newWin.document.close();

      setTimeout(function(){newWin.close();},10);
    });***/
   
/*************** Print ticket function ends here ************************/




/********************* Ticket Preview *****************************/

    $('.view_particular_ticket').click(function(){
        var event_id1=$(this).attr('data-id');
        if (event_id1 == ''){
            var event_id='0';
        }else{
            var event_id=event_id1
        }
        var sub_event_id1=$(this).attr('data-id-2');
        if (sub_event_id1 == undefined){
            var sub_event_id='0';
        }else{
            var sub_event_id=sub_event_id1
        }
        var transaction_id=$(this).attr('data-id-1');
        params={'event_id':event_id,'sub_event_id':sub_event_id,'transaction_id':transaction_id}
            $.ajax({
                type:"POST",
                data:params,
                url:"/view_qr_image/",
                success: function(res){
                    window.location.href='/booked_particular_ticket/'

                }
            });

    })



/****************For Saved Events****************/
    
     $('.saved_events').click(function(){
        $(this).addClass("saved_favicon");
        $(this).removeClass("saved_events");
        var event_id=$(this).attr('data-id-1');
        var user_name=$(this).attr('data-id-2');
        var saved_flag=$(this).attr('data-id-3');
        params={'event_id':event_id,'user_name':user_name,'saved_flag':saved_flag}
            $.ajax({
                type:"POST",
                data:params,
                url:"/saved_events/",
                success: function(res){
                    if(res=='success'){
                        console.log("Done")
                    }
                }
            });
     }) 


     $('.saved_favicon').click(function(){
        $(this).addClass("saved_events");
        $(this).removeClass("saved_favicon");
        var event_id=$(this).attr('data-id-1');
        var user_name=$(this).attr('data-id-2');
        var saved_flag1=$(this).attr('data-id-3');
        if (saved_flag1='1'){
            saved_flag='0';
        }
        params={'event_id':event_id,'user_name':user_name,'saved_flag':saved_flag}
            $.ajax({
                type:"POST",
                data:params,
                url:"/saved_events/",
                success: function(res){
                    if(res=='success'){
                        console.log("Done")
                    }
                }
            });
     })   

/********************** Save the user account information ********************/
    $('.text_field').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^a-zA-Z\s]/g,'') );
    });
    $('.num_field').bind('keyup blur', function(){
        $(this).val( $(this).val().replace(/[^0-9\s]/g,'') );
    });

$('#save_account').click(function(){
    var profile_pic = profile_image
    /************ Account details *******************/
    var prefix= $('#prefix').val();
    var first_name= $('#first_name').val();
    var last_name= $('#last_name').val();
    var suffix= $('#suffix').val();
    var home_phone= $('#home_phone').val();
    var cell_phone= $('#cell_phone').val();
    var job_title= $('#job_title').val();
    var company= $('#company').val();
    var website= $('#website').val();
    var blog= $('#blog').val();

    /****************** Home address ***************/
    var address= $('#address').val();
    var countryId= $('#countryId :selected').text();
    var stateId= $('#stateId :selected').text();
    var cityId= $('#cityId :selected').text();
    var zip= $('#zip').val();

    /***************** Billing address *************/
    var b_address= $('#b_address').val();
    var countryId1= $('#countryId1 :selected').text();
    var stateId1= $('#stateId1 :selected').text();
    var cityId1= $('#cityId1 :selected').text();
    var b_zip= $('#b_zip').val();

    /*************** Shipping address **************/
    var s_address= $('#s_address').val();
    var countryId2= $('#countryId2 :selected').text();
    var stateId2= $('#stateId2 :selected').text();
    var cityId2= $('#cityId2 :selected').text();
    var s_zip= $('#s_zip').val();

    /*************** Work address ******************/
    var w_address= $('#w_address').val();
    var countryId3= $('#countryId3 :selected').text();
    var stateId3= $('#stateId3 :selected').text();
    var cityId3= $('#cityId3 :selected').text();
    var w_postal= $('#w_postal').val();


    var birth_month= $('#birth_month').val();
    var birth_day= $('#birth_day').val();
    var birth_year= $('#birth_year').val();
    var gender= $('#gender').val();
    var age= $('#age').val();

    if(first_name == '' || cell_phone == '' || address =='' || countryId == '' || stateId == '' || cityId == '' ||
            zip == '' || gender == '' ){
                $('#field_error').show();
    }else{
        params={'prefix':prefix,'first_name':first_name,'last_name':last_name,'suffix':suffix,'home_phone':home_phone,
                'cell_phone':cell_phone,'job_title':job_title,'company':company,'website':website,'blog':blog,
                'address':address,'countryId':countryId,'stateId':stateId,'cityId':cityId,'zip':zip,
                'b_address':b_address,'countryId1':countryId1,'stateId1':stateId1,'cityId1':cityId1,'b_zip':b_zip,
                's_address':s_address,'countryId2':countryId2,'stateId2':stateId2,'cityId2':cityId2,'s_zip':s_zip,
                'w_address':w_address,'countryId3':countryId3,'stateId3':stateId3,'cityId3':cityId3,'w_postal':w_postal,
                'birth_month':birth_month,'birth_day':birth_day,'birth_year':birth_year,'gender':gender,'age':age,
                'profile_pic':profile_pic}
        $.ajax({
                    type:"POST",
                    url:"/update_user_info/",
                    data: params,
                    success: function(res){
                        if(res=='success'){
                            $('#saved_successfully').show();
                            window.location.href='/account_settings/'
                        }
                        else{
                            $('#update_failed').show();
                        }
                    }
                });

    }
})


});


