from django.conf.urls import url
from . import views


app_name = 'home'
urlpatterns = [
   	 url(r'^$',views.index,name='index'),
	 # url(r'^contact_us/$',views.contact_us,name='contact_us'),
  #    url(r'^about_us/$',views.about_us,name='about_us'),
  #    url(r'^careers/$',views.careers,name='careers'),
  #    url(r'^security/$',views.security,name='security'),
  #    url(r'^help/$',views.help,name='help'),
  #    url(r'^press/$',views.press,name='press'),
  #    url(r'^terms/$',views.terms,name='terms'),
  #    url(r'^cookies/$',views.cookies,name='cookies'),
  #    url(r'^privacy/$',views.privacy,name='privacy'),
  #    url(r'^developers/$',views.developers,name='developers'),
  #    url(r'^blog/$',views.blog,name='blog'),
  url(r'^event/(?P<id>\d+)/$',views.view_event,name='view_event'),
  url(r'^autocomplete_events/$',views.autocomplete_events,name='autocomplete_events'),
  url(r'^search_events/$',views.search_events,name='search_events'),
  url(r'^browse_events/$',views.browse_events,name='browse_events'),
  url(r'^filter_events/$',views.get_filtered_events,name='get_filtered_events'),
  url(r'^free_events/$',views.get_free_events,name='get_free_events'),
  url(r'^paid_events/$',views.get_free_events,name='get_free_events'),
  url(r'^category_filters/$',views.category_filters,name='category_filters'),
  url(r'^category_filters_data/$',views.category_events_data,name='category_events_data'),
     #Use Eevents
     # url(r'^how_it_works/$',views.how_it_works,name='how_it_works'),
     # url(r'^large_complex_events/$',views.large_complex_events,name='large_complex_events'),
     # url(r'^mobile_app/$',views.mobile_app,name='mobile_app'),
     # url(r'^checkin_app/$',views.checkin_app,name='checkin_app'),
     # url(r'^reserved_seating/$',views.reserved_seating,name='reserved_seating'),
     # url(r'^event_spectrum/$',views.event_spectrum,name='event_spectrum'),
     # url(r'^rally/$',views.rally,name='rally'),
     # url(r'^sitemap/$',views.sitemap,name='sitemap'),
     #Plan Events
     # url(r'^conference_management/$',views.conference_management,name='conference_management'),
     # url(r'^classes_workshop/$',views.classes_workshop,name='classes_workshop'),
     # url(r'^nonprofit_fundraises/$',views.nonprofit_fundraises,name='nonprofit_fundraises'),
     # url(r'^reunions/$',views.reunions,name='reunions'),
     # url(r'^sell_tickets/$',views.sell_tickets,name='sell_tickets'),
     # url(r'^event_management/$',views.event_management,name='event_management'),
     # url(r'^event_registration/$',views.event_registration,name='event_registration'),
     # url(r'^rsvp_online/$',views.rsvp_online,name='event_rsvp_online'),
     # url(r'^venue_booking/$',views.venue_booking,name='venue_booking'),
]