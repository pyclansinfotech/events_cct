from django.shortcuts import redirect
#from json_response import JsonResponse
#from models import ActionState

###########################################################
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib

#import gdata.contacts.service
from django.http import JsonResponse
from django.utils import timezone
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
#from .google_contacts.utils import google_get_state, google_import

from django.shortcuts import render
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from dashboard.models import Category,Event,EventTickets,SubEvent,TicketBooked,ImageQR,SavedEvent,UserAccount,Location,EventBanner,SubEventBanner
from  datetime import timedelta
import time
import datetime
from django.template.loader import render_to_string
from datetime import datetime
from dateutil.relativedelta import relativedelta
import base64 
from django.db.models import Q
import operator
import os
import datetime
import time
import json
import qrcode
#import pdfcrowd

from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User
from django.contrib.auth import logout as auth_logout

from utils import image_path_editor
from django.contrib import messages
#import ho.pisa as pisa
#import cStringIO as StringIO
# Create your views here.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))



#############Index page###################

#############Index page###################
def index(request):
	try:
		logged_in_user=request.user.id
	except Exception as e:
		loggedin_user=''
	saved_event_list=[]
	saved_event_dict={'saved_event_no':''}
	events_data_list=[]
	events_data_dict={'publish_event':'','event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
	ticket_info=TicketBooked.objects.filter(user=logged_in_user)
	total_tickets=len(ticket_info)
	request.session['total_tickets']=total_tickets
	saved_info=SavedEvent.objects.filter(user=logged_in_user).filter(saved_flag='1')
	saved_events_length=len(saved_info)
	saved_event_dict['saved_event_no']=saved_events_length
	saved_event_list.append(saved_event_dict.copy())
	events_info=Event.objects.filter(start_datetime__gte=datetime.datetime.now(tz=None).today().date()).filter(publish_event=1).order_by('start_datetime')	
	if events_info:
		for info in events_info:
			event_id=str(info.id)
			city=info.location
			location_object = Location.objects.get(city=city)
			country = location_object.country
			time_register=info.start_datetime.strftime("%B %d, %Y, %I:%M %p")
			name=info.event_name
			description=info.description
			category_name=info.category
			publish_event=info.publish_event
			category_name=info.category
			eventsticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)
			if eventsticket_object:
				price = eventsticket_object.price
			events_data_dict['event_id']=event_id
			events_data_dict['city']=city
			events_data_dict['country']=country
			events_data_dict['time']=time_register
			events_data_dict['name']=name
			events_data_dict['price']=price
			events_data_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
			events_data_dict['description']=description
			events_data_dict['category']=category_name
			events_data_dict['publish_event']=publish_event
			events_data_list.append(events_data_dict.copy())
	else:
		events_data_list=[]
	try:
		logged_in_user=request.session['loggedin_user']
	except Exception as e:
		logged_in_user=''
	return render(request,'home/index.html',{'saved_event':saved_event_list,'total_tickets':total_tickets,'events_data':events_data_list,'logged_in_user':logged_in_user})


def contact_us(request):
	return render(request,'home/contactus.html',{})

def about_us(request):
    return render(request,'home/aboutus.html',{})

def careers(request):
    return render(request,'home/careers.html',{})

def security(request):
    return render(request,'home/security.html',{})

def help(request):
    return render(request,'home/help.html',{})

def press(request):
    return render(request,'home/press.html',{})

def terms(request):
    return render(request,'home/terms.html',{})

def cookies(request):
    return render(request,'home/cookies.html',{})

def privacy(request):
    return render(request,'home/privacy.html',{})

def developers(request):
    return render(request,'home/developers.html',{})

def blog(request):
    return render(request,'home/blog.html',{})


###############################How it works################################
def how_it_works(request):
	return render(request,'home/how_it_works.html',{})
###############################Large complex events################################
def large_complex_events(request):
	return render(request,'home/large_complex_events.html',{})	
############################# mobile app ##############################
def mobile_app(request):
	return render(request,'home/mobile_app.html',{})
######################### Check-in App ###############################
def checkin_app(request):
	return render(request,'home/checkin_app.html',{})
############################# Reserved Seating ##############################
def reserved_seating(request):
	return render(request,'home/reserved_seating.html',{})
######################## Event Spectrum##########################################
def event_spectrum(request):
	return render(request,'home/event_spectrum.html',{})
############################ Rally ##############################
def rally(request):
	return render(request,'home/rally.html',{})
######################## Site Map####################################
def sitemap(request):
	return render(request,'home/sitemap.html',{})
######################## Conference MAnagement ##################################
def conference_management(request):
	return render(request,'home/conference_management.html',{})   
	
######################### Classes & Workshops #############################
def classes_workshop(request):
	return render(request,'home/classes_workshop.html',{})	
##################### Nonprofits & Fundraises ############################
def nonprofit_fundraises(request):
	return render(request,'home/nonprofit_fundraises.html',{})
################## Reunions #########################
def reunions(request):
	return render(request,'home/reunions.html',{})

######################### Sell Tickets ############################
def sell_tickets(request):
	return render(request,'home/sell_tickets.html',{})	
################### Event management & planning ############################
def event_management(request):
	return render(request,'home/event_management.html',{})
	
######################## Event Registration ######################
def event_registration(request):
	return render(request,'home/event_registration.html',{})
########################### RSVP Online #############################
def rsvp_online(request):
	return render(request,'home/rsvp_online.html',{})
######################### Venue Booking ############################
def venue_booking(request):
	return render(request,'home/venue_booking.html',{})

@csrf_exempt
def autocomplete_events(request):
	try:
		term=str(request.POST['term'])
		results=[]
		events_data=Event.objects.filter(event_name__contains=term).filter(publish_event=1).filter(start_datetime__gte=datetime.datetime.now(tz=None).today().date())
		if events_data:
			for data in events_data:
				event_name=data.event_name
				user_json = {}
				user_json['label'] = event_name
				user_json['value'] = event_name
				results.append(user_json)
				data = json.dumps(results)
				mimetype = 'application/json'
		else:
			data = 'not found'
			mimetype = 'application/json'		
		return HttpResponse(data, mimetype)
	except Exception as e:
		print e	

############Search for events from home###################
@csrf_exempt
def search_events(request):
	try:
		if(request.POST['event_name']):
			event_name=request.POST['event_name']
		else:
			event_name=''
		if(request.POST['location']):
			location=request.POST['location']
		else:
			location=''	
		if(request.POST['date']):
			date=request.POST['date']
		else:
			date=''
		today=datetime.datetime.now().strftime('%Y-%m-%d') 
		events_list=[]
		events_dict={'publish_event':'','event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}	
		##########################Category and date Filter##################################
		if((event_name != '') and (date == 'All Dates')):
			events_data=Event.objects.filter(event_name=event_name).filter(publish_event=1)
		if((event_name != '') and (date == 'Today')):
			events_data=Event.objects.filter(event_name=event_name).filter(start_datetime__startswith=today).filter(publish_event=1)
		if((event_name != '') and (date == 'Tomorrow')):
			date_tomorrow = datetime.datetime.now()+ relativedelta(days=1)
			date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(event_name=event_name).filter(start_datetime__startswith=date_tomorrow_new).filter(publish_event=1)
		if((event_name != '') and (date == 'This Week')):
			start_date=today
			end_date=datetime.datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(event_name=event_name).filter(start_datetime__range=(start_date, end_date_new)).filter(publish_event=1)
		if((event_name != '') and (date == 'This Weekend')):
			start_date=today
			start_date_new=datetime.datetime.now()+ relativedelta(days=5)
			start_date_new1=start_date_new.strftime('%Y-%m-%d')
			end_date=datetime.datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(event_name=event_name).filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1)
		if((event_name != '') and (date == 'Next Week')):
			start_date=today
			start_date_new=datetime.datetime.now()+ relativedelta(days=7)
			start_date_new1=start_date_new.strftime('%Y-%m-%d')
			end_date=datetime.datetime.now()+ relativedelta(days=14)
			end_date_new=end_date.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(event_name=event_name).filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1)
		if((event_name != '') and (date == 'Next Month')):
			start_date=today
			start_date_new=datetime.datetime.now()+ relativedelta(days=30)
			start_date_new1=start_date_new.strftime('%Y-%m-%d')
			end_date=datetime.datetime.now()+ relativedelta(days=60)
			end_date_new=end_date.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(event_name=event_name).filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1)
		#########################################Ends here###################################################################

		##########################################Location and dates filter##################################################
		if((location != '') and (date == 'All Dates')):
			events_data=Event.objects.filter(location_id=Location.objects.get(city=location).id).filter(publish_event=1).filter(start_datetime__gte=datetime.datetime.now(tz=None).today().date()).order_by('start_datetime')
		if((location != '') and (date == 'Today')):
			events_data=Event.objects.filter(location=Location.objects.get(city=location).id).filter(start_datetime__contains=today).filter(publish_event=1).order_by('start_datetime')
		if((location != '') and (date == 'Tomorrow')):
			date_tomorrow = datetime.datetime.now()+ relativedelta(days=1)
			date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d') 
			events_data=Event.objects.filter(location_id=Location.objects.get(city=location).id).filter(start_datetime__startswith=date_tomorrow_new).filter(publish_event=1).order_by('start_datetime')
		if((location != '') and (date == 'This Week')):
			start_date=today
			end_date=datetime.datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(location_id=Location.objects.get(city=location).id).filter(start_datetime__range=(start_date, end_date_new)).filter(publish_event=1).order_by('start_datetime')
		if((location != '') and (date == 'This Weekend')):
			start_date=today
			start_date_new=datetime.datetime.now()+ relativedelta(days=5)
			start_date_new1=start_date_new.strftime('%Y-%m-%d')
			end_date=datetime.datetime.now()+ relativedelta(days=7)
			end_date_new=end_date.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(location_id=Location.objects.get(city=location).id).filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1).order_by('start_datetime')
		if((location != '') and (date == 'Next Week')):
			start_date=today
			start_date_new=datetime.datetime.now()+ relativedelta(days=7)
			start_date_new1=start_date_new.strftime('%Y-%m-%d')
			end_date=datetime.datetime.now()+ relativedelta(days=14)
			end_date_new=end_date.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(location_id=Location.objects.get(city=location).id).filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1).order_by('start_datetime')
		if((location != '') and (date == 'Next Month')):
			start_date=today
			start_date_new=datetime.datetime.now()+ relativedelta(days=30)
			start_date_new1=start_date_new.strftime('%Y-%m-%d')
			end_date=datetime.datetime.now()+ relativedelta(days=60)
			end_date_new=end_date.strftime('%Y-%m-%d')
			events_data=Event.objects.filter(location_id=Location.objects.get(city=location).id).filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1).order_by('start_datetime')
		try:			
			if events_data:
				for event in events_data:
					event_id=str(event.id)
					city=event.location
					location_object=Location.objects.get(city=city)
					country=location_object.country
					time_new=event.start_datetime
					time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
					event_name=event.event_name
					eventsticket_object=EventTickets.objects.get(event_id=event_id,sub_event_id=None)
					price=eventsticket_object.price
					description=event.description
					category_name=event.category
					publish_event=event.publish_event
					events_dict['event_id']=event_id
					events_dict['city']=city
					events_dict['country']=country
					events_dict['time']=time_register
					events_dict['name']=event_name
					events_dict['price']=price
					events_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
					events_dict['description']=description
					events_dict['category']=category_name
					events_dict['publish_event']=publish_event
					events_list.append(events_dict.copy())
			else:
				events_data=[]
			html = render_to_string('home/search_results.html', {'events_data':events_list })
			return HttpResponse(html)	
		except Exception as e :
			print('error',e)					
	except Exception as e:
		print('error',e)


######################################Browse Events Page#############################
@csrf_exempt
def browse_events(request):
	try:
		current_datetime=datetime.datetime.now()
		today_date=current_datetime.date().strftime('%Y-%m-%d')
		try:
			logged_in_user=request.user.id
			total_tickets=request.session['total_tickets']
			
		except Exception as e:
			logged_in_user=''
			total_ticket=''
		events_data_list=[]
		categories_list=[]
		categories=Category.objects.all()
		if categories:
			for category in categories:
				category_name=str(category.category_name)
				category_id=category.id
				categories_list.append(category_name)
				try:
					categories_list_new=list(set(categories_list))
				except:
					categories_list_new = []
		else:
			categories_list_new = []			
		cities_list=[]
		events_data_dict={'saved_flag':'','user_name':'','publish_event':'','event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		events_data_dict['user_name']=request.user
		########Get Todays Events#############
		events_today_list=[]
		events_today_dict={'publish_event':'','event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		today=datetime.datetime.now().strftime('%Y-%m-%d') 
		events_today_data=Event.objects.filter(start_datetime__startswith=today).filter(publish_event=1).order_by('start_datetime')
		if events_today_data:
			for info in events_today_data:
				event_id=str(info.id)
				city = info.location
				location_object=Location.objects.get(city=city)
				country = location_object.country
				time=info.start_datetime
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				name=info.event_name
				eventticket_object = EventTickets.objects.filter(event_id=event_id,sub_event_id=None)
				for val in eventticket_object:
					price=val.price
					ticket_type = val.ticket_type
				description=info.description
				category_name=info.category
				publish_event=info.publish_event		
				events_today_dict['event_id']=event_id
				events_today_dict['city']=city
				events_today_dict['country']=country
				events_today_dict['time']=time_register
				events_today_dict['name']=name
				events_today_dict['price']=price
				events_today_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
				events_today_dict['description']=description
				events_today_dict['category']=category_name
				events_today_dict['publish_event']=publish_event
				events_today_list.append(events_today_dict.copy())
		else:
			events_today_list = []				
		##############Ends here#########################
		########Get Coming Events#############
		events_coming_list=[]
		events_coming_dict={'publish_event':'','event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		today=datetime.datetime.now().strftime('%Y-%m-%d')
		start_date=today
		end_date=datetime.datetime.now()+ relativedelta(days=400)
		end_date_new=end_date.strftime('%Y-%m-%d')
		events_coming_data=Event.objects.filter(start_datetime__gte=datetime.datetime.now(tz=None).today().date()+timedelta(days=1)).order_by('start_datetime')
		if events_coming_data:
			for info in events_coming_data:
				event_id=str(info.id)
				city=info.location
				location_object = Location.objects.get(city=city)
				country=location_object.country
				time=info.start_datetime
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				name=info.event_name
				eventsticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)
				price=eventsticket_object.price
				description=info.description
				category_name=info.category
				publish_event=info.publish_event				
				events_coming_dict['event_id']=event_id
				events_coming_dict['city']=city
				events_coming_dict['country']=country
				events_coming_dict['time']=time_register
				events_coming_dict['name']=name
				events_coming_dict['price']=price
				events_coming_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
				events_coming_dict['description']=description
				events_coming_dict['category']=category_name
				events_coming_dict['publish_event']=publish_event
				events_coming_list.append(events_coming_dict.copy())
		else:
			events_coming_list = []		
		##############Ends here#########################
		events_info=Event.objects.filter(publish_event=1).filter(start_datetime__gte=datetime.datetime.now(tz=None).today().date()).order_by('start_datetime')
		if events_info:
			for info in events_info:
				event_id=str(info.id)
				city=info.location
				location_object=Location.objects.get(city=city)
				country=location_object.country
				time=info.start_datetime
				time_register=time.strftime("%B %d, %Y, %I:%M %p")
				event_name=info.event_name
				eventsticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)
				price=eventsticket_object.price
				user_id=info.user_id
				description=info.description
				category_name=info.category
				publish_event=info.publish_event
				saved_info=SavedEvent.objects.filter(event_id=event_id).filter(user_id=user_id)
				for info in saved_info:
					events_data_dict['saved_flag']=info.saved_flag
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['time']=time_register
				events_data_dict['name']=event_name
				events_data_dict['price']=price
				events_data_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
				events_data_dict['description']=description
				events_data_dict['category']=category_name
				events_data_dict['publish_event']=publish_event
				events_data_list.append(events_data_dict.copy())
				cities_list.append(city)
				cities_list_new =list(set(cities_list))
		else:		
			events_data_list =[]
			cities_list_new = []
		return render(request,'home/browse_events.html',{"total_tickets":total_tickets,"events_data":events_data_list,'categories':categories_list_new,
			'locations':cities_list_new,'today_events':events_today_list,'coming_events':events_coming_list,'logged_in_user':logged_in_user})
	except Exception as e:
		print(e)


############################Browse Events page get filtered events############################
@csrf_exempt
def get_filtered_events(request):
	try:
		current_datetime=datetime.datetime.now()
		today_date=current_datetime.date().strftime('%Y-%m-%d')
		try:
			logged_in_user=request.user.id
		except Exception as e:
			logged_in_user=''
		try:
			start_date=request.POST['start_date']
			end_date=request.POST['end_date']
		except Exception as e:
			end_date=''
			start_date=''
		try:
			period=request.POST['period']
		except Exception as e:
			period=''
		try:
			category_name=request.POST['category']
		except Exception as e:
			category_name=''
		try:
			location_name=request.POST['location']
		except Exception as e:
			location_name=''
		today=datetime.datetime.now().strftime('%Y-%m-%d') 
		events_data_list=[]
		events_data_dict={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		if(start_date != '' and end_date != ''):
			start_date=str(start_date)
			end_date=str(end_date)
			start_date_list=start_date.split('/')
			year=start_date_list[2]
			day=start_date_list[1]
			month=start_date_list[0]
			start=year+'-'+month+'-'+day
			start_filter=datetime.strptime(start, '%Y-%m-%d').date()
			end_date_list=end_date.split('/')
			year1=end_date_list[2]
			day1=end_date_list[1]
			month1=end_date_list[0]
			end=year1+'-'+month1+'-'+day1
			end_filter=datetime.strptime(end, '%Y-%m-%d').date()
			events_data=Event.objects.filter(start_datetime__range=(start_filter, end_filter)).filter(publish_event=1)
		if(period != ''):
			if(period == 'Today'):
				today=datetime.datetime.now().strftime('%Y-%m-%d') 
				events_data=Event.objects.filter(start_datetime__startswith=today).filter(publish_event=1).order_by('start_datetime')
			if(period == 'Tomorrow'):
				date_tomorrow = datetime.datetime.now()+ relativedelta(days=1)
				date_tomorrow_new=date_tomorrow.strftime('%Y-%m-%d') 
				events_data=Event.objects.filter(start_datetime__startswith=date_tomorrow_new).filter(publish_event=1).order_by('start_datetime')
			if(period == 'This Week'):
				start_date=today
				end_date=datetime.datetime.now()+ relativedelta(days=7)
				end_date_new=end_date.strftime('%Y-%m-%d')
				events_data=Event.objects.filter(start_datetime__range=(start_date, end_date_new)).filter(publish_event=1).order_by('start_datetime')
			if(period =='This Weekend'):
				start_date=today
				start_date_new=datetime.datetime.now()+ relativedelta(days=5)
				start_date_new1=start_date_new.strftime('%Y-%m-%d')
				end_date=datetime.datetime.now()+ relativedelta(days=7)
				end_date_new=end_date.strftime('%Y-%m-%d')
				events_data=Event.objects.filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1).order_by('start_datetime')
			if(period == 'Next Week'):
				start_date=today
				start_date_new=datetime.datetime.now()+ relativedelta(days=7)
				start_date_new1=start_date_new.strftime('%Y-%m-%d')
				end_date=datetime.datetime.now()+ relativedelta(days=14)
				end_date_new=end_date.strftime('%Y-%m-%d')
				events_data=Event.objects.filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1).order_by('start_datetime')
			if(period == 'Next Month'):
				start_date=today
				start_date_new=datetime.datetime.now()+ relativedelta(days=30)
				start_date_new1=start_date_new.strftime('%Y-%m-%d')
				end_date=datetime.datetime.now()+ relativedelta(days=60)
				end_date_new=end_date.strftime('%Y-%m-%d')
				events_data=Event.objects.filter(start_datetime__range=(start_date_new1, end_date_new)).filter(publish_event=1).order_by('start_datetime')

		if (category_name != ''):
			category_object = Category.objects.get(category_name=category_name)
			category_id = category_object.id
			events_data=Event.objects.filter(category_id=category_id).filter(start_datetime__gte=today_date).filter(publish_event=1).order_by('start_datetime')
		if(location_name  != ''):
			location_object=Location.objects.get(city=location_name)
			events_data=Event.objects.filter(location_id=location_object.id).filter(start_datetime__gte=today_date).filter(publish_event=1).order_by('start_datetime')
		for event in events_data:
			event_id=str(event.id)
			city=event.location
			location_object=Location.objects.get(city=city)
			country=location_object.country
			time_new=event.start_datetime
			time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
			event_name=event.event_name
			eventsticket_object=EventTickets.objects.get(event_id=event_id,sub_event_id=None)
			price=eventsticket_object.price
			description=event.description
			category_name=event.category			
			events_data_dict['event_id']=event_id
			events_data_dict['city']=city
			events_data_dict['country']=country
			events_data_dict['time']=time_register
			events_data_dict['name']=event_name
			events_data_dict['price']=price				
			events_data_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
			events_data_dict['description']=description
			events_data_dict['category']=category_name
			events_data_list.append(events_data_dict.copy())
		user_saved_events_list=[]
		user_saved_events_dict={'user_name':'','event_id':'','saved_event_flag':''}
		saved_events_data=SavedEvent.objects.filter(user_id=logged_in_user)
		for saved in saved_events_data:
			user_id=saved.user_id
			event_id=saved.event_id
			saved_event_flag=saved.saved_flag
			user_saved_events_dict['user_name']=user_id
			user_saved_events_dict['event_id']=event_id
			user_saved_events_dict['saved_event_flag']=saved_event_flag
			user_saved_events_list.append(user_saved_events_dict.copy())
		html = render_to_string('home/browse_events_filter.html', {'saved_events_data':user_saved_events_list,'events_data':events_data_list})
		return HttpResponse(html)
	except Exception as e:
		print(e)		


############################Browse Events page get free events############################
@csrf_exempt
def get_free_events(request):
	try:
		current_datetime=datetime.datetime.now()
		today_date=current_datetime.date().strftime('%Y-%m-%d')
		events_data_list=[]
		image_path_list = []
		events_data_dict={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}
		filter_name=request.POST['filter']
		if(filter_name == 'free'):
			eventsticket_object=EventTickets.objects.values_list('event_id', flat=True).filter(ticket_type='free').filter(sub_event_id=None)
			if eventsticket_object:
				events_data=Event.objects.filter(id__in=eventsticket_object).filter(publish_event=1).filter(start_datetime__gte=today_date).order_by('start_datetime')
			else:
				events_data=[]		
		else:
			eventsticket_object=EventTickets.objects.values_list('event',flat=True).filter(~Q(ticket_type='free')).filter(sub_event_id=None)
			if eventsticket_object:
				events_data=Event.objects.filter(id__in=eventsticket_object).filter(publish_event=1).filter(start_datetime__gte=today_date).order_by('start_datetime')
			else:
				events_data=[]
		for event in events_data:
			event_id=str(event.id)
			city=event.location
			location_object=Location.objects.get(city=city)
			country=location_object.country
			time_new=event.start_datetime
			time_register=time_new.strftime("%B %d, %Y, %I:%M %p")
			name=event.event_name
			eventsticket_object=EventTickets.objects.get(event_id=event_id,sub_event_id=None)
			price=eventsticket_object.price
			description=event.description
			category_name=event.category			
			events_data_dict['event_id']=event_id
			events_data_dict['city']=city
			events_data_dict['country']=country
			events_data_dict['time']=time_register
			events_data_dict['name']=name
			events_data_dict['price']=price
			events_data_dict['image']= image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))				
			events_data_dict['description']=description
			events_data_dict['category']=category_name
			events_data_list.append(events_data_dict.copy())
		html = render_to_string('home/browse_events_filter.html', {'events_data':events_data_list})
		return HttpResponse(html)
	except Exception as e:
		print(e)


############Search for events from home###################
@csrf_exempt
def view_event(request,id):
	try:
		total_tickets=request.session['total_tickets']
		event_id=id
		events_data_list=[]
		subevents_data_list=[]
		event_ticket_list=[]
		subevent_ticket_list=[]
		events_data_dict={'booking_check':'','event_id':'','city':'','country':'','event_type':'','time':'','name':'','price':'','image':'','description':'','category':''}
		subevents_data_dict={'booking_check':'','sub_event_id':'','start_time':'','end_time':'','sub_event_type':'','name':'','price':'','image':'','description':'','category':''}
		event_ticket_dict={'timestamp_end':'','order_max':'','order_min':'','ticket_name':'','ticket_id':'','pending_tickets':''}
		subevent_ticket_dict={'subevent_id_id':'','timestamp_end':'','order_max':'','order_min':'','ticket_name':'','ticket_id':'','pending_tickets':''}
		user=request.user
		
		events_info=Event.objects.filter(id=event_id)
		if events_info:
			for info in events_info:
				event_id=str(info.id)
				city=info.location
				location_object=Location.objects.get(city=city)
				country = location_object.country
				start_datetime=info.start_datetime
				start_datetime=start_datetime.strftime("%B %d, %Y, %I:%M %p")
				event_name=info.event_name
				eventbanner_object = EventBanner.objects.filter(event_id=event_id)
				description=info.description
				category_name=info.category
				eventsticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=None)
				price=eventsticket_object.price
				event_type=eventsticket_object.ticket_type
				ticket_name = eventsticket_object.ticket_name
				ticket_id =eventsticket_object.id
				tic_count = eventsticket_object.ticket_count
				order_min = eventsticket_object.order_min
				order_max = eventsticket_object.order_max
				end_datetime =eventsticket_object.end_datetime
				tic_time_close=end_datetime.strftime("%B %d, %Y, %I:%M %p")
				events_data_dict['event_id']=event_id
				events_data_dict['city']=city
				events_data_dict['country']=country
				events_data_dict['time']=start_datetime
				events_data_dict['name']=event_name
				events_data_dict['price']=price
				events_data_dict['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
				events_data_dict['description']=description
				events_data_dict['category']=category_name
				events_data_dict['event_type']=event_type
				events_data_list.append(events_data_dict.copy())
				tic_count1=int(tic_count)
				quantity_booked=0
				book_info=TicketBooked.objects.filter(ticket_id=ticket_id)
				for info in book_info:
					transaction_id=info.transaction_id
					eventsticket_object = EventTickets.objects.get(id=ticket_id)
					event_id=str(eventsticket_object.event_id)
					ticket_booked1=int(info.quantity_booked)
					quantity_booked=quantity_booked+ticket_booked1
				pending_tickets=tic_count1-quantity_booked
				event_ticket_dict['timestamp_end']=tic_time_close
				event_ticket_dict['order_min']=order_min
				event_ticket_dict['order_max']=order_max
				event_ticket_dict['ticket_name']=ticket_name
				event_ticket_dict['ticket_id']=ticket_id
				event_ticket_dict['pending_tickets']=pending_tickets
				event_ticket_list.append(event_ticket_dict.copy())
				sub_events_info=SubEvent.objects.filter(event_id=event_id).filter(visible_check=1)
				for info in sub_events_info:
					sub_event_id=str(info.id)
					registered_datetime=info.registered_datetime
					registered_datetime=registered_datetime.strftime("%B %d, %Y, %I:%M %p")
					start_datetime=info.start_datetime
					start_datetime=start_datetime.strftime("%B %d, %Y, %I:%M %p")
					end_datetime=info.end_datetime
					end_datetime=end_datetime.strftime("%B %d, %Y, %I:%M %p")
					name=info.sub_event_name
					description=info.description
					sub_category_name=info.category
					subeventticket_object = EventTickets.objects.get(event_id=event_id,sub_event_id=sub_event_id)
					price=subeventticket_object.price
					sub_event_type=subeventticket_object.ticket_type
					order_min = subeventticket_object.order_min
					order_max = subeventticket_object.order_max
					ticket_id = subeventticket_object.id
					ticket_name = subeventticket_object.ticket_name
					tic_time_close= subeventticket_object.end_datetime
					tic_time_close=tic_time_close.strftime("%B %d, %Y, %I:%M %p")
					ticket_count = subeventticket_object.ticket_count
					booking_check=info.booking_check
					subevents_data_dict['sub_event_id']=sub_event_id
					subevents_data_dict['start_time']=start_datetime
					subevents_data_dict['end_time']=end_datetime
					subevents_data_dict['name']=name
					subevents_data_dict['price']=price
					subevents_data_dict['image']=image_path_editor(SubEventBanner.objects.values_list('sub_event_banner', flat=True).filter(sub_event_id=sub_event_id))
					subevents_data_dict['description']=description
					subevents_data_dict['category']=sub_category_name
					subevents_data_dict['sub_event_type']=sub_event_type
					subevents_data_dict['booking_check']=booking_check
					subevents_data_list.append(subevents_data_dict.copy())
					tic_count1=int(ticket_count)
					subevent_ticket_dict['timestamp_end']=tic_time_close
					subevent_ticket_dict['order_min']=order_min
					subevent_ticket_dict['order_max']=order_max
					subevent_ticket_dict['ticket_name']=ticket_name
					subevent_ticket_dict['ticket_id']=ticket_id
					subevent_ticket_list.append(subevent_ticket_dict.copy())
		return render(request,'home/events.html',{'events_data':events_data_list,'logged_in_user':user,
			'subevents_data':subevents_data_list,'event_ticket_data':event_ticket_list,'subevent_ticket_data':subevent_ticket_list,'total_tickets':total_tickets})
	except Exception as e:
		print('e',e)

###########################Render to browsepage ###############################
@csrf_exempt
def category_filters(request):
	try:
		logged_in_user=request.user

		if logged_in_user != "":
			logged_in_user = logged_in_user
		else:
			logged_in_user=""
		events_data_list1=[]
		events_data_dict1={'event_id':'','city':'','country':'','time':'','name':'','price':'','image':'','description':'','category':''}		
		category_get=(request.POST['category']).lower()
		if Category.objects.filter(category_name=category_get).exists():
			events_data=Event.objects.filter(category_id=Category.objects.get(category_name=category_get).id).filter(start_datetime__gte=datetime.datetime.now(tz=None).today().date()).filter(publish_event=1).order_by('start_datetime')
			if events_data:
				for event in events_data:
					event_id=str(event.id)
					city=str(event.location)
					location_object=Location.objects.get(city=city)
					country=location_object.country
					start_datetime=event.start_datetime
					start_datetime=start_datetime.strftime("%B %d, %Y, %I:%M %p")
					name=event.event_name
					eventsticket_object=EventTickets.objects.get(event_id=event_id,sub_event_id=None)
					price=eventsticket_object.price
					if price == "":
						price=0
					else:
						price=price					
					description=event.description
					category=event.category
					events_data_dict1['event_id']=event_id
					events_data_dict1['city']=city
					events_data_dict1['country']=country
					events_data_dict1['time']=str(start_datetime)
					events_data_dict1['name']=name
					events_data_dict1['price']=price
					events_data_dict1['image']=image_path_editor(EventBanner.objects.values_list('event_banner', flat=True).filter(event_id=event_id))
					events_data_dict1['description']=description
					events_data_dict1['category']=category_get
					events_data_list1.append(events_data_dict1.copy())
		else:
			events_data_list1 = []
		request.session["category_events_data"]=events_data_list1
		return HttpResponse('success')	

			# return render(request,'home/browse_events.html',{'category_events_data':events_data_list1,'logged_in_user':logged_in_user})
	except Exception as e:
		print 'error',e
		###################################Ends here#############################################

def category_events_data(request):
	try:
		loggedin_user=request.user
		if loggedin_user != "":
			loggedin_user = loggedin_user
		else:
			loggedin_user = ""	
		event_data=request.session["category_events_data"]
		total_tickets=request.session["total_tickets"]
		if total_tickets == "":
			total_tickets = 0
		else:
			total_tickets = total_tickets				
		return render(request,'home/category_events_data.html',{"total_tickets":total_tickets,"events_data":event_data,"loggedin_user":loggedin_user})
	except Exception as e:
		print 'error',e						